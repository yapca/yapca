# <img src="frontend/public/icon.svg"  width="30" height="30"> Yet Another Patient Care Application

[![pipeline status](https://gitlab.com/yapca/yapca/badges/master/pipeline.svg)](https://gitlab.com/yapca/yapca/pipelines/latest)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=Star-Man_yapca&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=Star-Man_yapca)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=Star-Man_yapca&metric=coverage)](https://sonarcloud.io/summary/new_code?id=Star-Man_yapca)

## **[Check Out The Website First!](https://star-man.gitlab.io/yapca-site/#/)**

_This application allows for simple patient care and logging in elderly/disabled day care centres_

This application is heavily tested and uses a [modern frontend framework](https://vuejs.org/).
The backend and frontend are completly separate, even in language.
The build process uses Gitlab runner to run unit tests, e2e tests and linting.

This project attempts to solve the problem of existing medical applications being either
non-free, or extremly outdated and bloated, this project is far more lightweight and
capable of running on very low end servers that could be hosted on premises.

## Running Locally

### Requirements

1. [PostgreSQL](https://www.postgresql.org/) installed and running on the default port (5432)
2. [Redis>5.0](https://redis.io/) installed and running on the default port (6379)
3. [Python>3.10 + Poetry](https://python-poetry.org/)
4. [Yarn](https://yarnpkg.com/getting-started/install)
5. [Git](https://git-scm.com/downloads)

### Steps

1. Clone this repo

   ```
   git clone -b stable https://gitlab.com/Star-Man/yapca.git
   ```

2. Create a user and database in your postgres database that matches the user in the
   [default config file](config/local.env).

   ```
   psql -U postgres -c "create role db_user with password 'password' LOGIN CREATEDB;"
   psql -U postgres -c "CREATE DATABASE yapca_db OWNER db_user;"
   ```

   <b>If you wish to use a different username/password, adjust the values in the
   [config file](config/local.env)</b>

3. Enter the backend folder

   ```
   cd yapca/backend
   ```

4. Install required python packages for running the backend server

   ```
   poetry install --only main
   ```

5. Run migrations

   ```
   poetry run python3 manage.py migrate
   ```

6. Run the backend server

   ```
   poetry run uvicorn app.asgi:application
   ```

   <b>This will run the server locally, keep this running.</b>

7. Run the tasks cluster (runs periodic tasks)

   ```
   poetry run python manage.py qcluster
   ```

   <b>Keep this running too.</b>

8. Open a new window and enter the frontend folder
   ```
   cd yapca/frontend
   ```
9. Install required node modules for running the frontend server
   ```
   yarn
   ```
10. Start the frontend server

    ```
    yarn serve
    ```

## Running With Docker

### Requirements

1. [Docker](https://docs.docker.com)
2. [Git](https://git-scm.com/downloads)

### Steps

1. Run docker compose command:
   ```
   docker compose -f build/dockercompose.yaml up frontend
   ```

## Configuring

Configuration of project settings can be done through the .env files in the yapca/config
folder. If running the project locally, edit the [local.env](config/local.env) file,
if using docker, edit the [docker-local.env](config/docker-local.env) file.

You can also create additional .env files by changing the environment variable
`YAPCA_ENV`. When running locally run:

```
export YAPCA_ENV=prod
```

Now create a file called `prod.env` in the config folder and this file will be used
instead of `local.env`

If running docker, edit the file [build/dockercompose.yaml](build/dockercompose.yaml),
and update all cases where the `YAPCA_ENV` variable is passed to a service, then edit
any instances where an `env_file` parameter is used and change it to your new file.

## Deploying

<b>NOTE: It is highly advised to run YAPCA on a local network while you register your first user.
Once you have registered a user them the site can be securly hosted online.
</b>

### Supervisord

Install supervisord and uvicorn, below is a template setup for supervisord:

```toml
[supervisord]

[fcgi-program:uvicorn]
environment=YAPCA_ENV=default
socket=tcp://localhost:8000
command=uvicorn --fd 0 --app-dir PATH_TO_YACPA_BACKEND app.asgi:application
numprocs=4
process_name=uvicorn-%(process_num)d
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0

[program:django-q]
environment=YAPCA_ENV=default
command = python PATH_TO_YACPA_BACKEND/manage.py qcluster
stopasgroup = true
```

### NGINX

To deploy with nginx, firstly make sure the backend is running on port 8000
(or change to your taste). Then generate the `dist` folder in the frontend by running the
command below with an env of your choosing

```
YAPCA_ENV=default yarn build
```

Nginx Conf:

```nginx
upstream app {
    server 127.0.0.1:8000;
}

server {
    listen       80;
    charset     utf-8;
    client_max_body_size 75M;   #Should be >= VUE_APP_MAX_FILE_SIZE env var for file uploads
    location /backend {
        try_files $uri @proxy_to_app;
    }
    location @proxy_to_app {
        proxy_pass http://app;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host $server_name;
    }
    location / {
        root /yapca/dist;
    }
    access_log /var/log/nginx/main;
}
```

## Providing feedback

Feedback on the software can be provided by
[opening a new issue on gitlab](https://gitlab.com/yapca/yapca/issues/new)

## [Contributing](CONTRIBUTING.md)
