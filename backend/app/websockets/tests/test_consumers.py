"""Test consumers."""
import asyncio

import pytest
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator
from rest_framework.authtoken.models import Token

from ...authentication.models import CustomUser
from ...routing import APPLICATION

WEBSOCKET_URL = "ws/main/"


@pytest.mark.django_db()
@database_sync_to_async
def tokens():
    """Create users and return tokens."""
    admin, _ = CustomUser.objects.get_or_create(username="testUser")
    admin_token, _ = Token.objects.get_or_create(user=admin)
    nurse, _ = CustomUser.objects.get_or_create(username="testUser1")
    nurse_token, _ = Token.objects.get_or_create(user=nurse)
    return {
        "nurse": nurse_token,
        "admin": admin_token,
    }


@pytest.mark.django_db()
@pytest.mark.asyncio
async def test_consumer_valid_group_unauthenticated_external():
    """Consumer allowed for unauthenticated users when group is 'external'."""
    communicator = WebsocketCommunicator(APPLICATION, WEBSOCKET_URL)
    connected, _ = await communicator.connect()
    assert connected
    await communicator.send_json_to(
        {"type": "update", "message": "hello", "group": "external"}
    )
    response = await communicator.receive_json_from()
    assert response == {"message": "hello", "type": "update", "group": "external"}
    await communicator.disconnect()


@pytest.mark.django_db()
@pytest.mark.asyncio
async def test_consumer_invalid_group_unauthenticated_nurse():
    """Consumer denied for unauthenticated users when group is not 'external'."""
    communicator = WebsocketCommunicator(APPLICATION, WEBSOCKET_URL)
    connected, _ = await communicator.connect()
    assert connected
    await communicator.send_json_to(
        {"type": "update", "message": "hello", "group": "nurse"}
    )
    with pytest.raises(asyncio.TimeoutError):
        await communicator.receive_json_from(timeout=0.5)


@pytest.mark.django_db()
@pytest.mark.asyncio
async def test_consumer_valid_group_admin_external():
    """Consumer allowed for admin users when group is 'external'."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await communicator.send_json_to(
        {"type": "update", "message": "hello", "group": "external"}
    )
    response = await communicator.receive_json_from()
    assert response == {"message": "hello", "type": "update", "group": "external"}
    await communicator.disconnect()


@pytest.mark.django_db()
@pytest.mark.asyncio
async def test_consumer_invalid_group_nurse_admin():
    """Consumer denied for nurse users when group is 'admin'."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await communicator.send_json_to(
        {"type": "update", "message": "hello", "group": "admin"}
    )
    with pytest.raises(asyncio.TimeoutError):
        await communicator.receive_json_from(timeout=0.5)


@pytest.mark.django_db()
@pytest.mark.asyncio
async def test_consumer_valid_group_admin_admin():
    """Consumer allowed for admin users when group is 'admin'."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await communicator.send_json_to(
        {"type": "update", "message": "hello", "group": "admin"}
    )
    response = await communicator.receive_json_from()
    assert response == {"message": "hello", "type": "update", "group": "admin"}
    await communicator.disconnect()


@pytest.mark.django_db()
@pytest.mark.asyncio
async def test_consumer_headers_invalid():
    """Does not authenticate if 'token' is not passed as query name."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?TEST={user_tokens["admin"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await communicator.send_json_to(
        {"type": "update", "message": "hello", "group": "admin"}
    )
    with pytest.raises(asyncio.TimeoutError):
        await communicator.receive_json_from(timeout=0.5)


@pytest.mark.django_db()
@pytest.mark.asyncio
async def test_consumer_token_invalid_external():
    """Uses anonymous user that can access 'external' group if token is invalid."""
    communicator = WebsocketCommunicator(APPLICATION, f"{WEBSOCKET_URL}?token=TEST")
    connected, _ = await communicator.connect()
    assert connected
    await communicator.send_json_to(
        {"type": "update", "message": "hello", "group": "external"}
    )
    response = await communicator.receive_json_from()
    assert response == {"message": "hello", "type": "update", "group": "external"}
    await communicator.disconnect()


@pytest.mark.django_db()
@pytest.mark.asyncio
async def test_invalid_group_message():
    """Message does not send if group not provided."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await communicator.send_json_to({"type": "update", "message": "hello"})
    with pytest.raises(asyncio.TimeoutError):
        await communicator.receive_json_from(timeout=0.5)
