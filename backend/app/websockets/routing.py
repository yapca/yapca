"""Default app websocket routing."""
from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf import settings
from django.core.asgi import get_asgi_application
from django.urls import re_path

from . import consumers
from .middleware import token_auth_middleware_stack

API_PATH = ""
if settings.API_PATH:
    API_PATH = settings.API_PATH + "/"
application = ProtocolTypeRouter(  # pylint: disable=C0103
    {
        "websocket": token_auth_middleware_stack(
            URLRouter(
                [
                    re_path(
                        API_PATH + r"ws/main/$",
                        consumers.MainConsumer.as_asgi(),
                    )
                ]
            )
        ),
        "http": get_asgi_application(),
    }
)
