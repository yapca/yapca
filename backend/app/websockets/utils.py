"""Util functions for websockets."""
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django_redis import get_redis_connection

from ..authentication.models import CustomUser
from ..authentication.permissions import (
    can_view_user_all_info,
    can_view_user_basic_info,
    can_view_user_display_name,
)
from ..centre.permissions import (
    can_get_deleted_centre_updates,
    centre_exists_and_user_can_view,
    user_can_view_centre_from_event,
    user_can_view_centre_from_need,
    user_can_view_centre_from_policy,
)
from ..client.permissions import (
    can_view_client_document,
    can_view_client_or_admin,
    can_view_client_permissions,
    client_exists_and_user_can_view,
    user_can_view_client_from_care_plan,
    user_can_view_client_from_client_need,
)

redis_connection = get_redis_connection("default")


def send_to_permitted_users(data, client_centres=None):
    """
    Send a websocket message to users who are allowed to see it.

    Can receive a dict of keys that are names of permissions.
    For each logged in user, it loops through each key in the dict
    and sends the user data for the first valid permission.

    Eg:
    When updating a user it first checks if the receiving user can access
    all data for the updated user. (user theme, user specific settings)
    If that fails it checks if the receiving user can access shared data
    for the updated user (is active, username, email)
    If that fails it checks if the receiving user can access basic data
    for the updated user (just displayname)
    If that fails the receiving user is not updated and the loop continues
    to the next user.
    """
    channel_layer = get_channel_layer()
    connected_users = redis_connection.lrange("connectedUserIds", 0, -1)
    for bytes_user_id in connected_users:
        user_id = bytes_user_id.decode("utf-8")
        for permission in data.keys():
            if user_can_view_data(
                user_id, data[permission]["id"], permission, client_centres
            ):
                async_to_sync(channel_layer.group_send)(
                    user_id,
                    {
                        "type": "update",
                        "action": data[permission]["action"],
                        "data": data[permission]["data"],
                    },
                )
                break


permission_functions = {
    "can_view_client_all_info": client_exists_and_user_can_view,
    "can_view_client_basic_info": can_view_client_or_admin,
    "can_view_client_feeding_risks": client_exists_and_user_can_view,
    "can_view_client_attendance_records": client_exists_and_user_can_view,
    "can_view_client_centres": client_exists_and_user_can_view,
    "can_view_client_needs": user_can_view_client_from_client_need,
    "can_view_client_care_plans": user_can_view_client_from_care_plan,
    "can_view_centre_info": centre_exists_and_user_can_view,
    "can_get_deleted_centre_updates": can_get_deleted_centre_updates,
    "can_view_centre_close_events": user_can_view_centre_from_event,
    "can_view_centre_policies": user_can_view_centre_from_policy,
    "can_view_centre_needs": user_can_view_centre_from_need,
    "can_view_centre_events": user_can_view_centre_from_event,
    "can_view_user_all_info": can_view_user_all_info,
    "can_view_user_basic_info": can_view_user_basic_info,
    "can_view_user_display_name": can_view_user_display_name,
    "can_view_deleted_clients": can_view_client_or_admin,
    "can_view_client_visiting_days": can_view_client_or_admin,
    "can_view_client_documents": can_view_client_document,
    "can_view_client_permissions": can_view_client_permissions,
}


def user_can_view_data(user_id, data_id, permission, client_centres):
    """Check if user has correct permissions to view a variety of data types."""
    try:
        user = CustomUser.objects.get(id=user_id)
    except CustomUser.DoesNotExist:
        return False
    if client_centres:
        return permission_functions[permission](user, data_id, client_centres)
    return permission_functions[permission](user, data_id)
