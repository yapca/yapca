"""Django models are python object representations of database models."""

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.forms.models import model_to_dict


class RegisterAbility(models.Model):
    """A boolean that checks if users are allowed to register."""

    canRegister = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Register Ability"
        verbose_name_plural = "Register Abilities"


@receiver(post_save, sender=RegisterAbility)
def save_register_ability(sender, instance, created, **kwargs):  # pylint: disable=W0613
    """Post updates to the websocket consumer."""
    serialized_obj = model_to_dict(instance)
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "external",
        {"type": "update", "action": "updateRegisterAbility", "data": serialized_obj},
    )
