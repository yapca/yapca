"""Serializers allow querysets and model instances to be converted to native Python."""

from rest_framework import serializers

from . import models


class RegisterAbilitySerializer(serializers.ModelSerializer):
    """View the RegisterAbility model as a serializer."""

    class Meta:
        model = models.RegisterAbility
        fields = ("canRegister",)
