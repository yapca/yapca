"""apps.py Tests."""

from django.apps import apps
from django.test import TestCase

from ..apps import AdministrationAppConfig


class AdministrationAppConfigTestCase(TestCase):
    """Tests for the administration app configuration."""

    def test_name(self):
        """Tests name is correct."""
        self.assertEqual(AdministrationAppConfig.name, "app.administration")
        self.assertEqual(
            apps.get_app_config("administration").name, "app.administration"
        )
