"""Test websocket consumers for administration models."""
import pytest
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator

from ...administration.models import RegisterAbility
from ...routing import APPLICATION


def update_register_ability(value):
    """Update the register ability model."""
    register_ability = RegisterAbility.objects.first()
    register_ability.canRegister = value
    return register_ability.save()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_on_save_register_model():
    """Validate websocket message is sent on update to RegisterAbility model."""
    communicator = WebsocketCommunicator(APPLICATION, "ws/main/")
    connected, _ = await communicator.connect()
    assert connected
    await database_sync_to_async(update_register_ability)(False)
    response = await communicator.receive_json_from()
    assert response["type"] == "update"
    assert response["data"] == {"canRegister": False, "id": 1}
    await communicator.disconnect()
