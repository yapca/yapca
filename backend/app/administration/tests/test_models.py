"""Profile Model Tests."""
from django.test import TestCase

from ..models import RegisterAbility


class RegisterAbilityTestCase(TestCase):
    """Tests for the RegisterAbility model."""

    def test_single_register_ability_is_created_automatically(self):
        """A registerAbility row is added to the table as part of the migration."""
        register_ability_total = RegisterAbility.objects.count()
        register_ability_row = RegisterAbility.objects.first()

        self.assertEqual(register_ability_total, 1)
        self.assertTrue(register_ability_row.canRegister)
