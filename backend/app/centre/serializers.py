"""Serializers allow querysets and model instances to be converted to native Python."""

from datetime import timedelta

import arrow
import recurrence
from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied

from ..authentication.permissions import user_is_admin
from . import models
from .permissions import user_can_view_centre
from .validators import (
    validate_all_day,
    validate_clients_in_centre,
    validate_country,
    validate_event_length,
    validate_state,
)

CLOSE_EVENT = "User Close Event"


class PolicySerializer(serializers.ModelSerializer):
    """View the Policy model as a serializer."""

    class Meta:
        model = models.Policy
        fields = ("id", "description")


class NeedSerializer(serializers.ModelSerializer):
    """View the Need model as a serializer."""

    class Meta:
        model = models.Need
        fields = ("id", "description")


def create_recurrance(validated_data):
    """Create a recurrance object from a string input."""
    if validated_data["recurrences"] == "yearly":
        validated_data["recurrences"] = recurrence.Recurrence(
            rrules=[recurrence.Rule(recurrence.YEARLY)]
        )
    elif (
        validated_data["recurrences"] == "monthly"
        and "recurrenceWeek" not in validated_data
    ):
        validated_data["recurrences"] = recurrence.Recurrence(
            rrules=[recurrence.Rule(recurrence.MONTHLY)]
        )
    elif (
        validated_data["recurrences"] == "monthly"
        and "recurrenceWeek" in validated_data
    ):
        date = arrow.get(validated_data["start"])
        day_of_week = date.weekday()
        validated_data["recurrences"] = recurrence.Recurrence(
            rrules=[
                recurrence.Rule(
                    recurrence.MONTHLY,
                    bysetpos=validated_data["recurrenceWeek"],
                    byday=day_of_week,
                )
            ]
        )
    elif validated_data["recurrences"] == "weekly":
        validated_data["recurrences"] = recurrence.Recurrence(
            rrules=[recurrence.Rule(recurrence.WEEKLY)]
        )
    else:
        validated_data["recurrences"] = recurrence.Recurrence(
            rrules=[recurrence.Rule(recurrence.DAILY)]
        )
    return validated_data


def validate_users_in_centre(users, centre):
    """All users of an event must be subbed to the event centre."""
    for user in users:
        if not user_can_view_centre(user, centre):
            raise PermissionDenied(
                (
                    "One of the provided users is not subscribed to this centre. "
                    f"Centre: {centre.name}. "
                    f"User: {user.username}."
                )
            )


class EventSerializer(serializers.ModelSerializer):
    """View the Event model as a serializer."""

    recurrences = serializers.ChoiceField(
        choices=["daily", "weekly", "yearly", "monthly"], required=False
    )
    recurrenceWeek = serializers.IntegerField(min_value=1, max_value=4, required=False)

    class Meta:
        model = models.Event
        fields = (
            "id",
            "centre",
            "recurrences",
            "recurrenceWeek",
            "name",
            "description",
            "start",
            "createdBy",
            "users",
            "clients",
            "allDay",
            "timePeriod",
            "eventType",
            "link",
        )
        read_only_fields = ("id", "createdBy")

    def create(self, validated_data):
        """Override create method to handle recurrences field."""
        validated_data["createdBy"] = self.context["request"].user
        if "recurrences" in validated_data:
            validated_data = create_recurrance(validated_data)
        users = validated_data.pop("users", None)
        clients = validated_data.pop("clients", None)
        validated_data.pop("recurrenceWeek", None)
        instance = models.Event(**validated_data)
        instance.save()
        if users:
            instance.users.set(users)
        if clients:
            instance.clients.set(clients)
        return instance

    def validate_timePeriod(self, value):  # noqa: PLC0103
        """Time periods must be divisible by 15 minutes and not 0."""
        if value < timedelta(minutes=15):
            raise serializers.ValidationError("Duration cannot be less than 15 minutes")
        if value % timedelta(minutes=15) != timedelta(seconds=0):
            raise serializers.ValidationError(
                "Duration must be divisible by 15 minutes"
            )
        return value

    def validate_start(self, value):
        """Start must be divisible by 15 minutes."""
        midnight = value.replace(hour=0, minute=0, second=0, microsecond=0)
        if (value - midnight) % timedelta(minutes=15) != timedelta(seconds=0):
            raise serializers.ValidationError("Start must be divisible by 15 minutes")
        return value

    def validate_eventType(self, value):  # noqa: PLC0103
        """Do not allow "Public Holiday" as eventType."""
        if value == "Public Holiday":
            raise serializers.ValidationError(
                '"Public Holiday" is not a valid choice.', code="invalid_choice"
            )
        if value == CLOSE_EVENT and not user_is_admin(self.context["request"].user):
            raise PermissionDenied("You are not allowed to create close events.")
        return value

    def validate_centre(self, value):
        """Prevent access if user is not subbed to centre."""
        if not user_can_view_centre(self.context["request"].user, value):
            raise PermissionDenied(
                {"error": "You don't have permission to access this centre"}
            )
        return value

    def validate(self, attrs):
        """Object level serializer validation."""
        if "users" in attrs and attrs["users"]:
            validate_users_in_centre(attrs["users"], attrs["centre"])
        if "clients" in attrs and attrs["clients"]:
            validate_clients_in_centre(attrs["clients"], attrs["centre"])
        validate_all_day(attrs)
        if "recurrences" in attrs:
            validate_event_length(attrs["recurrences"], attrs["timePeriod"])
        if attrs["eventType"] == CLOSE_EVENT and (
            "recurrences" in attrs and attrs["recurrences"] == "daily"
        ):
            raise serializers.ValidationError(
                '"daily" is not a valid value when "eventType" is CLOSE_EVENT',
                code="invalid_choice",
            )
        return attrs


class DaySerializer(serializers.ModelSerializer):
    """View the Day model as a serializer."""

    class Meta:
        model = models.Day
        fields = ("day",)


def validate_centre(instance, data):
    """Validate centre data being posted."""
    if "country" in data:
        validate_country(data["country"])
        country = data["country"]
    else:
        if instance:
            country = instance.country
        else:
            country = None
    if "state" in data and data["state"] != "":
        validate_state(country, data["state"])
    return data


class CentreSerializer(serializers.ModelSerializer):
    """POST Centre model as a serializer."""

    class Meta:
        model = models.Centre
        fields = (
            "id",
            "name",
            "openingDays",
            "policies",
            "needs",
            "policiesFirstSetupComplete",
            "needsFirstSetupComplete",
            "openingDaysFirstSetupComplete",
            "closingDaysFirstSetupComplete",
            "setupComplete",
            "color",
            "created",
            "closedOnPublicHolidays",
            "country",
            "state",
        )
        read_only_fields = ("created",)

    def validate(self, attrs):  # pylint: disable=W0221
        """Object level serializer validation."""
        return validate_centre(self.instance, attrs)


class CentreSerializerNurse(serializers.ModelSerializer):
    """POST Centre model as a serializer as nurse setting some fields to read-only."""

    class Meta:
        model = models.Centre
        fields = (
            "id",
            "name",
            "openingDays",
            "policies",
            "needs",
            "policiesFirstSetupComplete",
            "needsFirstSetupComplete",
            "openingDaysFirstSetupComplete",
            "closingDaysFirstSetupComplete",
            "setupComplete",
            "color",
            "created",
            "closedOnPublicHolidays",
            "country",
            "state",
        )
        read_only_fields = (
            "id",
            "name",
            "openingDays",
            "policiesFirstSetupComplete",
            "needsFirstSetupComplete",
            "openingDaysFirstSetupComplete",
            "closingDaysFirstSetupComplete",
            "setupComplete",
            "created",
            "closedOnPublicHolidays",
            "country",
            "state",
            "color",
        )

    def to_representation(self, instance):
        """Return a nested view as a response to a POST request."""
        serializer = CentreSerializer(instance)
        return serializer.data

    def validate(self, attrs):  # pylint: disable=W0221
        """Object level serializer validation."""
        return validate_centre(self.instance, attrs)
