"""A view is a function that takes a Web request and returns a Web response."""

import copy
from datetime import datetime, timedelta

from rest_framework import generics, status
from rest_framework.exceptions import PermissionDenied, ValidationError
from rest_framework.permissions import SAFE_METHODS
from rest_framework.response import Response

from ..authentication.permissions import IsAdmin, IsClient, IsNurse, user_is_client
from ..client.tasks import get_country_holidays
from .models import Centre, Event, Need, Policy
from .permissions import (
    CanDeleteEvent,
    CanUpdateCentre,
    CanUpdateCentreFromNeed,
    CanUpdateCentreFromPolicy,
    CanViewCentre,
    centre_exists_and_user_can_view,
)
from .serializers import (
    CentreSerializer,
    CentreSerializerNurse,
    EventSerializer,
    NeedSerializer,
    PolicySerializer,
)
from .signals import delete_centre_websocket, update_event_websocket

DATE_FORMAT = "%Y-%m-%d"
ISO_DATE_FORMAT = "%Y-%m-%dT%H:%M:%SZ"


def validate_close_event_dates(start_date, end_date):
    """Validate parameters."""
    try:
        start_date = datetime.strptime(start_date, DATE_FORMAT)
        end_date = datetime.strptime(end_date, DATE_FORMAT)
        return (start_date, end_date)
    except ValueError as error:
        raise ValidationError(
            {"error": "Start and end dates must be in format YYYY-MM-DD"}
        ) from error


def get_country_holidays_for_centre(start_date, centre, end_date, all_events):
    """Get country holidays for a centre."""
    years = [start_date.year]
    if start_date.year != end_date.year:
        years.append(end_date.year)
    country_holidays = get_country_holidays(
        centre.country,
        centre.state,
        years=years,
    )
    for holiday_date in country_holidays[start_date:end_date]:
        all_events.append(
            {
                "name": country_holidays[holiday_date],
                "centre": centre.id,
                "start": datetime.combine(holiday_date, datetime.min.time()),
                "end": datetime.combine(
                    (holiday_date + timedelta(days=1)), datetime.min.time()
                ),
                "eventType": "Public Holiday",
                "timePeriod": "1 00:00:00",
            }
        )
    return all_events


class CentreView(generics.ListCreateAPIView):
    """Get all Centre objects from database and return for serializer."""

    queryset = Centre.objects.all()
    serializer_class = CentreSerializer
    permission_classes = [IsAdmin]

    def get_permissions(self):
        """Override default permissions settings."""
        if self.request.method in SAFE_METHODS:
            return [IsClient()]
        return [IsAdmin()]

    def get_queryset(self):
        """Filter centres to only subbed centres."""
        if self.request.user.groups.filter(name="admin"):
            return Centre.objects.all()
        user = self.request.user
        if user.client:
            user_centres = user.client.centres.all()
        else:
            user_centres = user.centres.all()
        return Centre.objects.filter(id__in=user_centres).order_by("id")


class CentreUpdate(generics.UpdateAPIView):
    """Update Centre objects."""

    queryset = Centre.objects.all()
    permission_classes = [CanUpdateCentre]

    def get_serializer_class(self):
        """Use a different serializer per user group."""
        if self.request.user and self.request.user.groups.filter(name="admin"):
            return CentreSerializer
        return CentreSerializerNurse


class CentreDelete(generics.DestroyAPIView):
    """Delete Centre objects."""

    queryset = Centre.objects.all()
    serializer_class = CentreSerializer
    permission_classes = [IsAdmin]

    def perform_destroy(self, instance):
        """Send ws message on centre delete."""
        delete_centre_websocket({"id": instance.id})
        return instance.delete()


class PolicyUpdate(generics.UpdateAPIView):
    """Update Policy objects."""

    queryset = Policy.objects.all()
    serializer_class = PolicySerializer
    permission_classes = [CanUpdateCentreFromPolicy]


class PolicyView(generics.ListAPIView):
    """Get all Policy objects from database and return for serializer."""

    queryset = Policy.objects.all()
    serializer_class = PolicySerializer
    permission_classes = [CanUpdateCentre]

    def get_queryset(self):
        """Filter needs by centre."""
        centre_id = self.kwargs["pk"]
        return Policy.objects.filter(centre__id=centre_id)


class PolicyCreateView(generics.CreateAPIView):
    """Create Policy objects."""

    queryset = Policy.objects.all()
    serializer_class = PolicySerializer
    permission_classes = [IsNurse]

    def create(self, request, *args, **kwargs):  # pylint: disable=W0613
        """Override default create settings to allow bulk creation."""
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )


class NeedUpdate(generics.UpdateAPIView):
    """Update Need objects."""

    queryset = Need.objects.all()
    serializer_class = NeedSerializer
    permission_classes = [CanUpdateCentreFromNeed]


class NeedView(generics.ListAPIView):
    """Get all Need objects from database and return for serializer."""

    serializer_class = NeedSerializer
    permission_classes = [CanViewCentre]

    def get_queryset(self):
        """Filter needs by centre."""
        centre_id = self.kwargs["pk"]
        return Need.objects.filter(centre__id=centre_id)


class NeedCreateView(generics.CreateAPIView):
    """Create Need objects."""

    serializer_class = NeedSerializer
    permission_classes = [IsNurse]

    def create(self, request, *args, **kwargs):  # pylint: disable=W0613
        """Override default create settings to allow bulk creation."""
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )


def get_event_occurrences(event, start_date, end_date):
    """Get all serialized occurrences for an event."""
    occurrence_events = []
    serialized_event = EventSerializer(event).data
    serialized_event["users"].sort()
    serialized_event["clients"].sort()
    serialized_event["timePeriod"] = int(event.timePeriod.total_seconds())
    serialized_event.pop("recurrences", None)
    occurrences = event.recurrences.between(
        start_date - event.timePeriod,
        end_date,
        dtstart=datetime.combine(event.start, datetime.min.time()),
        inc=True,
    )
    for occurrence in occurrences:
        occurrence_event = copy.copy(serialized_event)
        occurrence_event["end"] = occurrence + event.timePeriod
        occurrence_event["start"] = occurrence
        occurrence_events.append(occurrence_event)
    return occurrence_events


class EventListView(generics.ListAPIView):
    """Get event occurrences between two dates for a centre."""

    permission_classes = [IsClient]
    serializer_class = EventSerializer

    def get_queryset(self):
        """Filter events by centre."""
        centre = self.request.query_params.get("centre", None)
        event_type = self.request.query_params.get("eventType", None)

        user = self.request.user
        events = Event.objects.all()
        if centre:
            if not centre_exists_and_user_can_view(self.request.user, centre):
                raise PermissionDenied(
                    {"error": "You don't have permission to access this centre"}
                )
            events = events.filter(centre__id=centre)
            centres = [Centre.objects.get(pk=centre)]
        else:
            if user_is_client(user):
                centres = user.client.centres.all()
            else:
                centres = user.centres.all()
            events = events.filter(centre__in=centres)
        if event_type:
            events = events.filter(eventType=event_type)
        return events, centres, event_type

    def get(self, request, start_date, end_date):  # noqa: PLW0221
        """Return a filtered list based on date and client."""
        all_events = []
        (start_date, end_date) = validate_close_event_dates(start_date, end_date)
        events, centres, event_type = self.get_queryset()
        for event in events:
            all_events = all_events + get_event_occurrences(event, start_date, end_date)
        if not event_type or event_type == "User Close Event":
            for centre in centres:
                all_events = get_country_holidays_for_centre(
                    start_date, centre, end_date, all_events
                )
        return Response(all_events, status=200)


class EventCreateView(generics.CreateAPIView):
    """Create Event objects."""

    serializer_class = EventSerializer
    permission_classes = [IsNurse]

    def create(self, request, *args, **kwargs):  # pylint: disable=W0613
        """Override default create settings to return different object."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        data = serializer.data
        event = Event.objects.get(id=data["id"])
        data.pop("recurrences")
        data["end"] = (event.start + event.timePeriod).strftime(ISO_DATE_FORMAT)
        update_event_websocket(data)
        return Response(data, status=status.HTTP_201_CREATED, headers=headers)


class EventDelete(generics.DestroyAPIView):
    """Delete Event objects."""

    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = [CanDeleteEvent]

    def perform_destroy(self, instance):
        """Send ws message on client delete."""
        update_event_websocket({"id": instance.id}, action="deleteEvent")
        instance.delete()
