"""Signals to send websocket messages on model change."""

from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver
from django.forms.models import model_to_dict

from ..websockets.utils import send_to_permitted_users
from .models import Centre, Need, Policy


def update_centre_websocket(instance, opening_days=False, policies=False, needs=False):
    """Send the updated/created centre to the websocket consumer."""
    serialized_obj = model_to_dict(instance)
    data = {
        "id": serialized_obj["id"],
        "name": serialized_obj["name"],
        "policiesFirstSetupComplete": serialized_obj["policiesFirstSetupComplete"],
        "needsFirstSetupComplete": serialized_obj["needsFirstSetupComplete"],
        "openingDaysFirstSetupComplete": serialized_obj[
            "openingDaysFirstSetupComplete"
        ],
        "closingDaysFirstSetupComplete": serialized_obj[
            "closingDaysFirstSetupComplete"
        ],
        "setupComplete": serialized_obj["setupComplete"],
        "color": serialized_obj["color"],
        "closedOnPublicHolidays": serialized_obj["closedOnPublicHolidays"],
        "country": serialized_obj["country"],
        "state": serialized_obj["state"],
    }
    if opening_days:
        opening_days_list = []
        for day in instance.openingDays.all():
            opening_days_list.append(day.id)
        data["openingDays"] = opening_days_list
    if policies:
        policies_list = []
        for policy in instance.policies.all():
            policies_list.append(policy.id)
        data["policies"] = policies_list
    if needs:
        needs_list = []
        for need in instance.needs.all():
            needs_list.append(need.id)
        data["needs"] = needs_list
    send_to_permitted_users(
        {
            "can_view_centre_info": {
                "action": "updateCentre",
                "data": data,
                "id": serialized_obj["id"],
            }
        }
    )


def update_event_websocket(data, action="updateEvent"):
    """Update consumer on create/update."""
    send_to_permitted_users(
        {
            "can_view_centre_events": {
                "action": action,
                "data": data,
                "id": data["id"],
            }
        }
    )


def delete_centre_websocket(instance):
    """Send the delete centre command to the consumer."""
    send_to_permitted_users(
        {
            "can_get_deleted_centre_updates": {
                "action": "deleteCentre",
                "data": {"id": instance["id"]},
                "id": instance["id"],
            }
        }
    )


def update_policy_websocket(instance):
    """Send the updated/created policy to the websocket consumer."""
    serialized_obj = model_to_dict(instance)
    send_to_permitted_users(
        {
            "can_view_centre_policies": {
                "action": "updatePolicy",
                "data": serialized_obj,
                "id": serialized_obj["id"],
            }
        }
    )


def update_need_websocket(instance):
    """Send the updated/created policy to the websocket consumer."""
    serialized_obj = model_to_dict(instance)
    send_to_permitted_users(
        {
            "can_view_centre_needs": {
                "action": "updateNeed",
                "data": serialized_obj,
                "id": serialized_obj["id"],
            }
        }
    )


@receiver(post_save, sender=Centre)
def save_centre(
    sender, instance, created, update_fields, **kwargs
):  # pylint: disable=W0613
    """Update websocket on update/create."""
    update_centre_websocket(instance)


@receiver(m2m_changed, sender=Centre.policies.through)
def save_policies(sender, instance, action, **kwargs):  # pylint: disable=W0613
    """Update websocket on update/create."""
    if action in ("post_add", "post_remove"):
        update_centre_websocket(instance, policies=True)


@receiver(m2m_changed, sender=Centre.openingDays.through)
def save_opening_days(sender, instance, action, **kwargs):  # pylint: disable=W0613
    """Update websocket on update/create."""
    if action in ("post_add", "post_remove"):
        update_centre_websocket(instance, opening_days=True)


@receiver(m2m_changed, sender=Centre.needs.through)
def save_needs(sender, instance, action, **kwargs):  # pylint: disable=W0613
    """Update websocket on update/create."""
    if action in ("post_add", "post_remove"):
        update_centre_websocket(instance, needs=True)


@receiver(post_save, sender=Policy)
def save_policy(
    sender, instance, created, update_fields, **kwargs
):  # pylint: disable=W0613
    """Update websocket on update/create."""
    update_policy_websocket(instance)


@receiver(post_save, sender=Need)
def save_need(
    sender, instance, created, update_fields, **kwargs
):  # pylint: disable=W0613
    """Update websocket on update/create."""
    update_need_websocket(instance)
