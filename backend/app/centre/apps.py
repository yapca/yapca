"""Application configuration objects store metadata for an application."""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class CentreAppConfig(AppConfig):
    """Application configuration for the centre module."""

    name = "app.centre"

    def ready(self):
        """Import signals module on load."""
        from . import signals  # noqa: F401, PLW0611, PLC0415
