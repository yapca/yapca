"""Centre related permissions functions."""
from django.shortcuts import get_object_or_404

from ..authentication.permissions import IsClient, IsNurse
from .models import Centre, Event, Need, Policy


def user_can_view_centre(user, centre):
    """
    Check if user has correct permissions to view a centre.

    Returns bool if provided user is subbed to a centre or admihn.
    """
    if user.groups.filter(name="admin").exists():
        return True
    if user.groups.filter(name="client").exists():
        return user.client.centres.filter(id=centre.id).exists()
    return user.centres.filter(id=centre.id).exists()


def user_can_view_centre_from_need(user, data_id):
    """Validate user can view centre with provided need."""
    need = Need.objects.get(id=data_id)
    centres = need.centre_set.all()
    for centre in centres:
        if user_can_view_centre(user, centre):
            return True
    return False


def user_can_view_centre_from_policy(user, policy_id):
    """Validate user can view centre with provided policy."""
    policy = Policy.objects.get(id=policy_id)
    centres = policy.centre_set.all()
    for centre in centres:
        if user_can_view_centre(user, centre):
            return True
    return False


def user_can_view_centre_from_event(user, data_id):
    """Validate user can view centre with provided event."""
    event = Event.objects.get(id=data_id)
    return user_can_view_centre(user, event.centre)


def centre_exists_and_user_can_view(user, data_id):
    """First check if centre exists then validate user can view."""
    try:
        centre = Centre.objects.get(id=data_id)
    except Centre.DoesNotExist:
        return False
    return user_can_view_centre(user, centre)


def can_get_deleted_centre_updates(user, data_id):
    """Any authenticated user can know when a centre is deleted."""
    return user and data_id


class CanUpdateCentre(IsNurse):
    """Check if the user is allowed to update a centre."""

    def has_permission(self, request, view):
        """Check if user subbed to centre."""
        centre_id = view.kwargs.get("pk")
        if super().has_permission(request, view):
            return centre_exists_and_user_can_view(request.user, centre_id)
        return False


class CanViewCentre(IsClient):
    """Check if the user is allowed to view a centre."""

    def has_permission(self, request, view):
        """Check if user subbed to centre."""
        centre_id = view.kwargs.get("pk")
        if super().has_permission(request, view):
            return centre_exists_and_user_can_view(request.user, centre_id)
        return False


class CanUpdateCentreFromPolicy(IsNurse):
    """Check if the user is allowed to update a centre based on policy id."""

    def has_permission(self, request, view):
        """Check policy id centre subbed."""
        policy_id = view.kwargs.get("pk")
        if super().has_permission(request, view):
            return user_can_view_centre_from_policy(request.user, policy_id)
        return False


class CanUpdateCentreFromNeed(IsNurse):
    """Check if the user is allowed to update a centre based on need id."""

    def has_permission(self, request, view):
        """Check policy id centre subbed."""
        need_id = view.kwargs.get("pk")
        if super().has_permission(request, view):
            return user_can_view_centre_from_need(request.user, need_id)
        return False


class CanDeleteEvent(IsNurse):
    """Check if the user is allowed to update a centre based on event id."""

    def has_permission(self, request, view):
        """Check if user is subbed to centre."""
        event_id = view.kwargs.get("pk")
        if super().has_permission(request, view):
            event = get_object_or_404(Event, pk=event_id)
            if event.eventType == "User Close Event":
                return request.user.groups.filter(name="admin").exists()
            if event.eventType == "Public Holiday":
                return False
            return user_can_view_centre_from_event(request.user, event.id)
        return False
