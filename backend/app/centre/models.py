"""Django models are python object representations of database models."""

from __future__ import unicode_literals

from django.db import models
from recurrence.fields import RecurrenceField

from ..authentication.validators import validate_accent_color
from .validators import validate_country


class Day(models.Model):
    """The days of the week."""

    dayList = (
        ("Monday", "Monday"),
        ("Tuesday", "Tuesday"),
        ("Wednesday", "Wednesday"),
        ("Thursday", "Thursday"),
        ("Friday", "Friday"),
        ("Saturday", "Saturday"),
        ("Sunday", "Sunday"),
    )
    day = models.CharField(max_length=9, choices=dayList, unique=True)

    class Meta:
        verbose_name = "Day"
        verbose_name_plural = "Days"


class Policy(models.Model):
    """The policies a centre is required to be informed about."""

    description = models.TextField()

    class Meta:
        verbose_name = "Policy"
        verbose_name_plural = "Policies"


class Need(models.Model):
    """The needs a client can have."""

    description = models.TextField()

    class Meta:
        verbose_name = "Need"
        verbose_name_plural = "Needs"


class Centre(models.Model):
    """The Centre model contains data on centres."""

    name = models.CharField(
        max_length=255,
        unique=True,
        error_messages={"unique": "A centre with that name already exists."},
    )
    openingDays = models.ManyToManyField(Day, blank=True)
    policies = models.ManyToManyField(Policy, blank=True)
    needs = models.ManyToManyField(Need, blank=True)
    policiesFirstSetupComplete = models.BooleanField(default=False)
    needsFirstSetupComplete = models.BooleanField(default=False)
    openingDaysFirstSetupComplete = models.BooleanField(default=False)
    closingDaysFirstSetupComplete = models.BooleanField(default=False)
    setupComplete = models.BooleanField(default=False)
    color = models.CharField(
        blank=True, max_length=7, validators=[validate_accent_color]
    )
    created = models.DateTimeField(auto_now_add=True)
    closedOnPublicHolidays = models.BooleanField(default=True)
    country = models.CharField(
        default="IE", max_length=3, validators=[validate_country]
    )
    state = models.CharField(blank=True, max_length=3)

    class Meta:
        verbose_name = "Centre"
        verbose_name_plural = "Centres"


eventTypes = (
    ("Online", "Online"),
    ("On-Site", "On-Site"),
    ("User Close Event", "User Close Event"),
    ("Public Holiday", "Public Holiday"),
)


class Event(models.Model):
    """Event that can be participated in by clients and/or medical users."""

    centre = models.ForeignKey(Centre, on_delete=models.CASCADE)
    recurrences = RecurrenceField(blank=True)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    start = models.DateTimeField()
    createdBy = models.ForeignKey(
        "authentication.CustomUser",
        on_delete=models.DO_NOTHING,
        related_name="centre_event",
    )
    users = models.ManyToManyField(to="authentication.CustomUser", blank=True)
    clients = models.ManyToManyField(to="client.Client", blank=True)
    timePeriod = models.DurationField()
    eventType = models.CharField(max_length=16, choices=eventTypes)
    allDay = models.BooleanField(default=False)
    link = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name = "Event"
        verbose_name_plural = "Events"
