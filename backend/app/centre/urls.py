"""URLs routes that are part of the main django app."""

from django.urls import path

from . import views

urlpatterns = [  # pylint: disable=C0103
    path("centres/", views.CentreView.as_view(), name="centres"),
    path(
        "centres/<int:pk>/update/", views.CentreUpdate.as_view(), name="centres-update"
    ),
    path(
        "centres/<int:pk>/delete/", views.CentreDelete.as_view(), name="centres-delete"
    ),
    path("centres/<int:pk>/needs/", views.NeedView.as_view(), name="centre-needs"),
    path(
        "centres/<int:pk>/policies/",
        views.PolicyView.as_view(),
        name="centre-policies",
    ),
    path("policies/", views.PolicyCreateView.as_view(), name="policies"),
    path(
        "policies/<int:pk>/update/", views.PolicyUpdate.as_view(), name="policy-update"
    ),
    path("needs/", views.NeedCreateView.as_view(), name="needs"),
    path("needs/<int:pk>/update/", views.NeedUpdate.as_view(), name="need-update"),
    path(
        "events/<str:start_date>/<str:end_date>/",
        views.EventListView.as_view(),
        name="centre-events",
    ),
    path(
        "events/",
        views.EventCreateView.as_view(),
        name="centre-events",
    ),
    path(
        "events/<int:pk>/",
        views.EventDelete.as_view(),
        name="event-delete",
    ),
]
