"""Test websocket consumers for centre model."""
import asyncio
import datetime
from datetime import timedelta

import pytest
import pytz
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from ...authentication.models import CustomUser
from ...centre.models import Centre, Day, Event, Need, Policy
from ...routing import APPLICATION

WEBSOCKET_URL = "ws/main/"
CENTRE_NAME = "Test Centre"
POLICY_NAME = "Test Policy"
NEED_NAME = "Test Need"
DESCRIPTION = "New description"
CLOSE_EVENT = "User Close Event"


def create_default_groups():
    """Create the default groups since pytest doesn't apply migrations."""
    Group.objects.get_or_create(name="admin")
    Group.objects.get_or_create(name="nurse")


def check_m2m_data_not_passed(response):
    """Assert a centres did not receive m2m objects."""
    assert "openingDays" not in response["data"]
    assert "policies" not in response["data"]
    assert "needs" not in response["data"]


def check_centre_booleans(data):
    """Check booleans on centre."""
    assert data["policiesFirstSetupComplete"]
    assert data["closedOnPublicHolidays"]
    assert not data["needsFirstSetupComplete"]
    assert not data["openingDaysFirstSetupComplete"]
    assert not data["closingDaysFirstSetupComplete"]
    assert not data["setupComplete"]


def check_centre_data(response):
    """Assert a centres data is correct."""
    assert response["data"]["name"] == CENTRE_NAME
    assert response["data"]["country"] == "IE"
    assert response["data"]["state"] == ""
    assert response["type"] == "update"
    assert response["action"] == "updateCentre"
    assert response["data"]["color"] == "#FFFFFF"
    check_centre_booleans(response["data"])
    check_m2m_data_not_passed(response)


@database_sync_to_async
def tokens():
    """Create users and return tokens."""
    create_default_groups()
    admin, _ = CustomUser.objects.get_or_create(username="testUser")
    admin_token, _ = Token.objects.get_or_create(user=admin)
    nurse, _ = CustomUser.objects.get_or_create(username="testUser1")
    nurse_token, _ = Token.objects.get_or_create(user=nurse)
    return {
        "nurse": nurse_token,
        "nurse_user": nurse,
        "admin": admin_token,
        "admin_user": admin,
    }


@database_sync_to_async
def update_centres():
    """Update a centre."""
    user = CustomUser.objects.get(username="testUser1")
    test_centre, _ = Centre.objects.get_or_create(name=CENTRE_NAME, color="#FFFFFF")
    user.centres.add(test_centre)
    test_centre.policiesFirstSetupComplete = True
    return test_centre.save()


@database_sync_to_async
def create_close_event():
    """Create a close event."""
    admin_user = CustomUser.objects.get(username="testUser")
    nurse_user = CustomUser.objects.get(username="testUser1")
    client = APIClient()
    test_centre, _ = Centre.objects.get_or_create(name=CENTRE_NAME, color="#FFFFFF")
    nurse_user.centres.add(test_centre)
    client.force_authenticate(user=admin_user, token=admin_user.auth_token)
    close_event = client.post(
        "/api/events/",
        {
            "centre": test_centre.id,
            "name": "close event 1",
            "start": "2020-11-10 00:00:00",
            "timePeriod": 345600,
            "recurrences": "weekly",
            "eventType": CLOSE_EVENT,
            "allDay": True,
        },
        format="json",
    )
    return (close_event.data, test_centre)


@database_sync_to_async
def delete_close_event():
    """Delete a close event."""
    admin_user = CustomUser.objects.get(username="testUser")
    nurse_user = CustomUser.objects.get(username="testUser1")
    client = APIClient()
    test_centre, _ = Centre.objects.get_or_create(name=CENTRE_NAME, color="#FFFFFF")
    nurse_user.centres.add(test_centre)
    close_event = Event.objects.create(
        name="close event 2",
        centre=test_centre,
        start=datetime.datetime(2020, 3, 10, 0, 0, 00, tzinfo=pytz.UTC),
        timePeriod=timedelta(days=5),
        createdBy=nurse_user,
        eventType=CLOSE_EVENT,
    )
    client.force_authenticate(user=admin_user, token=admin_user.auth_token)
    client.delete(f"/api/events/{close_event.id}/")
    return close_event


@database_sync_to_async
def delete_centre():
    """Delete a centre."""
    nurse_user = CustomUser.objects.get(username="testUser1")
    admin_user = CustomUser.objects.get(username="testUser")
    test_centre, _ = Centre.objects.get_or_create(name=CENTRE_NAME)
    nurse_user.centres.add(test_centre)
    client = APIClient()
    client.force_authenticate(user=admin_user, token=admin_user.auth_token)
    client.delete(f"/api/centres/{test_centre.id}/delete/")
    return test_centre.id


@database_sync_to_async
def update_centre_policies():
    """Update a centres policies."""
    nurse_user = CustomUser.objects.get(username="testUser1")
    test_policy, _ = Policy.objects.get_or_create(description="")
    test_centre, _ = Centre.objects.get_or_create(name=CENTRE_NAME)
    nurse_user.centres.add(test_centre)
    test_centre.policies.add(test_policy)
    return test_policy


@database_sync_to_async
def update_centre_needs():
    """Update a centres needs."""
    nurse_user = CustomUser.objects.get(username="testUser1")
    test_need, _ = Need.objects.get_or_create(description="")
    test_centre, _ = Centre.objects.get_or_create(name=CENTRE_NAME)
    nurse_user.centres.add(test_centre)
    test_centre.needs.add(test_need)
    return test_need


@database_sync_to_async
def update_centre_opening_days():
    """Update a centres opening days."""
    nurse_user = CustomUser.objects.get(username="testUser1")
    test_centre, _ = Centre.objects.get_or_create(name=CENTRE_NAME)
    nurse_user.centres.add(test_centre)
    monday, _ = Day.objects.get_or_create(day="Monday")
    return test_centre.openingDays.add(monday)


@database_sync_to_async
def update_policy(sub_to_centre=True):
    """Update a policy."""
    nurse_user = CustomUser.objects.get(username="testUser1")
    test_centre, _ = Centre.objects.get_or_create(name=CENTRE_NAME)
    test_centre_2, _ = Centre.objects.get_or_create(name="centre2")
    nurse_user.centres.add(test_centre)
    test_policy, _ = Policy.objects.get_or_create(description="")
    if sub_to_centre:
        test_centre.policies.add(test_policy)
    test_centre_2.policies.add(test_policy)
    test_policy.description = DESCRIPTION
    test_policy.save()
    return test_policy.save()


@database_sync_to_async
def update_need(sub_to_centre=True):
    """Update a need."""
    nurse_user = CustomUser.objects.get(username="testUser1")
    test_centre, _ = Centre.objects.get_or_create(name=CENTRE_NAME)
    test_centre_2, _ = Centre.objects.get_or_create(name="centre2")
    nurse_user.centres.add(test_centre)
    test_need, _ = Need.objects.get_or_create(description="")
    if sub_to_centre:
        test_centre.needs.add(test_need)
    test_centre_2.needs.add(test_need)
    test_need.description = DESCRIPTION
    test_need.save()
    return test_need.save()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_centre_update():
    """Validate websocket message is sent when updating a centre."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_centres()
    response = await communicator.receive_json_from()
    check_centre_data(response)
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_centre_delete():
    """Validate websocket message is sent when deleting a centre."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    centre_id = await delete_centre()
    response = await communicator.receive_json_from()
    assert response == {
        "type": "update",
        "action": "deleteCentre",
        "data": {"id": centre_id},
    }
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_centre_policy_update():
    """Validate websocket message is sent when updating a centres policy."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    policy = await update_centre_policies()
    centre_update_response = await communicator.receive_json_from()
    assert len(centre_update_response["data"]["policies"]) == 1
    assert centre_update_response["data"]["policies"][0] == policy.id
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_centre_need_update():
    """Validate websocket message is sent when updating a centres need."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    need = await update_centre_needs()
    centre_update_response = await communicator.receive_json_from()
    assert len(centre_update_response["data"]["needs"]) == 1
    assert centre_update_response["data"]["needs"][0] == need.id
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_centre_opening_days_update():
    """Validate websocket message is sent when updating a centres opening days."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_centre_opening_days()
    response = await communicator.receive_json_from()
    assert len(response["data"]["openingDays"]) == 1
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_policy():
    """Validate websocket message is sent when updating a policy."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_policy()
    await communicator.receive_json_from()  # Update centre policies
    response = await communicator.receive_json_from()
    assert response["data"]["description"] == DESCRIPTION
    assert response["type"] == "update"
    assert response["action"] == "updatePolicy"
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_policy_not_subbed():
    """WS message is not sent when updating a policy for non subbed centre."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_policy(sub_to_centre=False)
    with pytest.raises(asyncio.TimeoutError):
        await communicator.receive_json_from(timeout=0.5)


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_need():
    """Validate websocket message is sent when updating a need."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_need()
    await communicator.receive_json_from()  # Update centre needs
    response = await communicator.receive_json_from()
    assert response["data"]["description"] == DESCRIPTION
    assert response["type"] == "update"
    assert response["action"] == "updateNeed"
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_need_not_subbed():
    """WS message is not sent when updating a need for non subbed centre."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_need(sub_to_centre=False)
    with pytest.raises(asyncio.TimeoutError):
        await communicator.receive_json_from(timeout=0.5)


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_create_close_event():
    """Validate websocket message is sent when creating a close event."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    (close_event, centre) = await create_close_event()
    response = await communicator.receive_json_from()
    assert response == {
        "type": "update",
        "action": "updateEvent",
        "data": {
            "id": close_event["id"],
            "centre": centre.id,
            "name": "close event 1",
            "description": "",
            "start": "2020-11-10T00:00:00Z",
            "createdBy": user_tokens["admin_user"].id,
            "users": [],
            "clients": [],
            "allDay": True,
            "timePeriod": "4 00:00:00",
            "eventType": CLOSE_EVENT,
            "link": "",
            "end": "2020-11-14T00:00:00Z",
        },
    }
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_delete_close_event():
    """Validate websocket message is sent when deleting a close event."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    close_event = await delete_close_event()
    response = await communicator.receive_json_from()
    assert response == {
        "type": "update",
        "action": "deleteEvent",
        "data": {"id": close_event.id},
    }
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_centre_admin():
    """Validate websocket message is always sent to admins on centre change."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_centres()
    create_response = await communicator.receive_json_from()
    assert create_response["action"] == "updateCentre"
    await communicator.disconnect()
