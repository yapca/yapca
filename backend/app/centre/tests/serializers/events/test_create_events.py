"""Create Events Tests."""

import recurrence
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....client.tests.utils import create_client
from ....models import Event
from ...utils import create_centre

TOKEN_AUTH = "Token "
URL = "/api/events/"
REQUIRED_MESSAGE = "This field is required."
CLOSE_EVENT = "User Close Event"


class CreateEventsTestCase(TestCase):  # noqa: PLR0904
    """Create Events Tests."""

    post_url = "/api/events/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        user_1_token = register_test_user(
            self.client, username="user1", email="user1@test.test"
        )
        user_2_token = register_test_user(
            self.client, username="user2", email="user2@test.test"
        )
        self.users = {
            "user_1": CustomUser.objects.get(id=user_1_token.data["user"]["id"]),
            "user_2": CustomUser.objects.get(id=user_2_token.data["user"]["id"]),
            "user_2_token": user_2_token,
        }
        self.centres = {
            "centre_1": create_centre(
                name="centre 1",
                country="US",
                state="TX",
                users=[self.users["user_1"], self.users["user_2"]],
            ),
            "centre_2": create_centre(name="centre 2", users=[self.users["user_1"]]),
        }
        self.clients = {
            "client_1": create_client(surname="1", centres=[self.centres["centre_1"]]),
            "client_2": create_client(surname="2", centres=[self.centres["centre_2"]]),
        }
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + user_1_token.data["key"]
        )

    def test_missing_data(self):
        """Should throw error if data missing."""
        response = self.client.post(
            URL,
            {},
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "centre": [ErrorDetail(string=REQUIRED_MESSAGE, code="required")],
                "name": [ErrorDetail(string=REQUIRED_MESSAGE, code="required")],
                "start": [ErrorDetail(string=REQUIRED_MESSAGE, code="required")],
                "timePeriod": [ErrorDetail(string=REQUIRED_MESSAGE, code="required")],
                "eventType": [ErrorDetail(string=REQUIRED_MESSAGE, code="required")],
            },
        )

    def test_invalid_clients_and_users(self):
        """Should throw error if invalid clients or users."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "days": 2,
                "recurrences": "yearly",
                "name": "marching band",
                "start": "2019-04-11 18:00:00",
                "timePeriod": 900,
                "eventType": "Online",
                "users": [400, 405],
                "clients": [289, 4125],
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "users": [
                    ErrorDetail(
                        string='Invalid pk "400" - object does not exist.',
                        code="does_not_exist",
                    )
                ],
                "clients": [
                    ErrorDetail(
                        string='Invalid pk "289" - object does not exist.',
                        code="does_not_exist",
                    )
                ],
            },
        )

    def test_non_divisible_time_period(self):
        """Time period has to be divisible by 15 mins."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "fishing",
                "start": "2018-11-08 18:00:00",
                "timePeriod": 20001,
                "eventType": "Online",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "timePeriod": [
                    ErrorDetail(
                        string="Duration must be divisible by 15 minutes",
                        code="invalid",
                    )
                ]
            },
        )

    def test_non_divisible_start(self):
        """Start has to be divisible by 15 mins."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "kayaking",
                "start": "2011-03-18 18:00:01",
                "timePeriod": 900,
                "eventType": "Online",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "start": [
                    ErrorDetail(
                        string="Start must be divisible by 15 minutes", code="invalid"
                    )
                ]
            },
        )

    def test_zero_time_period(self):
        """Time period cannot be 0."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "reading",
                "start": "2017-11-08 18:00:00",
                "timePeriod": 0,
                "eventType": "Online",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "timePeriod": [
                    ErrorDetail(
                        string="Duration cannot be less than 15 minutes", code="invalid"
                    )
                ]
            },
        )

    def test_invalid_event_type(self):
        """Does not accept an invalid event type."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "racing",
                "start": "2017-11-08 18:00:00",
                "timePeriod": 1800,
                "eventType": "Not Real Event Type",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "eventType": [
                    ErrorDetail(
                        string='"Not Real Event Type" is not a valid choice.',
                        code="invalid_choice",
                    )
                ]
            },
        )

    def test_invalid_event_type_public_holiday(self):
        """ "Public Holiday" is an invalid event type."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "horseback riding",
                "start": "2016-11-08 18:00:00",
                "timePeriod": 1800,
                "eventType": "Public Holiday",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "eventType": [
                    ErrorDetail(
                        string='"Public Holiday" is not a valid choice.',
                        code="invalid_choice",
                    )
                ]
            },
        )

    def test_valid_yearly_post(self):
        """Should return event data."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "bingo",
                "description": "bingo description",
                "start": "2020-11-10 10:30:00",
                "recurrences": "yearly",
                "timePeriod": 1800,
                "eventType": "On-Site",
                "link": "http://www.test.test",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data,
            {
                "id": response.data["id"],
                "centre": self.centres["centre_1"].id,
                "name": "bingo",
                "description": "bingo description",
                "start": "2020-11-10T10:30:00Z",
                "createdBy": self.users["user_1"].id,
                "users": [],
                "clients": [],
                "allDay": False,
                "timePeriod": "00:30:00",
                "eventType": "On-Site",
                "link": "http://www.test.test",
                "end": "2020-11-10T11:00:00Z",
            },
        )

    def test_valid_monthly_by_day_post(self):
        """Can create event that happens third tuesday of month."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "days": 2,
                "recurrences": "monthly",
                "recurrenceWeek": 3,
                "name": "singing",
                "start": "2019-04-11 18:00:00",
                "timePeriod": 20700,
                "eventType": CLOSE_EVENT,
                "allDay": True,
                "users": [self.users["user_1"].id],
                "clients": [self.clients["client_1"].id],
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data,
            {
                "id": response.data["id"],
                "centre": self.centres["centre_1"].id,
                "name": "singing",
                "description": "",
                "start": "2019-04-11T18:00:00+01:00",
                "createdBy": self.users["user_1"].id,
                "users": [self.users["user_1"].id],
                "clients": [self.clients["client_1"].id],
                "allDay": True,
                "timePeriod": "05:45:00",
                "eventType": CLOSE_EVENT,
                "link": "",
                "end": "2019-04-11T22:45:00Z",
            },
        )
        event = Event.objects.get(id=response.data["id"])
        self.assertEqual(
            event.recurrences,
            recurrence.Recurrence(
                rrules=[
                    recurrence.Rule(recurrence.MONTHLY, byday=recurrence.TH, bysetpos=3)
                ]
            ),
        )

    def test_valid_no_recurrence_post(self):
        """Should return event data."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "sleeping",
                "start": "2016-04-11 18:00:00",
                "timePeriod": 20700,
                "eventType": "Online",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data,
            {
                "id": response.data["id"],
                "centre": self.centres["centre_1"].id,
                "name": "sleeping",
                "description": "",
                "start": "2016-04-11T18:00:00+01:00",
                "createdBy": self.users["user_1"].id,
                "users": [],
                "clients": [],
                "allDay": False,
                "timePeriod": "05:45:00",
                "eventType": "Online",
                "link": "",
                "end": "2016-04-11T22:45:00Z",
            },
        )

    def test_invalid_date(self):
        """Should throw error when date is wrong."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "sleeping",
                "start": "2016-13-11 18:00:00",
                "timePeriod": 20700,
                "eventType": "Online",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "start": [
                    ErrorDetail(
                        string="Datetime has wrong format. "
                        + "Use one of these formats instead: "
                        + "YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z].",
                        code="invalid",
                    )
                ]
            },
        )

    def test_invalid_recurrence_value(self):
        """Should throw error when recurrence value is wrong."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "eating",
                "start": "2015-07-11 18:00:00",
                "timePeriod": 900,
                "eventType": "On-Site",
                "recurrences": "test",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "recurrences": [
                    ErrorDetail(
                        string='"test" is not a valid choice.', code="invalid_choice"
                    )
                ]
            },
        )

    def test_invalid_nurse_centre_create(self):
        """Should not be able to create events as nurse for wrong centre."""
        nurse_token = register_test_user(
            self.client, username="nurse_user", email="nurseemail@test.com"
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + nurse_token.data["key"])
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "close event 9",
                "start": "2010-11-10",
                "days": 14,
                "recurrences": "yearly",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "error": ErrorDetail(
                    string="You don't have permission to access this centre",
                    code="permission_denied",
                )
            },
        )

    def test_valid_nurse_create(self):
        """Can create for nurse if centre valid."""
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.users["user_2_token"].data["key"]
        )
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "swimming",
                "start": "2014-04-11 18:00:00",
                "timePeriod": 900,
                "eventType": "On-Site",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)

    def test_invalid_nurse_close_event(self):
        """Cannot create "Close Event" as nurse."""
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.users["user_2_token"].data["key"]
        )
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "swimming",
                "start": "2014-04-11 18:00:00",
                "timePeriod": 900,
                "eventType": CLOSE_EVENT,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You are not allowed to create close events.",
                    code="permission_denied",
                )
            },
        )

    def test_close_event_no_all_day(self):
        """Close events must be all day"""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "parading",
                "start": "2013-01-06 12:15:00",
                "timePeriod": 900,
                "eventType": CLOSE_EVENT,
                "allDay": False,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string='"allDay" must be True when "eventType" is CLOSE_EVENT',
                        code="invalid_choice",
                    )
                ]
            },
        )

    def test_close_event_no_daily(self):
        """Close events cannot repeat daily."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "wakeboarding",
                "start": "2010-01-06 12:15:00",
                "timePeriod": 900,
                "eventType": CLOSE_EVENT,
                "allDay": True,
                "recurrences": "daily",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string='"daily" is not a valid '
                        + 'value when "eventType" is CLOSE_EVENT',
                        code="invalid_choice",
                    )
                ]
            },
        )

    def test_users_not_in_centre(self):
        """Cannot create event if users not in centre."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_2"].id,
                "name": "knitting",
                "start": "2009-01-06 12:15:00",
                "timePeriod": 900,
                "eventType": "Online",
                "users": [self.users["user_1"].id, self.users["user_2"].id],
            },
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="One of the provided users is not"
                    + " subscribed to this centre. Centre: centre 2. User: user2.",
                    code="permission_denied",
                )
            },
        )

    def test_clients_not_in_centre(self):
        """Cannot create event if clients not in centre."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "golfing",
                "start": "2008-12-17 19:15:00",
                "timePeriod": 900,
                "eventType": "Online",
                "clients": [self.clients["client_1"].id, self.clients["client_2"].id],
            },
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="One of the provided clients is not subscribed to"
                    + " this centre. Centre: centre 1. Client: test 2.",
                    code="permission_denied",
                )
            },
        )

    def test_valid_daily(self):
        """Can create a daily recurrance shorter than 20 hours."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "spluenking",
                "start": "2009-08-10 10:30:00",
                "recurrences": "daily",
                "timePeriod": 72000,
                "eventType": "On-Site",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)

    def test_invalid_daily(self):
        """Cannot create a daily recurrance longer than 20 hours."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "skydiving",
                "start": "2008-02-10 10:30:00",
                "recurrences": "daily",
                "timePeriod": 72900,
                "eventType": "On-Site",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "timePeriod": [
                    ErrorDetail(
                        string="Daily events cannot be longer than 20 hours.",
                        code="invalid",
                    )
                ]
            },
        )

    def test_invalid_weekly(self):
        """Cannot create a weekly recurrance longer than 5 days."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "base jumping",
                "start": "2007-02-10 10:30:00",
                "recurrences": "weekly",
                "timePeriod": 432900,
                "eventType": "On-Site",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "timePeriod": [
                    ErrorDetail(
                        string="Weekly events cannot be longer than 5 days.",
                        code="invalid",
                    )
                ]
            },
        )

    def test_valid_monthly(self):
        """Can create a monthly recurrance shorter than 20 days."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "fencing",
                "start": "2001-11-10 10:30:00",
                "recurrences": "monthly",
                "timePeriod": 1728000,
                "eventType": "Online",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)

    def test_invalid_monthly(self):
        """Cannot create a monthly recurrance longer than 20 days."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "base jumping",
                "start": "2006-11-10 10:30:00",
                "recurrences": "monthly",
                "timePeriod": 1728900,
                "eventType": "Online",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "timePeriod": [
                    ErrorDetail(
                        string="Monthly events cannot be longer than 20 days.",
                        code="invalid",
                    )
                ]
            },
        )

    def test_invalid_yearly(self):
        """Cannot create a yearly recurrance longer than 90 days."""
        response = self.client.post(
            URL,
            {
                "centre": self.centres["centre_1"].id,
                "name": "gymnastics",
                "start": "2005-11-10 10:30:00",
                "recurrences": "yearly",
                "timePeriod": 7776900,
                "eventType": "Online",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "timePeriod": [
                    ErrorDetail(
                        string="Yearly events cannot be longer than 90 days.",
                        code="invalid",
                    )
                ]
            },
        )
