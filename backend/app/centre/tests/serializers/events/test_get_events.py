"""Get Events Tests."""

import datetime
from datetime import timedelta

import pytz
import recurrence
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_client_user, register_test_user
from .....client.tests.utils import create_client
from ....models import Event
from ...utils import create_centre

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
NO_AUTH_MESSAGE = "Authentication credentials were not provided."
URL = "/api/events/"
RECURRENCE_WEEKLY = recurrence.Rule(recurrence.WEEKLY)
RECURRENCE_YEARLY = recurrence.Rule(recurrence.YEARLY)
EVENT_1 = "event 1"
EVENT_2 = "event 2"
EVENT_3 = "event 3"
EVENT_5 = "monthly second friday"
DESCRIPTION_1 = "description 1"
CLOSE_EVENT = "User Close Event"
PUBLIC_HOLIDAY = "Public Holiday"


class GetEventsTestCase(TestCase):
    """Get Events Tests."""

    def create_events(self):
        """Create events for test."""
        event1 = Event.objects.create(
            name=EVENT_1,
            centre=self.centres["centre_1"],
            description=DESCRIPTION_1,
            start=datetime.datetime(2020, 1, 3, 16, 30, 00, tzinfo=pytz.UTC),
            timePeriod=timedelta(days=4),
            recurrences=recurrence.Recurrence(
                rrules=[RECURRENCE_WEEKLY],
            ),
            createdBy=self.users["admin_user"],
            eventType=CLOSE_EVENT,
        )
        event1.users.set([self.users["admin_user"], self.users["nurse_user"]])
        event2 = Event.objects.create(
            name=EVENT_2,
            centre=self.centres["centre_1"],
            start=datetime.datetime(2019, 1, 1, tzinfo=pytz.UTC),
            timePeriod=timedelta(days=19),
            recurrences=recurrence.Recurrence(
                rrules=[RECURRENCE_YEARLY],
            ),
            createdBy=self.users["admin_user"],
            eventType="Online",
        )
        event2.users.set([self.users["admin_user"]])
        event2.clients.set([self.clients["client_2"]])
        event3 = Event.objects.create(
            name=EVENT_3,
            centre=self.centres["centre_1"],
            start=datetime.datetime(2020, 1, 6, tzinfo=pytz.UTC),
            timePeriod=timedelta(days=56),
            createdBy=self.users["admin_user"],
            eventType="On-Site",
        )
        event3.users.set([self.users["nurse_user"]])
        event3.clients.set([self.clients["client_1"]])
        event4 = Event.objects.create(
            name="event 4",
            centre=self.centres["centre_2"],
            start=datetime.datetime(2020, 1, 6, tzinfo=pytz.UTC),
            timePeriod=timedelta(days=56),
            createdBy=self.users["admin_user"],
            eventType="Online",
        )
        event5 = Event.objects.create(
            name=EVENT_5,
            centre=self.centres["centre_1"],
            start=datetime.datetime(2023, 1, 6, tzinfo=pytz.UTC),
            timePeriod=timedelta(days=3),
            recurrences=recurrence.Recurrence(
                rrules=[
                    recurrence.Rule(
                        freq=recurrence.MONTHLY, bysetpos=2, byday=recurrence.FRIDAY
                    )
                ]
            ),
            createdBy=self.users["admin_user"],
        )
        event5.clients.set([self.clients["client_1"], self.clients["client_2"]])
        return [event1, event2, event3, event4, event5]

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        self.admin_token = register_test_user(self.client)
        nurse_token = register_test_user(
            self.client, username="nurse", email="nurseemail@test.test"
        )

        self.users = {
            "admin_user": CustomUser.objects.get(
                id=self.admin_token.data["user"]["id"]
            ),
            "nurse_user": CustomUser.objects.get(id=nurse_token.data["user"]["id"]),
        }
        self.centres = {
            "centre_1": create_centre(
                users=[self.users["nurse_user"]], country="US", state="TX"
            ),
            "centre_2": create_centre(name="centre2", users=[self.users["admin_user"]]),
        }
        self.clients = {
            "client_1": create_client(surname="1", centres=[self.centres["centre_1"]]),
            "client_2": create_client(surname="2", centres=[self.centres["centre_2"]]),
        }
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + nurse_token.data["key"])
        self.events = self.create_events()

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(
            URL,
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_AUTH_MESSAGE,
                    code="not_authenticated",
                )
            },
        )

    def test_incorrect_date(self):
        """Should return error when date is incorrect."""
        response = self.client.get(
            f"{URL}2020-15-13/2020-02-04/?centre={self.centres['centre_1'].id}"
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "error": ErrorDetail(
                    string="Start and end dates must be in format YYYY-MM-DD",
                    code="invalid",
                )
            },
        )

    def test_get(self):
        """Should get occurrences between two dates."""
        response = self.client.get(
            f"{URL}2020-01-13/2020-02-04/?centre={self.centres['centre_1'].id}"
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                {
                    "id": self.events[0].id,
                    "centre": self.centres["centre_1"].id,
                    "name": EVENT_1,
                    "description": DESCRIPTION_1,
                    "start": datetime.datetime(2020, 1, 10, 0, 0),
                    "createdBy": self.users["admin_user"].id,
                    "users": [self.users["admin_user"].id, self.users["nurse_user"].id],
                    "clients": [],
                    "allDay": False,
                    "timePeriod": 345600,
                    "eventType": CLOSE_EVENT,
                    "link": "",
                    "end": datetime.datetime(2020, 1, 14, 0, 0),
                },
                {
                    "id": self.events[0].id,
                    "centre": self.centres["centre_1"].id,
                    "name": EVENT_1,
                    "description": DESCRIPTION_1,
                    "start": datetime.datetime(2020, 1, 17, 0, 0),
                    "createdBy": self.users["admin_user"].id,
                    "users": [self.users["admin_user"].id, self.users["nurse_user"].id],
                    "clients": [],
                    "allDay": False,
                    "timePeriod": 345600,
                    "eventType": CLOSE_EVENT,
                    "link": "",
                    "end": datetime.datetime(2020, 1, 21, 0, 0),
                },
                {
                    "id": self.events[0].id,
                    "centre": self.centres["centre_1"].id,
                    "name": EVENT_1,
                    "description": DESCRIPTION_1,
                    "start": datetime.datetime(2020, 1, 24, 0, 0),
                    "createdBy": self.users["admin_user"].id,
                    "users": [self.users["admin_user"].id, self.users["nurse_user"].id],
                    "clients": [],
                    "allDay": False,
                    "timePeriod": 345600,
                    "eventType": CLOSE_EVENT,
                    "link": "",
                    "end": datetime.datetime(2020, 1, 28, 0, 0),
                },
                {
                    "id": self.events[0].id,
                    "centre": self.centres["centre_1"].id,
                    "name": EVENT_1,
                    "description": DESCRIPTION_1,
                    "start": datetime.datetime(2020, 1, 31, 0, 0),
                    "createdBy": self.users["admin_user"].id,
                    "users": [self.users["admin_user"].id, self.users["nurse_user"].id],
                    "clients": [],
                    "allDay": False,
                    "timePeriod": 345600,
                    "eventType": CLOSE_EVENT,
                    "link": "",
                    "end": datetime.datetime(2020, 2, 4, 0, 0),
                },
                {
                    "id": self.events[1].id,
                    "centre": self.centres["centre_1"].id,
                    "name": EVENT_2,
                    "description": "",
                    "start": datetime.datetime(2020, 1, 1, 0, 0),
                    "createdBy": self.users["admin_user"].id,
                    "users": [self.users["admin_user"].id],
                    "clients": [self.clients["client_2"].id],
                    "allDay": False,
                    "timePeriod": 1641600,
                    "eventType": "Online",
                    "link": "",
                    "end": datetime.datetime(2020, 1, 20, 0, 0),
                },
                {
                    "id": self.events[2].id,
                    "centre": self.centres["centre_1"].id,
                    "name": EVENT_3,
                    "description": "",
                    "start": datetime.datetime(2020, 1, 6, 0, 0),
                    "createdBy": self.users["admin_user"].id,
                    "users": [self.users["nurse_user"].id],
                    "clients": [self.clients["client_1"].id],
                    "allDay": False,
                    "timePeriod": 4838400,
                    "eventType": "On-Site",
                    "link": "",
                    "end": datetime.datetime(2020, 3, 2, 0, 0),
                },
                {
                    "name": "Confederate Memorial Day",
                    "centre": self.centres["centre_1"].id,
                    "start": datetime.datetime(2020, 1, 19),
                    "end": datetime.datetime(2020, 1, 20),
                    "eventType": PUBLIC_HOLIDAY,
                    "timePeriod": "1 00:00:00",
                },
                {
                    "name": "Martin Luther King Jr. Day",
                    "centre": self.centres["centre_1"].id,
                    "start": datetime.datetime(2020, 1, 20),
                    "end": datetime.datetime(2020, 1, 21),
                    "eventType": PUBLIC_HOLIDAY,
                    "timePeriod": "1 00:00:00",
                },
            ],
        )

    def test_multi_year_get(self):
        """Should get occurrences between two dates in different years."""
        response = self.client.get(
            f"{URL}2019-12-25/2020-01-04/?centre={self.centres['centre_1'].id}"
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 5)
        self.assertEqual(
            response.data[0],
            {
                "id": self.events[0].id,
                "centre": self.centres["centre_1"].id,
                "name": EVENT_1,
                "description": DESCRIPTION_1,
                "start": datetime.datetime(2020, 1, 3, 0, 0),
                "createdBy": self.users["admin_user"].id,
                "users": [self.users["admin_user"].id, self.users["nurse_user"].id],
                "clients": [],
                "allDay": False,
                "timePeriod": 345600,
                "eventType": CLOSE_EVENT,
                "link": "",
                "end": datetime.datetime(2020, 1, 7, 0, 0),
            },
        )
        self.assertEqual(
            response.data[4],
            {
                "name": "New Year's Day",
                "centre": self.centres["centre_1"].id,
                "start": datetime.datetime(2020, 1, 1, 0, 0),
                "end": datetime.datetime(2020, 1, 2, 0, 0),
                "eventType": PUBLIC_HOLIDAY,
                "timePeriod": "1 00:00:00",
            },
        )

    def test_get_user(self):
        """Should get all events for a user."""
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.admin_token.data["key"]
        )
        response = self.client.get(f"{URL}2020-01-13/2020-02-04/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                {
                    "id": self.events[3].id,
                    "centre": self.centres["centre_2"].id,
                    "name": "event 4",
                    "description": "",
                    "start": datetime.datetime(2020, 1, 6, 0, 0),
                    "createdBy": self.users["admin_user"].id,
                    "users": [],
                    "clients": [],
                    "allDay": False,
                    "timePeriod": 4838400,
                    "eventType": "Online",
                    "link": "",
                    "end": datetime.datetime(2020, 3, 2, 0, 0),
                }
            ],
        )

    def test_get_nurse(self):
        """Should get occurrences between two dates as a nurse."""
        response = self.client.get(
            f"{URL}2020-01-13/2020-02-04/?centre={self.centres['centre_1'].id}"
        )
        self.assertEqual(response.status_code, 200)

    def test_get_second_thursday_of_month(self):
        """Should get occurrences on second friday of month."""
        response = self.client.get(
            f"{URL}2023-01-13/2023-04-04/?centre={self.centres['centre_1'].id}"
        )
        self.assertEqual(response.status_code, 200)
        event_5_events = [event for event in response.data if event["name"] == EVENT_5]
        self.assertEqual(
            event_5_events,
            [
                {
                    "id": self.events[4].id,
                    "centre": self.centres["centre_1"].id,
                    "name": EVENT_5,
                    "description": "",
                    "start": datetime.datetime(2023, 1, 13, 0, 0),
                    "createdBy": self.users["admin_user"].id,
                    "users": [],
                    "clients": [
                        self.clients["client_1"].id,
                        self.clients["client_2"].id,
                    ],
                    "allDay": False,
                    "timePeriod": 259200,
                    "eventType": "",
                    "link": "",
                    "end": datetime.datetime(2023, 1, 16, 0, 0),
                },
                {
                    "id": self.events[4].id,
                    "centre": self.centres["centre_1"].id,
                    "name": EVENT_5,
                    "description": "",
                    "start": datetime.datetime(2023, 2, 10, 0, 0),
                    "createdBy": self.users["admin_user"].id,
                    "users": [],
                    "clients": [
                        self.clients["client_1"].id,
                        self.clients["client_2"].id,
                    ],
                    "allDay": False,
                    "timePeriod": 259200,
                    "eventType": "",
                    "link": "",
                    "end": datetime.datetime(2023, 2, 13, 0, 0),
                },
                {
                    "id": self.events[4].id,
                    "centre": self.centres["centre_1"].id,
                    "name": EVENT_5,
                    "description": "",
                    "start": datetime.datetime(2023, 3, 10, 0, 0),
                    "createdBy": self.users["admin_user"].id,
                    "users": [],
                    "clients": [
                        self.clients["client_1"].id,
                        self.clients["client_2"].id,
                    ],
                    "allDay": False,
                    "timePeriod": 259200,
                    "eventType": "",
                    "link": "",
                    "end": datetime.datetime(2023, 3, 13, 0, 0),
                },
            ],
        )

    def test_invalid_centre(self):
        """Should fail to get centre info if not subbed to centre"""
        response = self.client.get(
            f"{URL}2020-01-13/2020-02-04/?centre={self.centres['centre_2'].id}"
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "error": ErrorDetail(
                    string="You don't have permission to access this centre",
                    code="permission_denied",
                )
            },
        )

    def test_get_event_type(self):
        """Should get events of specific type."""
        response = self.client.get(f"{URL}2023-01-13/2023-04-04/?eventType=Online")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                {
                    "id": self.events[1].id,
                    "centre": self.centres["centre_1"].id,
                    "name": EVENT_2,
                    "description": "",
                    "start": datetime.datetime(2023, 1, 1, 0, 0),
                    "createdBy": self.users["admin_user"].id,
                    "users": [self.users["admin_user"].id],
                    "clients": [self.clients["client_2"].id],
                    "allDay": False,
                    "timePeriod": 1641600,
                    "eventType": "Online",
                    "link": "",
                    "end": datetime.datetime(2023, 1, 20, 0, 0),
                }
            ],
        )

    def test_get_client_user(self):
        """Should get occurrences between two dates as a client user."""
        client_token = register_client_user(self.client)
        client_user = CustomUser.objects.get(id=client_token.data["user"]["id"])
        client_user.client.centres.add(self.centres["centre_1"])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + client_token.data["key"])
        response = self.client.get(f"{URL}2020-01-13/2020-02-04/")
        self.assertEqual(response.status_code, 200)
