"""URLs routes that are part of the main django app."""

from django.urls import path

from . import views

urlpatterns = [  # pylint: disable=C0103
    path("clients/", views.ClientView.as_view(), name="clients"),
    path("nextOfKin/", views.NextOfKinView.as_view(), name="next-of-kin"),
    path(
        "nextOfKin/<int:pk>/update/",
        views.NextOfKinUpdate.as_view(),
        name="next-of-kin-update",
    ),
    path(
        "clients/<int:pk>/update/", views.ClientUpdate.as_view(), name="client-update"
    ),
    path(
        "clients-storage-usage/",
        views.ClientStorageUsageView.as_view(),
        name="clients-storage-usage",
    ),
    path(
        "clients/centres/update/",
        views.ClientCentresUpdate.as_view(),
        name="client-centres-update",
    ),
    path(
        "clients/<int:pk>/", views.ClientRetrieveView.as_view(), name="client-retrieve"
    ),
    path("clientNeeds/", views.ClientNeedView.as_view(), name="clientneed-create"),
    path(
        "clientNeeds/<int:pk>/update/",
        views.ClientNeedUpdateView.as_view(),
        name="clientneed-update",
    ),
    path(
        "clients/<int:pk>/delete/", views.ClientDelete.as_view(), name="client-delete"
    ),
    path(
        "clients/<int:pk>/needs/",
        views.NeedView.as_view(),
        name="centre-needs-for-client",
    ),
    path(
        "clients/<int:pk>/policies/",
        views.PolicyView.as_view(),
        name="centre-policies-for-client",
    ),
    path(
        "clients/<int:pk>/policies/agree/",
        views.PolicyAgreeView.as_view(),
        name="centre-policies-for-client-agree",
    ),
    path(
        "clients/<int:client>/clientNeeds/",
        views.ClientNeedView.as_view(),
        name="client-needs-for-client",
    ),
    path(
        "nextOfKin/<int:pk>/delete/",
        views.NextOfKinDelete.as_view(),
        name="next-of-kin-delete",
    ),
    path("medications/", views.MedicationView.as_view(), name="medications"),
    path(
        "feedingRisks/<int:pk>/", views.FeedingRiskView.as_view(), name="feeding-risks"
    ),
    path(
        "medications/<int:pk>/update/",
        views.MedicationUpdate.as_view(),
        name="medication-update",
    ),
    path(
        "medications/<int:pk>/delete/",
        views.MedicationDelete.as_view(),
        name="medication-delete",
    ),
    path(
        "clientDocuments/upload/",
        views.ClientDocumentUpload.as_view(),
        name="client-documents-upload",
    ),
    path(
        "clientDocuments/<int:pk>/download/",
        views.ClientDocumentDownload.as_view(),
        name="client-documents-download",
    ),
    path(
        "clientDocuments/<int:pk>/delete/",
        views.ClientDocumentDelete.as_view(),
        name="client-documents-delete",
    ),
    path(
        "clientDocuments/<int:pk>/update/",
        views.ClientDocumentUpdate.as_view(),
        name="client-documents-update",
    ),
    path(
        "carePlans/",
        views.CarePlanList.as_view(),
        name="care-plan-list",
    ),
    path(
        "attendanceRecord/",
        views.AttendanceRecordCreate.as_view(),
        name="attendance-record-create",
    ),
    path(
        "attendanceRecord/<int:pk>/",
        views.AttendanceRecordList.as_view(),
        name="attendance-record-list",
    ),
    path(
        "carePlansDetail/<int:client>/<str:date>/",
        views.CarePlanDetail.as_view(),
        name="care-plan-retreive",
    ),
    path(
        "carePlansHistory/<int:pk>/",
        views.CarePlanHistory.as_view(),
        name="care-plan-history",
    ),
    path(
        "carePlans/<int:pk>/<str:date>/input/",
        views.CarePlanInput.as_view(),
        name="care-plan-input",
    ),
    path(
        "carePlans/<int:pk>/<str:date>/complete/",
        views.CarePlanComplete.as_view(),
        name="care-plan-complete",
    ),
    path(
        "carePlans/<int:pk>/<str:date>/incomplete/",
        views.CarePlanIncomplete.as_view(),
        name="care-plan-incomplete",
    ),
    path(
        "carePlans/<int:pk>/update/",
        views.CarePlanUpdate.as_view(),
        name="care-plan-update",
    ),
    path(
        "set-client-user-activation/<int:pk>/",
        views.SetClientUserActivation.as_view(),
        name="client-retrieve",
    ),
    path(
        "clients/update-permissions/<int:pk>/",
        views.ClientPermissionsUpdate.as_view(),
        name="client-update-permissions",
    ),
]
