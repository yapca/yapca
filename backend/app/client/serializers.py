"""Serializers allow querysets and model instances to be converted to native Python."""

from rest_framework import serializers

from ..authentication.models import UserPermission
from ..centre.serializers import NeedSerializer
from . import models
from .signals import save_client_visiting_days, update_client_websocket
from .validators import (
    validate_file_size,
    validate_need_in_centres,
    validate_space_left_for_file,
    validate_visiting_day_in_centres,
)

CLIENT_NEED = "need.client"
CLIENT_BASIC_INFO_FIELDS = (
    "id",
    "firstName",
    "surname",
    "centres",
    "isActive",
    "visitingDays",
    "accountStatus",
)
CLIENT_LIST_FIELDS = CLIENT_BASIC_INFO_FIELDS + (
    "id",
    "firstName",
    "surname",
    "gender",
    "admittedBy",
    "createdDate",
    "centres",
    "isActive",
    "visitingDays",
    "basicInfoSetup",
    "additionalInfoSetup",
    "servicesSetup",
    "abilitySetup",
    "visitingDaysSetup",
    "setupComplete",
    "dateOfBirth",
    "address",
    "phoneNumber",
    "publicHealthNurseName",
    "publicHealthNursePhoneNumber",
    "homeHelpName",
    "homeHelpPhoneNumber",
    "gpName",
    "gpPhoneNumber",
    "chemistName",
    "chemistPhoneNumber",
    "mobility",
    "toileting",
    "hygiene",
    "sight",
    "hearing",
    "usesDentures",
    "requiresAssistanceEating",
    "thicknerGrade",
    "dietSetup",
    "storageSize",
    "reasonForInactivity",
    "accountStatus",
)
CLIENT_FIELDS = CLIENT_LIST_FIELDS + (
    "agreedPolicies",
    "nextOfKin",
    "medicalHistory",
    "surgicalHistory",
    "medications",
    "feedingRisks",
    "caseFormulation",
    "allergies",
    "intolerances",
    "needs",
    "documents",
    "permissions",
)


class MedicationSerializer(serializers.ModelSerializer):
    """View the Medication model as a serializer."""

    class Meta:
        model = models.Medication
        fields = ("id", "name", "dosage", "frequency", "inactiveDate", "client")


class ClientDocumentSerializer(serializers.ModelSerializer):
    """Serialize client documents."""

    class Meta:
        model = models.ClientDocument
        fields = (
            "id",
            "document",
            "client",
            "name",
            "createdDate",
            "originalName",
            "size",
        )
        read_only_fields = ("size", "createdDate")

    def validate(self, attrs):  # pylint: disable=W0221
        """Object level serializer validation."""
        validate_file_size(attrs["document"])
        validate_space_left_for_file(attrs["document"])
        return attrs

    def create(self, validated_data):
        """Override create method to add document size."""
        validated_data["size"] = validated_data["document"].size
        return models.ClientDocument.objects.create(**validated_data)


class ClientDocumentUpdateSerializer(serializers.ModelSerializer):
    """Serialize client documents."""

    class Meta:
        model = models.ClientDocument
        fields = ("name",)


class ClientStorageUsageSerializer(serializers.ModelSerializer):
    """Serialize client with total document size."""

    class Meta:
        model = models.Client
        fields = ("storageSize", "id", "firstName", "surname", "setupComplete")


class ClientCentresSerializer(serializers.Serializer):  # pylint: disable=W0223
    """Serialize client centres."""

    centre = serializers.IntegerField()
    client = serializers.IntegerField()


class FeedingRiskSerializer(serializers.ModelSerializer):
    """View the FeedingRisk model as a serializer."""

    class Meta:
        model = models.FeedingRisk
        fields = ("id", "feedingRisk", "client")


class ClientNextOfKinSerializer(serializers.ModelSerializer):
    """Serializer for next of kin."""

    class Meta:
        model = models.NextOfKin
        fields = ("id", "name", "phoneNumber", "client")


class ClientAgreedPoliciesSerializer(serializers.ModelSerializer):
    """Serializer for agreed policies junction object."""

    class Meta:
        model = models.ClientAgreedPolicies
        fields = ("centre", "policy")


class ClientNeedSerializer(serializers.ModelSerializer):
    """Serializer for needs junction object."""

    class Meta:
        model = models.ClientNeed
        fields = ("id", "centre", "need", "plan", "isActive", "client")
        read_only_fields = ("id",)

    def validate(self, attrs):
        """Object level serializer validation."""
        validate_need_in_centres(attrs["need"], attrs["centre"])
        return attrs


class ClientNeedUpdateSerializer(serializers.ModelSerializer):
    """Serializer for updating client needs."""

    class Meta:
        model = models.ClientNeed
        fields = ("id", "centre", "need", "plan", "isActive", "client")
        read_only_fields = ("id", "centre", "need", "client")


class ClientVisitingDaySerializer(serializers.ModelSerializer):
    """Serializer for visiting days junction object."""

    class Meta:
        model = models.ClientVisitingDays
        fields = ("centre", "day")


class CarePlanSerializer(serializers.ModelSerializer):
    """List incomplete care plans."""

    client = serializers.PrimaryKeyRelatedField(source=CLIENT_NEED, read_only=True)
    clientNeed = ClientNeedSerializer(source="need", read_only=True)

    class Meta:
        model = models.CarePlan
        fields = ("id", "date", "need", "client", "inputted", "clientNeed")


class AttendanceRecordSerializer(serializers.ModelSerializer):
    """List client attendance."""

    class Meta:
        model = models.AttendanceRecord
        fields = (
            "id",
            "client",
            "centre",
            "date",
            "active",
            "reasonForInactivity",
            "reasonForAbsence",
            "addedBy",
        )
        read_only_fields = ("id", "active", "reasonForInactivity")
        extra_kwargs = {"addedBy": {"default": serializers.CurrentUserDefault()}}


class CarePlanHistorySerializer(serializers.ModelSerializer):
    """Retreive historical care plan info."""

    needObject = NeedSerializer(source="need.need", read_only=True)
    client = serializers.PrimaryKeyRelatedField(source=CLIENT_NEED, read_only=True)
    clientNeed = ClientNeedSerializer(source="need", read_only=True)

    class Meta:
        model = models.CarePlan
        fields = (
            "id",
            "date",
            "need",
            "client",
            "inputted",
            "comment",
            "completed",
            "needObject",
            "clientNeed",
            "addedBy",
        )
        read_only_fields = (
            "id",
            "date",
            "need",
            "client",
            "needObject",
            "clientNeed",
            "addedBy",
        )


class CarePlanDetailSerializer(serializers.ModelSerializer):
    """Retreive extra care plan info."""

    needObject = NeedSerializer(source="need.need", read_only=True)
    client = serializers.PrimaryKeyRelatedField(source=CLIENT_NEED, read_only=True)

    class Meta:
        model = models.CarePlan
        fields = (
            "id",
            "date",
            "need",
            "client",
            "inputted",
            "comment",
            "completed",
            "needObject",
            "addedBy",
        )
        read_only_fields = ("id", "date", "need", "client", "needObject", "addedBy")


class ClientRetreiveSerializer(serializers.ModelSerializer):
    """Full model for retrieves."""

    agreedPolicies = ClientAgreedPoliciesSerializer(many=True, required=False)
    visitingDays = ClientVisitingDaySerializer(many=True)
    nextOfKin = ClientNextOfKinSerializer(many=True)
    medications = MedicationSerializer(many=True)
    feedingRisks = FeedingRiskSerializer(many=True)
    documents = ClientDocumentSerializer(many=True, required=False)
    permissions = serializers.SlugRelatedField(
        read_only=True,
        allow_null=True,
        source="user.permissions",
        slug_field="codename",
        many=True,
    )

    class Meta:
        model = models.Client
        fields = CLIENT_FIELDS
        read_only_fields = (
            "id",
            "createdDate",
            "storageSize",
            "accountStatus",
            "permissions",
        )


class ClientListSerializer(serializers.ModelSerializer):
    """A minimal serializer for client lists."""

    visitingDays = ClientVisitingDaySerializer(many=True)

    class Meta:
        model = models.Client
        fields = CLIENT_LIST_FIELDS
        read_only_fields = ("id", "createdDate", "accountStatus")


class ClientBasicInfoSerializer(serializers.ModelSerializer):
    """Most minimal client serializer. Just the name and some centre info."""

    visitingDays = ClientVisitingDaySerializer(many=True)

    class Meta:
        model = models.Client
        fields = CLIENT_BASIC_INFO_FIELDS
        read_only_fields = ("id", "accountStatus")


def update_visiting_days(validated_data, instance):
    """Update visiting days as part of update."""
    visiting_days = validated_data.pop("visitingDays")
    centres = list({visiting_day["centre"] for visiting_day in visiting_days})
    models.ClientVisitingDays.objects.filter(
        client=instance.id, centre__in=centres
    ).delete()
    visiting_day_objects = []
    for visiting_day in visiting_days:
        visiting_day_objects.append(
            models.ClientVisitingDays(
                client=instance,
                day=visiting_day["day"],
                centre=visiting_day["centre"],
            )
        )
    models.ClientVisitingDays.objects.bulk_create(visiting_day_objects)
    save_client_visiting_days(visiting_day_objects, instance.id)
    return validated_data


class ClientSerializer(serializers.ModelSerializer):
    """POST Client model as a serializer."""

    needs = serializers.PrimaryKeyRelatedField(
        source="client.needs", many=True, read_only=True
    )
    visitingDays = ClientVisitingDaySerializer(many=True, required=False)
    nextOfKin = serializers.PrimaryKeyRelatedField(
        many=True, queryset=models.NextOfKin.objects.all(), required=False
    )
    medications = serializers.PrimaryKeyRelatedField(
        many=True, queryset=models.Medication.objects.all(), required=False
    )
    feedingRisks = serializers.PrimaryKeyRelatedField(
        many=True, queryset=models.FeedingRisk.objects.all(), required=False
    )
    documents = serializers.PrimaryKeyRelatedField(
        many=True, queryset=models.ClientDocument.objects.all(), required=False
    )

    class Meta:
        model = models.Client
        fields = list(set(CLIENT_FIELDS) - {"permissions"})
        read_only_fields = ("id", "createdDate", "storageSize", "agreedPolicies")
        extra_kwargs = {"admittedBy": {"default": serializers.CurrentUserDefault()}}

    def to_representation(self, instance):
        """Return a nested view as a response to a POST request."""
        serializer = ClientListSerializer(instance)
        return serializer.data

    def create(self, validated_data):
        """Override create method to handle custom m2m fields."""
        visiting_days = []
        if "visitingDays" in validated_data:
            visiting_days = validated_data.pop("visitingDays")
        centres = validated_data.pop("centres")
        client = models.Client.objects.create(**validated_data)
        for visiting_day in visiting_days:
            models.ClientVisitingDays.objects.create(
                client=client, day=visiting_day["day"], centre=visiting_day["centre"]
            )
        client.centres.add(*centres)
        update_client_websocket(client, m2m_field="centres", m2m_id=True)
        return client

    def update(self, instance, validated_data):
        """Override create method to handle custom m2m fields."""
        call_normal_websocket = True
        if "visitingDays" in validated_data:
            call_normal_websocket = False
            validated_data = update_visiting_days(validated_data, instance)
        instance = super().update(instance, validated_data)
        if call_normal_websocket:
            update_client_websocket(instance, filter_keys=list(validated_data.keys()))
        return instance

    def validate(self, attrs):  # pylint: disable=W0221
        """Object level serializer validation."""
        if "centres" in attrs:
            centres = attrs["centres"]
        else:
            centres = self.instance.centres.all()
        if "visitingDays" in attrs:
            for day in attrs["visitingDays"]:
                validate_visiting_day_in_centres(day, centres)
        return attrs


class ClientUpdateSerializer(ClientSerializer):
    """PUT/PATCH Client model as a serializer."""

    class Meta(ClientSerializer.Meta):
        model = models.Client
        fields = list(set(CLIENT_FIELDS) - {"permissions"})
        read_only_fields = (
            "id",
            "createdDate",
            "storageSize",
            "agreedPolicies",
            "admittedBy",
            "centres",
        )


class SetClientUserActivationSerializer(
    serializers.Serializer
):  # pylint: disable=W0223
    """Set client user activation state."""

    active = serializers.BooleanField(required=True)
    sendEmail = serializers.BooleanField(required=False, default=False)


class ClientPermissionsSerializer(serializers.ModelSerializer):
    """Update client permissions."""

    permissions = serializers.SlugRelatedField(
        read_only=False,
        allow_null=True,
        source="user.permissions",
        slug_field="codename",
        many=True,
        queryset=UserPermission.objects.all(),
    )

    class Meta:
        model = models.Client
        fields = ("permissions",)
