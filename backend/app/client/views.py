"""A view is a function that takes a Web request and returns a Web response."""

from django.conf import settings
from django.db.models import F
from django.http import FileResponse
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.exceptions import PermissionDenied
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from ..authentication.models import UserPermission
from ..authentication.permissions import (
    IsAdmin,
    IsClient,
    IsNurse,
    user_is_admin,
    user_is_nurse,
)
from ..authentication.utils import EmailThread, get_email_templates
from ..authentication.views import (
    get_backend_address,
    get_frontend_address,
    get_latest_app_versions,
)
from ..centre.models import Centre
from ..centre.serializers import NeedSerializer, PolicySerializer
from ..websockets.utils import send_to_permitted_users
from .models import (
    AttendanceRecord,
    CarePlan,
    Client,
    ClientAgreedPolicies,
    ClientDocument,
    ClientNeed,
    FeedingRisk,
    Medication,
    NextOfKin,
)
from .permissions import (
    CanCreateAttendanceRecords,
    CanEditCarePlan,
    CanEditClient,
    CanEditClientDocuments,
    CanEditClientFeedingRisk,
    CanEditClientMedication,
    CanEditClientNeed,
    CanEditClientNextOfKin,
    CanViewClient,
    can_change_client_centres,
    client_can_view_client,
)
from .serializers import (
    AttendanceRecordSerializer,
    CarePlanDetailSerializer,
    CarePlanHistorySerializer,
    CarePlanSerializer,
    ClientBasicInfoSerializer,
    ClientCentresSerializer,
    ClientDocumentSerializer,
    ClientDocumentUpdateSerializer,
    ClientListSerializer,
    ClientNeedSerializer,
    ClientNeedUpdateSerializer,
    ClientNextOfKinSerializer,
    ClientPermissionsSerializer,
    ClientRetreiveSerializer,
    ClientSerializer,
    ClientStorageUsageSerializer,
    ClientUpdateSerializer,
    FeedingRiskSerializer,
    MedicationSerializer,
    SetClientUserActivationSerializer,
)
from .signals import (
    create_attendance_records_websocket,
    save_client_centres,
    save_feeding_risks,
    update_care_plans_websocket,
    update_client_permissions,
    update_client_websocket,
)


class ClientNeedView(generics.ListCreateAPIView):
    """Get/Create ClientNeed objects."""

    queryset = ClientNeed.objects.all()
    permission_classes = [CanEditClientNeed]
    serializer_class = ClientNeedSerializer

    def get_queryset(self):
        """Filter needs by client."""
        client_id = self.kwargs["client"]
        return Client.objects.get(pk=client_id).needs.all()


class ClientNeedUpdateView(generics.UpdateAPIView):
    """Update ClientNeed objects."""

    queryset = ClientNeed.objects.all()
    permission_classes = [CanEditClientNeed]
    serializer_class = ClientNeedUpdateSerializer


class ClientView(generics.ListCreateAPIView):
    """Get all Client objects from database and return for serializer."""

    queryset = Client.objects.all()
    permission_classes = [IsClient]

    def get_serializer_class(self):
        """Use a simplified list serializer for getting clients."""
        if self.request.method == "POST":
            return ClientSerializer
        user = self.request.user
        if (
            user_is_nurse(user)
            or user_is_admin(user)
            or client_can_view_client(self.request.user)
        ):
            return ClientListSerializer
        return ClientBasicInfoSerializer

    def get_queryset(self):
        """Filter clients by subscribed centres."""
        user = self.request.user
        if user.client:
            return Client.objects.filter(id=user.client.id).distinct("id")
        user_centres = user.centres.all()
        return Client.objects.filter(centres__in=user_centres).distinct("id")


class ClientStorageUsageView(generics.ListCreateAPIView):
    """
    Get Client objects who have documents and their total size.

    Only include clients who are not part of the users subbed
    centres.
    """

    permission_classes = [IsAdmin]
    serializer_class = ClientStorageUsageSerializer

    def get_queryset(self):
        """Filter clients by non subbed centres."""
        user = self.request.user
        user_centres = user.centres.all()
        return (
            Client.objects.filter(storageSize__gt=0)
            .exclude(centres__in=user_centres)
            .distinct("id")
        )


class ClientRetrieveView(generics.RetrieveAPIView):
    """Get a single Client object from database and return for serializer."""

    queryset = Client.objects.all()
    permission_classes = [CanViewClient]
    serializer_class = ClientRetreiveSerializer


class ClientUpdate(generics.UpdateAPIView):
    """Update clients."""

    queryset = Client.objects.all()
    serializer_class = ClientUpdateSerializer
    permission_classes = [CanEditClient]


def client_centre_change_base(request):
    """Return objects or 404."""
    serializer = ClientCentresSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    if not can_change_client_centres(request):
        raise PermissionDenied(
            detail="You do not have permission to perform this action.",
            code="permission_denied",
        )
    centre_id = request.data.get("centre", None)
    client_id = request.data.get("client", None)
    centre = get_object_or_404(Centre, pk=centre_id)
    client = get_object_or_404(Client, pk=client_id)
    return (client, centre)


class ClientCentresUpdate(APIView):
    """Update client centres."""

    serializer_class = ClientCentresSerializer
    permission_classes = [IsNurse]

    def post(self, request):
        """Add a clients centre."""
        client, centre = client_centre_change_base(request)
        if centre not in client.centres.all():
            original_centre_ids = list(
                client.centres.all().values_list("pk", flat=True)
            )
            client.centres.add(centre)
            new_centre_ids = list(
                client.centres.all().order_by("id").values_list("pk", flat=True)
            )
            save_client_centres(original_centre_ids, new_centre_ids, client.id)
            return Response(
                {"client": client.id, "centres": new_centre_ids},
                status=status.HTTP_200_OK,
            )
        return Response(
            "Failed to add centre, client is already subscribed to this centre.",
            status=status.HTTP_400_BAD_REQUEST,
        )

    def delete(self, request):
        """Remove a clients centre."""
        client, centre = client_centre_change_base(request)
        if centre in client.centres.all():
            original_centre_ids = list(
                client.centres.all().values_list("pk", flat=True)
            )
            client.centres.remove(centre)
            new_centre_ids = list(client.centres.all().values_list("pk", flat=True))
            save_client_centres(original_centre_ids, new_centre_ids, client.id)
            return Response(
                {"client": client.id, "centres": new_centre_ids},
                status=status.HTTP_200_OK,
            )
        return Response(
            "Failed to remove centre, client is not subscribed to this centre.",
            status=status.HTTP_400_BAD_REQUEST,
        )


class ClientDelete(generics.DestroyAPIView):
    """
    Delete client objects.

    When deleting clients, also update admins to update user file size list.
    """

    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [CanEditClient]

    def perform_destroy(self, instance):
        """Send ws message on client delete."""
        send_to_permitted_users(
            {
                "can_view_deleted_clients": {
                    "action": "deleteClient",
                    "data": {"id": instance.id},
                    "id": instance.id,
                }
            }
        )
        return instance.delete()


class NextOfKinDelete(generics.DestroyAPIView):
    """Delete Next of Kin objects."""

    queryset = NextOfKin.objects.all()
    serializer_class = ClientNextOfKinSerializer
    permission_classes = [CanEditClientNextOfKin]

    def perform_destroy(self, instance):
        """Send ws message on NoK delete."""
        instance.delete()
        return update_client_websocket(instance.client, filter_keys=["nextOfKin"])


class NextOfKinView(generics.CreateAPIView):
    """Create Next of Kin objects."""

    queryset = NextOfKin.objects.all()
    permission_classes = [CanEditClientNextOfKin]
    serializer_class = ClientNextOfKinSerializer


class NextOfKinUpdate(generics.UpdateAPIView):
    """Update next of kin."""

    queryset = NextOfKin.objects.all()
    serializer_class = ClientNextOfKinSerializer
    permission_classes = [CanEditClientNextOfKin]


class MedicationUpdate(generics.UpdateAPIView):
    """Update Medication objects."""

    queryset = Medication.objects.all()
    serializer_class = MedicationSerializer
    permission_classes = [CanEditClientMedication]


class MedicationView(generics.ListCreateAPIView):
    """Get all Medication objects from database and return for serializer."""

    queryset = Medication.objects.all()
    serializer_class = MedicationSerializer
    permission_classes = [CanEditClientMedication]


class MedicationDelete(generics.DestroyAPIView):
    """Delete Medication objects."""

    queryset = Medication.objects.all()
    serializer_class = MedicationSerializer
    permission_classes = [CanEditClientMedication]

    def perform_destroy(self, instance):
        """Send ws message on medication delete."""
        instance.delete()
        return update_client_websocket(instance.client, filter_keys=["medications"])


class FeedingRiskView(generics.CreateAPIView):
    """Get all FeedingRisk objects from database and return for serializer."""

    queryset = FeedingRisk.objects.all()
    serializer_class = FeedingRiskSerializer
    permission_classes = [CanEditClientFeedingRisk]

    def create(self, request, *args, **kwargs):  # pylint: disable=W0613
        """Override default create settings to allow bulk creation."""
        client_id = self.kwargs["pk"]
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        FeedingRisk.objects.filter(client__id=client_id).delete()
        if len(request.data) > 0 and "feedingRisk" in request.data[0]:
            serializer = self.get_serializer(data=request.data, many=True)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            save_feeding_risks(serializer.data, client_id)
            return Response(
                serializer.data, status=status.HTTP_201_CREATED, headers=headers
            )
        save_feeding_risks([], client_id)
        return Response([], status=status.HTTP_204_NO_CONTENT)


class ClientDocumentUpload(generics.CreateAPIView):
    """Upload documents for clients."""

    parser_class = (MultiPartParser,)
    queryset = ClientDocument.objects.all()
    permission_classes = [CanEditClientDocuments]
    serializer_class = ClientDocumentSerializer


class ClientDocumentDelete(generics.DestroyAPIView):
    """Delete ClientDocument objects."""

    queryset = ClientDocument.objects.all()
    serializer_class = ClientDocumentSerializer
    permission_classes = [CanEditClientDocuments]

    def perform_destroy(self, instance):
        """
        Send ws message on document delete.

        Admins must get this message too to update client documents table.
        """
        Client.objects.filter(id=instance.client.id).update(
            storageSize=F("storageSize") - instance.document.size
        )
        new_client_document_size = instance.client.storageSize - instance.document.size
        document_data = {
            "clientId": instance.client.id,
            "newClientSize": new_client_document_size,
            "document": {"id": instance.id},
        }
        send_data = {
            "action": "deleteClientDocument",
            "data": document_data,
            "id": document_data["document"]["id"],
        }
        send_to_permitted_users({"can_view_client_documents": send_data})
        instance.delete()


class ClientDocumentUpdate(generics.UpdateAPIView):
    """Update ClientDocument objects."""

    queryset = ClientDocument.objects.all()
    serializer_class = ClientDocumentUpdateSerializer
    permission_classes = [CanEditClientDocuments]


class ClientDocumentDownload(generics.RetrieveAPIView):
    """Allow downloading of client documents."""

    permission_classes = [CanEditClientDocuments]
    queryset = ClientDocument.objects.all()

    def retrieve(self, request, *args, **kwargs):
        """Provide client document for download."""
        document = self.get_object()
        try:
            return FileResponse(open(document.document.path, "rb"), as_attachment=True)
        except FileNotFoundError:
            return Response(
                "Client Document File Not Found", status=status.HTTP_404_NOT_FOUND
            )


class CarePlanList(generics.ListAPIView):
    """

    List all incomplete care plans.

    The list will only contain care plans by clients
    who are in the same centres as the current user.
    """

    queryset = CarePlan.objects.all()
    serializer_class = CarePlanSerializer
    permission_classes = [CanEditCarePlan]

    def get_queryset(self):
        """Filter needs by client."""
        user = self.request.user
        user_centres = user.centres.all()
        return (
            CarePlan.objects.filter(inputted=False)
            .filter(need__centre__in=user_centres)
            .exclude(need__need=None)
            .exclude(need__need__centre=None)
        )


class CarePlanUpdate(generics.UpdateAPIView):
    """Update CarePlan objects."""

    queryset = (
        CarePlan.objects.filter(inputted=False)
        .exclude(need__need=None)
        .exclude(need__need__centre=None)
    )
    serializer_class = CarePlanDetailSerializer
    permission_classes = [CanEditCarePlan]


class CarePlanDetail(generics.ListAPIView):
    """Retreive a specific subset of care plans."""

    queryset = (
        CarePlan.objects.filter(inputted=False)
        .exclude(need__need=None)
        .exclude(need__need__centre=None)
    )
    serializer_class = CarePlanDetailSerializer
    permission_classes = [CanEditCarePlan]

    def get_queryset(self):
        """Return a filtered list based on date and client."""
        client = self.kwargs["client"]
        date = self.kwargs["date"]
        user = self.request.user
        user_centres = user.centres.all()
        return (
            CarePlan.objects.filter(need__client__id=client)
            .filter(need__centre__in=user_centres)
            .filter(date=date)
        )


def set_care_plans(user, client_id, date, key_to_set, new_value):
    """
    Set care plans to be inputted/completed.
    """
    user_centres = user.centres.all()
    care_plans = (
        CarePlan.objects.filter(need__client__id=client_id)
        .filter(date=date)
        .filter(need__centre__in=user_centres)
        .filter(need__isActive=True)
    )
    for care_plan in care_plans:
        setattr(care_plan, key_to_set, new_value)
        care_plan.addedBy = user
    CarePlan.objects.bulk_update(care_plans, [key_to_set, "addedBy"])
    update_care_plans_websocket(care_plans)
    return Response(f"Care Plans {key_to_set}", status=status.HTTP_200_OK)


class CarePlanComplete(APIView):
    """Mark care plans as completed for client."""

    serializer_class = CarePlanDetailSerializer
    permission_classes = [CanEditClient]

    def get(self, request, pk, date):  # noqa: PLC0103
        """Return a filtered list based on date and client."""
        return set_care_plans(self.request.user, pk, date, "completed", True)


class CarePlanIncomplete(APIView):
    """Mark care plans as incomplete for client."""

    serializer_class = CarePlanDetailSerializer
    permission_classes = [CanEditClient]

    def get(self, request, pk, date):  # noqa: PLC0103
        """Return a filtered list based on date and client."""
        return set_care_plans(self.request.user, pk, date, "completed", False)


class CarePlanInput(APIView):
    """
    Mark care plans as inputted for client.

    Client and date must be specified.
    Also filters down to only centres the user is
    subbed to.
    """

    serializer_class = CarePlanDetailSerializer
    permission_classes = [CanEditClient]

    def get(self, request, pk, date):  # noqa: PLC0103
        """Return a filtered list based on date and client."""
        return set_care_plans(self.request.user, pk, date, "inputted", True)


class NeedView(generics.ListAPIView):
    """Get all centre needs for a client."""

    serializer_class = NeedSerializer
    permission_classes = [CanEditClient]

    def get_queryset(self):
        """Filter needs by client."""
        client_id = self.kwargs["pk"]
        needs = []
        for client_need in Client.objects.get(pk=client_id).needs.all():
            needs.append(client_need.need)
        return needs


class PolicyView(generics.ListAPIView):
    """Get all centre policies for a client."""

    serializer_class = PolicySerializer
    permission_classes = [CanEditClient]

    def get_queryset(self):
        """Filter needs by client."""
        client_id = self.kwargs["pk"]
        policies = []
        for centre in Client.objects.get(pk=client_id).centres.all():
            policies += list(centre.policies.all())
        return policies


class PolicyAgreeView(APIView):
    """Agree to all centre policies for client."""

    serializer_class = ClientSerializer
    permission_classes = [CanEditClient]

    def get(self, request, pk):  # noqa: PLC0103
        """Agree to all policies for client."""
        policies = []
        ClientAgreedPolicies.objects.filter(client=pk).delete()
        client_object = Client.objects.get(pk=pk)
        for centre in client_object.centres.all():
            for policy in list(centre.policies.all()):
                policies.append(
                    ClientAgreedPolicies(
                        client=client_object, policy=policy, centre=centre
                    )
                )
        ClientAgreedPolicies.objects.bulk_create(policies)
        update_client_websocket(
            client_object, m2m_field="agreedPolicies", filter_keys=["agreedPolicies"]
        )
        return Response("Policies Accepted", status=status.HTTP_200_OK)


class CarePlanHistory(generics.ListAPIView):
    """
    Retreive historic care plans for a single client.

    The care plans will be filtered to care plans that are specifically
    part of centres that the request user is part of.
    """

    queryset = CarePlan.objects.all()

    serializer_class = CarePlanHistorySerializer
    permission_classes = [CanEditClient]

    def get_queryset(self):
        """Return a filtered list based on date and client."""
        client = self.kwargs["pk"]
        user = self.request.user
        user_centres = user.centres.all()
        return (
            CarePlan.objects.filter(need__client__id=client)
            .filter(need__centre__in=user_centres)
            .exclude(need__need=None)
            .exclude(need__need__centre=None)
        )


class AttendanceRecordList(generics.ListAPIView):
    """
    Retreive attendance records for a single client.

    The attendance records will be filtered to recordss that are specifically
    for centres that the request user is part of.
    """

    serializer_class = AttendanceRecordSerializer
    permission_classes = [CanEditClient]

    def get_queryset(self):
        """Return a filtered list based on client."""
        client = self.kwargs["pk"]
        user = self.request.user
        user_centres = user.centres.all()
        return (
            AttendanceRecord.objects.filter(client__id=client)
            .filter(centre__in=user_centres)
            .order_by("id")
        )


class AttendanceRecordCreate(generics.CreateAPIView):
    """Create multiple new attendance records for a client."""

    serializer_class = AttendanceRecordSerializer
    permission_classes = [CanCreateAttendanceRecords]

    def perform_create(self, serializer):
        """Override create to send ws message on complete."""
        serializer.save()
        centres = []
        for record in serializer.instance:
            centres.append(record.centre)
        care_plans_to_delete = CarePlan.objects.filter(
            date=serializer.instance[0].date,
            need__client=serializer.instance[0].client,
            need__centre__in=centres,
        )
        create_attendance_records_websocket(serializer.instance)
        if care_plans_to_delete:
            update_care_plans_websocket(care_plans_to_delete, action="deleteCarePlans")
            care_plans_to_delete.delete()

    def create(self, request, *args, **kwargs):
        """Override default create settings to allow bulk creation."""
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )


def send_client_activate_email(user, is_active):
    """Send email to clientUser informing them of account/activation/deactivation."""
    links = get_latest_app_versions()
    context = {
        "links": links,
        "server_address": get_backend_address(),
        "displayName": user.client.firstName,
        "web_version_exists": settings.WEB_VERSION_EXISTS,
        "app_version_exists": settings.APP_VERSION_EXISTS,
        "frontend_address": get_frontend_address(),
        "email": user.email,
    }
    if is_active:
        template = get_email_templates("account-reactivated", context)
        subject = "Your YAPCA account was reactivated!"
    else:
        template = get_email_templates("account-deactivated", context)
        subject = "Your YAPCA account was deactivated"
    EmailThread(
        subject,
        template["plain"],
        template["html"],
        [user.email],
    ).start()


class SetClientUserActivation(APIView):
    """Set a client user to active or inactive."""

    serializer_class = SetClientUserActivationSerializer
    permission_classes = [CanEditClient]

    def post(self, request, *args, **kwargs):
        """Post client user active status."""
        serializer = SetClientUserActivationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        client = Client.objects.get(pk=kwargs.get("pk"))
        user = client.user
        user.is_active = request.data.get("active")
        user.save()
        if request.data.get("sendEmail"):
            send_client_activate_email(user, request.data.get("active"))
        update_client_websocket(client, filter_keys=["accountStatus"])
        return Response(
            status=status.HTTP_200_OK,
        )


class ClientPermissionsUpdate(APIView):
    """Update client permissions."""

    serializer_class = ClientPermissionsSerializer
    permission_classes = [CanEditClient]

    def post(self, request, *args, **kwargs):
        """Override create to send ws message on complete."""
        serializer = ClientPermissionsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        permissions = []
        for permission in serializer.data["permissions"]:
            permissions.append(UserPermission.objects.get(codename=permission))
        client = Client.objects.get(id=kwargs.get("pk"))
        client.user.permissions.set(permissions)

        update_client_permissions(client)
        return Response(
            status=status.HTTP_200_OK,
        )
