"""Tests for sending get requests to the CarePlanHistorySerializer."""
from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.models import Need
from .....centre.tests.utils import create_centre
from ....models import CarePlan, ClientNeed
from ...utils import create_client

URL = "/api/carePlansHistory/"


def create_care_plans(token):
    """Create care plans for test."""

    test_user = CustomUser.objects.get(id=token.data["user"]["id"])

    test_need_1 = Need.objects.create(description="Example Need 1")
    test_need_2 = Need.objects.create(description="Example Need 2")
    test_centre = create_centre(users=[test_user], needs=[test_need_1, test_need_2])
    test_centre_2 = create_centre(
        name="test_centre_2", needs=[test_need_1, test_need_2]
    )

    client_1 = create_client()
    client_2 = create_client(centres=[test_centre])

    client_need_1 = ClientNeed.objects.create(
        client=client_1,
        need=test_need_1,
        centre=test_centre,
    )
    client_need_2 = ClientNeed.objects.create(
        client=client_1,
        need=test_need_2,
        centre=test_centre,
    )
    client_need_3 = ClientNeed.objects.create(
        client=client_2,
        need=test_need_1,
        centre=test_centre,
    )
    client_need_4 = ClientNeed.objects.create(
        client=client_2,
        need=test_need_2,
        centre=test_centre,
    )
    client_need_5 = ClientNeed.objects.create(
        client=client_2,
        need=test_need_2,
        centre=test_centre_2,
    )
    CarePlan.objects.create(
        date="2011-08-12", need=client_need_1, addedBy=test_user
    )  # 1
    CarePlan.objects.create(
        date="2012-08-12", need=client_need_2, addedBy=test_user
    )  # 2
    CarePlan.objects.create(
        date="2013-08-12", need=client_need_3, inputted=True, addedBy=test_user
    )  # 3
    CarePlan.objects.create(
        date="2014-08-12", need=client_need_4, addedBy=test_user
    )  # 4
    CarePlan.objects.create(
        date="2015-08-12", need=client_need_3, comment="TEST COMMENT", addedBy=test_user
    )  # 5
    CarePlan.objects.create(
        date="2016-08-12", need=client_need_5, addedBy=test_user
    )  # 6
    return (client_2, client_need_3, test_user)


class CarePlanHistorySerializerTestCase(TestCase):
    """Tests for sending get requests to the CarePlanHistorySerializer."""

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        self.test_client, self.test_need, self.test_user = create_care_plans(token)

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(
            f"{URL}{self.test_client.id}/",
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_get(self):
        """
        Test sending a valid get request to the serializer.

        Will return care plans 3, 4, and 5 since they are related to
        client_need_3 and client_need_4 which both belong to a client
        in the same centre as the user
        """
        response = self.client.get(
            f"{URL}{self.test_client.id}/",
            {},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data[0]["date"], "2013-08-12")
        care_plan = CarePlan.objects.filter(date="2015-08-12").get(need=self.test_need)
        self.assertEqual(
            response.data[1],
            OrderedDict(
                [
                    ("id", care_plan.id),
                    ("date", "2015-08-12"),
                    ("need", self.test_need.id),
                    ("client", self.test_client.id),
                    ("inputted", False),
                    ("comment", "TEST COMMENT"),
                    ("completed", False),
                    (
                        "needObject",
                        OrderedDict(
                            [
                                ("id", self.test_need.need.id),
                                ("description", "Example Need 1"),
                            ]
                        ),
                    ),
                    (
                        "clientNeed",
                        OrderedDict(
                            [
                                ("id", self.test_need.id),
                                ("centre", self.test_need.centre.id),
                                ("need", self.test_need.need.id),
                                ("plan", ""),
                                ("isActive", True),
                                ("client", self.test_client.id),
                            ]
                        ),
                    ),
                    ("addedBy", self.test_user.id),
                ]
            ),
        )

    def test_bad_client(self):
        """Cannot get care plan history if not subbed to centre."""
        self.test_user.centres.clear()
        response = self.client.get(
            f"{URL}{self.test_client.id}/",
            {},
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
