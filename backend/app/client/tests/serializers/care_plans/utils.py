"""Utils for care plans tests."""

from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.models import Need
from .....centre.tests.utils import create_centre
from ....models import CarePlan, ClientNeed
from ...utils import create_client


def create_care_plans(user_id):
    """Create care plans for test."""
    user = CustomUser.objects.get(id=user_id)

    centre = create_centre(users=[user])
    test_need_1 = Need.objects.create(description="Example Need 1")
    test_need_2 = Need.objects.create(description="Example Need 2")
    centre.needs.add(test_need_1.id)
    centre.needs.add(test_need_2.id)

    centre_2 = create_centre(name="test centre 2")
    client_1 = create_client(centres=[centre])
    client_2 = create_client(centres=[centre])
    client_3 = create_client(centres=[centre_2])
    client_need_1 = ClientNeed.objects.create(
        client=client_1,
        need=test_need_1,
        centre=centre,
    )
    client_need_2 = ClientNeed.objects.create(
        client=client_1,
        need=test_need_2,
        centre=centre,
    )
    client_need_3 = ClientNeed.objects.create(
        client=client_2,
        need=test_need_1,
        centre=centre,
    )
    client_need_4 = ClientNeed.objects.create(
        client=client_2,
        need=test_need_2,
        centre=centre,
    )
    client_need_5 = ClientNeed.objects.create(
        client=client_3,
        need=test_need_2,
        centre=centre_2,
    )
    care_plan_1 = CarePlan.objects.create(date="2011-08-12", need=client_need_1)  # 1
    CarePlan.objects.create(date="2012-08-12", need=client_need_2)  # 2
    CarePlan.objects.create(date="2013-08-12", need=client_need_3, inputted=True)  # 3
    CarePlan.objects.create(date="2014-08-12", need=client_need_4)  # 4
    CarePlan.objects.create(date="2015-08-12", need=client_need_5)  # 5
    return (care_plan_1, client_need_1, client_1)


def input_complete_setup():
    """Setup task for care plan input/complete."""
    client = APIClient()
    token = register_test_user(client)
    test_user = CustomUser.objects.get(id=token.data["user"]["id"])
    client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
    (care_plan, test_need, test_client) = create_care_plans(token.data["user"]["id"])
    return client, care_plan, test_need, test_client, test_user


def input_complete_no_auth_test(self):
    """No results should return when authentication isn't provided."""
    self.client.credentials()
    response = self.client.get(
        f"/api/carePlans/{self.test_client.id}/2014-08-12/input/",
        {},
    )
    self.assertEqual(response.status_code, 401)
    self.assertEqual(
        response.data,
        {
            "detail": ErrorDetail(
                string="Authentication credentials were not provided.",
                code="not_authenticated",
            )
        },
    )
