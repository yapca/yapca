"""Tests for sending get requests to the CarePlanDetailSerializer."""
from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.tests.utils import register_test_user
from ....models import CarePlan
from .utils import create_care_plans

URL = "/api/carePlansDetail/"


class CarePlanDetailSerializerTestCase(TestCase):
    """Tests for sending get requests to the CarePlanDetailSerializer."""

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        (self.care_plan, self.test_need, self.test_client) = create_care_plans(
            token.data["user"]["id"]
        )

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(
            f"{URL}{self.test_client.id}/2014-08-12/",
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_get(self):
        """Test sending a valid get request to the serializer."""
        response = self.client.get(
            f"{URL}{self.test_client.id}/2011-08-12/",
            {},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["date"], "2011-08-12")
        care_plan = CarePlan.objects.filter(date="2011-08-12").get(need=self.test_need)
        self.assertEqual(
            response.data[0],
            OrderedDict(
                [
                    ("id", care_plan.id),
                    ("date", "2011-08-12"),
                    ("need", self.test_need.id),
                    ("client", self.test_client.id),
                    ("inputted", False),
                    ("comment", ""),
                    ("completed", False),
                    (
                        "needObject",
                        OrderedDict(
                            [
                                ("id", self.test_need.need.id),
                                ("description", "Example Need 1"),
                            ]
                        ),
                    ),
                    ("addedBy", None),
                ]
            ),
        )
