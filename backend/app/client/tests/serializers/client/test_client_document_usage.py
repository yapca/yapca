"""Tests for sending get requests to the ClientStorageUsageSerializer."""
from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ...utils import create_client
from ..client_document.utils import create_test_documents


class ClientStorageUsageSerializerTestCase(TestCase):
    """Tests for sending get requests to the ClientStorageUsageSerializer."""

    url = "/api/clients-storage-usage/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre_1 = create_centre(users=[self.user])
        centre_2 = create_centre(users=[], name="centre2")
        self.test_client = create_client(centres=[centre_1])
        self.test_client_2 = create_client(
            centres=[centre_2], first_name="test", surname="name"
        )
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        create_test_documents([self.test_client, self.test_client_2])

    def test_no_auth_get(self):
        """Should be unable to get data when not authenticated."""
        self.client.credentials()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_get(self):
        """
        Test sending a valid get request to the serializer.

        Should return clients that are NOT in the users centres.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                OrderedDict(
                    [
                        ("storageSize", 54),
                        ("id", response.data[0]["id"]),
                        ("firstName", "test"),
                        ("surname", "name"),
                        ("setupComplete", False),
                    ]
                )
            ],
        )

    def test_invalid_group_get(self):
        """Should be unable to get data when not an admin."""
        self.client = APIClient()
        token = register_test_user(
            self.client,
            username="testUser2",
            email="testemail2@test.test",
            display_name="testuser2",
        )
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
