"""Tests for sending post requests to the ClientSerializer."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.models import Centre, Need
from ....models import Client


class ClientSerializerPostTestCase(TestCase):
    """Tests for sending post requests to the ClientSerializer."""

    url = "/api/clients/"
    needDescription = "Example Need"
    centreName = "Test Centre"
    centre2Name = "Test Centre 2"
    fieldRequired = "This field is required."
    noClientSubText = "None of this clients subscribed centres have this "
    needPlan = "Need Plan 1"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        test_need = Need.objects.create(description=self.needDescription)
        test_centre = Centre.objects.create(name=self.centreName)
        test_centre.save()
        test_centre.openingDays.add(1, 2, 5, 6)
        test_centre.needs.add(test_need.id)
        response = self.client.post(
            self.url,
            {
                "firstName": "random",
                "surname": "client",
                "gender": "Male",
                "visitingDays": [
                    {"centre": test_centre.id, "day": 1},
                    {"centre": test_centre.id, "day": 2},
                    {"centre": test_centre.id, "day": 5},
                ],
                "centres": [test_centre.id],
                "isActive": False,
                "basicInfoSetup": True,
                "additionalInfoSetup": True,
                "servicesSetup": True,
                "abilitySetup": True,
                "dietSetup": True,
                "visitingDaysSetup": True,
                "setupComplete": True,
                "publicHealthNurseName": "PHN Name",
                "publicHealthNursePhoneNumber": "0000001",
                "homeHelpName": "HH Name",
                "homeHelpPhoneNumber": "0000002",
                "gpName": "GP Name",
                "gpPhoneNumber": "0000003",
                "chemistName": "C Name",
                "chemistPhoneNumber": "0000004",
                "medicalHistory": "Medical History",
                "surgicalHistory": "Surgical History",
                "mobility": "Walks With Frame",
                "toileting": "Incontinent",
                "hygiene": "Self",
                "sight": "Normal",
                "hearing": "Impaired",
                "caseFormulation": "Case Formulation",
                "allergies": "Allergies",
                "intolerances": "Intolerances",
                "requiresAssistanceEating": True,
                "thicknerGrade": 2,
                "reasonForInactivity": "Entered Long Term Care",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)
        test_client = Client.objects.get(pk=response.data["id"])
        self.assertEqual(test_client.admittedBy.username, "testUser1")
        self.assertEqual(test_client.centres.count(), 1)
        self.assertEqual(test_client.isActive, False)
        self.assertEqual(test_client.visitingDays.count(), 3)
        self.assertTrue(test_client.basicInfoSetup)
        self.assertTrue(test_client.additionalInfoSetup)
        self.assertTrue(test_client.servicesSetup)
        self.assertTrue(test_client.abilitySetup)
        self.assertTrue(test_client.dietSetup)
        self.assertTrue(test_client.visitingDaysSetup)
        self.assertTrue(test_client.setupComplete)
        self.assertEqual(test_client.publicHealthNurseName, "PHN Name")
        self.assertEqual(test_client.publicHealthNursePhoneNumber, "0000001")
        self.assertEqual(test_client.homeHelpName, "HH Name")
        self.assertEqual(test_client.homeHelpPhoneNumber, "0000002")
        self.assertEqual(test_client.gpName, "GP Name")
        self.assertEqual(test_client.gpPhoneNumber, "0000003")
        self.assertEqual(test_client.chemistName, "C Name")
        self.assertEqual(test_client.chemistPhoneNumber, "0000004")
        self.assertEqual(test_client.medicalHistory, "Medical History")
        self.assertEqual(test_client.surgicalHistory, "Surgical History")
        self.assertEqual(test_client.mobility, "Walks With Frame")
        self.assertEqual(test_client.toileting, "Incontinent")
        self.assertEqual(test_client.hygiene, "Self")
        self.assertEqual(test_client.sight, "Normal")
        self.assertEqual(test_client.hearing, "Impaired")
        self.assertFalse(test_client.usesDentures)
        self.assertEqual(test_client.caseFormulation, "Case Formulation")
        self.assertEqual(test_client.allergies, "Allergies")
        self.assertEqual(test_client.intolerances, "Intolerances")
        self.assertTrue(test_client.requiresAssistanceEating)
        self.assertEqual(test_client.thicknerGrade, 2)
        self.assertEqual(test_client.reasonForInactivity, "Entered Long Term Care")
        self.assertEqual(test_client.gender, "Male")

    def test_post_without_days(self):
        """Test sending a valid post without visiting days."""
        test_centre = Centre.objects.create(name=self.centreName)
        test_centre.save()
        test_centre.openingDays.add(1, 2, 5, 6)

        response = self.client.post(
            self.url,
            {
                "firstName": "random",
                "surname": "client",
                "centres": [test_centre.id],
                "isActive": False,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)
        test_client = Client.objects.get(pk=response.data["id"])
        self.assertEqual(test_client.admittedBy.username, "testUser1")
        self.assertEqual(test_client.needs.count(), 0)
        self.assertEqual(test_client.centres.count(), 1)
        self.assertEqual(test_client.isActive, False)
        self.assertEqual(test_client.visitingDays.count(), 0)
        self.assertFalse(test_client.basicInfoSetup)
        self.assertFalse(test_client.additionalInfoSetup)
        self.assertFalse(test_client.servicesSetup)
        self.assertFalse(test_client.abilitySetup)
        self.assertFalse(test_client.dietSetup)
        self.assertFalse(test_client.visitingDaysSetup)
        self.assertFalse(test_client.setupComplete)
        self.assertEqual(test_client.publicHealthNurseName, "")
        self.assertEqual(test_client.publicHealthNursePhoneNumber, "")
        self.assertEqual(test_client.homeHelpName, "")
        self.assertEqual(test_client.homeHelpPhoneNumber, "")
        self.assertEqual(test_client.gpName, "")
        self.assertEqual(test_client.gpPhoneNumber, "")
        self.assertEqual(test_client.chemistName, "")
        self.assertEqual(test_client.chemistPhoneNumber, "")
        self.assertEqual(test_client.medicalHistory, "")
        self.assertEqual(test_client.surgicalHistory, "")

    def test_valid_post_multiple_centres(self):
        """Test sending a valid post request with multiple centres."""
        test_need = Need.objects.create(description=self.needDescription)
        test_need_2 = Need.objects.create(description="test need 2")

        test_centre = Centre.objects.create(name=self.centreName)
        test_centre.save()
        test_centre.openingDays.add(1, 2, 5, 6)
        test_centre.needs.add(test_need.id)

        test_centre_2 = Centre.objects.create(name="centre 2")
        test_centre_2.save()
        test_centre_2.openingDays.add(1, 3, 4)
        test_centre_2.needs.add(test_need_2.id)

        response = self.client.post(
            self.url,
            {
                "firstName": "random",
                "surname": "client",
                "visitingDays": [
                    {"centre": test_centre.id, "day": 1},
                    {"centre": test_centre.id, "day": 2},
                    {"centre": test_centre.id, "day": 5},
                    {"centre": test_centre_2.id, "day": 1},
                    {"centre": test_centre_2.id, "day": 3},
                ],
                "centres": [test_centre.id, test_centre_2.id],
                "isActive": False,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)
        test_client = Client.objects.get(pk=response.data["id"])
        self.assertEqual(test_client.admittedBy.username, "testUser1")
        self.assertEqual(test_client.centres.count(), 2)
        self.assertEqual(test_client.isActive, False)
        self.assertEqual(test_client.visitingDays.count(), 5)

    def test_missing_data(self):
        """Test not sending required data to serializer."""
        response = self.client.post(self.url, {},)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "firstName": [ErrorDetail(string=self.fieldRequired, code="required")],
                "surname": [ErrorDetail(string=self.fieldRequired, code="required")],
                "centres": [
                    ErrorDetail(string="This list may not be empty.", code="empty")
                ],
            },
        )

    def test_visiting_day_not_in_centre_opening_days(self):
        """Should reject a visiting day that doesnt match its centre."""
        test_user = CustomUser.objects.create(username="testUser", is_active=True)

        test_centre = Centre.objects.create(name=self.centreName)
        test_centre.save()
        test_centre.openingDays.add(1, 2, 5, 6)

        response = self.client.post(
            self.url,
            {
                "admittedBy": test_user.id,
                "firstName": "random",
                "surname": "client",
                "visitingDays": [
                    {"centre": test_centre.id, "day": 1},
                    {"centre": test_centre.id, "day": 2},
                    {"centre": test_centre.id, "day": 3},
                ],
                "centres": [test_centre.id],
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string="The provided visiting day was not found in the provided"
                        + " centre",
                        code="invalid",
                    )
                ]
            },
        )

    def test_visiting_day_not_in__subbed_centre_opening_days(self):
        """Should reject a visiting day not in the users subbed centres."""
        test_user = CustomUser.objects.create(username="testUser", is_active=True)

        test_centre = Centre.objects.create(name=self.centreName)
        test_centre.save()
        test_centre.openingDays.add(1, 2, 5, 6)

        test_centre_2 = Centre.objects.create(name=self.centre2Name)
        test_centre_2.save()
        test_centre_2.openingDays.add(7)

        response = self.client.post(
            self.url,
            {
                "admittedBy": test_user.id,
                "firstName": "random",
                "surname": "client",
                "visitingDays": [
                    {"centre": test_centre.id, "day": 1},
                    {"centre": test_centre.id, "day": 2},
                    {"centre": test_centre_2.id, "day": 7},
                ],
                "centres": [test_centre.id],
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string=self.noClientSubText + "opening day.", code="invalid",
                    )
                ]
            },
        )

    def test_incorrect_choice(self):
        """Test sending a post request where choice fields are invalid."""
        test_centre = Centre.objects.create(name=self.centreName)
        test_centre.save()
        test_centre.openingDays.add(1, 2, 5, 6)
        response = self.client.post(
            self.url,
            {
                "firstName": "random",
                "surname": "client",
                "centres": [test_centre.id],
                "mobility": "Example Mobility",
                "toileting": "Example Toileting",
                "hygiene": "Example Hygiene",
                "sight": "Example Sight",
                "hearing": "Example Hearing",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "mobility": [
                    ErrorDetail(
                        string='"Example Mobility" is not a valid choice.',
                        code="invalid_choice",
                    )
                ],
                "toileting": [
                    ErrorDetail(
                        string='"Example Toileting" is not a valid choice.',
                        code="invalid_choice",
                    )
                ],
                "hygiene": [
                    ErrorDetail(
                        string='"Example Hygiene" is not a valid choice.',
                        code="invalid_choice",
                    )
                ],
                "sight": [
                    ErrorDetail(
                        string='"Example Sight" is not a valid choice.',
                        code="invalid_choice",
                    )
                ],
                "hearing": [
                    ErrorDetail(
                        string='"Example Hearing" is not a valid choice.',
                        code="invalid_choice",
                    )
                ],
            },
        )
