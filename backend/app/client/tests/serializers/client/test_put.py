"""Tests for sending put/patch requests to the ClientSerializer."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.models import Centre, Day, Need
from ....models import Client, ClientNeed, ClientVisitingDays, NextOfKin


class ClientSerializerPutTestCase(TestCase):
    """Tests for sending put/patch requests to the ClientSerializer."""

    url = "/api/clients/"
    needDescription = "Example Need"
    centreName = "Test Centre"
    centre2Name = "Test Centre 2"
    noClientSubText = "None of this clients subscribed centres have this "
    need_plan = "Example Plan"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])

    def test_no_auth_put(self):
        """Should be unable to put data when not authenticated."""
        test_need = Need.objects.create(description=self.needDescription)
        test_user = CustomUser.objects.create(username="testUser")
        test_user.is_active = True
        test_user.save()
        test_centre = Centre.objects.create()
        test_centre.save()
        test_centre.openingDays.add(1, 2, 5, 6)
        test_centre.needs.add(test_need.id)
        test_client = Client.objects.create(admittedBy=test_user)
        test_client.centres.add(test_centre.id)
        test_user.centres.add(test_centre)
        test_client.save()
        self.client.credentials()
        response = self.client.put(
            f"{self.url}{test_client.id}/update/",
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_patch(self):
        """Test sending a valid patch request to the serializer."""
        test_user = CustomUser.objects.get(username="testUser1")
        test_user.is_active = True
        test_user.save()
        test_need = Need.objects.create(description=self.needDescription)
        test_need_2 = Need.objects.create(description="different need")
        test_centre = Centre.objects.create(name=self.centreName)
        test_centre.save()
        test_centre_2 = Centre.objects.create(name="centre 2")
        test_centre_2.save()
        test_user.centres.add(test_centre)
        test_centre.openingDays.add(1, 2, 3, 4, 5, 6)
        test_centre.needs.add(test_need_2.id)
        test_client = Client.objects.create(admittedBy=test_user)
        test_client_2 = Client.objects.create(admittedBy=test_user)
        test_client.centres.add(test_centre.id)
        next_of_kin = NextOfKin.objects.create(
            client=test_client_2, name="Jregory", phoneNumber="111111111"
        )
        monday = Day.objects.get(pk=1)
        ClientVisitingDays.objects.create(
            client=test_client, day=monday, centre=test_centre
        )
        ClientNeed.objects.create(
            client=test_client, need=test_need, centre=test_centre, plan=self.need_plan
        )
        response = self.client.patch(
            f"{self.url}{test_client.id}/update/",
            {
                "visitingDays": [
                    {"centre": test_centre.id, "day": 3},
                    {"centre": test_centre.id, "day": 4},
                    {"centre": test_centre.id, "day": 6},
                ],
                "nextOfKin": [next_of_kin.id],
                "isActive": False,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        test_client = Client.objects.get(pk=response.data["id"])
        self.assertEqual(test_client.admittedBy.username, "testUser1")
        self.assertEqual(test_client.needs.count(), 1)
        self.assertEqual(test_client.nextOfKin.count(), 1)
        self.assertEqual(test_client.centres.count(), 1)
        self.assertEqual(test_client.isActive, False)
        self.assertEqual(test_client.visitingDays.count(), 3)
        self.assertEqual(
            test_client.visitingDays.all().values()[0]["centre_id"], test_centre.id
        )

        self.assertEqual(test_client.visitingDays.all().values()[0]["day_id"], 3)
        self.assertEqual(test_client.visitingDays.all().values()[1]["day_id"], 4)
        self.assertEqual(test_client.visitingDays.all().values()[2]["day_id"], 6)

    def test_partial_visiting_day_updates(self):
        """Test sending a valid small visiting day patch request to the serializer."""
        test_user = CustomUser.objects.get(username="testUser1")
        test_user.is_active = True
        test_user.save()
        test_centre = Centre.objects.create(name=self.centreName)
        test_centre.save()
        test_centre.openingDays.add(1, 2, 3, 4, 5, 6)
        test_client = Client.objects.create(admittedBy=test_user)
        test_client.centres.add(test_centre)
        test_user.centres.add(test_centre)
        test_client.save()
        monday = Day.objects.get(pk=1)
        ClientVisitingDays.objects.create(
            client=test_client, day=monday, centre=test_centre
        )
        response = self.client.patch(
            f"{self.url}{test_client.id}/update/",
            {
                "visitingDays": [
                    {"centre": test_centre.id, "day": 3},
                    {"centre": test_centre.id, "day": 4},
                    {"centre": test_centre.id, "day": 6},
                ]
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        test_client = Client.objects.get(pk=response.data["id"])
        self.assertEqual(test_client.admittedBy.username, "testUser1")
        self.assertEqual(test_client.needs.count(), 0)
        self.assertEqual(test_client.centres.count(), 1)
        self.assertEqual(test_client.visitingDays.count(), 3)
        self.assertEqual(
            test_client.visitingDays.all().values()[0]["centre_id"], test_centre.id
        )
        self.assertEqual(test_client.visitingDays.all().values()[0]["day_id"], 3)
        self.assertEqual(test_client.visitingDays.all().values()[1]["day_id"], 4)
        self.assertEqual(test_client.visitingDays.all().values()[2]["day_id"], 6)

    def test_visiting_day_not_in_centre_opening_days(self):
        """Should reject a visiting day that doesnt match its centre."""
        test_user = CustomUser.objects.get(username="testUser1")
        test_centre = Centre.objects.create(name=self.centreName)
        test_centre.save()
        test_centre.openingDays.add(1, 2, 5, 6)
        test_user.centres.add(test_centre)
        test_client = Client.objects.create(admittedBy=test_user)
        test_client.centres.add(test_centre)
        test_client.save()

        response = self.client.patch(
            f"{self.url}{test_client.id}/update/",
            {
                "visitingDays": [
                    {"centre": test_centre.id, "day": 1},
                    {"centre": test_centre.id, "day": 2},
                    {"centre": test_centre.id, "day": 3},
                ],
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string="The provided visiting day was not found in the provided"
                        + " centre",
                        code="invalid",
                    )
                ]
            },
        )

    def test_visiting_day_not_in__subbed_centre_opening_days(self):
        """Should reject a visiting day not in the users subbed centres."""
        test_user = CustomUser.objects.get(username="testUser1")
        test_centre = Centre.objects.create(name=self.centreName)
        test_centre.save()
        test_centre.openingDays.add(1, 2, 5, 6)
        test_centre_2 = Centre.objects.create(name=self.centre2Name)
        test_centre_2.save()
        test_centre_2.openingDays.add(7)
        test_client = Client.objects.create(admittedBy=test_user)
        test_user.centres.add(test_centre)
        test_client.centres.add(test_centre.id)
        test_client.save()

        response = self.client.patch(
            f"{self.url}{test_client.id}/update/",
            {
                "visitingDays": [
                    {"centre": test_centre.id, "day": 1},
                    {"centre": test_centre.id, "day": 2},
                    {"centre": test_centre_2.id, "day": 7},
                ],
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string=self.noClientSubText + "opening day.",
                        code="invalid",
                    )
                ]
            },
        )

    def test_storage_size(self):
        """Should ignore storage size as a parameter."""
        test_user = CustomUser.objects.create(username="testUser", is_active=True)
        test_client = Client.objects.create(admittedBy=test_user)
        self.client.patch(
            f"{self.url}{test_client.id}/update/",
            {"storageSize": 600},
            format="json",
        )
        test_client = Client.objects.get(pk=test_client.id)
        self.assertEqual(test_client.storageSize, 0)

    def test_cannot_update_centres(self):
        """Cannot update client centres with this url."""
        test_user = CustomUser.objects.get(username="testUser1")
        test_client = Client.objects.create(admittedBy=test_user)
        test_centre = Centre.objects.create(name=self.centreName)
        test_centre_2 = Centre.objects.create(name="centre 2")
        test_client.centres.add(test_centre)
        test_user.centres.add(test_centre)
        test_user.centres.add(test_centre_2)
        self.client.patch(
            f"{self.url}{test_client.id}/update/",
            {"centres": [test_centre_2.id]},
            format="json",
        )
        new_client_data = Client.objects.get(id=test_client.id)
        self.assertEqual(new_client_data.centres.all()[0].id, test_centre.id)
