"""Tests for sending delete requests to the ClientDocumentSerializer."""
import os
import pathlib

from django.conf import settings
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail

from ....models import Client, ClientDocument
from .utils import document_setup

NO_AUTH_MESSAGE = "Authentication credentials were not provided."
settings.MEDIA_ROOT = os.path.join(pathlib.Path(__file__).parent.absolute(), "media")


class ClientDocumentSerializerDeleteTestCase(TestCase):
    """Tests for sending delete requests to the ClientDocumentSerializer."""

    url = "/api/clientDocuments/"

    def setUp(self):
        """Create a user for auth."""
        self.client, self.user, self.test_client, self.document = document_setup()

    def test_valid_delete(self):
        """Test sending a valid delete request to the serializer."""
        response = self.client.delete(f"{self.url}{self.document.id}/delete/")
        self.assertEqual(response.status_code, 204)
        files = next(os.walk(settings.MEDIA_ROOT))[2]
        self.assertEqual(len(files), 2)

    def test_no_auth_delete(self):
        """Should be unable to delete data when not authenticated."""
        self.client.credentials()
        response = self.client.delete(f"{self.url}{self.document.id}/delete/")
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_AUTH_MESSAGE,
                    code="not_authenticated",
                )
            },
        )

    def test_client_storage_size_updated(self):
        """A clients storage value should change on delete."""
        client = Client.objects.get(id=self.test_client.id)
        self.assertEqual(client.storageSize, 54)
        self.client.delete(f"{self.url}{self.document.id}/delete/")
        updated_client = Client.objects.get(id=self.test_client.id)
        self.assertEqual(updated_client.storageSize, 27)

    def test_no_client_delete(self):
        """Should be unable to delete data when not subbed to client centre."""
        self.user.centres.clear()
        response = self.client.delete(f"{self.url}{self.document.id}/delete/")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
        self.assertEqual(
            ClientDocument.objects.filter(id=self.document.id).exists(), True
        )
