"""Tests for sending put/patch requests to the ClientDocumentSerializer."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail

from ....models import ClientDocument
from .utils import document_setup


class ClientDocumentSerializerPutTestCase(TestCase):
    """Tests for sending put/patch requests to the ClientDocumentSerializer."""

    url = "/api/clientDocuments/"

    def setUp(self):
        """Create a user for auth."""
        self.client, self.user, self.test_client, self.document = document_setup()

    def test_no_auth_put(self):
        """Should be unable to put data when not authenticated."""
        self.client.credentials()
        response = self.client.put(
            f"{self.url}{self.document.id}/update/",
            {"name": "test"},
            format="json",
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_patch(self):
        """Test sending a valid patch request to the serializer."""
        response = self.client.put(
            f"{self.url}{self.document.id}/update/",
            {"name": "test"},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        document = ClientDocument.objects.get(pk=self.document.id)
        self.assertEqual(document.name, "test")

    def test_cannot_patch_other_data(self):
        """Test sending patch to update other fields."""
        response = self.client.put(
            f"{self.url}{self.document.id}/update/",
            {
                "size": 100,
                "client": "test",
                "createdDate": "2020-01-01",
                "originalName": "example_original_name",
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {"name": [ErrorDetail(string="This field is required.", code="required")]},
        )

    def test_cannot_patch_invalid_client(self):
        """Cannot send request when not valid user clients."""
        self.user.centres.clear()
        response = self.client.patch(
            f"{self.url}{self.document.id}/update/",
            {"name": "test"},
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
