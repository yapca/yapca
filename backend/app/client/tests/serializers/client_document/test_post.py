"""Tests for sending post requests to the ClientDocumentSerializer."""
import glob
import os
import pathlib
import tempfile

from django.conf import settings
from django.test import TestCase
from PIL import Image  # pylint: disable=E0401
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ....models import Client, ClientDocument
from ...utils import create_client, validate_iso8601

settings.MEDIA_ROOT = os.path.join(pathlib.Path(__file__).parent.absolute(), "media")
settings.MAX_UPLOAD_SIZE = 4000
settings.MAX_UPLOAD_FOLDER_SIZE = 4000


class ClientDocumentSerializerPostTestCase(TestCase):
    """Tests for sending post requests to the ClientDocumentSerializer."""

    url = "/api/clientDocuments/upload/"
    fieldRequiredString = "This field is required."
    documentName = "Example Document Name"
    originalDocumentName = "Original Name.txt"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre = create_centre(users=[self.user])
        self.test_client = create_client(centres=[centre])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        files = glob.glob(f"{settings.MEDIA_ROOT}/*")
        for file in files:
            os.remove(file)

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        image = Image.new("RGB", (100, 100))
        with tempfile.NamedTemporaryFile(suffix=".jpg") as tmp_file:
            image.save(tmp_file)
            tmp_file.seek(0)

            response = self.client.post(
                self.url,
                {
                    "document": tmp_file,
                    "client": self.test_client.id,
                    "name": self.documentName,
                    "originalName": self.originalDocumentName,
                },
                format="multipart",
            )
        self.assertEqual(response.status_code, 201)
        document = ClientDocument.objects.get(client=self.test_client)
        self.assertEqual(response.data["id"], document.id)
        self.assertEqual(
            response.data["document"], f"http://testserver/media/{document.document}"
        )
        self.assertEqual(response.data["client"], self.test_client.id)
        self.assertEqual(response.data["name"], self.documentName)
        self.assertTrue(validate_iso8601(response.data["createdDate"]))

    def test_post_fails_without_data(self):
        """Test should throw an error message when the request is missing data."""
        response = self.client.post(
            self.url, {"client": self.test_client.id}, format="multipart",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "document": [
                    ErrorDetail(string="No file was submitted.", code="required")
                ],
                "name": [ErrorDetail(string=self.fieldRequiredString, code="required")],
                "originalName": [
                    ErrorDetail(string=self.fieldRequiredString, code="required")
                ],
            },
        )

    def test_post_fails_too_large(self):
        """Test should throw an error when the request is too large."""
        with tempfile.NamedTemporaryFile(suffix=".txt") as temp_file:
            temp_file.write(os.urandom(5000))
            temp_file.seek(0)
            response = self.client.post(
                self.url,
                {
                    "document": temp_file,
                    "client": self.test_client.id,
                    "name": self.documentName,
                    "originalName": self.originalDocumentName,
                },
                format="multipart",
            )
            self.assertEqual(response.status_code, 400)
            self.assertEqual(
                response.data,
                {
                    "non_field_errors": [
                        ErrorDetail(
                            string="Document must be under 100Mib", code="invalid"
                        )
                    ]
                },
            )

    def test_post_fails_folder_full(self):
        """Test should throw an error when the request is too large."""
        with tempfile.NamedTemporaryFile(suffix=".txt") as temp_file:
            temp_file.write(os.urandom(3000))
            temp_file.seek(0)
            self.client.post(
                self.url,
                {
                    "document": temp_file,
                    "client": self.test_client.id,
                    "name": self.documentName,
                    "originalName": self.originalDocumentName,
                },
                format="multipart",
            )

        with tempfile.NamedTemporaryFile(suffix=".txt") as temp_file:
            temp_file.write(os.urandom(3000))
            temp_file.seek(0)
            response = self.client.post(
                self.url,
                {
                    "document": temp_file,
                    "client": self.test_client.id,
                    "name": self.documentName,
                    "originalName": self.originalDocumentName,
                },
                format="multipart",
            )
            self.assertEqual(response.status_code, 400)
            self.assertEqual(
                response.data,
                {
                    "non_field_errors": [
                        ErrorDetail(
                            string="No space left in upload folder for this file.",
                            code="invalid",
                        )
                    ]
                },
            )

    def test_client_storage_size_updated(self):
        """A clients storage value should change on post."""
        with tempfile.NamedTemporaryFile(suffix=".txt") as temp_file:
            temp_file.write(os.urandom(1000))
            temp_file.seek(0)
            self.client.post(
                self.url,
                {
                    "document": temp_file,
                    "client": self.test_client.id,
                    "name": self.documentName,
                    "originalName": self.originalDocumentName,
                },
                format="multipart",
            )
        updated_client = Client.objects.get(id=self.test_client.id)
        self.assertEqual(updated_client.storageSize, 1000)
        with tempfile.NamedTemporaryFile(suffix=".txt") as temp_file:
            temp_file.write(os.urandom(2000))
            temp_file.seek(0)
            self.client.post(
                self.url,
                {
                    "document": temp_file,
                    "client": self.test_client.id,
                    "name": self.documentName,
                    "originalName": self.originalDocumentName,
                },
                format="multipart",
            )
        updated_client_2 = Client.objects.get(id=self.test_client.id)
        self.assertEqual(updated_client_2.storageSize, 3000)

    def test_cannot_post_invalid_client(self):
        """Cannot post if user not subbed to client centre."""
        self.user.centres.clear()
        image = Image.new("RGB", (100, 100))
        with tempfile.NamedTemporaryFile(suffix=".jpg") as tmp_file:
            image.save(tmp_file)
            tmp_file.seek(0)
            response = self.client.post(
                self.url,
                {
                    "document": tmp_file,
                    "client": self.test_client.id,
                    "name": self.documentName,
                    "originalName": self.originalDocumentName,
                },
                format="multipart",
            )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_cannot_post_missing_client(self):
        """Cannot post if missing client value."""
        image = Image.new("RGB", (100, 100))
        with tempfile.NamedTemporaryFile(suffix=".jpg") as tmp_file:
            image.save(tmp_file)
            tmp_file.seek(0)
            response = self.client.post(
                self.url,
                {
                    "document": tmp_file,
                    "name": self.documentName,
                    "originalName": self.originalDocumentName,
                },
                format="multipart",
            )
        self.assertEqual(response.status_code, 403)
