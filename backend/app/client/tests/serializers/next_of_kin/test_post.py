"""Tests for sending post requests to the NextOfKinSerializer."""

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ....models import NextOfKin
from ...utils import create_client


class NextOfKinSerializerPostTestCase(TestCase):
    """Tests for sending post requests to the NextOfKinSerializer."""

    url = "/api/nextOfKin/"
    policyDescription = "Example Description"
    nextOfKinName = "Test Name"
    fieldRequired = "This field is required."

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        centre = create_centre(users=[self.user])
        self.test_client = create_client(centres=[centre])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])

    def test_no_auth_post(self):
        """Should be unable to post data when not authenticated."""
        self.client.credentials()
        response = self.client.post(self.url, {},)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        response = self.client.post(
            self.url,
            {
                "phoneNumber": "01239712",
                "name": self.nextOfKinName,
                "client": self.test_client.id,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)
        next_of_kin = NextOfKin.objects.get(pk=response.data["id"])
        self.assertEqual(next_of_kin.client, self.test_client)
        self.assertEqual(next_of_kin.phoneNumber, "01239712")
        self.assertEqual(next_of_kin.name, self.nextOfKinName)
        self.assertEqual(
            response.data,
            {
                "id": next_of_kin.id,
                "name": self.nextOfKinName,
                "phoneNumber": "01239712",
                "client": self.test_client.id,
            },
        )

    def test_missing_data(self):
        """Test not sending required data to serializer."""
        response = self.client.post(self.url, {"client": self.test_client.id},)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "name": [ErrorDetail(string=self.fieldRequired, code="required")],
                "phoneNumber": [
                    ErrorDetail(string=self.fieldRequired, code="required")
                ],
            },
        )

    def test_no_client_post(self):
        """Should be unable to post data when not subbed to client centre."""
        self.user.centres.clear()
        response = self.client.post(
            self.url,
            {
                "phoneNumber": "01239712",
                "name": self.nextOfKinName,
                "client": self.test_client.id,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
