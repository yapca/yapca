"""Tests for sending post requests to the ClientNeedSerializer."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.models import Centre, Need
from ....models import Client, ClientNeed

NO_PERMISSION = "You do not have permission to perform this action."


class ClientNeedSerializerPostTestCase(TestCase):
    """Tests for sending post requests to the ClientNeedSerializer."""

    url = "/api/clientNeeds/"
    needDescription = "Example Need"
    centreName = "Test Centre"
    needPlan = "Need Plan 1"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        user_data = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + user_data.data["key"])
        self.test_user = CustomUser.objects.get(id=user_data.data["user"]["id"])
        self.test_client = Client.objects.create(admittedBy=self.test_user)

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        test_need = Need.objects.create(description=self.needDescription)
        test_centre = Centre.objects.create(name=self.centreName)
        self.test_user.centres.add(test_centre)
        test_centre.needs.add(test_need.id)
        self.test_client.centres.add(test_centre.id)
        response = self.client.post(
            self.url,
            {
                "centre": test_centre.id,
                "need": test_need.id,
                "client": self.test_client.id,
                "plan": self.needPlan,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 201)
        client_need = ClientNeed.objects.get(plan=self.needPlan)
        self.assertEqual(
            response.data,
            {
                "id": client_need.id,
                "centre": test_centre.id,
                "need": test_need.id,
                "plan": self.needPlan,
                "isActive": True,
                "client": self.test_client.id,
            },
        )

    def test_invalid_post_centre(self):
        """Test sending a non existant centre to the serializer."""
        test_need = Need.objects.create(description=self.needDescription)

        response = self.client.post(
            self.url,
            {
                "centre": 999,
                "need": test_need.id,
                "client": self.test_client.id,
                "plan": self.needPlan,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {"detail": ErrorDetail(string=NO_PERMISSION, code="permission_denied",)},
        )

    def test_invalid_post_need(self):
        """Test sending a non existant need to the serializer."""
        test_centre = Centre.objects.create(name=self.centreName)
        self.test_user.centres.add(test_centre)
        self.test_client.centres.add(test_centre.id)
        response = self.client.post(
            self.url,
            {
                "centre": test_centre.id,
                "need": 999,
                "client": self.test_client.id,
                "plan": self.needPlan,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "need": [
                    ErrorDetail(
                        string='Invalid pk "999" - object does not exist.',
                        code="does_not_exist",
                    )
                ]
            },
        )

    def test_invalid_post_need_not_in_centre_needs(self):
        """Test sending a need that is not in the provided centre."""
        test_need = Need.objects.create(description=self.needDescription)
        test_centre = Centre.objects.create(name=self.centreName)
        self.test_user.centres.add(test_centre)
        self.test_client.centres.add(test_centre.id)
        response = self.client.post(
            self.url,
            {
                "centre": test_centre.id,
                "need": test_need.id,
                "client": self.test_client.id,
                "plan": self.needPlan,
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string="The provided need was not found in the provided "
                        + f"centre. Centre: {self.centreName}. "
                        + f"Need: {self.needDescription}.",
                        code="invalid",
                    )
                ]
            },
        )

    def test_invalid_post_need_not_in_subbed_centre_needs(self):
        """Test sending a centre that is not in that clients subscribed centres."""
        test_need = Need.objects.create(description=self.needDescription)
        test_centre = Centre.objects.create(name=self.centreName)
        self.test_user.centres.add(test_centre)
        test_centre.needs.add(test_need.id)
        response = self.client.post(
            self.url,
            {
                "centre": test_centre.id,
                "need": test_need.id,
                "client": self.test_client.id,
                "plan": self.needPlan,
            },
            format="json",
        )
        self.assertEqual(
            response.data,
            {"detail": ErrorDetail(string=NO_PERMISSION, code="permission_denied",)},
        )

    def test_invalid_post_missing_client(self):
        """Test sending a with a missing client value."""
        test_need = Need.objects.create(description=self.needDescription)
        test_centre = Centre.objects.create(name=self.centreName)
        self.test_user.centres.add(test_centre)
        test_centre.needs.add(test_need.id)
        self.test_client.centres.add(test_centre.id)
        response = self.client.post(
            self.url,
            {"centre": test_centre.id, "need": test_need.id, "plan": self.needPlan},
            format="json",
        )
        self.assertEqual(
            response.data,
            {"detail": ErrorDetail(string=NO_PERMISSION, code="permission_denied",)},
        )

    def test_invalid_post_inactive_user(self):
        """Inactive users cannot post."""
        test_need = Need.objects.create(description=self.needDescription)
        test_centre = Centre.objects.create(name=self.centreName)
        self.test_user.centres.add(test_centre)
        test_centre.needs.add(test_need.id)
        self.test_client.centres.add(test_centre.id)
        self.test_user.is_active = False
        self.test_user.save()
        response = self.client.post(
            self.url,
            {
                "centre": test_centre.id,
                "need": test_need.id,
                "client": self.test_client.id,
                "plan": self.needPlan,
            },
            format="json",
        )
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="User inactive or deleted.", code="authentication_failed"
                )
            },
        )
