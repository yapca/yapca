"""Tests for sending post requests to the ClientNeedSerializer."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_client_user, register_test_user
from .....centre.tests.utils import create_centre

NO_PERMISSION = "You do not have permission to perform this action."


class ClientPermissionsSerializerPostTestCase(TestCase):
    """Tests for sending post requests to the ClientPermissionsSerializer."""

    url = "/api/clients/update-permissions/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        register_test_user(self.client)
        user_data = register_test_user(
            self.client, username="testNurse", email="testnurse@test.com"
        )
        self.client.credentials(HTTP_AUTHORIZATION="Token " + user_data.data["key"])
        self.test_user = CustomUser.objects.get(id=user_data.data["user"]["id"])
        test_centre_1 = create_centre(name="test_centre_1", users=[self.test_user])
        client_token = register_client_user(self.client, email="clientemail@email.com")
        client_user = CustomUser.objects.get(id=client_token.data["user"]["id"])
        self.test_client = client_user.client
        self.test_client.centres.set([test_centre_1])

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            {"permissions": ["client_can_view_calendar"]},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            list(
                self.test_client.user.permissions.all().values_list(
                    "codename", flat=True
                )
            ),
            ["client_can_view_calendar"],
        )

    def test_invalid_post_centre(self):
        """Test cannot post when user centres do not match client centres."""
        self.test_client.centres.set([])
        response = self.client.post(
            f"{self.url}{self.test_client.id}/",
            {"permissions": ["client_can_view_calendar"]},
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string=NO_PERMISSION,
                    code="permission_denied",
                )
            },
        )
