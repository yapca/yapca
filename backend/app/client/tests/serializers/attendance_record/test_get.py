"""Tests for sending get requests to the AttendanceRecordSerializer."""
from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ....models import AttendanceRecord
from ...utils import create_client

URL = "/api/attendanceRecord/"
REASON_FOR_ABSENCE = "example reason for absence 1"


def create_attendance_record(token):
    """Create attendance records for test."""
    test_user = CustomUser.objects.get(id=token.data["user"]["id"])
    test_centre_1 = create_centre(name="test_centre_1", users=[test_user])
    test_centre_2 = create_centre(name="test_centre_2", users=[test_user])
    test_centre_3 = create_centre(name="test_centre_3", users=[])
    client_1 = create_client(centres=[test_centre_1, test_centre_2])
    client_2 = create_client(centres=[test_centre_1, test_centre_2, test_centre_3])

    attendance_record_1 = AttendanceRecord.objects.create(
        client=client_1,
        centre=test_centre_1,
        date="2020-01-03",
        reasonForAbsence=REASON_FOR_ABSENCE,
        active=True,
        reasonForInactivity="",
        addedBy=test_user,
    )
    attendance_record_2 = AttendanceRecord.objects.create(
        client=client_1,
        centre=test_centre_2,
        date="2020-02-03",
        reasonForAbsence="",
        active=False,
        reasonForInactivity="example reason for inactivity 1",
        addedBy=test_user,
    )

    # Should not appear.
    # Attendance record for a client the user can view, but
    # this specific record is for a different centre.
    AttendanceRecord.objects.create(
        client=client_1,
        centre=test_centre_3,
        date="2020-03-03",
        reasonForAbsence="",
        active=False,
        reasonForInactivity="",
        addedBy=test_user,
    )

    # Should not appear.
    # Only one client can be selected.
    # Other clients will not be shown.
    AttendanceRecord.objects.create(
        client=client_2,
        centre=test_centre_1,
        date="2020-04-03",
        reasonForAbsence=REASON_FOR_ABSENCE,
        active=True,
        reasonForInactivity="",
        addedBy=test_user,
    )

    return (
        attendance_record_1,
        attendance_record_2,
        test_user,
        test_centre_1,
        test_centre_2,
        client_1,
    )


class AttendanceRecordGetTestCase(TestCase):
    """Tests for sending get requests to the AttendanceRecordSerializer."""

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])
        (
            self.attendance_record_1,
            self.attendance_record_2,
            self.test_user,
            self.test_centre_1,
            self.test_centre_2,
            self.test_client,
        ) = create_attendance_record(token)

    def test_no_auth_get(self):
        """No results should return when authentication isn't provided."""
        self.client.credentials()
        response = self.client.get(
            f"{URL}{self.test_client.id}/",
            {},
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Authentication credentials were not provided.",
                    code="not_authenticated",
                )
            },
        )

    def test_valid_get(self):
        """Test sending a valid get request to the serializer."""
        response = self.client.get(
            f"{URL}{self.test_client.id}/",
            {},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                OrderedDict(
                    [
                        ("id", self.attendance_record_1.id),
                        ("client", self.test_client.id),
                        ("centre", self.test_centre_1.id),
                        ("date", "2020-01-03"),
                        ("active", True),
                        ("reasonForInactivity", ""),
                        ("reasonForAbsence", REASON_FOR_ABSENCE),
                        ("addedBy", self.test_user.id),
                    ]
                ),
                OrderedDict(
                    [
                        ("id", self.attendance_record_2.id),
                        ("client", self.test_client.id),
                        ("centre", self.test_centre_2.id),
                        ("date", "2020-02-03"),
                        ("active", False),
                        ("reasonForInactivity", "example reason for inactivity 1"),
                        ("reasonForAbsence", ""),
                        ("addedBy", self.test_user.id),
                    ]
                ),
            ],
        )

    def test_bad_client(self):
        """Cannot get attendance records for a client not in the same centres."""
        self.test_user.centres.clear()
        response = self.client.get(
            f"{URL}{self.test_client.id}/",
            {},
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
