"""Tests for sending post requests to the AttendanceRecordSerializer."""
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .....authentication.models import CustomUser
from .....authentication.tests.utils import register_test_user
from .....centre.tests.utils import create_centre
from ....models import AttendanceRecord
from ...utils import create_client


class AttendanceRecordPostTestCase(TestCase):
    """Tests for sending post requests to the AttendanceRecordSerializer."""

    url = "/api/attendanceRecord/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        self.test_centre = create_centre(users=[self.user])
        self.test_client = create_client(centres=[self.test_centre])
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        response = self.client.post(
            self.url,
            [
                {
                    "date": "2021-01-17",
                    "client": self.test_client.id,
                    "centre": self.test_centre.id,
                    "reasonForAbsence": "example reason for absence 1",
                },
                {
                    "date": "2021-01-18",
                    "client": self.test_client.id,
                    "centre": self.test_centre.id,
                    "reasonForAbsence": "example reason for absence 2",
                },
            ],
            format="json",
        )
        self.assertEqual(response.status_code, 201)
        absence_1 = AttendanceRecord.objects.get(date="2021-01-17")
        absence_2 = AttendanceRecord.objects.get(date="2021-01-18")
        self.assertEqual(
            response.data,
            [
                {
                    "id": absence_1.id,
                    "client": self.test_client.id,
                    "centre": self.test_centre.id,
                    "date": "2021-01-17",
                    "active": True,
                    "reasonForInactivity": "",
                    "reasonForAbsence": "example reason for absence 1",
                    "addedBy": self.user.id,
                },
                {
                    "id": absence_2.id,
                    "client": self.test_client.id,
                    "centre": self.test_centre.id,
                    "date": "2021-01-18",
                    "active": True,
                    "reasonForInactivity": "",
                    "reasonForAbsence": "example reason for absence 2",
                    "addedBy": self.user.id,
                },
            ],
        )

    def test_missing_data(self):
        """Test not sending required data to serializer."""
        response = self.client.post(self.url, [{}], format="json")
        self.assertEqual(response.status_code, 403)

    def test_user_not_subbed_to_centre(self):
        """
        Test cannot post data if user not subbed to centre.

        Client is subbed to centre but user is not
        """
        test_centre_2 = create_centre(name="centre2")
        self.test_client.centres.add(test_centre_2)

        response = self.client.post(
            self.url,
            [
                {
                    "date": "2022-01-17",
                    "client": self.test_client.id,
                    "centre": self.test_centre.id,
                    "reasonForAbsence": "",
                },
                {
                    "date": "2022-01-18",
                    "client": self.test_client.id,
                    "centre": test_centre_2.id,
                    "reasonForAbsence": "",
                },
            ],
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_client_not_subbed_to_centre(self):
        """
        Test cannot post data if client not subbed to centre.

        User is subbed to centre but client is not
        """
        test_centre_2 = create_centre(name="centre2", users=[self.user])

        response = self.client.post(
            self.url,
            [
                {
                    "date": "2022-01-17",
                    "client": self.test_client.id,
                    "centre": self.test_centre.id,
                    "reasonForAbsence": "",
                },
                {
                    "date": "2022-01-18",
                    "client": self.test_client.id,
                    "centre": test_centre_2.id,
                    "reasonForAbsence": "",
                },
            ],
            format="json",
        )
        self.assertEqual(response.status_code, 403)
