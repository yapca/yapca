"""Test websocket consumers for centre model."""
import datetime
from asyncio.exceptions import TimeoutError as AsyncTimeoutError

import pytest
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator
from django.contrib.auth.models import Group
from django.core.files.uploadedfile import SimpleUploadedFile
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from ...authentication.models import CustomUser, UserPermission
from ...centre.models import Centre, Day, Need
from ...client.models import (
    CarePlan,
    Client,
    ClientDocument,
    ClientNeed,
    ClientVisitingDays,
    Medication,
    NextOfKin,
)
from ...routing import APPLICATION

WEBSOCKET_URL = "ws/main/"
CENTRE_NAME = "Test Centre"
POLICY_NAME = "Test Policy"
NEED_NAME = "Test Need"
DESCRIPTION = "New description"
COMMENT = "example comment"
TOKEN = "Token "


def create_default_groups():
    """Create the default groups since pytest doesn't apply migrations."""
    Group.objects.get_or_create(name="admin")
    Group.objects.get_or_create(name="nurse")


def create_default_permissions():
    """Create the default permissions since pytest doesn't apply migrations."""
    UserPermission.objects.get_or_create(
        name="Client Can View Calendar",
        codename="client_can_view_calendar",
    )
    UserPermission.objects.get_or_create(
        name="Client Can View Own Data",
        codename="client_can_view_own_data",
    )


@database_sync_to_async
def delete_client():
    """Delete a client."""
    user = CustomUser.objects.get(username="testUser1")
    test_centre = Centre.objects.get(name="testCentre")
    test_client, _ = Client.objects.get_or_create(admittedBy=user, firstName="Hugh")
    test_client.centres.add(test_centre)
    client = APIClient()
    client.force_authenticate(user=user, token=user.auth_token)
    return client.delete(f"/api/clients/{test_client.id}/delete/")


def check_client_additional_data(response):
    """Assert a clients additional data is correct."""
    assert response["data"]["dateOfBirth"] == "2011-11-11"
    assert response["data"]["address"] == "New Mexico"
    assert response["data"]["phoneNumber"] == "0118 999 881 999 119"
    assert response["data"]["additionalInfoSetup"]
    assert response["data"]["servicesSetup"]
    assert response["data"]["abilitySetup"]
    assert response["data"]["dietSetup"]
    assert response["data"]["setupComplete"]
    assert response["data"]["basicInfoSetup"]


@database_sync_to_async
def check_client_data(response):
    """Assert a clients data is correct."""
    user = CustomUser.objects.get(username="testUser1")
    assert response["data"]["admittedBy"] == user.id
    assert response["data"]["firstName"] == "first name"
    assert response["data"]["surname"] == "surname"
    assert response["data"]["gender"] == "Female"
    assert response["data"]["storageSize"] == 0
    assert response["data"]["surgicalHistory"] == "Client Surgical History"
    assert response["data"]["isActive"]
    assert response["data"]["reasonForInactivity"] == "Needs Unmet By Service"
    return check_client_additional_data(response)


@database_sync_to_async
def check_visiting_days(provided_visiting_day):
    """Assert a clients data is correct."""
    friday = Day.objects.get(day="Friday")
    test_centre = Centre.objects.first()
    test_client = Client.objects.get(surname="client2")
    visiting_day = ClientVisitingDays.objects.get(client=test_client)
    assert provided_visiting_day == {
        "visitingDays": [
            {
                "id": visiting_day.id,
                "client": test_client.id,
                "day": friday.id,
                "centre": test_centre.id,
            }
        ],
        "clientId": test_client.id,
    }


@database_sync_to_async
def check_client_centres():
    """Assert a clients data is correct."""
    test_centre = Centre.objects.get(name="clientCentre")
    test_client = Client.objects.get(surname="client2")
    assert test_client.centres.all().values()[0]["id"] == test_centre.id


@database_sync_to_async
def tokens():
    """Create users and return tokens."""
    create_default_groups()
    create_default_permissions()
    test_centre = Centre.objects.create(name="testCentre")
    Day.objects.get_or_create(day="Friday")
    admin, _ = CustomUser.objects.get_or_create(username="testUser")
    admin_token, _ = Token.objects.get_or_create(user=admin)
    nurse, _ = CustomUser.objects.get_or_create(username="testUser1")
    nurse2, _ = CustomUser.objects.get_or_create(username="testUser2")
    client_user, _ = CustomUser.objects.get_or_create(username="clientUser")
    client_user.centres.add(test_centre)
    client_group, _ = Group.objects.get_or_create(name="client")
    client_user.groups.set([client_group])
    test_client = Client.objects.create(admittedBy=nurse, surname="test_client")
    test_client.centres.add(test_centre)
    client_user.client = test_client
    client_user.save()
    nurse.centres.add(test_centre)
    nurse_token, _ = Token.objects.get_or_create(user=nurse)
    nurse2_token, _ = Token.objects.get_or_create(user=nurse2)
    client_user_token, _ = Token.objects.get_or_create(user=client_user)
    return {
        "nurse": nurse_token,
        "admin": admin_token,
        "nurse2": nurse2_token,
        "client": client_user_token,
    }


@database_sync_to_async
def update_client():
    """Update a client."""
    user = CustomUser.objects.get(username="testUser1")
    test_centre = Centre.objects.get(name="testCentre")
    client = APIClient()
    user_token = Token.objects.get(user=user)
    client.credentials(HTTP_AUTHORIZATION=TOKEN + user_token.key)
    client.post(
        "/api/clients/",
        {
            "firstName": "first name",
            "surname": "surname",
            "centres": [test_centre.id],
            "gender": "Female",
            "dateOfBirth": "2011-11-11",
            "address": "New Mexico",
            "phoneNumber": "0118 999 881 999 119",
            "setupComplete": True,
            "basicInfoSetup": True,
            "additionalInfoSetup": True,
            "servicesSetup": True,
            "abilitySetup": True,
            "dietSetup": True,
            "surgicalHistory": "Client Surgical History",
            "reasonForInactivity": "Needs Unmet By Service",
        },
        format="json",
    )
    test_client = Client.objects.get(surname="surname")
    return client.patch(
        f"/api/clients/{test_client.id}/update/",
        {"surname": "new surname", "dietSetup": False, "dateOfBirth": "2012-01-01"},
        format="json",
    )


@database_sync_to_async
def add_client_next_of_kin():
    """Update a clients next of kin."""
    user = CustomUser.objects.get(username="testUser1")
    test_client, _ = Client.objects.get_or_create(admittedBy=user, surname="client2")
    test_centre = Centre.objects.get(name="testCentre")
    test_client.centres.add(test_centre)
    return NextOfKin.objects.get_or_create(
        client=test_client, name="Test", phoneNumber="1111"
    )


@database_sync_to_async
def add_client_document():
    """Add document for client."""
    user = CustomUser.objects.get(username="testUser1")
    test_client, _ = Client.objects.get_or_create(admittedBy=user, surname="client2")
    ClientDocument.objects.create(
        client=test_client,
        name="Example Document",
        size=200,
        document=SimpleUploadedFile("example_file.txt", b"these are the file contents"),
    )


@database_sync_to_async
def add_client_medications():
    """Update a clients medications."""
    user = CustomUser.objects.get(username="testUser")
    test_client, _ = Client.objects.get_or_create(admittedBy=user)
    test_centre = Centre.objects.get(name="testCentre")
    test_client.centres.add(test_centre)
    med, _ = Medication.objects.get_or_create(
        client=test_client,
        name="Test Med",
        dosage="100mg",
        frequency="weekly",
        inactiveDate=datetime.date(2012, 9, 2),
    )
    return test_client, med


@database_sync_to_async
def update_visiting_days():
    """Update a clients visiting days."""
    client = APIClient()
    user = CustomUser.objects.get(username="testUser1")
    user_token = Token.objects.get(user=user)
    client.credentials(HTTP_AUTHORIZATION=TOKEN + user_token.key)
    test_centre = Centre.objects.get(name="testCentre")
    user.centres.add(test_centre)
    test_client = Client.objects.create(admittedBy=user, surname="client2")
    test_client.centres.add(test_centre)
    friday = Day.objects.get(day="Friday")
    test_centre.openingDays.add(friday)
    return client.patch(
        f"/api/clients/{test_client.id}/update/",
        {"visitingDays": [{"centre": test_centre.id, "day": friday.id}]},
        format="json",
    )


@database_sync_to_async
def update_client_centres():
    """Update a clients centres."""
    client = APIClient()
    user = CustomUser.objects.get(username="testUser1")
    test_centre = Centre.objects.create(name="clientCentre")
    user.centres.add(test_centre)
    test_client, _ = Client.objects.get_or_create(admittedBy=user, surname="client2")
    user_token = Token.objects.get(user=user)
    client.credentials(HTTP_AUTHORIZATION=TOKEN + user_token.key)
    return client.post(
        "/api/clients/centres/update/",
        {"centre": test_centre.id, "client": test_client.id},
        format="json",
    )


@database_sync_to_async
def update_client_permissions():
    """Update a clients permissions."""
    client = APIClient()
    nurse, _ = CustomUser.objects.get_or_create(username="testUser1")
    nurse_token, _ = Token.objects.get_or_create(user=nurse)
    test_client = Client.objects.get(surname="test_client")
    client.credentials(HTTP_AUTHORIZATION=TOKEN + nurse_token.key)
    return client.post(
        f"/api/clients/update-permissions/{test_client.id}/",
        {"permissions": ["client_can_view_calendar"]},
        format="json",
    )


@database_sync_to_async
def update_care_plan():
    """Update a care plan."""
    user = CustomUser.objects.get(username="testUser1")
    test_centre = Centre.objects.get(name="testCentre")
    test_need_1 = Need.objects.create(description="Example Need 1")
    client_1 = Client.objects.create(
        admittedBy=user, setupComplete=True, surname="client2"
    )
    client_1.centres.add(test_centre)
    client_need_1 = ClientNeed.objects.create(
        client=client_1,
        need=test_need_1,
        centre=test_centre,
    )
    care_plan, _ = CarePlan.objects.get_or_create(
        date="2011-08-12", need=client_need_1, comment=COMMENT, addedBy=user
    )
    return (care_plan, client_need_1, client_1, user)


@database_sync_to_async
def create_attendance_record():
    """Create an attendance record."""
    user = CustomUser.objects.get(username="testUser1")
    test_centre = Centre.objects.first()
    client_1 = Client.objects.get(surname="client2")
    client = APIClient()
    client.force_authenticate(user=user, token=user.auth_token)

    attendance_record = client.post(
        "/api/attendanceRecord/",
        [
            {
                "date": "2011-08-12",
                "client": client_1.id,
                "centre": test_centre.id,
                "reasonForAbsence": "example reason for absence",
            }
        ],
        format="json",
    )
    return (attendance_record, client_1, test_centre, user)


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_client_update():
    """Validate websocket message is sent when updating a client."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_client()
    create_response = await communicator.receive_json_from()
    update_response = await communicator.receive_json_from()
    assert update_response == {
        "type": "update",
        "action": "updateClient",
        "data": {
            "surname": "new surname",
            "id": update_response["data"]["id"],
            "dateOfBirth": "2012-01-01",
            "dietSetup": False,
        },
    }
    await check_client_data(create_response)
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_client_document_admin():
    """Validate websocket message is always sent to admins on document change."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await add_client_document()
    create_response = await communicator.receive_json_from()
    assert create_response == {
        "type": "update",
        "action": "updateClientDocument",
        "data": {
            "newClientSize": 27,
            "document": {
                "client": create_response["data"]["document"]["client"],
                "createdDate": create_response["data"]["document"]["createdDate"],
                "document": create_response["data"]["document"]["document"],
                "id": create_response["data"]["document"]["id"],
                "name": "Example Document",
                "originalName": "",
                "size": 200,
            },
        },
    }
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_client_visiting_days_admin():
    """Validate websocket message is always sent to admins on visiting day change."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_visiting_days()
    await communicator.receive_json_from()  # Set centre opening days
    response = await communicator.receive_json_from()  # Creation of client visiting day
    await check_visiting_days(response["data"])
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_no_centre():
    """Validate websocket message is not sent client and user not in same centres."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse2"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_client()
    with pytest.raises(AsyncTimeoutError):
        await communicator.receive_json_from()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_client_update_visiting_days():
    """Validate websocket message is sent updating visiting days."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_visiting_days()
    await communicator.receive_json_from()  # Update centre opening days
    response = await communicator.receive_json_from()  # Creation of client visiting day
    await check_visiting_days(response["data"])
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_client_centres():
    """Validate websocket message is sent updating client centres."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_client_centres()
    await communicator.receive_json_from()  # Creation of client centres
    await check_client_centres()
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_client_delete():
    """Validate websocket message is sent when deleting a client."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await delete_client()
    response = await communicator.receive_json_from()
    assert response["type"] == "update"
    assert response["action"] == "deleteClient"
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_client_next_of_kin():
    """Validate websocket message is sent when change client next of kin."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await add_client_next_of_kin()
    response = await communicator.receive_json_from()
    assert response["type"] == "update"
    assert response["action"] == "updateClient"
    assert response["data"]["nextOfKin"][0]["name"] == "Test"
    assert response["data"]["nextOfKin"][0]["phoneNumber"] == "1111"
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_client_medications():
    """Validate websocket message is sent when change client medications."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    test_client, med = await add_client_medications()
    response = await communicator.receive_json_from()
    assert response["type"] == "update"
    assert response["action"] == "updateClient"
    assert response["data"]["medications"] == [
        {
            "id": med.id,
            "name": "Test Med",
            "dosage": "100mg",
            "frequency": "weekly",
            "inactiveDate": "2012-09-02",
            "client": test_client.id,
        }
    ]
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_not_updated_for_admins():
    """Validate websocket message is not sent to admins."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_client_centres()
    await communicator.receive_json_from()  # Update centre
    with pytest.raises(AsyncTimeoutError):
        await communicator.receive_json_from()  # Update client centre
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_updated_care_plan():
    """Validate websocket message is sent when change care plan."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    (care_plan, client_need, client, user) = await update_care_plan()
    await communicator.receive_json_from()  # creating centre
    await communicator.receive_json_from()  # creating client need
    response = await communicator.receive_json_from()
    assert response == {
        "type": "update",
        "action": "updateCarePlan",
        "data": {
            "id": care_plan.id,
            "date": "2011-08-12",
            "need": client_need.id,
            "comment": COMMENT,
            "completed": False,
            "inputted": False,
            "client": client.id,
            "addedBy": user.id,
        },
    }
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_consumer_create_attendance_record():
    """Validate websocket message is sent when creating attendance record."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nurse"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    (care_plan, client_need, _, _) = await update_care_plan()
    (attendance_record, client, centre, user) = await create_attendance_record()
    await communicator.receive_json_from()  # updating client with new client need
    await communicator.receive_json_from()  # creating care_plan
    await communicator.receive_json_from()  # update care_plan

    create_attendance_response = await communicator.receive_json_from()
    delete_care_plans_reponse = await communicator.receive_json_from()
    assert create_attendance_response == {
        "type": "update",
        "action": "createAttendanceRecords",
        "data": [
            {
                "id": attendance_record.data[0]["id"],
                "date": "2011-08-12",
                "client": client.id,
                "centre": centre.id,
                "reasonForInactivity": "",
                "reasonForAbsence": "example reason for absence",
                "active": True,
                "addedBy": user.id,
            }
        ],
    }
    assert delete_care_plans_reponse == {
        "type": "update",
        "action": "deleteCarePlans",
        "data": [
            {
                "id": care_plan.id,
                "date": "2011-08-12",
                "need": client_need.id,
                "comment": COMMENT,
                "completed": False,
                "inputted": False,
                "client": client.id,
                "addedBy": user.id,
            }
        ],
    }
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_post_client_permission_update():
    """Validate websocket message is not sent to client."""
    user_tokens = await tokens()
    communicator = WebsocketCommunicator(
        APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["client"]}'
    )
    connected, _ = await communicator.connect()
    assert connected
    await update_client_permissions()
    permissions_response = (
        await communicator.receive_json_from()
    )  # Update client permissions
    assert permissions_response == {
        "type": "update",
        "action": "updateClientPermissions",
        "data": {
            "clientId": permissions_response["data"]["clientId"],
            "permissions": ["client_can_view_calendar"],
        },
    }
    await communicator.disconnect()
