"""Model and serializer validators."""
from pathlib import Path

from django.conf import settings
from rest_framework.serializers import ValidationError


def validate_need_in_centres(need, centre):
    """Validate the need exists in the selected centres."""
    if need not in list(centre.needs.all()):
        raise ValidationError(
            (
                "The provided need was not found in the provided centre. "
                f"Centre: {centre.name}. "
                f"Need: {need.description}."
            )
        )


def validate_visiting_day_in_centres(day, centres):
    """Validate the visiting day exists in the selected centres opening days."""
    if day["day"] not in list(day["centre"].openingDays.all()):
        raise ValidationError(
            "The provided visiting day was not found in the provided centre"
        )
    if day["centre"] not in list(centres):
        raise ValidationError(
            "None of this clients subscribed centres have this opening day."
        )


def validate_space_left_for_file(document):
    """Prevent creation of new files if limit is being passes."""
    root_directory = Path(settings.MEDIA_ROOT)
    folder_size = sum(
        f.stat().st_size for f in root_directory.glob("**/*") if f.is_file()
    )
    if (document.size + folder_size) > settings.MAX_UPLOAD_FOLDER_SIZE:
        raise ValidationError("No space left in upload folder for this file.")


def validate_file_size(document):
    """Prevent creation of new files if they are larger than the set size."""
    if document.size > settings.MAX_UPLOAD_SIZE:
        raise ValidationError("Document must be under 100Mib")
