"""Django models are python object representations of database models."""

from __future__ import unicode_literals

from django.db import models

from ..authentication.models import CustomUser
from ..centre.models import Centre, Day, Need, Policy


class Client(models.Model):
    """The Client model contains data on clients."""

    admittedBy = models.ForeignKey(
        CustomUser, on_delete=models.DO_NOTHING, related_name="+"
    )
    createdDate = models.DateTimeField(auto_now_add=True)
    firstName = models.CharField(verbose_name="First Name", max_length=128)
    surname = models.CharField(verbose_name="Surname", max_length=256)
    genderChoices = [
        ("Unknown", "Unknown"),
        ("Male", "Male"),
        ("Female", "Female"),
    ]
    gender = models.CharField(
        max_length=7,
        choices=genderChoices,
        default="Unknown",
    )
    clientAgreedPolicies = models.ManyToManyField(
        Policy, through="ClientAgreedPolicies"
    )
    clientVisitingDays = models.ManyToManyField(Day, through="ClientVisitingDays")
    centres = models.ManyToManyField(Centre)
    isActive = models.BooleanField(default=True)
    setupComplete = models.BooleanField(default=False)
    basicInfoSetup = models.BooleanField(default=False)
    additionalInfoSetup = models.BooleanField(default=False)
    servicesSetup = models.BooleanField(default=False)
    abilitySetup = models.BooleanField(default=False)
    dietSetup = models.BooleanField(default=False)
    visitingDaysSetup = models.BooleanField(default=False)
    dateOfBirth = models.DateField(null=True, blank=True)
    address = models.CharField(max_length=512, blank=True)
    phoneNumber = models.CharField(max_length=20, blank=True)
    publicHealthNurseName = models.CharField(max_length=256, blank=True)
    publicHealthNursePhoneNumber = models.CharField(max_length=20, blank=True)
    homeHelpName = models.CharField(max_length=256, blank=True)
    homeHelpPhoneNumber = models.CharField(max_length=20, blank=True)
    gpName = models.CharField(max_length=256, blank=True)
    gpPhoneNumber = models.CharField(max_length=20, blank=True)
    chemistName = models.CharField(max_length=256, blank=True)
    chemistPhoneNumber = models.CharField(max_length=20, blank=True)
    medicalHistory = models.TextField(blank=True)
    surgicalHistory = models.TextField(blank=True)
    walksUnaided = "Walks Unaided"
    mobilityChoices = [
        (walksUnaided, walksUnaided),
        ("Walks With Stick", "Walks With Stick"),
        ("Walks With Frame", "Walks With Frame"),
        ("Walks With Walker", "Walks With Walker"),
        ("Uses Wheelchair", "Uses Wheelchair"),
    ]
    mobility = models.CharField(
        max_length=17,
        choices=mobilityChoices,
        default=walksUnaided,
    )
    fullyContinent = "Fully Continent"
    toiletingChoices = [
        (fullyContinent, fullyContinent),
        ("Occasionally Incontinent", "Occasionally Incontinent"),
        ("Incontinent", "Incontinent"),
    ]
    toileting = models.CharField(
        max_length=24,
        choices=toiletingChoices,
        default=fullyContinent,
    )
    selfHygiene = "Self"
    hygieneChoices = [
        (selfHygiene, selfHygiene),
        ("Needs Assistance", "Needs Assistance"),
        ("Full Hoist Shower", "Full Hoist Shower"),
    ]
    hygiene = models.CharField(
        max_length=17,
        choices=hygieneChoices,
        default=selfHygiene,
    )
    normal = "Normal"
    impaired = "Impaired"
    sightChoices = [
        (normal, normal),
        ("Glasses", "Glasses"),
        (impaired, impaired),
    ]
    sight = models.CharField(
        max_length=8,
        choices=sightChoices,
        default=normal,
    )
    hearingChoices = [
        (normal, normal),
        (impaired, impaired),
    ]
    hearing = models.CharField(
        max_length=8,
        choices=hearingChoices,
        default=normal,
    )
    usesDentures = models.BooleanField(default=False)
    caseFormulation = models.TextField(blank=True)
    allergies = models.TextField(blank=True)
    intolerances = models.TextField(blank=True)
    requiresAssistanceEating = models.BooleanField(default=False)
    thicknerGrade = models.IntegerField(blank=True, default=0)
    clientNeeds = models.ManyToManyField(Need, through="ClientNeed", blank=True)
    storageSize = models.BigIntegerField(blank=True, default=0)
    reasonForInactivityChoices = [
        ("Deceased", "Deceased"),
        ("Entered Long Term Care", "Entered Long Term Care"),
        ("No Longer Attending", "No Longer Attending"),
        ("Needs Unmet By Service", "Needs Unmet By Service"),
    ]
    reasonForInactivity = models.CharField(
        max_length=22,
        choices=reasonForInactivityChoices,
        default="Deceased",
    )

    class Meta:
        verbose_name = "Client"
        verbose_name_plural = "Clients"

    @property
    def accountStatus(self):  # noqa: PLC0103
        """Return status of clientUser."""
        if not hasattr(self, "user"):
            if self.tempCode.exists():
                return "Pending"
            return "No Account"
        if self.user.is_active:  # noqa: PLE1101
            return "Active"
        return "Inactive"


class NextOfKin(models.Model):
    """A next of kin for a client."""

    name = models.CharField(max_length=256)
    phoneNumber = models.CharField(max_length=20)
    client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="nextOfKin"
    )

    class Meta:
        verbose_name = "Next Of Kin"
        verbose_name_plural = "Next Of Kin"


class FeedingRisk(models.Model):
    """A feeding risk for a client."""

    chewing = "Chewing"
    feedingRisksChoices = [
        (chewing, chewing),
        ("Swallowing", "Swallowing"),
        ("Poor Appetite", "Poor Appetite"),
    ]
    feedingRisk = models.CharField(
        max_length=13, choices=feedingRisksChoices, default=chewing
    )
    client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="feedingRisks"
    )

    class Meta:
        verbose_name = "Feeding Risk"
        verbose_name_plural = "Feeding Risks"


class Medication(models.Model):
    """A medication client."""

    name = models.CharField(max_length=256)
    dosage = models.CharField(max_length=20)
    frequency = models.CharField(max_length=40)
    inactiveDate = models.DateField(null=True, blank=True)
    client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="medications"
    )

    class Meta:
        verbose_name = "Medication"
        verbose_name_plural = "Medications"


class ClientAgreedPolicies(models.Model):
    """A junction object for a clients agreed policies for a specific centre."""

    client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="agreedPolicies"
    )
    policy = models.ForeignKey(
        Policy, on_delete=models.CASCADE, related_name="policyToClient"
    )
    centre = models.ForeignKey(Centre, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Client Agreed Policy"
        verbose_name_plural = "Client Agreed Policies"


class ClientNeed(models.Model):
    """A junction object for a clients needs for a specific centre."""

    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="needs")
    need = models.ForeignKey(
        Need, on_delete=models.CASCADE, related_name="needToClient"
    )
    centre = models.ForeignKey(Centre, on_delete=models.CASCADE)
    plan = models.TextField(blank=True)
    isActive = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Client Need"
        verbose_name_plural = "Client Needs"


class ClientVisitingDays(models.Model):
    """A junction object for a clients visiting days for a specific centre."""

    client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="visitingDays"
    )
    day = models.ForeignKey(Day, on_delete=models.CASCADE, related_name="dayToClient")
    centre = models.ForeignKey(Centre, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Client Visiting Day"
        verbose_name_plural = "Client Visiting Days"


class ClientDocument(models.Model):
    """A file relating to a client."""

    client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="documents"
    )
    name = models.CharField(max_length=72)
    document = models.FileField()
    createdDate = models.DateTimeField(auto_now_add=True)
    originalName = models.CharField(max_length=512)
    size = models.BigIntegerField()

    class Meta:
        verbose_name = "Client Document"
        verbose_name_plural = "Client Documents"


class CarePlan(models.Model):
    """An update to a clients care progress."""

    date = models.DateField()
    need = models.ForeignKey(
        ClientNeed, on_delete=models.CASCADE, related_name="carePlans"
    )
    comment = models.TextField(blank=True)
    completed = models.BooleanField(default=False)
    inputted = models.BooleanField(default=False)
    addedBy = models.ForeignKey(
        CustomUser, on_delete=models.DO_NOTHING, blank=True, null=True
    )

    class Meta:
        verbose_name = "Care Plan"
        verbose_name_plural = "Care Plans"


class AttendanceRecord(models.Model):
    """An attendance record for a client for a centre."""

    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    centre = models.ForeignKey(Centre, on_delete=models.CASCADE)
    date = models.DateField()
    active = models.BooleanField(default=True)
    reasonForInactivity = models.TextField(blank=True)
    reasonForAbsence = models.TextField(blank=True)
    addedBy = models.ForeignKey(
        CustomUser, on_delete=models.DO_NOTHING, blank=True, null=True
    )

    class Meta:
        verbose_name = "Attendance Record"
        verbose_name_plural = "Attendance Records"
