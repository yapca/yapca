"""Django settings for the yapca backend project."""

import os
from datetime import timedelta
from pathlib import Path

from environs import Env, EnvError

ENV_PATH = str(
    Path(__file__).resolve().parent.parent.parent
    / "config"
    / f'{os.getenv("YAPCA_ENV", "local")}.env'
)
ENV = Env()
ENV.read_env(ENV_PATH)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = ENV("DJANGO_SECRET_KEY")

DEBUG = ENV.bool("DJANGO_DEBUG")
SESSION_COOKIE_SECURE = ENV.bool("SESSION_COOKIE_SECURE")
CSRF_COOKIE_SECURE = ENV.bool("CSRF_COOKIE_SECURE")
X_FRAME_OPTIONS = "DENY"
SECURE_HSTS_SECONDS = 3600
ALLOWED_HOSTS = [ENV("VUE_APP_API_HOST"), "127.0.0.1", "localhost"]
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.sites",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "rest_framework",
    "rest_framework.authtoken",
    "dj_rest_auth",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "corsheaders",
    "recurrence",
    "app.administration",
    "app.authentication",
    "app.websockets",
    "channels",
    "django_q",
    "axes",
    "app.client.apps.ClientAppConfig",
    "app.centre.apps.CentreAppConfig",
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "axes.middleware.AxesMiddleware",
]

ROOT_URLCONF = "app.urls"

CORS_ORIGIN_ALLOW_ALL = ENV.bool("CORS_ORIGIN_ALLOW_ALL")
CSRF_TRUSTED_ORIGINS = [
    f'http://{ENV("FRONTEND_INTEGRATION_TEST_HOST")}:{ENV("FRONTEND_PORT")}',
    f'https://{ENV("FRONTEND_INTEGRATION_TEST_HOST")}:{ENV("FRONTEND_PORT")}',
    f'http://{ENV("FRONTEND_HOST")}:{ENV("FRONTEND_PORT")}',
    f'https://{ENV("FRONTEND_HOST")}:{ENV("FRONTEND_PORT")}',
    f'http://127.0.0.1:{ENV("FRONTEND_PORT")}',
    f'https://127.0.0.1:{ENV("FRONTEND_PORT")}',
    f'http://localhost:{ENV("FRONTEND_PORT")}',
    f'https://localhost:{ENV("FRONTEND_PORT")}',
]
CORS_ALLOWED_ORIGINS = CSRF_TRUSTED_ORIGINS
for frontend_host in ENV.json("FRONTEND_HOSTS"):
    CSRF_TRUSTED_ORIGINS.append(f'http://{frontend_host}:{ENV("FRONTEND_PORT")}')
    CSRF_TRUSTED_ORIGINS.append(f'https://{frontend_host}:{ENV("FRONTEND_PORT")}')
    ALLOWED_HOSTS.append(frontend_host)


TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "app.wsgi.application"
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": ENV("POSTGRES_DB"),
        "USER": ENV("POSTGRES_USER"),
        "PASSWORD": ENV("POSTGRES_PASSWORD"),
        "HOST": ENV("POSTGRES_HOST"),
        "PORT": ENV("POSTGRES_PORT"),
    }
}

LANGUAGE_CODE = "en-us"
TIME_ZONE = "Europe/Dublin"
USE_I18N = True
USE_TZ = True

STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"),)
STATIC_URL = "/static/"
try:
    MEDIA_ROOT = ENV("MEDIA_ROOT")
except EnvError:
    MEDIA_ROOT = os.path.join(BASE_DIR, ENV("MEDIA_FOLDER"))
MEDIA_URL = "/media/"
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {"hosts": [(ENV("CACHE_HOST"), ENV("CACHE_PORT"))]},
    }
}
LOGOUT_REDIRECT_URL = "/"

AUTHENTICATION_BACKENDS = [
    "axes.backends.AxesBackend",
    "django.contrib.auth.backends.ModelBackend",
]

ACCOUNT_AUTHENTICATION_METHOD = "username"

ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = True
ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_ADAPTER = "app.authentication.adapter.CustomAccountAdapter"
AUTH_USER_MODEL = "authentication.CustomUser"


REG_SERIALIZER = "app.authentication.serializers.CustomRegisterSerializer"
LOG_SERIALIZER = "app.authentication.serializers.CustomLoginSerializer"

REST_AUTH_REGISTER_SERIALIZERS = {"REGISTER_SERIALIZER": REG_SERIALIZER}
REST_AUTH_SERIALIZERS = {
    "LOGIN_SERIALIZER": LOG_SERIALIZER,
    "USER_DETAILS_SERIALIZER": "app.authentication.serializers.UserSerializer",
    "TOKEN_SERIALIZER": "app.authentication.serializers.TokenSerializer",
}
REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework.authentication.TokenAuthentication"
    ],
    "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.IsAuthenticated"],
}
DJANGO_SITE = 1
SITE_ID = 1
ASGI_APPLICATION = "app.routing.APPLICATION"
LOGOUT_ON_PASSWORD_CHANGE = False
OLD_PASSWORD_FIELD_ENABLED = True
MAX_UPLOAD_SIZE = ENV.int("VUE_APP_MAX_FILE_SIZE")
MAX_UPLOAD_FOLDER_SIZE = ENV.int("VUE_APP_MAX_STORAGE")
Q_CLUSTER = {"name": "yapca_q_cluster", "orm": "default", "retry": 180, "timeout": 120}
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"
ACCOUNT_EMAIL_VERIFICATION = "none"

BACKEND_PORT = ENV.int("VUE_APP_API_PORT")
BACKEND_PROTOCOL = ENV("VUE_APP_API_PROTOCOL")
BACKEND_HOST = ENV("VUE_APP_API_HOST")

WEB_VERSION_EXISTS = ENV.bool("WEB_VERSION_EXISTS")
APP_VERSION_EXISTS = ENV.bool("APP_VERSION_EXISTS")
FRONTEND_HOST = ENV("FRONTEND_HOST")
FRONTEND_BASE_PATH = ENV("FRONTEND_BASE_PATH")
FRONTEND_PORT = ENV.int("FRONTEND_PORT")
FRONTEND_PROTOCOL = ENV("FRONTEND_PROTOCOL")

# Email settings
EMAIL_HOST = ENV("EMAIL_HOST")
EMAIL_PORT = ENV.int("EMAIL_PORT")
EMAIL_HOST_USER = ENV("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = ENV("EMAIL_HOST_PASSWORD")
EMAIL_USE_TLS = ENV.bool("EMAIL_USE_TLS")
EMAIL_USE_SSL = ENV.bool("EMAIL_USE_SSL")
DEFAULT_FROM_EMAIL = ENV("DEFAULT_FROM_EMAIL")


AXES_COOLOFF_TIME = timedelta(minutes=10)
AXES_FAILURE_LIMIT = 8
CACHE_HOST = ENV("CACHE_HOST")
CACHE_PORT = ENV("CACHE_PORT")

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": f"redis://{CACHE_HOST}:{CACHE_PORT}/1",
        "OPTIONS": {"CLIENT_CLASS": "django_redis.client.DefaultClient"},
    }
}
API_PATH = ENV("VUE_APP_API_PATH")
