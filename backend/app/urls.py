"""Root URLs routes."""

from django.conf.urls import include
from django.urls import re_path

urlpatterns = [  # pylint: disable=C0103
    re_path(r"api/", include("app.administration.urls"), name="administration"),
    re_path(r"api/", include("app.authentication.urls"), name="auth"),
    re_path(r"api/", include("app.centre.urls"), name="centre"),
    re_path(r"api/", include("app.client.urls"), name="client"),
]
