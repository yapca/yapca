"""Override default user creation."""

from allauth.account.adapter import DefaultAccountAdapter

from .models import TempCode


class CustomAccountAdapter(DefaultAccountAdapter):
    """Override default user creation to add extra fields."""

    def save_user(self, request, user, form, commit=False):
        """Override save_user function."""
        user = super().save_user(request, user, form, commit)
        data = form.cleaned_data
        user.darkTheme = data.get("darkTheme")
        user.displayName = data.get("displayName")
        user.accentColor = data.get("accentColor")
        user.otpDeviceKey = data.get("otpDeviceKey")
        if "tempCode" in request.data:
            temp_code = TempCode.objects.filter(code=request.data["tempCode"])[0]
            if temp_code.action == "client-registration":
                user.client = temp_code.associatedClient
        user.save()
        return user
