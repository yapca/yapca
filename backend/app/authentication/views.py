"""A view is a function that takes a Web request and returns a Web response."""

import secrets
import string
from datetime import timedelta

from dj_rest_auth.registration.views import RegisterView
from dj_rest_auth.views import LoginView
from django.conf import settings
from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404
from django.utils import timezone
from requests import get
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from ..administration.models import RegisterAbility
from ..centre.models import Centre
from ..client.models import Client
from ..client.permissions import CanEditClient
from ..client.signals import update_client_websocket
from . import models, serializers
from .permissions import (
    CanRegister,
    IsAdmin,
    IsClient,
    IsOwnClient,
    IsUser,
    IsUserOrAdmin,
    user_is_client,
)
from .signals import save_user_centres
from .utils import EmailThread, get_email_templates


class UserRetreiveView(generics.RetrieveAPIView):
    """Get all user objects from database and return for serializer."""

    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = [IsUser]


class UserListDisplayNameView(generics.ListAPIView):
    """Get all user display names from database and return for serializer."""

    serializer_class = serializers.UserDisplayNameSerializer
    permission_classes = [IsClient]

    def get_queryset(self):
        """Filter user to only users in shared centres if not admin."""
        user = self.request.user
        if user_is_client(user):
            user_centres = user.client.centres.all()
        else:
            user_centres = user.centres.all()
        return (
            models.CustomUser.objects.filter(centres__in=user_centres)
            .distinct()
            .order_by("id")
        )


class UserListSharedDataView(generics.ListAPIView):
    """Get all user shared data from database and return for serializer."""

    queryset = models.CustomUser.objects.all().order_by("id")
    serializer_class = serializers.UserListSharedDataSerializer
    permission_classes = [IsAdmin]


class GroupListView(generics.ListAPIView):
    """Get all group objects from database and return for serializer."""

    queryset = Group.objects.all()
    serializer_class = serializers.GroupSerializer
    permission_classes = [IsAdmin]


def send_email_on_register(user, is_client=False):
    """Send signup email on registration."""
    if is_client:
        display_name = user.client.firstName
    else:
        display_name = user.displayName
    context = {"displayName": display_name}
    template = get_email_templates("welcome", context)
    EmailThread(
        "Welcome to YAPCA", template["plain"], template["html"], [user.email]
    ).start()


class CustomRegisterView(RegisterView):
    """Allow a user to register based on the CustomUser object."""

    queryset = models.CustomUser.objects.all()

    def perform_create(self, serializer):
        """
        Overwrite create method.

        If CanRegister is true, set canRegister to false and allow registration.
        This means the first user is able to register without generating an email code.
        All future users will require validating their email code.
        """
        temp_code = None
        if CanRegister.has_permission(self, self.request, CustomRegisterView):
            can_register_object = RegisterAbility.objects.first()
            can_register_object.canRegister = False
            can_register_object.save()
        else:
            if "tempCode" not in self.request.data or not self.request.data["tempCode"]:
                raise ValidationError(
                    {"tempCode": "This field is required."}, code="required"
                )
            temp_code = validate_temp_code(
                self.request.data["tempCode"],
                None,
                "registration",
                self.request.data["email"],
            )
        user = super().perform_create(serializer)
        if temp_code:
            user.centres.add(*temp_code.centres.all())
            temp_code.delete()
        send_email_on_register(user)
        return user


class ClientRegisterView(RegisterView):
    """Allow a client to register based on the CustomUser object."""

    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.ClientRegisterSerializer

    def perform_create(self, serializer):
        """Overwrite create method."""
        temp_code = validate_temp_code(
            self.request.data["tempCode"],
            None,
            "client-registration",
            self.request.data["email"],
        )
        client_user = super().perform_create(serializer)
        client_user.permissions.set(temp_code.permissions.all())
        temp_code.delete()
        send_email_on_register(client_user, True)
        update_client_websocket(client_user.client, filter_keys=["accountStatus"])
        return client_user


class CustomLoginView(LoginView):
    """Allow a user to login based on the CustomUser object."""

    queryset = models.CustomUser.objects.all()


class UserUpdate(generics.UpdateAPIView):
    """Update User objects."""

    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = [IsUserOrAdmin]


def validate_temp_code(code, username, action, email):
    """Check if temp code is valid for current time, user/email and action."""
    try:
        return models.TempCode.objects.get(
            associatedUser__username=username or None,
            code=code,
            action=action,
            startTime__lte=timezone.now(),
            endTime__gte=timezone.now(),
            email=email or None,
        )
    except models.TempCode.DoesNotExist as error:
        raise ValidationError(
            {"error": "Code/User is invalid or outdated"}, code=400
        ) from error


class ValidateTempCode(APIView):
    """Check if a temporary code is valid."""

    queryset = models.TempCode.objects.all()
    permission_classes = [AllowAny]
    serializer_class = serializers.ValidateTempCode

    def post(self, request):
        """Return success or fail if valid."""
        serializer = serializers.ValidateTempCode(data=request.data)
        serializer.is_valid(raise_exception=True)
        code = self.request.data.get("code", None)
        username = self.request.data.get("username", None)
        action = self.request.data.get("action", None)
        email = self.request.data.get("email", None)
        validate_temp_code(code, username, action, email)
        return Response(status=200)


class ResetTotp(APIView):
    """Reset a users TOTP security key."""

    queryset = models.TempCode.objects.all()
    permission_classes = [AllowAny]
    serializer_class = serializers.ResetTotpKey

    def put(self, request):
        """
        Change a users TOTP code.

        First validate code then change users otpDeviceKey.
        Finally delete the tempCode object.
        """
        serializer = serializers.ResetTotpKey(data=request.data)
        serializer.is_valid(raise_exception=True)
        code = self.request.data.get("code", None)
        username = self.request.data.get("username", None)
        otp_secret = self.request.data.get("otpSecret", None)
        temp_code = validate_temp_code(code, username, "reset-totp", None)
        user = models.CustomUser.objects.get(username=username)
        user.otpDeviceKey = otp_secret
        user.save()
        temp_code.delete()
        return Response(status=200)


class ResetPaswordTempCode(APIView):
    """Reset a users password."""

    queryset = models.TempCode.objects.all()
    permission_classes = [AllowAny]
    serializer_class = serializers.ResetPasswordTempCode

    def put(self, request):
        """
        Change a users password code.

        First validate code then change users password.
        Finally delete the tempCode object.
        """
        serializer = serializers.ResetPasswordTempCode(data=request.data)
        serializer.is_valid(raise_exception=True)
        code = self.request.data.get("code", None)
        username = self.request.data.get("username", None)
        password = self.request.data.get("password", None)
        temp_code = validate_temp_code(code, username, "reset-password", None)
        user = models.CustomUser.objects.get(username=username)
        user.set_password(password)
        user.save()
        temp_code.delete()
        return Response(status=200)


def get_latest_app_versions():
    """Check what the latest app versions are."""
    response = get(
        "https://gitlab.com/api/v4/projects/16681524/releases",
        headers={"Accept": "*/*"},
        timeout=5,
    )
    links = response.json()[0]["assets"]["links"]
    windows_link = ""
    for link_object in links:
        if link_object["name"] == "Windows Release":
            windows_link = link_object["url"]
    linux_link = response.json()[0]["_links"]["self"]
    return {"windows": windows_link, "linux": linux_link}


def generate_temp_code(data, centres=None, permissions=None):
    """Generate a temp code."""
    code = "".join(
        secrets.SystemRandom().choice(
            string.ascii_uppercase + string.ascii_lowercase + string.digits
        )
        for _ in range(32)
    )
    tempcode = models.TempCode.objects.create(
        associatedUser=data["user"],
        associatedClient=data["client"],
        code=code,
        action=data["action"],
        startTime=timezone.now(),
        endTime=timezone.now() + timedelta(days=1),
        email=data["email"],
    )
    if permissions:
        tempcode.permissions.set(permissions)
    if centres:
        for centre_id in centres:
            centre = Centre.objects.get(id=centre_id)
            tempcode.centres.add(centre)
    return tempcode


def temp_code_exists(user, action):
    """Return true or false if a temp code is active for the user with this action."""
    return models.TempCode.objects.filter(
        associatedUser=user,
        action=action,
        startTime__lte=timezone.now(),
        endTime__gte=timezone.now(),
    ).exists()


def get_frontend_address():
    """Get frontend address from settings."""
    frontend_host_address = ""
    if settings.WEB_VERSION_EXISTS:
        frontend_host_address = (
            settings.FRONTEND_PROTOCOL + "://" + settings.FRONTEND_HOST
        )
        if settings.FRONTEND_PORT and settings.FRONTEND_PORT != 80:
            frontend_host_address = (
                frontend_host_address + ":" + str(settings.FRONTEND_PORT)
            )
        frontend_host_address = frontend_host_address + settings.FRONTEND_BASE_PATH
    return frontend_host_address


def get_backend_address():
    """Get backend address from settings."""
    backend_host_address = ""
    if settings.WEB_VERSION_EXISTS:
        if not settings.BACKEND_PORT or settings.BACKEND_PORT == 80:
            backend_host_address = (
                settings.BACKEND_PROTOCOL + "://" + settings.BACKEND_HOST
            )
        else:
            backend_host_address = (
                settings.BACKEND_PROTOCOL
                + "://"
                + settings.BACKEND_HOST
                + ":"
                + str(settings.BACKEND_PORT)
            )
    return backend_host_address


def send_reset_email(action, user, subject):
    """Send email to user to reset password/email etc."""
    links = get_latest_app_versions()
    if temp_code_exists(user, action):
        return False
    temp_code = generate_temp_code(
        {"user": user, "client": None, "action": action, "email": None}
    )
    context = {
        "displayName": user.displayName,
        "links": links,
        "server_address": get_backend_address(),
        "temp_code": temp_code.code,
        "username": user.username,
        "web_version_exists": settings.WEB_VERSION_EXISTS,
        "app_version_exists": settings.APP_VERSION_EXISTS,
        "frontend_address": get_frontend_address(),
        "expiry_time": temp_code.endTime,
        "action": action,
    }
    template = get_email_templates(action, context)
    EmailThread(
        subject,
        template["plain"],
        template["html"],
        [user.email],
    ).start()
    return True


def send_client_registraion_email(client, email, permissions):
    """Send email to client to allow them to register."""
    links = get_latest_app_versions()
    temp_code = generate_temp_code(
        {
            "user": None,
            "client": client,
            "action": "client-registration",
            "email": email,
        },
        permissions=permissions,
    )
    context = {
        "clientName": client.firstName,
        "links": links,
        "server_address": get_backend_address(),
        "temp_code": temp_code.code,
        "web_version_exists": settings.WEB_VERSION_EXISTS,
        "app_version_exists": settings.APP_VERSION_EXISTS,
        "frontend_address": get_frontend_address(),
        "email": email,
        "expiry_time": temp_code.endTime,
    }
    template = get_email_templates("client-registration", context)
    EmailThread(
        "You've been invited to YAPCA!",
        template["plain"],
        template["html"],
        [email],
    ).start()


def send_registraion_email(display_name, email, centres=None):
    """Send email to user to allow them to register."""
    links = get_latest_app_versions()
    temp_code = generate_temp_code(
        {"user": None, "client": None, "action": "registration", "email": email},
        centres=centres,
    )
    context = {
        "displayName": display_name,
        "links": links,
        "server_address": get_backend_address(),
        "temp_code": temp_code.code,
        "web_version_exists": settings.WEB_VERSION_EXISTS,
        "app_version_exists": settings.APP_VERSION_EXISTS,
        "frontend_address": get_frontend_address(),
        "email": email,
        "expiry_time": temp_code.endTime,
    }
    template = get_email_templates("registration", context)
    return EmailThread(
        "You've been invited to YAPCA!",
        template["plain"],
        template["html"],
        [email],
    ).start()


class RequestPasswordReset(APIView):
    """Request a password reset email be sent."""

    queryset = models.CustomUser.objects.all()
    permission_classes = [AllowAny]
    serializer_class = serializers.RequestReset

    def get(self, request):
        """
        Request email.

        Return 200 either way.
        Check if tempCode exists, if true do not send email.
        """
        query = serializers.RequestReset(data=request.query_params)
        query.is_valid(raise_exception=True)
        email = self.request.query_params.get("email", None)
        try:
            user = models.CustomUser.objects.get(email=email)
            send_reset_email("reset-password", user, "Password Reset Request")
            return Response(status=200)
        except models.CustomUser.DoesNotExist:
            # Return a 200 even if user is not found
            # So an attacker can't tell a user from a non-user email
            return Response(status=200)


class RequestTOTPReset(APIView):
    """Request a TOTP reset email be sent."""

    queryset = models.CustomUser.objects.all()
    permission_classes = [IsAdmin]
    serializer_class = serializers.RequestReset

    def get(self, request):
        """
        Request email.

        Return error when user not found
        Check if tempCode exists, if true do not send email.
        """
        query = serializers.RequestReset(data=request.query_params)
        query.is_valid(raise_exception=True)
        user_id = self.request.query_params.get("userId", None)
        try:
            user = models.CustomUser.objects.get(id=user_id)
            if not send_reset_email("reset-totp", user, "2FA Device Reset Request"):
                raise ValidationError(
                    {"error": "User was already sent email in last 24 hours"}, code=400
                )
            return Response(status=200)
        except models.CustomUser.DoesNotExist as error:
            raise ValidationError({"error": "User was not found"}, code=400) from error


class RequestRegistrationEmail(APIView):
    """Request a registration email be sent."""

    queryset = models.CustomUser.objects.all()
    permission_classes = [IsAdmin]
    serializer_class = serializers.RequestRegistrationEmail

    def post(self, request):
        """Request email."""
        query = serializers.RequestRegistrationEmail(data=request.data)
        query.is_valid(raise_exception=True)
        email = request.data.get("email", None)
        centres = request.data.get("centres", None)
        display_name = request.data.get("displayName", None)
        send_registraion_email(display_name, email, centres)
        models.UserRegistrationRequestRecord.objects.create(email=email)
        return Response(status=200)


class RequestClientRegistrationEmail(APIView):
    """Request a registration email be sent for a ClientUser."""

    queryset = models.CustomUser.objects.all()
    permission_classes = [CanEditClient]
    serializer_class = serializers.RequestClientRegistrationEmail

    def post(self, request, *args, **kwargs):
        """Request email."""
        client_id = kwargs.get("pk")
        client = Client.objects.get(id=client_id)

        query = serializers.RequestClientRegistrationEmail(data=request.data)
        query.is_valid(raise_exception=True)
        email = request.data.get("email", None)
        can_view_calendar = request.data.get("canViewCalendar", False)
        can_view_own_data = request.data.get("canViewOwnData", False)
        permissions = []
        if can_view_calendar:
            permissions.append(
                models.UserPermission.objects.get(codename="client_can_view_calendar")
            )
        if can_view_own_data:
            permissions.append(
                models.UserPermission.objects.get(codename="client_can_view_own_data")
            )
        send_client_registraion_email(client, email, permissions)
        models.UserRegistrationRequestRecord.objects.create(email=email)
        update_client_websocket(client, filter_keys=["accountStatus"])
        return Response(status=200)


def user_centre_change_base(request):
    """Return objects or 404."""
    serializer = serializers.UserCentresSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    centre_id = request.data.get("centre", None)
    user_id = request.data.get("user", None)
    centre = get_object_or_404(Centre, pk=centre_id)
    user = get_object_or_404(models.CustomUser, pk=user_id)
    return (user, centre)


class UserCentresUpdate(APIView):
    """Update user centres."""

    serializer_class = serializers.UserCentresSerializer
    permission_classes = [IsAdmin]

    def post(self, request):
        """Add a users centre."""
        user, centre = user_centre_change_base(request)
        if centre not in user.centres.all():
            user.centres.add(centre)
            new_centre_ids = list(user.centres.all().values_list("pk", flat=True))
            save_user_centres(new_centre_ids, user.id)
            return Response(
                {"user": user.id, "centres": new_centre_ids},
                status=status.HTTP_200_OK,
            )
        return Response(
            "Failed to add centre, user is already subscribed to this centre.",
            status=status.HTTP_400_BAD_REQUEST,
        )

    def delete(self, request):
        """Remove a users centre."""
        user, centre = user_centre_change_base(request)
        if centre in user.centres.all():
            user.centres.remove(centre)
            new_centre_ids = list(user.centres.all().values_list("pk", flat=True))
            save_user_centres(new_centre_ids, user.id)
            return Response(
                {"user": user.id, "centres": new_centre_ids},
                status=status.HTTP_200_OK,
            )
        return Response(
            "Failed to remove centre, user is not subscribed to this centre.",
            status=status.HTTP_400_BAD_REQUEST,
        )


class UserCentresSet(APIView):
    """Set user centres."""

    serializer_class = serializers.UserCentreSetSerializer
    permission_classes = [IsAdmin]

    def post(self, request):
        """Add a list of centres."""
        serializer = serializers.UserCentreSetSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        centre_ids = request.data.get("centres", None)
        centres = []
        for centre_id in centre_ids:
            centre = get_object_or_404(Centre, pk=centre_id)
            centres.append(centre)
        request.user.centres.add(*centres)
        new_centre_ids = list(request.user.centres.all().values_list("pk", flat=True))
        save_user_centres(new_centre_ids, request.user.id)
        return Response(
            {"centres": new_centre_ids},
            status=status.HTTP_200_OK,
        )


class DisableTOTP(APIView):
    """Disable TOTP."""

    permission_classes = [IsOwnClient]

    def get(self, request):
        """Disable TOTP for user."""
        request.user.otpDeviceKey = None
        request.user.save()
        return Response(
            status=status.HTTP_200_OK,
        )
