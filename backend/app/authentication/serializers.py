"""Serializers allow querysets and model instances to be converted to native Python."""

from datetime import datetime, timedelta

from dj_rest_auth.registration.serializers import RegisterSerializer
from dj_rest_auth.serializers import LoginSerializer
from django.contrib.auth.models import Group
from django.utils import timezone
from pyotp import TOTP
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import PermissionDenied, ValidationError

from . import models
from .permissions import user_is_client
from .validators import validate_accent_color

ASSOCIATED_USERNAME = "associatedUser.username"


class GroupSerializer(serializers.ModelSerializer):
    """View the Group model as a serializer."""

    class Meta:
        model = Group
        fields = ("id", "name")


class UserSerializerGet(serializers.ModelSerializer):
    """View the User model as a serializer."""

    groups = GroupSerializer(many=True, read_only=True)
    isActive = serializers.BooleanField(source="is_active")
    userClientTableHiddenRows = serializers.JSONField()
    permissions = serializers.SlugRelatedField(
        many=True, read_only=True, slug_field="codename"
    )

    class Meta:
        model = models.CustomUser
        fields = (
            "id",
            "displayName",
            "username",
            "accentColor",
            "darkTheme",
            "groups",
            "isActive",
            "centres",
            "userClientTableHiddenRows",
            "email",
            "client",
            "hasTOTP",
            "permissions",
        )
        read_only_fields = ("username", "client", "hasTOTP")


class UserDisplayNameSerializer(serializers.ModelSerializer):
    """View user display names as a serializer."""

    class Meta:
        model = models.CustomUser
        fields = ("id", "displayName", "client")


class UserListSharedDataSerializer(serializers.ModelSerializer):
    """View user shared data as a serializer."""

    groups = GroupSerializer(many=True, read_only=True)
    isActive = serializers.BooleanField(source="is_active")

    class Meta:
        model = models.CustomUser
        fields = (
            "id",
            "displayName",
            "username",
            "isActive",
            "email",
            "groups",
            "centres",
            "client",
        )


class UserSerializer(serializers.ModelSerializer):
    """Patch the User model as a serializer."""

    isActive = serializers.BooleanField(source="is_active")

    class Meta:
        model = models.CustomUser
        fields = (
            "id",
            "username",
            "displayName",
            "accentColor",
            "darkTheme",
            "groups",
            "email",
            "isActive",
            "userClientTableHiddenRows",
            "otpDeviceKey",
        )

    def to_representation(self, instance):
        """Return a nested view as a response to a POST request."""
        serializer = UserSerializerGet(instance)
        return serializer.data

    def validate(self, attrs):
        # pylint: disable=R1725
        """Override validate to check email."""
        validation = super(UserSerializer, self).validate(attrs)
        if "email" in attrs:
            email_changed_recently = models.EmailChangeRecord.objects.filter(
                user=self.instance, time__gte=timezone.now() - timedelta(hours=12)
            ).exists()
            if email_changed_recently:
                raise ValidationError(
                    {
                        "error": (
                            "Cannot Change Email: "
                            + "Email was changed within the last 12 hours"
                        )
                    },
                    code=400,
                )
        if "otpDeviceKey" in attrs and not user_is_client(self.context["request"].user):
            raise PermissionDenied(
                {"error": ("Cannot change TOTP key of a non-client user")},
            )
        return validation


class CustomRegisterSerializer(RegisterSerializer):
    # pylint: disable=W0223
    """Allow a user to register and returns their JWT token."""

    password1 = serializers.CharField(write_only=True)
    username = serializers.CharField(required=True)
    displayName = serializers.CharField(required=True)
    darkTheme = serializers.BooleanField(default=False)
    accentColor = serializers.CharField(
        required=True, max_length=7, min_length=7, validators=[validate_accent_color]
    )
    otpDeviceKey = serializers.CharField(max_length=128, min_length=8, required=False)
    email = serializers.EmailField(required=True)
    tempCode = serializers.CharField(required=False, min_length=32, max_length=32)

    def get_cleaned_data(self):
        # pylint: disable=E1101
        """Clean registration data for serializer."""
        data_dict = super().get_cleaned_data()
        data_dict["darkTheme"] = self.validated_data.get("darkTheme", "")
        data_dict["displayName"] = self.validated_data.get("displayName", "")
        data_dict["accentColor"] = self.validated_data.get("accentColor", "")
        data_dict["otpDeviceKey"] = self.validated_data.get("otpDeviceKey", "")
        data_dict["email"] = self.validated_data.get("email", "")
        return data_dict


class ClientRegisterSerializer(RegisterSerializer):  # pylint: disable=W0223
    """Allow a client to register and returns their JWT token."""

    password1 = serializers.CharField(write_only=True)
    username = serializers.CharField(required=True)
    darkTheme = serializers.BooleanField(default=False)
    accentColor = serializers.CharField(
        required=True, max_length=7, min_length=7, validators=[validate_accent_color]
    )
    otpDeviceKey = serializers.CharField(
        max_length=128, min_length=8, required=False, allow_blank=True
    )
    email = serializers.EmailField(required=True)
    tempCode = serializers.CharField(required=True, min_length=32, max_length=32)

    def get_cleaned_data(self):
        # pylint: disable=E1101
        """Clean registration data for serializer."""
        data_dict = super().get_cleaned_data()
        data_dict["darkTheme"] = self.validated_data.get("darkTheme", "")
        data_dict["accentColor"] = self.validated_data.get("accentColor", "")
        data_dict["otpDeviceKey"] = self.validated_data.get("otpDeviceKey", "")
        data_dict["email"] = self.validated_data.get("email", "")
        return data_dict


class CustomLoginSerializer(LoginSerializer):
    # pylint: disable=W0223
    """Allow a user to login and returns their JWT token."""

    otpCode = serializers.CharField(
        required=False, max_length=6, min_length=6, allow_blank=True
    )

    def validate(self, attrs):
        # pylint: disable=R1725
        """Override validate to check OTP codes."""
        validation = super(CustomLoginSerializer, self).validate(attrs)
        user = models.CustomUser.objects.get(username=attrs["username"])
        if user.otpDeviceKey:
            if "otpCode" not in attrs or not attrs["otpCode"]:
                raise serializers.ValidationError(
                    {"otpCode": ["You must provide an OTP code"]}
                )
            provided_otp_code = attrs["otpCode"]
            try:
                validated_otp_code = int(provided_otp_code)
            except ValueError as error:
                raise serializers.ValidationError(
                    {"otpCode": ["OTP Code must be a 6 digit number"]}
                ) from error
            user_otp_object = TOTP(user.otpDeviceKey, interval=30)
            user_current_otp_codes = [
                int(user_otp_object.at(datetime.now() - timedelta(seconds=30))),
                int(user_otp_object.now()),
            ]
            if validated_otp_code not in user_current_otp_codes:
                raise serializers.ValidationError(
                    {"otpCode": ["Entered OTP Code was incorrect"]}
                )
        return validation

    class Meta:
        model = models.CustomUser
        fields = ("username", "displayName", "accentColor", "darkTheme")


class TokenSerializer(serializers.ModelSerializer):
    """Return token and user data."""

    user = UserSerializer(read_only=True)

    class Meta:
        model = Token
        fields = ("key", "user")


class ValidateTempCode(serializers.ModelSerializer):
    """Return success or fail is temp code is valid."""

    code = serializers.CharField(required=True, min_length=32, max_length=32)
    username = serializers.CharField(
        required=True, source=ASSOCIATED_USERNAME, allow_blank=True
    )
    email = serializers.EmailField(required=True, allow_blank=True)

    class Meta:
        model = models.TempCode
        fields = ("code", "username", "action", "email")


class ResetTotpKey(serializers.ModelSerializer):
    """Reset a users TOTP device key."""

    code = serializers.CharField(required=True, min_length=32, max_length=32)
    username = serializers.CharField(required=True, source=ASSOCIATED_USERNAME)
    otpSecret = serializers.CharField(required=True, source="associatedUser.otpSecret")

    class Meta:
        model = models.TempCode
        fields = ("code", "username", "otpSecret")


class ResetPasswordTempCode(serializers.ModelSerializer):
    """Reset a users password using a temp code."""

    code = serializers.CharField(required=True, min_length=32, max_length=32)
    username = serializers.CharField(required=True, source=ASSOCIATED_USERNAME)
    password = serializers.CharField(required=True, source="associatedUser.password")

    class Meta:
        model = models.TempCode
        fields = ("code", "username", "password")


class RequestReset(serializers.Serializer):  # pylint: disable=W0223
    """Request a reset email."""

    email = serializers.EmailField(required=False)
    userId = serializers.IntegerField(required=False)


def validate_registration_email(email):
    """Validate email not taken by user."""
    email_in_use = models.CustomUser.objects.filter(email=email).exists()
    if email_in_use:
        raise serializers.ValidationError("Email is already in use.")

    email_used_recently = models.UserRegistrationRequestRecord.objects.filter(
        email=email, time__gte=timezone.now() - timedelta(hours=12)
    ).exists()
    if email_used_recently:
        raise ValidationError(
            "Registration was already requested for this "
            + "email address in the last 12 hours"
        )
    return email


class RequestRegistrationEmail(serializers.ModelSerializer):  # pylint: disable=W0223
    """Request a registration email."""

    email = serializers.EmailField()
    displayName = serializers.CharField(max_length=256)

    class Meta:
        model = models.TempCode
        fields = ("email", "displayName", "centres")

    def validate_email(self, email):
        """Validate email is valid email and not in use."""
        return validate_registration_email(email)

    def validate_centres(self, centres):
        """Validate centres list not empty."""
        if not centres:
            raise serializers.ValidationError("You must provide at least one centre.")
        return centres


class RequestClientRegistrationEmail(serializers.ModelSerializer):
    """Request a registration email for a client user."""

    email = serializers.EmailField(required=True)
    canViewCalendar = serializers.BooleanField(default=False, required=False)
    canViewOwnData = serializers.BooleanField(default=False, required=False)

    class Meta:
        model = models.TempCode
        fields = ("email", "canViewCalendar", "canViewOwnData")

    def validate_email(self, email):
        """Validate email is valid email and not in use."""
        return validate_registration_email(email)

    def validate(self, attrs):
        """Make sure to select at least one permission."""
        validation = super().validate(attrs)
        if not (
            ("canViewCalendar" in attrs and attrs["canViewCalendar"])
            or ("canViewOwnData" in attrs and attrs["canViewOwnData"])
        ):
            raise ValidationError(
                {
                    "error": (
                        "Client must be able to access "
                        + "either calendar or user data."
                    )
                },
                code=400,
            )
        return validation


class UserCentresSerializer(serializers.Serializer):  # pylint: disable=W0223
    """Serialize user centres."""

    centre = serializers.IntegerField()
    user = serializers.IntegerField()


class UserCentreSetSerializer(serializers.Serializer):  # pylint: disable=W0223
    """Set multiple user centres."""

    centres = serializers.ListField(child=serializers.IntegerField())
