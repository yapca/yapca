"""Declaration of serializer permissions."""
from django.db.models import Q
from rest_framework import permissions

from ..administration.models import RegisterAbility


class IsAdmin(permissions.IsAuthenticated):
    """Check if the user is authenticated and is an admin."""

    def has_permission(self, request, view):
        """Check the permission."""
        return super().has_permission(request, view) and user_is_admin(request.user)


class IsNurse(permissions.IsAuthenticated):
    """Check if the user is a nurse."""

    def has_permission(self, request, view):
        """Check the permission."""
        if super().has_permission(request, view) and request.user.groups.filter(
            Q(name="admin") | Q(name="nurse")
        ):
            return True
        return False


class IsClient(permissions.IsAuthenticated):
    """Check if the user is a client or above."""

    def has_permission(self, request, view):
        """Check the permission."""
        if super().has_permission(request, view) and request.user.groups.filter(
            Q(name="admin") | Q(name="nurse") | Q(name="client")
        ):
            return True
        return False


class IsUser(permissions.BasePermission):
    """Only allow user to edit their own user object."""

    def has_object_permission(self, request, view, obj):
        """Validate permissions for single object."""
        return obj == request.user


class IsUserOrAdmin(permissions.BasePermission):
    """Only allow user or admin to edit their own user object."""

    def has_object_permission(self, request, view, obj):
        """Validate permissions for single object."""
        if request.user and request.user.groups.filter(name="admin"):
            return True
        return obj == request.user


class IsOwnClient(IsUser):
    """Check if the user is client user and own user."""

    def has_permission(self, request, view):
        """Check the permission."""
        if super().has_permission(request, view) and user_is_client(request.user):
            return True
        return False


class CanRegister(permissions.BasePermission):
    """Toggle register ability depending on admin settings."""

    def has_permission(self, request, view):
        """Check the permission."""
        return RegisterAbility.objects.first().canRegister


def can_view_user_all_info(request_user, user_id):
    """
    Validate user can view all of a users info.

    A user can always view their own data.
    """
    return request_user and request_user.id == user_id


def can_view_user_basic_info(request_user, user_id):
    """Validate user can view another users basic info (email, username)."""
    return (
        user_id
        and request_user
        and (user_is_admin(request_user) or request_user.id == user_id)
    )


def can_view_user_display_name(request_user, user_id):
    """Validate user can view another users display name."""
    return request_user and user_id


def user_is_admin(user):
    """Check if the user is an admin."""
    return user.groups.filter(name="admin").exists()


def user_is_nurse(user):
    """Check if the user is a nurse."""
    return user.groups.filter(name="nurse").exists()


def user_is_client(user):
    """Check if the user is an admin."""
    return user.groups.filter(name="client").exists()
