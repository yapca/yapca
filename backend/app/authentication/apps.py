"""Application configuration objects store metadata for an application."""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class AuthenticationAppConfig(AppConfig):
    """Application configuration for the authentication module."""

    name = "app.authentication"

    def ready(self):
        """Import signals module on load."""
        from . import signals  # noqa: F401, PLW0611, PLC0415
