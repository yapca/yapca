"""Django models are python object representations of database models."""

from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser
from django.db import models

from .validators import validate_accent_color

temp_code_actions = (
    ("reset-totp", "TOTP Reset"),
    ("reset-password", "Password Reset"),
    ("registration", "Registration"),
    ("client-registration", "Client Registration"),
)


class UserPermission(models.Model):
    """Permissions for a user."""

    name = models.CharField(max_length=512)
    codename = models.CharField(max_length=64)

    class Meta:
        verbose_name = "User Permission"
        verbose_name_plural = "User Permissions"


class CustomUser(AbstractUser):
    """Expand on the existing django user field."""

    email = models.EmailField(verbose_name="Email", blank=True, null=True)  # noqa: DJ01
    displayName = models.CharField(  # noqa: DJ01
        verbose_name="Display Name", max_length=256, blank=True, null=True
    )
    darkTheme = models.BooleanField(default=False)
    accentColor = models.CharField(
        verbose_name="Accent Color", max_length=7, validators=[validate_accent_color]
    )
    centres = models.ManyToManyField(to="centre.Centre", blank=True)
    userClientTableHiddenRows = models.CharField(max_length=1024)
    otpDeviceKey = models.CharField(max_length=128, blank=True, null=True)  # noqa: DJ01
    client = models.OneToOneField(
        "client.Client",
        primary_key=False,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="user",
    )
    permissions = models.ManyToManyField(UserPermission, blank=True)

    class Meta:
        verbose_name = "Custom User"
        verbose_name_plural = "Custom Users"

    @property
    def hasTOTP(self):  # noqa: PLC0103
        """Return TOTP status."""
        return bool(self.otpDeviceKey)


class TempCode(models.Model):
    """Temporary codes used for password/OTP resets."""

    startTime = models.DateTimeField(auto_now_add=True)
    endTime = models.DateTimeField()
    action = models.CharField(max_length=19, choices=temp_code_actions)
    associatedUser = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, blank=True, null=True
    )
    associatedClient = models.ForeignKey(
        "client.Client",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="tempCode",
    )
    code = models.CharField(max_length=32)
    email = models.EmailField(verbose_name="Email", blank=True, null=True)  # noqa: DJ01
    centres = models.ManyToManyField(to="centre.Centre", blank=True)
    permissions = models.ManyToManyField(UserPermission, blank=True)

    class Meta:
        verbose_name = "Temporary Code"
        verbose_name_plural = "Temporary Codes"


class EmailChangeRecord(models.Model):
    """Record of user email changes."""

    time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Email Change Record"
        verbose_name_plural = "Email Change Records"


class UserRegistrationRequestRecord(models.Model):
    """Record registration requests."""

    time = models.DateTimeField(auto_now_add=True)
    email = models.EmailField(verbose_name="Email")

    class Meta:
        verbose_name = "User Registration Request Record"
        verbose_name_plural = "User Registration Request Records"
