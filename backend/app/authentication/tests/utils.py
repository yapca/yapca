"""Reusable test functions."""
from datetime import timedelta

from django.contrib.auth.models import Group
from django.utils import timezone

from ...client.tests.utils import create_client
from ..models import CustomUser, TempCode, UserPermission

TEST_EMAIL = "test@test.com"


def register_test_user(  # pylint: disable=R0913
    client,
    username="testUser1",
    display_name="Test User 1",
    password1="testPassword",
    password2="testPassword",
    accent_color="#FFFFFF",
    dark_theme=False,
    email=TEST_EMAIL,
    temp_code_email=None,
    temp_code="wzxL8piy4J5YfF0DMQdGbrtYrENUpx0V",
    centres=None,
):
    """Register an example user."""
    if temp_code_email:
        setup_registration_temp_codes(centres, temp_code_email)
    else:
        setup_registration_temp_codes(centres, email)
    return client.post(
        "/api/registration/",
        {
            "username": username,
            "displayName": display_name,
            "password1": password1,
            "password2": password2,
            "accentColor": accent_color,
            "darkTheme": dark_theme,
            "email": email,
            "tempCode": temp_code,
        },
    )


def register_client_user(  # pylint: disable=R0913
    client,
    username="clientUser1",
    password1="testPassword",
    password2="testPassword",
    accent_color="#FFFFFF",
    dark_theme=False,
    email="client_user@test.com",
    temp_code="54hxRmQJY9riP0WIj0kQscw8kGjmnrib",
):
    """Register an example user."""
    setup_client_registration_temp_codes(email)
    return client.post(
        "/api/client-registration/",
        {
            "username": username,
            "password1": password1,
            "password2": password2,
            "accentColor": accent_color,
            "darkTheme": dark_theme,
            "email": email,
            "tempCode": temp_code,
        },
    )


def create_default_groups():
    """Create the default groups since pytest doesn't apply migrations."""
    Group.objects.get_or_create(name="admin")
    Group.objects.get_or_create(name="nurse")
    Group.objects.get_or_create(name="client")


def setup_temp_codes():
    """Create test temp codes."""
    user = CustomUser.objects.create(username="testUser1")
    # Valid temp code TOTP Reset
    TempCode.objects.create(
        associatedUser=user,
        endTime=(timezone.now() + timedelta(days=1)),
        action="reset-totp",
        code="Y789B629G897R236F7U24F23F223R33S",
    )
    # Outdated temp code TOTP Reset
    TempCode.objects.create(
        associatedUser=user,
        startTime=(timezone.now() - timedelta(days=1)),
        endTime=(timezone.now()),
        action="reset-totp",
        code="FYCFbisIE5luA0NMbfwFVdOgI2R8L5mQ",
    )

    # Valid temp code Password Reset
    TempCode.objects.create(
        associatedUser=user,
        endTime=(timezone.now() + timedelta(days=1)),
        action="reset-password",
        code="96DIgcpP0yN7PWVdFbE9Okun8IB1qnUy",
    )
    # Outdated temp code Password Reset
    TempCode.objects.create(
        associatedUser=user,
        startTime=(timezone.now() - timedelta(days=1)),
        endTime=(timezone.now()),
        action="reset-password",
        code="UNJWDqcm6ofTAUfXojwcm6wHwWNSEo2b",
    )


def setup_registration_temp_codes(centres, email=TEST_EMAIL):
    """Create temp codes for registrations."""
    # Valid temp code Registration
    valid_temp_code = TempCode.objects.create(
        email=email,
        endTime=(timezone.now() + timedelta(days=1)),
        action="registration",
        code="wzxL8piy4J5YfF0DMQdGbrtYrENUpx0V",
    )
    if centres:
        valid_temp_code.centres.add(*centres)
    # Outdated temp code Registration
    TempCode.objects.create(
        email=email,
        startTime=(timezone.now() - timedelta(days=1)),
        endTime=(timezone.now()),
        action="registration",
        code="zUs5eqenhgG6wk3ysVs7Svlj307YtfXe",
    )


def setup_client_registration_temp_codes(email=TEST_EMAIL):
    """Create temp codes for client registrations."""
    # Valid temp code client registration
    client = create_client()
    temp_code = TempCode.objects.create(
        email=email,
        associatedClient=client,
        endTime=(timezone.now() + timedelta(days=1)),
        action="client-registration",
        code="54hxRmQJY9riP0WIj0kQscw8kGjmnrib",
    )
    temp_code.permissions.set(
        [UserPermission.objects.get(codename="client_can_view_own_data")]
    )
    # Outdated temp code client registration
    TempCode.objects.create(
        email=email,
        associatedClient=client,
        startTime=(timezone.now() - timedelta(days=1)),
        endTime=(timezone.now()),
        action="client-registration",
        code="yrgjvtMigD2ggLsF18fAEB4AAdaB1Odi",
    )


class MockResponse:  # pylint: disable=R0903
    """Mock response object."""

    def __init__(self, json_data, status_code):
        """Init json data."""
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        """Define json function used by view."""
        return self.json_data


example_release_data = [
    {
        "assets": {
            "links": [
                {"name": "Linux Release", "url": "https://linux-release.example.com"},
                {
                    "name": "Windows Release",
                    "url": "https://windows-release.example.com",
                },
            ]
        },
        "_links": {"self": "https://all-release.example.com"},
    }
]
JSON_RELEASE_DATA = example_release_data
EMAIL_OVERRIDES = {
    "BACKEND_HOST": "127.0.0.1",
    "BACKEND_PORT": 9000,
    "BACKEND_PROTOCOL": "http",
    "WEB_VERSION_EXISTS": True,
    "APP_VERSION_EXISTS": True,
    "FRONTEND_HOST": "127.0.0.6",
    "FRONTEND_PORT": 1234,
    "FRONTEND_PROTOCOL": "https",
    "FRONTEND_BASE_PATH": "/yapca/",
}
