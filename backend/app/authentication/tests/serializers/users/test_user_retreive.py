"""Tests for UserRetreive Serializer."""
from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from .utils import setup_users_for_get


class UserRetreive(TestCase):
    """Tests for UserRetreive."""

    url = "/api/user/"
    user = None

    def setUp(self):
        """Create users for auth."""
        self.client = APIClient()
        (
            self.nurse_user_1_token,
            self.admin_user,
            self.nurse_user_1,
            self.client_token,
        ) = setup_users_for_get(self.client)

    def test_valid_get_as_admin(self):
        """Test sending a valid get request to the serializer as admin."""
        response = self.client.get(f"{self.url}{self.admin_user.id}/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                "id": response.data["id"],
                "displayName": "admin",
                "username": "testUser1",
                "accentColor": "#FFFFFF",
                "darkTheme": False,
                "groups": [OrderedDict([("id", 1), ("name", "admin")])],
                "isActive": True,
                "centres": [response.data["centres"][0], response.data["centres"][1]],
                "userClientTableHiddenRows": "",
                "email": "admin@test.test",
                "client": None,
                "hasTOTP": False,
                "permissions": [],
            },
        )

    def test_valid_get_as_nurse(self):
        """Test can get as nurse."""
        self.client.credentials(
            HTTP_AUTHORIZATION="Token " + self.nurse_user_1_token.data["key"]
        )
        response = self.client.get(f"{self.url}{self.nurse_user_1.id}/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                "id": response.data["id"],
                "displayName": "nurse1",
                "username": "nurse1",
                "accentColor": "#FFFFFF",
                "darkTheme": False,
                "groups": [OrderedDict([("id", 2), ("name", "nurse")])],
                "isActive": True,
                "centres": [response.data["centres"][0]],
                "userClientTableHiddenRows": "",
                "email": "test1@test.test",
                "client": None,
                "hasTOTP": False,
                "permissions": [],
            },
        )

    def test_valid_get_as_client_user(self):
        """Test can get as client user."""
        self.client.credentials(
            HTTP_AUTHORIZATION="Token " + self.client_token.data["key"]
        )
        response = self.client.get(f"{self.url}{self.client_token.data['user']['id']}/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                "id": response.data["id"],
                "displayName": None,
                "username": "clientUser1",
                "accentColor": "#FFFFFF",
                "darkTheme": False,
                "groups": [OrderedDict([("id", 3), ("name", "client")])],
                "isActive": True,
                "centres": [],
                "userClientTableHiddenRows": "",
                "email": "client_user@test.com",
                "client": response.data["client"],
                "hasTOTP": False,
                "permissions": ["client_can_view_own_data"],
            },
        )

    def test_invalid_get_as_admin(self):
        """Cannot get any users besides self."""
        response = self.client.get(f"{self.url}{self.nurse_user_1.id}/")
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )
