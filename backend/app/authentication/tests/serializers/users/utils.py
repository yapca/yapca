"""Reusable test functions."""
from .....centre.tests.utils import create_centre
from ....models import CustomUser
from ...utils import register_client_user, register_test_user


def setup_users_for_get(client):
    """Create multiple users for get requests"""
    admin_user_token = register_test_user(
        client,
        email="admin@test.test",
        display_name="admin",
    )
    nurse_user_1_token = register_test_user(
        client,
        username="nurse1",
        email="test1@test.test",
        display_name="nurse1",
    )
    nurse_user_2_token = register_test_user(
        client,
        username="nurse2",
        email="test2@test.test",
        display_name="nurse2",
    )
    admin_user = CustomUser.objects.get(id=admin_user_token.data["user"]["id"])
    nurse_user_1 = CustomUser.objects.get(id=nurse_user_1_token.data["user"]["id"])
    nurse_user_2 = CustomUser.objects.get(id=nurse_user_2_token.data["user"]["id"])
    create_centre(users=[nurse_user_1, admin_user])
    centre_2 = create_centre(users=[nurse_user_2, admin_user], name="centre2")

    client_token = register_client_user(client)
    client_user = CustomUser.objects.get(id=client_token.data["user"]["id"])
    client_user.client.centres.add(centre_2)
    client.credentials(HTTP_AUTHORIZATION="Token " + admin_user_token.data["key"])
    return nurse_user_1_token, admin_user, nurse_user_1, client_token
