"""Tests for Login Serializers."""
from collections import OrderedDict
from importlib import import_module

from django.conf import settings
from django.http import HttpRequest
from django.test import TestCase
from pyotp import TOTP
from rest_framework.exceptions import ErrorDetail

from ...models import CustomUser
from ..utils import register_test_user


class CustomLoginSerializerTestCase(TestCase):
    """Tests for CustomLoginSerializer."""

    url = "/api/login/"

    def setUp(self):
        """Create a user for auth."""
        engine = import_module(settings.SESSION_ENGINE)
        self.request = HttpRequest()
        self.request.session = engine.SessionStore()
        register_response = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=register_response.data["user"]["id"])

    def test_valid(self):
        """Test sending a valid post request to the serializer."""
        response = self.client.post(
            self.url, {"username": "testUser1", "password": "testPassword"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("key", response.data)
        self.assertIn("id", response.data["user"])
        self.assertEqual(response.data["user"]["username"], "testUser1")
        self.assertEqual(response.data["user"]["displayName"], "Test User 1")
        self.assertEqual(response.data["user"]["accentColor"], "#FFFFFF")
        self.assertEqual(response.data["user"]["darkTheme"], False)
        self.assertEqual(
            response.data["user"]["groups"],
            [OrderedDict([("id", 1), ("name", "admin")])],
        )

    def test_missing_otp(self):
        """Login rejected if user has OTP Device Key."""
        self.user.otpDeviceKey = "JBSWY3DPEHPK3PXP"
        self.user.save()
        response = self.client.post(
            self.url, {"username": "testUser1", "password": "testPassword"},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "otpCode": [
                    ErrorDetail(string="You must provide an OTP code", code="invalid")
                ]
            },
        )

    def test_incorrect_otp(self):
        """Login rejected if user has OtpDeviceKey and incorrect OTP code provided."""
        self.user.otpDeviceKey = "JBSWY3DPEHPK3PXP"
        self.user.save()
        response = self.client.post(
            self.url,
            {"username": "testUser1", "password": "testPassword", "otpCode": "123456"},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "otpCode": [
                    ErrorDetail(string="Entered OTP Code was incorrect", code="invalid")
                ]
            },
        )

    def test_invalid_otp_length(self):
        """Login rejected if user has OtpDeviceKey and OTP code is not 6 characters."""
        self.user.otpDeviceKey = "JBSWY3DPEHPK3PXP"
        self.user.save()
        response = self.client.post(
            self.url,
            {"username": "testUser1", "password": "testPassword", "otpCode": "1234567"},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "otpCode": [
                    ErrorDetail(
                        string="Ensure this field has no more than 6 characters.",
                        code="max_length",
                    )
                ]
            },
        )

    def test_invalid_otp_characters(self):
        """Login rejected if user has OTP Device Key and OTP code is not all digits."""
        self.user.otpDeviceKey = "JBSWY3DPEHPK3PXP"
        self.user.save()
        response = self.client.post(
            self.url,
            {"username": "testUser1", "password": "testPassword", "otpCode": "123n56"},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "otpCode": [
                    ErrorDetail(
                        string="OTP Code must be a 6 digit number", code="invalid",
                    )
                ]
            },
        )

    def test_valid_otp(self):
        """Login accepted if user has OTP Device Key and OTP code is correct."""
        self.user.otpDeviceKey = "JBSWY3DPEHPK3PXP"
        self.user.save()
        otp_code = TOTP(self.user.otpDeviceKey, interval=30).now()
        response = self.client.post(
            self.url,
            {"username": "testUser1", "password": "testPassword", "otpCode": otp_code},
        )
        self.assertEqual(response.status_code, 200)

    def test_invalid_password(self):
        """Test sending an invalid password to the serializer."""
        response = self.client.post(
            self.url, {"username": "testUser1", "password": "incorrectPassword"},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string="Unable to log in with provided credentials.",
                        code="invalid",
                    )
                ]
            },
        )

    def test_invalid_username(self):
        """Test sending an invalid username to the serializer."""
        response = self.client.post(
            self.url, {"username": "incorrectUser", "password": "testPassword"},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string="Unable to log in with provided credentials.",
                        code="invalid",
                    )
                ]
            },
        )

    def test_missing_data(self):
        """Test missing data being sent to serializer."""
        response = self.client.post(self.url, {},)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "password": [
                    ErrorDetail(string="This field is required.", code="required")
                ]
            },
        )

        response = self.client.post(self.url, {"password": "incorrectPassword"},)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string='Must include "username" and "password".', code="invalid"
                    )
                ]
            },
        )

    def test_axes(self):
        """Automatically reject logins after 8 attempts."""
        for _ in range(7):
            self.client.post(
                self.url,
                {"username": "abcd", "password": "incorrectPassword"},
                REMOTE_ADDR="127.0.0.1",
                HTTP_USER_AGENT="test-browser",
            )
        response = self.client.post(
            self.url,
            {"username": "abcd", "password": "incorrectPassword"},
            REMOTE_ADDR="127.0.0.1",
            HTTP_USER_AGENT="test-browser",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="Too many failed login attempts", code="permission_denied"
                )
            },
        )
