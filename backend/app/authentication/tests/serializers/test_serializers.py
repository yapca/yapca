"""General Serializers Tests."""

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ..utils import setup_temp_codes

NO_AUTH_MESSAGE = "Authentication credentials were not provided."


class ApiAuthEndpoints(TestCase):
    """Tests many endpoints for authentication."""

    post_urls = [
        "/api/policies/",
        "/api/attendanceRecord/",
        "/api/clients/",
        "/api/clientDocuments/upload/",
        "/api/clientNeeds/",
        "/api/medications/",
        "/api/nextOfKin/",
        "/api/request-reset-totp/",
    ]

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        setup_temp_codes()

    def test_no_auth_post(self):
        """Should be unable to post data when not authenticated."""
        for url in self.post_urls:
            self.client.credentials()
            response = self.client.post(
                url,
                {},
            )
            self.assertEqual(response.status_code, 401)
            self.assertEqual(
                response.data,
                {
                    "detail": ErrorDetail(
                        string=NO_AUTH_MESSAGE,
                        code="not_authenticated",
                    )
                },
            )
