"""Tests for User Serializers."""
from collections import OrderedDict
from datetime import timedelta

from django.test import TestCase
from django.utils import timezone
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ...models import CustomUser, EmailChangeRecord
from ..utils import register_client_user, register_test_user

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
DISPLAY_NAME = "Test User"
DISPLAY_NAME_2 = "Test User 2"
EMAIL = "test@test.com"
EMAIL_2 = "test2@test.com"
NEW_EMAIL = "newemail@test.com"


class UserSerializer(TestCase):
    """Tests for UserSerializer."""

    url = "/api/users/"
    user = None

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        self.user = register_test_user(
            self.client,
            email=EMAIL,
            display_name=DISPLAY_NAME,
        )
        self.user_id = self.user.data["user"]["id"]
        self.user_object = CustomUser.objects.get(id=self.user_id)
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + self.user.data["key"])
        EmailChangeRecord.objects.all().delete()

    def test_valid_update_as_admin(self):
        """Test sending a valid patch request to the serializer as admin."""
        user_id = self.user.data["user"]["id"]  # pylint: disable=E1101
        response = self.client.patch(
            f"{self.url}{user_id}/update/",
            {"accentColor": "#CCCCCC"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.data)
        self.assertEqual(response.data["username"], "testUser1")
        self.assertEqual(response.data["displayName"], DISPLAY_NAME)
        self.assertEqual(response.data["accentColor"], "#CCCCCC")
        self.assertEqual(response.data["email"], EMAIL)
        self.assertEqual(response.data["darkTheme"], False)
        self.assertEqual(
            response.data["groups"], [OrderedDict([("id", 1), ("name", "admin")])]
        )

    def test_valid_update_as_nurse(self):
        """Test sending a valid patch request to the serializer as nurse."""
        nurse_user = register_test_user(
            self.client,
            username="testUser2",
            email=EMAIL_2,
            display_name=DISPLAY_NAME_2,
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + nurse_user.data["key"])
        user_id = nurse_user.data["user"]["id"]
        response = self.client.patch(
            f"{self.url}{user_id}/update/",
            {"accentColor": "#CCCCCC"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.data)
        self.assertEqual(response.data["username"], "testUser2")
        self.assertEqual(response.data["displayName"], DISPLAY_NAME_2)
        self.assertEqual(response.data["accentColor"], "#CCCCCC")
        self.assertEqual(response.data["darkTheme"], False)
        self.assertEqual(response.data["email"], EMAIL_2)
        self.assertEqual(
            response.data["groups"], [OrderedDict([("id", 2), ("name", "nurse")])]
        )

    def test_invalid_otp_update(self):
        """Cannot update clientUser otpkey as admin"""
        client_user = register_client_user(self.client)
        client_user_id = client_user.data["user"]["id"]
        response = self.client.patch(
            f"{self.url}{client_user_id}/update/",
            {"otpDeviceKey": "ABCDEFG"},
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "error": ErrorDetail(
                    string="Cannot change TOTP key of a non-client user",
                    code="permission_denied",
                )
            },
        )

    def test_incorrect_user_update_as_nurse(self):
        """Send put request where the user id is not the request user as nurse."""
        nurse_user = register_test_user(
            self.client,
            username="testUser2",
            email=EMAIL_2,
            display_name=DISPLAY_NAME_2,
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + nurse_user.data["key"])
        new_user = CustomUser.objects.create(username="testUser3")
        response = self.client.put(
            f"{self.url}{new_user.id}/update/",
            {"accentColor": "#CCCCCC"},
        )
        self.assertEqual(response.status_code, 403)

    def test_other_user_update_as_admin(self):
        """Send patch request where the user id is not the request user as admin."""
        new_user = CustomUser.objects.create(username="testUser2")
        response = self.client.patch(
            f"{self.url}{new_user.id}/update/",
            {"accentColor": "#CCCCCC"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.data)
        self.assertEqual(response.data["username"], "testUser2")
        self.assertEqual(response.data["accentColor"], "#CCCCCC")
        self.assertEqual(response.data["darkTheme"], False)
        self.assertEqual(
            response.data["groups"], [OrderedDict([("id", 2), ("name", "nurse")])]
        )

    def test_update_user_groups_as_admin(self):
        """Send patch request to update a users groups."""
        new_user = CustomUser.objects.create(username="testUser2")
        response = self.client.patch(
            f"{self.url}{new_user.id}/update/",
            {"groups": [1]},
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.data)
        self.assertEqual(response.data["username"], "testUser2")
        self.assertEqual(
            response.data["groups"], [OrderedDict([("id", 1), ("name", "admin")])]
        )

    def test_email_already_changed(self):
        """Raises error if email changed too recently."""
        EmailChangeRecord.objects.create(user=self.user_object)
        response = self.client.patch(
            f"{self.url}{self.user_id}/update/",
            {"email": NEW_EMAIL},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "error": [
                    ErrorDetail(
                        string=(
                            "Cannot Change Email: "
                            + "Email was changed within the last 12 hours"
                        ),
                        code=400,
                    )
                ]
            },
        )
        self.user_object.refresh_from_db()
        self.assertEqual(self.user_object.email, EMAIL)

    def test_email_not_changed_recently(self):
        """Does not raises error if error email changed more than 12 hours ago."""
        email_change_record = EmailChangeRecord.objects.create(user=self.user_object)
        email_change_record.time = timezone.now() - timedelta(hours=12, minutes=1)
        email_change_record.save()
        response = self.client.patch(
            f"{self.url}{self.user_id}/update/",
            {"email": NEW_EMAIL},
        )
        self.assertEqual(response.status_code, 200)
        self.user_object.refresh_from_db()
        self.assertEqual(self.user_object.email, NEW_EMAIL)
