"""Tests for Client Registrations Serializers."""
import datetime
from collections import OrderedDict
from datetime import timedelta

from django.test import TestCase
from django.utils import timezone
from rest_framework.exceptions import ErrorDetail

from ....client.models import Client
from ...models import CustomUser, TempCode

DISPLAY_NAME = "Test User"
REQUIRED_ERROR = "This field is required."
EMAIL = "test@test.com"
NO_AUTH_MESSAGE = "Authentication credentials were not provided."
INVALID_TEMP_CODE_ERROR = "Code/User is invalid or outdated"
TEMP_CODE = "UNJWDqcm6ofTAUfXojwcm6wHwWNSEo2b"


class ClientRegisterSerializerTestCase(TestCase):
    """Tests for ClientRegisterSerializer."""

    url = "/api/client-registration/"

    def register_client_user(  # pylint: disable=R0913
        self,
        accent_color="#CCCCCC",
        password1="password",
        password2="password",
        created_temp_code=TEMP_CODE,
        used_temp_code=TEMP_CODE,
        email="testclient@test.test",
    ):
        """Register a client user."""
        test_user = CustomUser.objects.create(username="testUser")

        test_client = Client.objects.create(
            firstName="Client First Name",
            admittedBy=test_user,
            dateOfBirth=datetime.date(1992, 10, 19),
            address="Greece",
            phoneNumber="24342535234234",
        )

        TempCode.objects.create(
            endTime=(timezone.now() + timedelta(days=1)),
            action="client-registration",
            code=created_temp_code,
            associatedClient=test_client,
            email=email,
        )
        return self.client.post(
            self.url,
            {
                "username": "clientUser1",
                "password1": password1,
                "password2": password2,
                "accentColor": accent_color,
                "darkTheme": False,
                "email": email,
                "tempCode": used_temp_code,
            },
        )

    def test_valid(self):
        """Test sending a valid post request to the serializer."""
        response = self.register_client_user()
        self.assertEqual(response.status_code, 201)
        self.assertIn("key", response.data)
        self.assertIn("id", response.data["user"])
        self.assertEqual(response.data["user"]["username"], "clientUser1")
        self.assertEqual(response.data["user"]["displayName"], None)
        self.assertEqual(response.data["user"]["accentColor"], "#CCCCCC")
        self.assertEqual(response.data["user"]["darkTheme"], False)
        self.assertEqual(
            response.data["user"]["groups"],
            [OrderedDict([("id", 3), ("name", "client")])],
        )
        client_user = CustomUser.objects.get(id=response.data["user"]["id"])
        self.assertEqual(client_user.client.firstName, "Client First Name")
        self.assertFalse(
            TempCode.objects.filter(
                code="UNJWDqcm6ofTAUfXojwcm6wHwWNSEo2b",
            ).exists()
        )

    def test_invalid_password(self):
        """Test sending an invalid password to the serializer."""
        response = self.register_client_user(
            password1="12",
            password2="12",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "password1": [
                    ErrorDetail(
                        string="Password must be a minimum of 6 characters.",
                        code="invalid",
                    )
                ]
            },
        )

    def test__full_temp_code(self):
        """Temp code must be 32 chars."""
        response = self.register_client_user(used_temp_code="aaaaaaa")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "tempCode": [
                    ErrorDetail(
                        string="Ensure this field has at least 32 characters.",
                        code="min_length",
                    )
                ]
            },
        )

    def test_invalid_email(self):
        """Rejects non-emails."""
        response = self.register_client_user(email="notanemail")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "email": [
                    ErrorDetail(string="Enter a valid email address.", code="invalid")
                ]
            },
        )

    def test_invalid_temp_code(self):
        """Rejects invalid temp code."""
        response = self.register_client_user(
            used_temp_code="zUs5eqenhgG6wk3ysVs7Svlj307YtfXe"
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {"error": ErrorDetail(string=INVALID_TEMP_CODE_ERROR, code=400)},
        )

    def test_invalid_accent_color(self):
        """Test sending an invalid accent color to the serializer."""
        response = self.register_client_user(accent_color="test")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "accentColor": [
                    ErrorDetail(
                        string="Accent color must be a hex value starting with #",
                        code="invalid",
                    ),
                    ErrorDetail(
                        string="Ensure this field has at least 7 characters.",
                        code="min_length",
                    ),
                ]
            },
        )
        self.assertTrue(
            TempCode.objects.filter(
                code="UNJWDqcm6ofTAUfXojwcm6wHwWNSEo2b",
            ).exists()
        )

    def test_missing_data(self):
        """Test missing data being sent to serializer."""
        response = self.client.post(
            self.url,
            {},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "username": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "email": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "password1": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "password2": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "accentColor": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
                "tempCode": [ErrorDetail(string=REQUIRED_ERROR, code="required")],
            },
        )
