"""Tests for Request Registration Email Serializers."""
import time

from django.conf import settings
from django.core import mail
from django.test import TestCase, override_settings
from django.utils import timezone
from mock import patch
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ....centre.models import Centre
from ...models import CustomUser, TempCode, UserRegistrationRequestRecord
from ..utils import EMAIL_OVERRIDES, JSON_RELEASE_DATA, MockResponse, register_test_user

EMAIL = "test1@email.com"


@override_settings(**EMAIL_OVERRIDES)
@patch(
    "app.authentication.views.get", return_value=MockResponse(JSON_RELEASE_DATA, 200)
)
class RequestRequestRegistrationEmailSerializerTestCase(TestCase):
    """Tests for RequestRegistrationEmail."""

    url = "/api/request-registration-email/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        register_response = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=register_response.data["user"]["id"])
        self.centre = Centre.objects.create(color="#CCCCCC", state="TX", country="US")
        self.client.credentials(
            HTTP_AUTHORIZATION="Token " + register_response.data["key"]
        )
        mail.outbox = []

    def test_valid_request(self, get_mock):
        """
        Test sending a valid request to the serializer.

        Check email is sent and it contains correct links to download.
        Check email contains link to temp_code query.
        Check email contains link to website if WEB_VERSION_EXISTS==True
        Check email renders expiry date correctly.
        """
        response = self.client.post(
            self.url,
            {"email": EMAIL, "displayName": "Example", "centres": [self.centre.id]},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        time.sleep(0.2)
        self.assertEqual(len(mail.outbox), 1)

        email = mail.outbox[0]
        temp_code = TempCode.objects.last()
        self.assertEqual(temp_code.action, "registration")
        self.assertEqual(temp_code.email, EMAIL)
        self.assertEqual(temp_code.associatedUser, None)
        self.assertEqual(email.subject, "You've been invited to YAPCA!")
        self.assertEqual(email.to, [EMAIL])
        self.assertEqual(email.from_email, settings.DEFAULT_FROM_EMAIL)
        self.assertIn("Hello Example!", email.body)
        self.assertIn("https://windows-release.example.com", email.body)
        self.assertIn("https://all-release.example.com", email.body)
        self.assertIn(
            (
                "https://star-man.gitlab.io/yapca-site/#/uri-linker?address=http"
                + f"://127.0.0.1:9000&action=registration&code={temp_code.code}&"
                + f"email={EMAIL}&displayName=Example"
            ),
            email.body,
        )
        self.assertIn(
            (
                "https://star-man.gitlab.io/yapca-site/#/uri-linker?address=http"
                + f"://127.0.0.1:9000&action=registration&code={temp_code.code}&"
                + f"email={EMAIL}&displayName=Example"
            ),
            email.body,
        )
        self.assertIn(
            "How to for YAPCA web version",
            email.body,
        )
        self.assertIn(
            (
                "https://127.0.0.6:1234/yapca/temp-code?action=registration&cod"
                + f"e={temp_code.code}&email={EMAIL}&displayName=Example"
            ),
            email.body,
        )
        formatted_end_time = timezone.localtime(temp_code.endTime).strftime(
            "%H:%M %b %d"
        )
        self.assertIn(
            f"register will end: {formatted_end_time}",
            email.body,
        )
        get_mock.assert_called_with(
            "https://gitlab.com/api/v4/projects/16681524/releases",
            headers={"Accept": "*/*"},
            timeout=5,
        )
        self.assertTrue(
            UserRegistrationRequestRecord.objects.filter(email=EMAIL).exists()
        )

    @override_settings(WEB_VERSION_EXISTS=False)
    def test_web_disabled(self, _):
        """Should not show website related text if web not enabled."""
        centre = Centre.objects.get(color="#CCCCCC", state="TX", country="US")
        self.client.post(
            self.url,
            {"email": EMAIL, "displayName": "Example", "centres": [centre.id]},
            format="json",
        )
        time.sleep(0.1)
        email = mail.outbox[0]

        self.assertNotIn(
            "How to for YAPCA web version",
            email.body,
        )
        self.assertNotIn(
            "https://127.0.0.6:1234/yapca/temp-code?action=reset-password&cod",
            email.body,
        )

    @override_settings(BACKEND_PORT=80)
    @override_settings(FRONTEND_PORT="")
    def test_no_port_values(self, _):
        """Should generate correct frontend/backend links without port."""
        self.client.post(
            self.url,
            {"email": EMAIL, "displayName": "Example", "centres": [self.centre.id]},
            format="json",
        )
        time.sleep(0.1)
        email = mail.outbox[0]

        self.assertIn("address=http://127.0.0.1&", email.body)
        self.assertIn("https://127.0.0.6/yapca/", email.body)

    def test_email_taken(self, _):
        """Should throw error when email is taken."""
        response = self.client.post(
            self.url,
            {
                "email": "test@test.com",
                "displayName": "Example",
                "centres": [self.centre.id],
            },
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {"email": [ErrorDetail(string="Email is already in use.", code="invalid")]},
        )

    def test_email_invalid(self, _):
        """Should throw error when email is invalid."""
        response = self.client.post(
            self.url,
            {"email": "test", "displayName": "Example", "centres": [self.centre.id]},
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "email": [
                    ErrorDetail(string="Enter a valid email address.", code="invalid")
                ]
            },
        )

    def test_email_already_sent(self, _):
        """Should throw error when email already sent."""
        self.client.post(
            self.url,
            {"email": EMAIL, "displayName": "Example", "centres": [self.centre.id]},
            format="json",
        )
        response = self.client.post(
            self.url,
            {"email": EMAIL, "displayName": "Example", "centres": [self.centre.id]},
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "email": [
                    ErrorDetail(
                        string=(
                            "Registration was already requested"
                            + " for this email address in the last 12 hours"
                        ),
                        code="invalid",
                    )
                ]
            },
        )

    def test_invalid_centres(self, _):
        """Should throw error when centre does not exist."""
        response = self.client.post(
            self.url,
            {
                "email": EMAIL,
                "displayName": "Example",
                "centres": [self.centre.id, 999],
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "centres": [
                    ErrorDetail(
                        string='Invalid pk "999" - object does not exist.',
                        code="does_not_exist",
                    )
                ]
            },
        )

    def test_empty_centres(self, _):
        """Should throw error when centre list is empty."""
        response = self.client.post(
            self.url,
            {"email": EMAIL, "displayName": "Example", "centres": []},
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "centres": [
                    ErrorDetail(
                        string="You must provide at least one centre.", code="invalid"
                    )
                ]
            },
        )

    def test_missing_data(self, _):
        """Test missing data being sent to serializer."""
        response = self.client.post(self.url, {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "email": [
                    ErrorDetail(string="This field is required.", code="required")
                ],
                "displayName": [
                    ErrorDetail(string="This field is required.", code="required")
                ],
                "centres": [
                    ErrorDetail(
                        string="You must provide at least one centre.", code="invalid"
                    )
                ],
            },
        )
