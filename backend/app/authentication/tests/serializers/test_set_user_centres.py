"""Tests for sending post requests to the UserCentreSetSerializer."""
from django.contrib.auth.models import Group
from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ....centre.tests.utils import create_centre
from ...models import CustomUser
from ..utils import register_test_user


class UserCentreSetSerializerTestCase(TestCase):
    """Tests for sending post requests to the UserCentreSetSerializer."""

    url = "/api/users/centres/set/"

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        token = register_test_user(self.client)
        self.user = CustomUser.objects.get(id=token.data["user"]["id"])
        self.centre_1 = create_centre(users=[self.user])
        self.centre_2 = create_centre(name="example centre 2")
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.data["key"])

    def test_valid_set(self):
        """Should be able to set user centres."""
        response = self.client.post(
            self.url,
            {"centres": [self.centre_1.id, self.centre_2.id]},
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                "centres": [self.centre_1.id, self.centre_2.id],
            },
        )

    def test_no_group_post(self):
        """Should be unable to post data when not in valid group."""
        self.user.groups.clear()
        nurse = Group.objects.get(name="nurse")
        self.user.groups.add(nurse)
        response = self.client.post(
            self.url,
            {"centres": [self.centre_1.id, self.centre_2.id]},
            format="json",
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_missing_data(self):
        """Returns error if missing data."""
        response = self.client.post(
            self.url,
            {},
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "centres": [
                    ErrorDetail(string="This field is required.", code="required")
                ]
            },
        )
