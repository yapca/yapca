"""Profile Model Tests."""

from django.test import TestCase

from ..models import CustomUser


class CustomUserTestCase(TestCase):
    """Tests for the CustomUser model."""

    def test_creating_user_object(self):
        """Custom User object should create successfully."""
        test_user = CustomUser.objects.create(username="testUser")
        test_user.is_active = True
        test_user.save()
        self.assertEqual(test_user.username, "testUser")
        self.assertEqual(test_user.is_active, True)

    def test_first_user_added_to_admin_group(self):
        """First user created should be in the admin group."""
        test_user = CustomUser.objects.create(username="testUser")
        test_user.is_active = True
        test_user.save()
        self.assertEqual(len(CustomUser.objects.all()), 1)
        self.assertEqual(len(test_user.groups.all()), 1)
        self.assertEqual(test_user.groups.all()[0].name, "admin")

    def test_second_user_added_to_nurse_group(self):
        """First user created should be in the nurse group."""
        test_user = CustomUser.objects.create(username="testUser")
        test_user.is_active = True
        test_user.save()

        second_test_user = CustomUser.objects.create(username="testUser2")
        second_test_user.is_active = True
        second_test_user.save()

        third_test_user = CustomUser.objects.create(username="testUser3")
        third_test_user.is_active = True
        third_test_user.save()

        self.assertEqual(len(second_test_user.groups.all()), 1)
        self.assertEqual(second_test_user.groups.all()[0].name, "nurse")
        self.assertEqual(len(third_test_user.groups.all()), 1)
        self.assertEqual(third_test_user.groups.all()[0].name, "nurse")
