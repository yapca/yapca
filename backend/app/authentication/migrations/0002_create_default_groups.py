import os
from pathlib import Path

from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.db import migrations
from environs import Env

ENV_PATH = str(
    Path(__file__).resolve().parent.parent.parent.parent.parent
    / "config"
    / f'{os.getenv("YAPCA_ENV", "local")}.env'
)
ENV = Env()
ENV.read_env(ENV_PATH)


def create_groups(apps, schema_editor):
    Group.objects.create(name="admin"),
    Group.objects.create(name="nurse"),


def create_site(apps, schema_editor):
    Site.objects.create(domain=ENV("VUE_APP_API_HOST"), name="main")


class Migration(migrations.Migration):
    dependencies = [
        ("authentication", "0001_initial"),
        ("sites", "__first__"),
    ]
    operations = [
        migrations.RunPython(create_groups),
        migrations.RunPython(create_site),
    ]
