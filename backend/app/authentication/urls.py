"""URLs routes that are part of the main django app."""

from dj_rest_auth.views import PasswordChangeView
from django.urls import path

from . import views

urlpatterns = [  # pylint: disable=C0103
    path("user/<int:pk>/", views.UserRetreiveView.as_view(), name="user"),
    path(
        "users/display-names/",
        views.UserListDisplayNameView.as_view(),
        name="user-display-names",
    ),
    path(
        "users/shared-data/",
        views.UserListSharedDataView.as_view(),
        name="user-shared-data",
    ),
    path(
        "users/centres/update/",
        views.UserCentresUpdate.as_view(),
        name="user-centres-update",
    ),
    path(
        "users/centres/set/",
        views.UserCentresSet.as_view(),
        name="user-centres-update",
    ),
    path("groups/", views.GroupListView.as_view(), name="groups"),
    path("registration/", views.CustomRegisterView.as_view(), name="register"),
    path(
        "client-registration/",
        views.ClientRegisterView.as_view(),
        name="client-registration",
    ),
    path("login/", views.CustomLoginView.as_view(), name="login"),
    path("change-password/", PasswordChangeView.as_view(), name="change-password"),
    path(
        "request-reset-password/",
        views.RequestPasswordReset.as_view(),
        name="request-reset-password",
    ),
    path(
        "request-registration-email/",
        views.RequestRegistrationEmail.as_view(),
        name="request-registration-email",
    ),
    path(
        "request-client-registration-email/<int:pk>/",
        views.RequestClientRegistrationEmail.as_view(),
        name="request-client-registration-email",
    ),
    path(
        "request-reset-totp/",
        views.RequestTOTPReset.as_view(),
        name="request-reset-totp",
    ),
    path("users/<int:pk>/update/", views.UserUpdate.as_view(), name="user-update"),
    path(
        "temp-code/validate/",
        views.ValidateTempCode.as_view(),
        name="validate-temp-code",
    ),
    path("temp-code/reset-totp/", views.ResetTotp.as_view(), name="reset-totp"),
    path(
        "temp-code/reset-password/",
        views.ResetPaswordTempCode.as_view(),
        name="reset-password",
    ),
    path(
        "users/disable-totp/",
        views.DisableTOTP.as_view(),
        name="disable-totp",
    ),
]
