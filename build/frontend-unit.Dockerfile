FROM node:20-alpine
RUN mkdir -p /builds/Star-Man/yapca/frontend
WORKDIR /builds/yapca/yapca/frontend

COPY ./frontend/*.* .
RUN corepack enable
RUN yarn install --immutable

COPY ./frontend/src ./src
COPY ./frontend/tests/unit ./tests/unit

CMD yarn test:unit:ci && mv coverage/* coverageOutput