FROM node:20-alpine
RUN mkdir -p /builds/Star-Man/yapca
COPY ./frontend/ /builds/yapca/yapca/frontend
WORKDIR /builds/yapca/yapca/frontend
RUN corepack enable
RUN yarn install --immutable
CMD yarn serve