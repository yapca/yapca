FROM python:3.11-alpine
ENV PYTHONUNBUFFERED 1
RUN apk add --no-cache postgresql-libs && \
    apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev \
    zlib-dev jpeg-dev postgresql postgresql-client docker
RUN pip install --upgrade pip
RUN pip install poetry==1.7.1
RUN poetry config virtualenvs.create false
COPY ./backend/pyproject.toml /yapca/backend/pyproject.toml
COPY ./backend/poetry.lock /yapca/backend/poetry.lock
WORKDIR /yapca/backend
RUN poetry install