ARG REGISTRY
ARG IMAGE_VERSION
FROM ${REGISTRY}/frontend-base:${IMAGE_VERSION}
ARG YAPCA_ENV
ENV YAPCA_ENV $YAPCA_ENV
RUN apk add chromium firefox
WORKDIR /yapca/frontend
CMD http-server -s -p 3000 -P http://localhost:3000? dist/spa