FROM node:20-alpine
RUN corepack enable
RUN yarn global add http-server
RUN mkdir -p /yapca/frontend
WORKDIR /yapca/frontend

COPY ./frontend/package.json package.json
COPY ./frontend/yarn.lock yarn.lock
COPY ./frontend/.yarnrc.yml .yarnrc.yml
COPY ./config/docker* /yapca/config/

RUN yarn install --immutable