ARG REGISTRY
ARG IMAGE_VERSION
FROM ${REGISTRY}/backend-base:${IMAGE_VERSION}
ARG YAPCA_ENV
ENV YAPCA_ENV=$YAPCA_ENV
WORKDIR /yapca/backend
COPY ./backend .
COPY ./config/docker* /yapca/config/
EXPOSE 8000/tcp
CMD uvicorn --host 0.0.0.0 app.asgi:application | tee django-output.log