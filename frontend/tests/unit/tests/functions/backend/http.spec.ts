import http from 'src/functions/backend/http';
import getBackendPath from 'src/functions/backend/getBackendPath';
import { describe, expect, it } from 'vitest';

describe('test http.ts', () => {
  it('can handle no backendpath', () => {
    process.env.VUE_APP_API_PROTOCOL = 'https';
    expect(http().defaults.baseURL).toStrictEqual(
      `${'https'}://${getBackendPath()}/api/`
    );
  });

  it('accepts backendpath', () => {
    expect(http('https://test.example.com').defaults.baseURL).toStrictEqual(
      `https://test.example.com/api/`
    );
  });

  it('handles missing api protocol', () => {
    process.env.VUE_APP_API_PROTOCOL = undefined;
    expect(http().defaults.baseURL).toStrictEqual(
      `${''}://${getBackendPath()}/api/`
    );
  });
});
