import { beforeEach, describe, expect, it, vi } from 'vitest';
import setup from 'tests/unit/functions/setup';
import { appUrlListener } from 'src/functions/app/appUrlListener';
import { getRouter } from 'vue-router-mock';
import { flushPromises } from '@vue/test-utils';

setup();
let url = 'yapca://temp-code?action=registration&username=testuser';
vi.mock('@capacitor/app', () => ({
  App: {
    addListener: async (
      _: string,
      listenerFunc: (arg0: { url: string }) => Promise<void>
    ) =>
      Promise.resolve(
        listenerFunc({
          url
        })
      )
  }
}));

describe('appUrlListener.ts', () => {
  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.resetModules();
  });

  it('sends correct request to router', async () => {
    const router = getRouter();
    router.push = vi.fn(() => Promise.resolve());
    appUrlListener(router);
    await flushPromises();
    expect(router.push).toHaveBeenCalledTimes(1);
    expect(router.push).toHaveBeenCalledWith({
      path: '/temp-code',
      query: { action: 'registration', username: 'testuser' }
    });
  });

  it('does not send request if invalid urlscheme', () => {
    url = '2yapca://temp-code';
    const router = getRouter();
    router.push = vi.fn(() => Promise.resolve());
    appUrlListener(router);
    expect(router.push).toHaveBeenCalledTimes(0);
  });
});
