import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import LoginComponent from 'src/components/auth/LoginComponent.vue';
import setup from 'tests/unit/functions/setup';
import { useSocketStore } from 'src/stores/websocketStore';
import type { SpyInstance } from 'vitest';

setup();

const userData = {
  data: {
    key: '11111111',
    user: {
      username: 'testUser',
      id: 3,
      groups: [{ id: 3, name: 'admin' }],
      accentColor: '#FFFFFF',
      darkTheme: false
    }
  }
};

describe('LoginSuccess.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let WS_RECONNECT: SpyInstance;
  let httpGet: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(LoginComponent);
    const wsStore = useSocketStore();
    WS_RECONNECT = vi.spyOn(wsStore, 'WS_RECONNECT');
    wrapper.vm.basicStore.appServerAddress = 'test';
    wrapper.vm.basicStore.backendPath = '1234';
    wrapper.vm.basicStore.token = 'abce';
    httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
  });

  it('test loginSuccess function adds user to store', async () => {
    wrapper.vm.router.push = () => Promise.resolve(true);
    const routerPush = vi.spyOn(wrapper.vm.router, 'push');
    httpGet.mockResolvedValue(true);
    routerPush.mockResolvedValueOnce(true);
    await wrapper.vm.context.loginSuccess(
      userData,
      wrapper.vm.router,
      wrapper.vm.route
    );
    expect(WS_RECONNECT).toHaveBeenCalledTimes(1);
    expect(WS_RECONNECT).toHaveBeenCalledWith('test', '11111111');
    expect(routerPush).toHaveBeenCalledTimes(1);
    expect(routerPush).toHaveBeenCalledWith({ name: 'Clients' });
    expect(wrapper.vm.basicStore.token).toStrictEqual('11111111');
    expect(wrapper.vm.userStore.user).toStrictEqual({
      username: 'testUser',
      id: 3,
      groups: [{ id: 3, name: 'admin' }],
      group: 'admin',
      accentColor: '#FFFFFF',
      darkTheme: false
    });
  });

  it('test loginSuccess catches error when router.push fails', async () => {
    const routerPush = vi.spyOn(wrapper.vm.$router, 'push');
    routerPush.mockRejectedValueOnce('Example Error');
    httpGet.mockResolvedValue(true);
    await wrapper.vm.context.loginSuccess(
      userData,
      wrapper.vm.router,
      wrapper.vm.route
    );
    expect(WS_RECONNECT).toHaveBeenCalledWith('test', '11111111');
    expect(wrapper.vm.basicStore.token).toStrictEqual('11111111');
  });
});
