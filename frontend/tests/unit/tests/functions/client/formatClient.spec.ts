import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import Clients from 'src/pages/ClientsPage.vue';
import setup from 'tests/unit/functions/setup';
import { mockClients, mockUsers } from 'tests/unit/mocks';
setup();

describe('formatClient.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    mockClients();
    mockUsers();
    wrapper = shallowMount(Clients);
  });

  it('formatClient() returns empty string if date invalid', () => {
    wrapper.vm.clientStore.clients['4'].createdDate = 'test';
    const formattedClient = wrapper.vm.context.formatClient(4);
    expect(formattedClient.createdDate).toStrictEqual('');
  });

  it('formatClient() returns correct formatted client values', () => {
    wrapper.vm.clientStore.clients['4'].createdDate = 'test';
    const formattedClient = wrapper.vm.context.formatClient(1);
    expect(formattedClient.createdDate).toStrictEqual('26/06/20');
    expect(formattedClient.admittedBy).toStrictEqual('Test User');
    expect(formattedClient.dateOfBirth).toContain('10/11/11 (');
  });

  it('formatClient() returns unknown when admittedBy user does not exist', () => {
    wrapper.vm.clientStore.clients['4'].createdDate = 'test';
    wrapper.vm.userStore.users['1'].displayName = '';
    const formattedClient = wrapper.vm.context.formatClient(1);
    expect(formattedClient.admittedBy).toStrictEqual('Unknown');
  });
});
