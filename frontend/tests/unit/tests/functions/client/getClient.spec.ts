/* eslint-disable @typescript-eslint/no-explicit-any */
import { beforeEach, describe, expect, it, vi } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { useBasicStore } from 'src/stores/basicStore';
import getClient from 'src/functions/client/getClient';
import { useClientStore } from 'src/stores/clientsStore';
import { mockClients, mockUsers } from '../../../mocks';

describe('getClient', () => {
  let basicStore: any;
  let clientStore: any;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));

    setActivePinia(createPinia());
    basicStore = useBasicStore();
    clientStore = useClientStore();
    mockUsers();
    mockClients();
  });

  it('sets active client if clientuser', async () => {
    const httpGet = vi.spyOn(basicStore.http, 'get');

    clientStore.activeClient = { id: 2 };
    httpGet.mockResolvedValueOnce({
      data: { id: 2, surname: 'surname 2' }
    });

    await getClient(2);
    expect(httpGet).toHaveBeenCalledWith('clients/2/');
    expect(clientStore.activeClient).toStrictEqual({
      id: 2,
      surname: 'surname 2'
    });
  });
});
