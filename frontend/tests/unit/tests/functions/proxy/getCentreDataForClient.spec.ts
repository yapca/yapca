/* eslint-disable @typescript-eslint/no-explicit-any */
import { beforeEach, describe, expect, it, vi } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import setup from 'tests/unit/functions/setup';
import { useBasicStore } from 'src/stores/basicStore';
import { useCentreStore } from 'src/stores/centresStore';
import getCentreDataForClient from 'src/functions/proxy/getCentreDataForClient';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import type { SpyInstance } from 'vitest';

setup();

describe('getCentreDataForClient', () => {
  let centreStore: any;
  let basicStore: any;
  let httpGet: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    setActivePinia(createPinia());
    basicStore = useBasicStore();
    centreStore = useCentreStore();
    httpGet = vi.spyOn(basicStore.http, 'get');
  });

  it('getCentreDataForClient gets client related centre data and merges to store', async () => {
    httpGet.mockResolvedValueOnce({
      data: [
        {
          id: 1,
          description: 'updated need 1'
        },
        {
          id: 2,
          description: 'example need 2'
        },
        {
          description: 'example need 5'
        }
      ]
    });
    centreStore.needs = {
      '1': { id: 1, description: 'example need 1' },
      '3': { id: 3, description: 'example need 3' }
    };
    await getCentreDataForClient(3, 'needs');
    expect(httpGet).toHaveBeenCalledWith('clients/3/needs');
    expect(centreStore.needs).toStrictEqual({
      '1': { id: 1, description: 'updated need 1' },
      '2': { id: 2, description: 'example need 2' },
      '3': { id: 3, description: 'example need 3' }
    });
  });

  it('getCentreNeedsForClient() calls axiosFailed() on failure ', async () => {
    const httpGet = vi.spyOn(basicStore.http, 'get');
    httpGet.mockRejectedValueOnce({});
    await getCentreDataForClient(4, 'needs');
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });
});
