import { beforeEach, describe, expect, it, vi } from 'vitest';
import setUsers from 'src/functions/proxy/setUsers';
import { createPinia, setActivePinia } from 'pinia';
import { useUserStore } from 'src/stores/userStore';
import type User from 'src/types/User';

describe('setUsers', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let userStore: any;
  let exampleUser: User;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    setActivePinia(createPinia());
    userStore = useUserStore();
    userStore.notify = {
      create: vi.fn()
    };
    exampleUser = {
      id: 3,
      username: 'name',
      displayName: 'User Name',
      accentColor: '#AAAAAA',
      darkTheme: false,
      group: 'nurse',
      groups: [{ id: 1, name: 'nurse' }],
      isActive: true,
      centres: [1, 2],
      userClientTableHiddenRows: [],
      email: 'example@test.test'
    };
  });

  it('set users sets user in store', () => {
    setUsers({ data: [exampleUser] });
    expect(userStore.users).toStrictEqual({ '3': exampleUser });
  });
});
