import { describe, expect, it } from 'vitest';

import autoColSize from 'src/functions/common/autoColSize';

describe('autoColSize', () => {
  it('gets auto col size for last in list of 2', () => {
    const colClass = autoColSize('xs', 2, 1);
    expect(colClass).toStrictEqual('col-12 col-xs-6');
  });

  it('gets auto col size for last in list of 3', () => {
    const colClass = autoColSize('xs', 3, 2);
    expect(colClass).toStrictEqual('col-12 col-xs-12');
  });

  it('gets auto col size for not last in list of 3', () => {
    const colClass = autoColSize('xs', 3, 1);
    expect(colClass).toStrictEqual('col-12 col-xs-6');
  });
});
