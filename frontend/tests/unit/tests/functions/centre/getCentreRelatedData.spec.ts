/* eslint-disable @typescript-eslint/no-explicit-any */
import { beforeEach, describe, expect, it, vi } from 'vitest';
import getCentreRelatedData from 'src/functions/centre/getCentreRelatedData';
import { useBasicStore } from 'src/stores/basicStore';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { createPinia, setActivePinia } from 'pinia';
import { mockCentres } from 'tests/unit/mocks';
import { useCentreStore } from 'src/stores/centresStore';
import type { SpyInstance } from 'vitest';

const exampleNeeds = [
  {
    id: 2,
    description: 'new need 2 description'
  },
  {
    id: 65,
    description: 'new need 65 description'
  }
];

const examplePolicies = [
  {
    id: 3,
    description: 'new policy 3 description'
  },
  {
    id: 43,
    description: 'new policy 43 description'
  }
];

describe('getCentreRelatedData', () => {
  let httpGet: SpyInstance;
  let basicStore: any;
  let centreStore: any;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    setActivePinia(createPinia());
    basicStore = useBasicStore();
    centreStore = useCentreStore();
    httpGet = vi.spyOn(basicStore.http, 'get');
    mockCentres();
  });

  it('getCentreRelatedData() can get needs', async () => {
    httpGet.mockResolvedValueOnce({ data: exampleNeeds });
    await getCentreRelatedData(3, 'needs');
    expect(httpGet).toHaveBeenCalledWith('centres/3/needs');
    expect(centreStore.needs).toStrictEqual({
      '1': { id: 1, description: 'Test Need 1' },
      '2': { id: 2, description: 'new need 2 description' },
      '3': { id: 3, description: 'Test Need 3' },
      '65': { id: 65, description: 'new need 65 description' }
    });
    expect(axiosFailed).toHaveBeenCalledTimes(0);
  });

  it('getCentreRelatedData() can get policies', async () => {
    httpGet.mockResolvedValueOnce({ data: examplePolicies });
    await getCentreRelatedData(2, 'policies');
    expect(httpGet).toHaveBeenCalledWith('centres/2/policies');
    expect(centreStore.policies).toStrictEqual({
      '2': { id: 2, description: 'Test Policy 2' },
      '3': { id: 3, description: 'new policy 3 description' },
      '43': { id: 43, description: 'new policy 43 description' }
    });
    expect(axiosFailed).toHaveBeenCalledTimes(0);
  });

  it('getCentreRelatedData() calls axiosFailed on failure', async () => {
    httpGet.mockRejectedValueOnce(false);
    await getCentreRelatedData(3, 'needs');
    expect(axiosFailed).toHaveBeenCalledOnce();
  });
});
