import { flushPromises, shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CentreSetup from 'src/pages/CentreSetup.vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import goToNextPage from 'src/functions/proxy/goToNextPage';
import { useCentreStore } from 'src/stores/centresStore';
import { mockCentres } from '../../mocks';
import type Centre from 'src/types/Centre';
import type { SpyInstance } from 'vitest';

setup();

describe('CentreSetup.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpPatch: SpyInstance;
  let httpPost: SpyInstance;

  beforeEach(() => {
    vi.clearAllMocks();
    mockCentres();
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.mock('src/functions/proxy/goToNextPage');
    wrapper = shallowMount(CentreSetup);
    httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
  });

  it('test validateCentre()', () => {
    wrapper.vm.step = 1;
    const createCentre = vi.spyOn(wrapper.vm.context, 'createCentre');
    wrapper.vm.validateCentre();
    expect(createCentre).not.toHaveBeenCalled();

    wrapper.vm.selectedColor = '#FFFFFF';
    wrapper.vm.centreName = 'test';
    wrapper.vm.validateCentre();
    expect(createCentre).toHaveBeenCalledTimes(1);
  });

  it('test createOpeningDays()', async () => {
    wrapper.vm.openingDays = [2, 4, 6];
    const upsertCentreSuccessful = vi.spyOn(
      wrapper.vm.context,
      'upsertCentreSuccessful'
    );
    httpPatch.mockRejectedValueOnce('Example Error');
    await wrapper.vm.createOpeningDays();
    expect(httpPatch).toHaveBeenCalledWith('centres/2/update/', {
      id: 2,
      openingDays: [3, 5, 7],
      openingDaysFirstSetupComplete: true
    });
    expect(axiosFailed).toHaveBeenCalledWith('Example Error');
    expect(upsertCentreSuccessful).toHaveBeenCalledTimes(0);

    httpPatch.mockResolvedValueOnce({ data: {} });
    await wrapper.vm.createOpeningDays();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(upsertCentreSuccessful).toHaveBeenCalledWith({ data: {} });
  });

  it('updateCentre() returns null if no setupCentre id', async () => {
    const response = await wrapper.vm.updateCentre({ id: null });
    expect(response).toStrictEqual(null);
  });

  it('createOpeningDays() returns null if no setupCentre ', async () => {
    wrapper.vm.centreStore.setupCentre.id = null;
    const response = await wrapper.vm.createOpeningDays();
    expect(response).toStrictEqual(null);
  });

  it('createClosingDays() sets setup value', async () => {
    const upsertCentreSuccessful = vi.spyOn(
      wrapper.vm.context,
      'upsertCentreSuccessful'
    );
    httpPatch.mockRejectedValueOnce('Example Error');
    await wrapper.vm.createClosingDays();
    expect(httpPatch).toHaveBeenCalledWith('centres/2/update/', {
      id: 2,
      closingDaysFirstSetupComplete: true
    });
    expect(axiosFailed).toHaveBeenCalledWith('Example Error');
    expect(upsertCentreSuccessful).toHaveBeenCalledTimes(0);

    httpPatch.mockResolvedValueOnce({ data: {} });
    await wrapper.vm.createClosingDays();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(upsertCentreSuccessful).toHaveBeenCalledWith({ data: {} });
  });

  it('createClosingDays() returns null if no setupCentre ', async () => {
    wrapper.vm.centreStore.setupCentre.id = null;
    const response = await wrapper.vm.createClosingDays();
    expect(response).toStrictEqual(null);
  });

  it('test createCentre()', async () => {
    const consoleError = vi.spyOn(console, 'error');
    consoleError.mockImplementation(() => {
      return true;
    });
    wrapper.vm.centreName = 'Test Centre';
    wrapper.vm.selectedColor = '#F1F1F1';
    const upsertCentreSuccessful = vi.spyOn(
      wrapper.vm.context,
      'upsertCentreSuccessful'
    );
    httpPost.mockRejectedValueOnce({ data: {} });
    await wrapper.vm.createCentre();
    expect(httpPost).toHaveBeenCalledWith('centres/', {
      name: 'Test Centre',
      color: '#F1F1F1'
    });
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(upsertCentreSuccessful).toHaveBeenCalledTimes(0);

    httpPost.mockResolvedValueOnce({ data: {} });
    await wrapper.vm.createCentre();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(upsertCentreSuccessful).toHaveBeenCalledWith({ data: {} });
    httpPost.mockClear();
  });

  it('test upsertCentreSuccessful()', () => {
    wrapper.vm.$router.push = () => Promise.resolve(true);
    const setSetupCentre = vi.spyOn(wrapper.vm.centreStore, 'setSetupCentre');
    const routerPush = vi.spyOn(wrapper.vm.$router, 'push');

    wrapper.vm.step = 1;
    expect(routerPush).toHaveBeenCalledTimes(0);

    wrapper.vm.upsertCentreSuccessful({
      data: { id: 2, name: 'Test Centre' }
    });
    expect(routerPush).toHaveBeenCalledTimes(0);
    expect(setSetupCentre).toHaveBeenCalledTimes(1);
    expect(setSetupCentre).toHaveBeenCalledWith({
      id: 2,
      name: 'Test Centre'
    });
    expect(wrapper.vm.step).toStrictEqual(2);
    expect(wrapper.vm.centreId).toStrictEqual(2);
  });

  it('setupComplete updates on state change', () => {
    wrapper.vm.centreId = 2;
    wrapper.vm.centreStore.centres[2] = { id: 2, setupComplete: false };
    expect(wrapper.vm.setupComplete).toStrictEqual(false);
    wrapper.vm.centreStore.centres[2].setupComplete = true;
    expect(wrapper.vm.setupComplete).toStrictEqual(true);
  });

  it('changing setupComplete sets subscribeToCentreDialog if centres > 1', async () => {
    wrapper.vm.centreId = 2;
    const subscribeToCentre = vi.spyOn(wrapper.vm.context, 'subscribeToCentre');
    wrapper.vm.centreStore.centres[2] = { id: 2, setupComplete: true };
    await nextTick();
    await flushPromises();
    expect(wrapper.vm.subscribeToCentreDialog).toStrictEqual(true);
    expect(subscribeToCentre).toHaveBeenCalledTimes(0);
  });

  it('changing setupComplete calls goToNextPage() if centres <= 1', async () => {
    wrapper.vm.centreStore.centres[2].setupComplete = false;
    wrapper.vm.centreId = 2;
    const subscribeToCentre = vi.spyOn(wrapper.vm.context, 'subscribeToCentre');
    wrapper.vm.centreStore.centres = { '2': { id: 2, setupComplete: true } };
    await nextTick();
    await flushPromises();
    expect(wrapper.vm.centreId).toStrictEqual(2);
    expect(wrapper.vm.totalCentres).toStrictEqual(1);
    expect(wrapper.vm.setupComplete).toStrictEqual(true);
    expect(wrapper.vm.subscribeToCentreDialog).toStrictEqual(false);
    expect(subscribeToCentre).toHaveBeenCalledTimes(1);
  });

  it('totalCentres is 0 when no centres', async () => {
    wrapper.vm.centreStore.centres = {};
    await nextTick();
    expect(wrapper.vm.totalCentres).toStrictEqual(0);
  });

  it('test upsertFailed()', () => {
    const expectedError = 'non field error';
    const nonFieldErrors = {
      response: {
        data: {
          non_field_errors: [expectedError]
        }
      }
    };
    wrapper.vm.upsertFailed(nonFieldErrors);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledWith(nonFieldErrors);
    const noResponseErr = {};
    wrapper.vm.upsertFailed(noResponseErr);
    expect(axiosFailed).toHaveBeenCalledTimes(2);
    expect(axiosFailed).toHaveBeenCalledWith({});

    const centreError = {
      response: {
        data: {
          name: 'Example'
        }
      }
    };
    wrapper.vm.upsertFailed(centreError);
    expect(axiosFailed).toHaveBeenCalledTimes(3);
    expect(axiosFailed).toHaveBeenCalledWith(centreError);
    expect(wrapper.vm.centreErrors).toStrictEqual('Example');
  });

  it('subscribeToCentre() sends patch to server with centres', async () => {
    wrapper.vm.userStore.user = { id: 4 };
    wrapper.vm.centreStore.centres = [{ id: 2 }, { id: 3 }];
    wrapper.vm.centreId = 4;
    httpPost.mockResolvedValueOnce({ data: {} });
    expect(httpPost).not.toHaveBeenCalled();
    await wrapper.vm.subscribeToCentre();
    expect(goToNextPage).toHaveBeenCalledTimes(1);
    expect(httpPost).toHaveBeenCalledWith('users/centres/update/', {
      centre: 4,
      user: 4
    });
    expect(axiosFailed).not.toHaveBeenCalled();
  });

  it('subscribeToCentre() calls axiosFailed() on fail', async () => {
    wrapper.vm.userStore.user = { id: 4 };
    wrapper.vm.centreStore.centres = [{ id: 2 }, { id: 3 }];
    wrapper.vm.centreId = 2;

    httpPost.mockRejectedValueOnce({});
    expect(httpPost).not.toHaveBeenCalled();
    await wrapper.vm.subscribeToCentre();
    expect(httpPost).toHaveBeenCalledWith('users/centres/update/', {
      centre: 2,
      user: 4
    });
    expect(axiosFailed).toHaveBeenCalled();
    expect(goToNextPage).toHaveBeenCalledTimes(0);
  });

  it('updateSelectedColor() sets selectedColor', () => {
    wrapper.vm.selectedColor = '#FFFFFF';
    wrapper.vm.updateSelectedColor('#CCCCCC');
    expect(wrapper.vm.selectedColor).toStrictEqual('#CCCCCC');
  });

  it('test created()', () => {
    expect(wrapper.vm.centreName).toStrictEqual('Test Centre');
    expect(wrapper.vm.step).toStrictEqual(2);
  });

  it('openingDaysSubmitEnabled is true when opening days and country selected', () => {
    wrapper.vm.openingDays = [1];
    wrapper.vm.locationReady = true;
    expect(wrapper.vm.openingDaysSubmitEnabled).toStrictEqual(true);
  });

  it('openingDaysSubmitEnabled is false when no opening days', () => {
    wrapper.vm.openingDays = [];
    expect(wrapper.vm.openingDaysSubmitEnabled).toStrictEqual(false);
  });

  it('closingDaysSubmitEnabled is false when no country', () => {
    wrapper.vm.locationReady = false;
    expect(wrapper.vm.closingDaysSubmitEnabled).toStrictEqual(false);
  });

  it('updateLocationReady sets locationReady value', () => {
    wrapper.vm.locationReady = false;
    wrapper.vm.updateLocationReady(true);
    expect(wrapper.vm.locationReady).toStrictEqual(true);

    wrapper.vm.updateLocationReady(false);
    expect(wrapper.vm.locationReady).toStrictEqual(false);
  });

  it('centreNameRules handle empty value', () => {
    expect(wrapper.vm.centreNameRules[0]('')).toStrictEqual(
      'Centre Name is required'
    );
    expect(wrapper.vm.centreNameRules[0]('test')).toStrictEqual(true);
  });
});

describe('CentreSetup.vue created hook', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.clearAllMocks();
    mockCentres();
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.mock('src/functions/proxy/goToNextPage');
  });

  it('test created with policies already setup', () => {
    const centreStore = useCentreStore();
    const setupCentre = centreStore.setupCentre as Centre;
    setupCentre.name = 'Test Centre';
    setupCentre.openingDaysFirstSetupComplete = true;
    setupCentre.policiesFirstSetupComplete = true;

    wrapper = shallowMount(CentreSetup);
    expect(wrapper.vm.centreName).toStrictEqual('Test Centre');
    expect(wrapper.vm.step).toStrictEqual(5);
  });

  it('test created with closing days already setup', () => {
    const centreStore = useCentreStore();
    const setupCentre = centreStore.setupCentre as Centre;
    setupCentre.name = 'Test Centre';
    setupCentre.openingDaysFirstSetupComplete = true;
    setupCentre.closingDaysFirstSetupComplete = true;

    wrapper = shallowMount(CentreSetup);
    expect(wrapper.vm.centreName).toStrictEqual('Test Centre');
    expect(wrapper.vm.step).toStrictEqual(4);
  });

  it('test created with opening days already setup', () => {
    const centreStore = useCentreStore();
    const setupCentre = centreStore.setupCentre as Centre;
    setupCentre.name = 'Test Centre';
    setupCentre.openingDaysFirstSetupComplete = true;
    setupCentre.policiesFirstSetupComplete = false;
    wrapper = shallowMount(CentreSetup);
    expect(wrapper.vm.centreName).toStrictEqual('Test Centre');
    expect(wrapper.vm.step).toStrictEqual(3);
  });

  it('test created with no setup centre', () => {
    const centreStore = useCentreStore();
    centreStore.setupCentre = null;
    wrapper = shallowMount(CentreSetup);
    expect(wrapper.vm.centreName).toStrictEqual('');
    expect(wrapper.vm.step).toStrictEqual(1);
  });

  it('setSubscribeToCentreDialog shows or hides dialog', () => {
    wrapper.vm.setSubscribeToCentreDialog(false);
    expect(wrapper.vm.subscribeToCentreDialog).toStrictEqual(false);

    wrapper.vm.setSubscribeToCentreDialog(true);
    expect(wrapper.vm.subscribeToCentreDialog).toStrictEqual(true);
  });

  it('can add or remove from opening days', () => {
    wrapper.vm.openingDays = [1, 2, 5];
    wrapper.vm.updateSelectedDays(3, true);
    expect(wrapper.vm.openingDays).toStrictEqual([1, 2, 5, 3]);

    wrapper.vm.updateSelectedDays(2, false);
    expect(wrapper.vm.openingDays).toStrictEqual([1, 5, 3]);
  });
});
