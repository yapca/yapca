import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CentreSelect from 'src/pages/CentreSelect.vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import { mockUsers } from '../../mocks';
import type { SpyInstance } from 'vitest';

setup();

describe('CentreSelect.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpPost: SpyInstance;

  beforeEach(() => {
    vi.clearAllMocks();
    mockUsers();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(CentreSelect);
    httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
  });

  it('updating user centres calls goToNextPage', async () => {
    // Set no centres
    wrapper.vm.userStore.user.id = 4;
    wrapper.vm.userStore.user.centres = [];
    await nextTick();
    wrapper.vm.loading = true;
    wrapper.vm.context.goToNextPage = vi.fn();

    // Set centres exist
    wrapper.vm.userStore.user.centres = ['1'];
    await nextTick();

    expect(wrapper.vm.loading).toStrictEqual(false);
    expect(wrapper.vm.context.goToNextPage).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.context.goToNextPage).toHaveBeenCalledWith(
      wrapper.vm.router
    );
  });

  it('changeSubscription sends post requests to server with passed centres', async () => {
    // Set no centres
    wrapper.vm.userStore.user.id = 4;
    httpPost.mockResolvedValue({ data: {} });
    expect(httpPost).not.toHaveBeenCalled();
    wrapper.vm.selectedCentres = [2, 3];
    await wrapper.vm.changeSubscription();
    expect(httpPost).toHaveBeenCalledWith('users/centres/set/', {
      centres: [2, 3]
    });
    expect(axiosFailed).not.toHaveBeenCalled();
  });

  it('changeSubscription() calls axiosFailed() on fail', async () => {
    wrapper.vm.userStore.user.id = 4;

    wrapper.vm.context.goToNextPage = vi.fn();
    httpPost.mockRejectedValueOnce({});
    expect(httpPost).not.toHaveBeenCalled();
    wrapper.vm.selectedCentres = [{ id: 2 }, { id: 3 }];
    await wrapper.vm.changeSubscription();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.context.goToNextPage).toHaveBeenCalledTimes(0);
  });

  it('centres() gets state centres as array', () => {
    wrapper.vm.centreStore.centres = {
      2: { id: 2, setupComplete: true, name: 'Centre 2' },
      3: { id: 3, setupComplete: false, name: 'Centre 3' }
    };
    expect(wrapper.vm.centres).toStrictEqual([
      { id: 2, name: 'Centre 2', setupComplete: true }
    ]);
  });
});
