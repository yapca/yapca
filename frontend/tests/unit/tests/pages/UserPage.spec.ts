import { mount, shallowMount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import UserPage from 'src/pages/UserPage.vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import setup from 'tests/unit/functions/setup';
import { getRouter } from 'vue-router-mock';
import { nextTick } from 'vue';
import type { SpyInstance } from 'vitest';

setup();

describe('UserPage.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpPatch: SpyInstance;

  beforeEach(() => {
    vi.clearAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.mock('src/functions/auth/logout');
    wrapper = mount(UserPage);
    wrapper.vm.userStore.user = {
      id: 1,
      username: 'testUser',
      displayName: 'Test User',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'nurse',
      groups: [{ id: 4, name: 'nurse' }],
      isActive: true
    };
    httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it("validateDisplayName() doesn't call updateUserSettings() when invalid", async () => {
    const updateDisplayName = vi.spyOn(wrapper.vm, 'updateDisplayName');
    await wrapper.vm.validateDisplayName();
    expect(updateDisplayName).not.toHaveBeenCalled();
  });

  it('validateDisplayName() calls updateUserSettings() when valid', async () => {
    const updateDisplayName = vi.spyOn(wrapper.vm.context, 'updateDisplayName');
    const displayNameInput = wrapper.find('#displayNameInput');
    await displayNameInput.setValue('newname');
    expect(wrapper.vm.displayNameValid).toStrictEqual(true);
    await wrapper.vm.validateDisplayName();
    expect(updateDisplayName).toHaveBeenCalledTimes(2);
  });

  it('closeChangePasswordDialog() closes dialog', () => {
    wrapper.vm.changePasswordDialog = true;
    wrapper.vm.closeChangePasswordDialog();
    expect(wrapper.vm.changePasswordDialog).toStrictEqual(false);
  });

  it('closeEmailDialog() closes dialog', () => {
    wrapper.vm.changeEmailDialog = true;
    wrapper.vm.closeEmailDialog();
    expect(wrapper.vm.changeEmailDialog).toStrictEqual(false);
  });

  it('updating display name sends http patch request to server', async () => {
    httpPatch.mockResolvedValueOnce({ data: {} });
    expect(httpPatch).not.toHaveBeenCalled();
    await wrapper.vm.updateDisplayName();
    expect(httpPatch).toHaveBeenCalledWith('users/1/update/', {
      displayName: 'Test User'
    });
    expect(axiosFailed).not.toHaveBeenCalled();
  });

  it('updating display name handles error on server fail', async () => {
    httpPatch.mockRejectedValueOnce({});
    expect(httpPatch).not.toHaveBeenCalled();
    await wrapper.vm.updateDisplayName();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('updating display name handles missing user id', async () => {
    wrapper.vm.userStore.user.id = undefined;
    httpPatch.mockResolvedValueOnce({ data: {} });
    expect(httpPatch).not.toHaveBeenCalled();
    await wrapper.vm.updateDisplayName();
    expect(httpPatch).toHaveBeenCalledWith('users//update/', {
      displayName: 'Test User'
    });
    expect(axiosFailed).not.toHaveBeenCalled();
  });

  it('displayName input updates from store', async () => {
    expect(wrapper.vm.displayNameInput).toStrictEqual('Test User');
    wrapper.vm.userStore.user.displayName = 'new name';
    await nextTick();
    expect(wrapper.vm.displayNameInput).toStrictEqual('new name');
  });

  it('displayNameRules handle empty value', () => {
    expect(wrapper.vm.displayNameRules[0]('')).toStrictEqual(
      "Display Name can't be blank"
    );
    expect(wrapper.vm.displayNameRules[0]('test')).toStrictEqual(true);
  });

  it('leaving password change dialog via back button closes dialog', async () => {
    await wrapper.vm.openChangePasswordDialog();
    await getRouter().setHash('#not-change-password-dialog');
    await nextTick();
    expect(wrapper.vm.changePasswordDialog).toStrictEqual(false);
  });

  it('return to password change dialog via forward button opens dialog', async () => {
    wrapper.vm.changePasswordDialog = false;
    await getRouter().setHash('#change-password-dialog');
    expect(wrapper.vm.changePasswordDialog).toStrictEqual(true);
  });

  it('leaving email change dialog via back button closes dialog', async () => {
    await wrapper.vm.openChangeEmailDialog();
    await getRouter().setHash('#not-change-email-dialog');
    expect(wrapper.vm.changeEmailDialog).toStrictEqual(false);
  });

  it('return to email change dialog via forward button opens dialog', async () => {
    wrapper.vm.changeEmailDialog = false;
    await getRouter().setHash('#change-email-dialog');
    expect(wrapper.vm.changeEmailDialog).toStrictEqual(true);
  });

  it('buttons are visible for client user', async () => {
    wrapper.vm.userStore.user.group = 'client';
    expect(wrapper.vm.isClient).eql(true);
    await nextTick();
    expect(wrapper.find('#clientTOTPButtons').exists()).toBe(true);
  });
});

describe('AdditionalInfo.vue shallow mount', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpPatch: SpyInstance;

  beforeEach(() => {
    vi.clearAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.mock('src/functions/auth/logout');
    wrapper = shallowMount(UserPage);
    wrapper.vm.userStore.user = {
      id: 1,
      username: 'testUser',
      displayName: 'Test User',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'client',
      groups: [{ id: 4, name: 'client' }],
      isActive: true
    };
    httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
  });

  it('openSetupTOTPDialog does that', () => {
    wrapper.vm.setupTOTPDialog = false;
    wrapper.vm.openSetupTOTPDialog();
    expect(wrapper.vm.setupTOTPDialog).toStrictEqual(true);
  });

  it('openDisableTOTPDialog does that', () => {
    wrapper.vm.disableTOTPDialog = false;
    wrapper.vm.openDisableTOTPDialog();
    expect(wrapper.vm.disableTOTPDialog).toStrictEqual(true);
  });

  it('otpSetupComplete sends patch request and notification', async () => {
    httpPatch.mockResolvedValueOnce({ data: {} });
    expect(httpPatch).not.toHaveBeenCalled();
    await wrapper.vm.otpSetupComplete('newcode');
    expect(httpPatch).toHaveBeenCalledWith('users/1/update/', {
      otpDeviceKey: 'newcode'
    });
    expect(axiosFailed).not.toHaveBeenCalled();
  });
});
