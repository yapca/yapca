import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CentresPage from 'src/pages/CentresPage.vue';
import setup from 'tests/unit/functions/setup';
import { mockUsers } from '../../mocks';

setup();

describe('CentresPage.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.clearAllMocks();
    mockUsers();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(CentresPage);
  });

  it('centres() gets state centres as array', () => {
    wrapper.vm.centreStore.centres = {
      2: { id: 2, setupComplete: true, name: 'centre 1', anotherField: false },
      3: { id: 3, setupComplete: false, name: 'centre 2' }
    };
    expect(wrapper.vm.centres).toStrictEqual([
      { id: 2, isSubscribed: false, name: 'centre 1' }
    ]);
  });

  it('openCentreDialog() does that', () => {
    wrapper.vm.showCentreDialog = false;
    wrapper.vm.openCentreDialog({ id: 400 });
    expect(wrapper.vm.selectedCentreId).toStrictEqual(400);
    expect(wrapper.vm.showCentreDialog).toStrictEqual(true);
  });

  it('closeCentreDialog() does that', () => {
    wrapper.vm.showCentreDialog = true;
    wrapper.vm.closeCentreDialog();
    expect(wrapper.vm.showCentreDialog).toStrictEqual(false);
  });

  it('centreListHeaders is different if admin', () => {
    expect(wrapper.vm.centreListHeaders).toStrictEqual([
      {
        label: 'Name',
        name: 'name',
        field: 'name',
        sortable: true,
        align: 'left'
      },
      {
        label: 'Subscribed?',
        name: 'isSubscribed',
        field: 'isSubscribed',
        sortable: true,
        align: 'right'
      }
    ]);
    wrapper.vm.userStore.user.group = 'notAdmin';
    expect(wrapper.vm.centreListHeaders).toStrictEqual([
      {
        label: 'Name',
        name: 'name',
        field: 'name',
        sortable: true,
        align: 'left'
      }
    ]);
  });

  it('setCentreDialog can set centre dialog', () => {
    wrapper.vm.showCentreDialog = true;
    wrapper.vm.setCentreDialog(false);
    expect(wrapper.vm.showCentreDialog).toStrictEqual(false);
    wrapper.vm.setCentreDialog(true);
    expect(wrapper.vm.showCentreDialog).toStrictEqual(true);
  });

  it('centre header color changes based on centre color', () => {
    expect(wrapper.vm.selectedCentreHeaderColor).toStrictEqual('grey');
    wrapper.vm.centreStore.centres = {
      1: { id: 1, color: '#FAF34A' }
    };
    wrapper.vm.selectedCentreId = 1;
    expect(wrapper.vm.selectedCentreHeaderColor).toStrictEqual('#FAF34A');
  });
});
