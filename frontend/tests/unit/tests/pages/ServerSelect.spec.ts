import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ServerSelect from 'src/pages/ServerSelect.vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import goToNextPage from 'src/functions/proxy/goToNextPage';
import { getRouter } from 'vue-router-mock';
import { mockUsers } from '../../mocks';
import type { DOMWrapper } from '@vue/test-utils';

setup();

describe('ServerSelect.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let serverAddressInput: DOMWrapper<HTMLInputElement>;

  beforeEach(() => {
    vi.clearAllMocks();
    mockUsers();
    vi.mock('src/functions/proxy/goToNextPage');
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = mount(ServerSelect);
    serverAddressInput = wrapper.find('#serverAddressInput');
  });

  it('validate function fails if url is empty', async () => {
    const connectToServer = vi.spyOn(wrapper.vm.context, 'connectToServer');
    await serverAddressInput.setValue('');
    await wrapper.vm.validate();
    expect(connectToServer).not.toHaveBeenCalled();
  });

  it('validate function fails if url is not valid domain name', async () => {
    const connectToServer = vi.spyOn(wrapper.vm.context, 'connectToServer');
    await serverAddressInput.setValue('test');
    await wrapper.vm.validate();
    expect(connectToServer).not.toHaveBeenCalled();
  });

  it('validate function passes if url is valid domain name', async () => {
    wrapper.vm.context.connectToServer = vi.fn();
    await serverAddressInput.setValue('https://examplewebsite.com');
    expect(wrapper.vm.isValid).toStrictEqual(true);
    await wrapper.vm.validate();
    expect(wrapper.vm.context.connectToServer).toHaveBeenCalledTimes(1);
  });

  it('url errors reset on change', async () => {
    wrapper.vm.serverAddressErrors = ['test1', 'test2'];
    expect(wrapper.vm.serverAddressErrors).toStrictEqual(['test1', 'test2']);
    await serverAddressInput.setValue('test');
    expect(wrapper.vm.serverAddressErrors).toStrictEqual([]);
  });

  it('connectToServer goes to next page when successful', async () => {
    wrapper.vm.context.connection = () => {
      return {
        get: () => Promise.resolve()
      };
    };
    await serverAddressInput.setValue('https://examplewebsite.com');
    await wrapper.vm.context.connectToServer();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(goToNextPage).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.basicStore.appServerAddress).toStrictEqual(
      'https://examplewebsite.com'
    );
  });

  it('connectToServer does not go to next page when failed', async () => {
    wrapper.vm.context.connection = () => {
      return {
        get: () => Promise.reject()
      };
    };
    await wrapper.vm.connectToServer();
    expect(goToNextPage).toHaveBeenCalledTimes(0);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.checkingServerAddress).toStrictEqual(false);
  });

  it('validateUrl checks for valid domain', () => {
    expect(wrapper.vm.validateURL('1234')).toStrictEqual(false);
    expect(wrapper.vm.validateURL('abcs')).toStrictEqual(false);
    expect(wrapper.vm.validateURL('https://examplewebsite.com')).toStrictEqual(
      true
    );
  });

  it('adding address as query updates serverAddress and validates', async () => {
    const validate = vi.spyOn(wrapper.vm.context, 'validate');
    await getRouter().setQuery({ serverAddress: 'serverAddressToSet' });
    expect(wrapper.vm.queryAddress).toStrictEqual('serverAddressToSet');
    expect(wrapper.vm.serverAddress).toStrictEqual('serverAddressToSet');
    await nextTick();
    expect(validate).toHaveBeenCalledTimes(1);
  });

  it('empty address as query does not set or validate', async () => {
    wrapper.vm.serverAddress = 'oldValue';
    const validate = vi.spyOn(wrapper.vm.context, 'validate');
    await getRouter().setQuery({ serverAddress: '' });
    expect(wrapper.vm.queryAddress).toStrictEqual('');
    expect(wrapper.vm.serverAddress).toStrictEqual('oldValue');
    await wrapper.vm.$nextTick();
    expect(validate).toHaveBeenCalledTimes(0);
  });
});
