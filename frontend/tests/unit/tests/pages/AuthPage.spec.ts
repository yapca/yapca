/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { flushPromises, mount, shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import AuthPage from 'src/pages/AuthPage.vue';
import { useBasicStore } from 'src/stores/basicStore';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';

setup();

describe('Auth.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  beforeEach(() => {
    vi.clearAllMocks();
  });

  it('test registerAbility gets register ability from store', () => {
    wrapper = mount(AuthPage);
    expect(wrapper.vm.registerAbility).toStrictEqual(false);
  });

  it('test create where canRegister is true', async () => {
    const basicStore = useBasicStore();
    const httpGet = vi.spyOn(basicStore.http, 'get');
    httpGet.mockResolvedValueOnce({ data: { canRegister: true } });
    wrapper = shallowMount(AuthPage);
    expect(httpGet).toHaveBeenCalledTimes(1);
    expect(httpGet).toHaveBeenCalledWith('register-ability/1/');
    await flushPromises();
    expect(basicStore.registerAbility).toStrictEqual(true);
    expect(wrapper.vm.loading).toStrictEqual(false);
  });

  it('test create where canRegister is false', async () => {
    const basicStore = useBasicStore();
    const httpGet = vi.spyOn(basicStore.http, 'get');
    httpGet.mockResolvedValueOnce({ data: { canRegister: false } });
    wrapper = shallowMount(AuthPage);
    expect(httpGet).toHaveBeenCalledTimes(1);
    expect(httpGet).toHaveBeenCalledWith('register-ability/1/');
    await flushPromises();
    expect(basicStore.registerAbility).toStrictEqual(false);
    expect(wrapper.vm.loading).toStrictEqual(false);
  });

  it('test create where httpget fails', async () => {
    vi.mock('src/functions/proxy/AxiosFailed');
    const basicStore = useBasicStore();
    const httpGet = vi.spyOn(basicStore.http, 'get');
    httpGet.mockRejectedValueOnce({});
    wrapper = shallowMount(AuthPage);
    await flushPromises();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('card becomes square on small breakpoints', async () => {
    wrapper.vm.loading = false;
    wrapper.vm.$q.screen.xs = false;
    await nextTick();
    const authPageCard = wrapper.get('#authPageCard');
    expect(authPageCard.attributes().square).toStrictEqual('false');
    wrapper.vm.$q.screen.xs = true;
    await wrapper.vm.$nextTick();
    expect(authPageCard.attributes().square).toStrictEqual('true');
  });
});
