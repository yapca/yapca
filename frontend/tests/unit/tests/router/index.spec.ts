/* eslint-disable @typescript-eslint/no-explicit-any */
import { beforeEach, describe, expect, it, vi } from 'vitest';
import route from 'src/router';

import { createPinia, setActivePinia } from 'pinia';
import { useBasicStore } from 'src/stores/basicStore';
import { useUserStore } from 'src/stores/userStore';
import { useCentreStore } from 'src/stores/centresStore';
import type { Pinia } from 'pinia';
import type { Router } from 'vue-router';

describe('Router', () => {
  let router: Router;
  let basicStore: any;
  let centreStore: any;
  let userStore: any;

  beforeEach(() => {
    setActivePinia(createPinia());
    basicStore = useBasicStore();
    centreStore = useCentreStore();
    userStore = useUserStore();
    userStore.user = {
      id: 1,
      username: 'testUser',
      displayName: 'Test User',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'admin',
      groups: [{ id: 4, name: 'admin' }],
      isActive: true,
      centres: [],
      userClientTableHiddenRows: [],
      email: 'test@test.test',
      permissions: ['client_can_view_calendar', 'client_can_view_own_data']
    };
    basicStore.token = 'test';
    basicStore.http.get = vi
      .fn()
      .mockImplementation(() =>
        Promise.resolve({ data: [{ id: 4, setupComplete: true }] })
      );
    basicStore.http.patch = vi
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    router = route(null as unknown as { store: Pinia }) as Router;
  });

  it('goes to error page when backend fails to respond', async () => {
    basicStore.http.get.mockRejectedValueOnce({});
    await router.replace('/clients');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/error');
  });

  it('unauthenticated users are always redirected to auth', async () => {
    basicStore.token = null;
    await router.replace('/test');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/auth?nextUrl=/clients'
    );
  });

  it('admin user sent to clients page when centre exists', async () => {
    await router.replace('/clients');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/clients');
  });

  it('admin is sent to centre setup when incomplete centre exists', async () => {
    centreStore.centres = {};
    basicStore.http.get.mockResolvedValue({
      data: [{ id: 3, setupComplete: false }, { setupComplete: false }]
    });
    await router.replace('/admin');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/centre-setup?nextUrl=/admin'
    );
  });

  it('non admin user sent to error page when no centre exists', async () => {
    basicStore.http.get.mockResolvedValue({ data: [] });
    userStore.user.groups = [];
    centreStore.centres = {};
    await router.replace('/user-settings');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/error');
  });

  it('admin user sent to centre setup page when no centre exists', async () => {
    basicStore.http.get.mockResolvedValue({ data: [] });
    centreStore.centres = {};
    await router.replace('/centres');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/centre-setup?nextUrl=/centres'
    );
    await router.replace('/clients');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/centre-setup?nextUrl=/clients'
    );
    await router.replace('/test');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/centre-setup?nextUrl=/clients'
    );
    await router.replace('/admin');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/centre-setup?nextUrl=/admin'
    );

    await router.replace('/');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/centre-setup?nextUrl=/clients'
    );
    await router.replace('/user-settings');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/centre-setup?nextUrl=/user-settings'
    );
  });

  it('non admin user sent to clients page when centre exists', async () => {
    userStore.user.groups = [];
    await router.replace('/test');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/clients');
  });

  it('non admin user going to /admin is redirected to /clients', async () => {
    userStore.user.groups = [];
    await router.replace('/admin');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/clients');
  });

  it('client user going to /clients is redirected to /my-medical-info', async () => {
    userStore.user.groups = [{ id: 3, name: 'client' }];
    await router.replace('/clients');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/my-medical-info'
    );
  });

  it('client user without access to medical info is redirected to calendar', async () => {
    userStore.user.groups = [{ id: 3, name: 'client' }];
    userStore.user.permissions = ['client_can_view_calendar'];
    await router.replace('/my-medical-info');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/calendar');
  });

  it('client user without access to calendar is redirected to medical info', async () => {
    userStore.user.groups = [{ id: 3, name: 'client' }];
    userStore.user.permissions = ['client_can_view_own_data'];
    await router.replace('/calendar');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/my-medical-info'
    );
  });
  it('non client user going to /my-medical-info is redirected to /clients', async () => {
    userStore.user.groups = [{ id: 3, name: 'nurse' }];
    await router.replace('/my-medical-info');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/clients');
  });

  it('handles user without groups ', async () => {
    userStore.user.groups = undefined;
    await router.replace('/admin');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/clients');
  });

  it('non admin user going to /centre-setup is redirected to /clients', async () => {
    userStore.user.groups = [];
    await router.replace('/centre-setup');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/clients');
  });

  it('nurse user sent to error page when no centre exists', async () => {
    basicStore.http.get.mockResolvedValue({ data: [] });
    userStore.user.groups = [{ id: 3, name: 'nurse' }];
    centreStore.centres = {};
    await router.replace('/centres');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/error');
  });

  it('going to /auth when logged in redirects to /clients', async () => {
    centreStore.centres = {
      '4': {
        id: 4,
        name: 'Test Centre 4',
        created: 'before',
        openingDaysFirstSetupComplete: true,
        policies: [],
        policiesFirstSetupComplete: true,
        needs: [],
        needsFirstSetupComplete: true,
        setupComplete: true,
        openingDays: [2, 4, 6],
        editingCentreName: false,
        nameValid: false,
        color: '#FFFFFF',
        closedOnPublicHolidays: true,
        country: 'AU',
        state: 'QLD'
      }
    };
    await router.replace('/auth');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/clients');
  });

  it('when more than one centres exist, user is sent to centre-select', async () => {
    basicStore.http.get.mockResolvedValue({
      data: [
        { id: 4, setupComplete: true },
        { id: 3, setupComplete: true }
      ]
    });
    centreStore.centres = {};
    await router.replace('/centres');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/centre-select?nextUrl=/centres'
    );
  });

  it('does not go to centre-select if user already has centres', async () => {
    userStore.user.centres = [3];
    basicStore.http.get.mockResolvedValue({
      data: [
        { id: 4, setupComplete: true },
        { id: 3, setupComplete: true }
      ]
    });
    await router.replace('/user-settings');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/user-settings');
  });

  it('isAdmin() returns false if user group is not object', async () => {
    userStore.user.groups = [3];
    basicStore.http.get.mockResolvedValue({
      data: [
        { id: 4, setupComplete: true },
        { id: 3, setupComplete: true }
      ]
    });
    centreStore.centres = {};
    await router.replace('/test');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/centre-select?nextUrl=/clients'
    );
  });

  it('routes to serverselect page if app and no appServerAddress', async () => {
    basicStore.appServerAddress = '';
    basicStore.platform.electron = true;
    await router.replace('/test');
    expect(router.currentRoute.value.fullPath).toStrictEqual(
      '/server-select?nextUrl=/clients'
    );
  });

  it('history mode can be changed with process.env', () => {
    expect(router.options.history.base).toStrictEqual('/#');

    process.env.SERVER = 'true';
    router = route(null as unknown as { store: Pinia }) as Router;
    expect(router.options.history.base).toStrictEqual('');

    process.env.SERVER = '';
    process.env.VUE_ROUTER_MODE = 'history';
    router = route(null as unknown as { store: Pinia }) as Router;
    expect(router.options.history.base).toStrictEqual('');
  });

  it('handles missing user id', async () => {
    userStore.user.id = undefined;
    basicStore.platform.electron = false;
    centreStore.centres = {
      '4': {
        id: 4,
        setupComplete: true
      }
    };
    await router.replace('/user-settings');
    expect(router.currentRoute.value.fullPath).toStrictEqual('/user-settings');
    expect(basicStore.http.patch).toHaveBeenCalledWith('users//update/', {
      centres: [4]
    });
  });
});
