/* eslint-disable @typescript-eslint/no-explicit-any */
import { createPinia, setActivePinia } from 'pinia';
import { useUserStore } from 'src/stores/userStore';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import { useSocketStore } from 'src/stores/websocketStore';
import { useBasicStore } from 'src/stores/basicStore';
import { useCentreStore } from 'src/stores/centresStore';
import { useClientStore } from 'src/stores/clientsStore';
import setup from '../../functions/setup';
import { mockClients } from '../../mocks';
import type { SpyInstance } from 'vitest';
import type User from 'src/types/User';

setup();
describe('User Store', () => {
  let userStore: any;
  let centreStore: any;
  let clientStore: any;
  let basicStore: any;
  let exampleUser: User;
  let httpGet: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    exampleUser = {
      id: 1,
      username: 'testUser',
      displayName: 'Test User',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'nurse',
      groups: [{ id: 4, name: 'nurse' }],
      isActive: true,
      centres: [2, 4, 5],
      userClientTableHiddenRows: [],
      email: 'test@test.test'
    };
    setActivePinia(createPinia());
    userStore = useUserStore();
    userStore.createNotification = vi.fn();
    basicStore = useBasicStore();
    centreStore = useCentreStore();
    clientStore = useClientStore();
    mockClients();
    httpGet = vi.spyOn(basicStore.http, 'get');
  });

  it('updateUser sets user', () => {
    userStore.updateUser({
      id: 2,
      username: 'test',
      groups: [{ id: 2, name: 'nurse' }]
    });

    expect(userStore.users).toStrictEqual({
      '2': {
        group: 'nurse',
        id: 2,
        username: 'test',
        groups: [{ id: 2, name: 'nurse' }]
      }
    });
  });

  it('updateUser does nothing without id', () => {
    userStore.updateUser({ username: 'test' });
    expect(userStore.users).toStrictEqual({});
  });

  it('updateUser can update existing users', () => {
    userStore.users = {
      2: exampleUser
    };
    userStore.updateUser({
      id: 2,
      username: 'test',
      groups: [{ id: 2, name: 'nurse' }],
      displayName: null
    });

    expect(userStore.users).toStrictEqual({
      '2': {
        ...exampleUser,
        username: 'test',
        groups: [{ id: 2, name: 'nurse' }]
      }
    });

    it('updateUser can update logged in user', () => {
      userStore.user = {
        id: 1,
        username: 'testUser',
        displayName: 'Test User',
        accentColor: '#FFFFFF',
        darkTheme: false,
        group: 'nurse',
        groups: [{ id: 4, name: 'nurse' }],
        isActive: true,
        centres: [2, 4, 5],
        userClientTableHiddenRows: [],
        email: 'test@test.test'
      };
      userStore.users = {};
      userStore.updateUser({
        id: 1,
        username: 'test',
        groups: [{ id: 2, name: 'admin' }],
        userClientTableHiddenRows: '["ROW"]'
      });
      expect(userStore.users).toStrictEqual({});
      expect(userStore.user).toStrictEqual({
        id: 1,
        username: 'test',
        displayName: 'Test User',
        accentColor: '#FFFFFF',
        darkTheme: false,
        group: 'admin',
        groups: [{ id: 2, name: 'admin' }],
        isActive: true,
        centres: [2, 4, 5],
        userClientTableHiddenRows: ['ROW'],
        email: 'test@test.test'
      });
    });
  });

  it('updateUser allows centres to be passed', () => {
    userStore.user = exampleUser;
    userStore.users = {
      '1': exampleUser
    };
    userStore.updateUser({
      id: 1,
      username: 'testUser',
      displayName: 'Test User',
      accentColor: '#FFFFFF',
      darkTheme: null,
      isActive: true,
      centres: [1, 2, 3],
      userClientTableHiddenRows: ['Date Added']
    });

    expect(userStore.users).toStrictEqual({
      '1': {
        id: 1,
        username: 'testUser',
        displayName: 'Test User',
        accentColor: '#FFFFFF',
        darkTheme: false,
        group: 'nurse',
        groups: [{ id: 4, name: 'nurse' }],
        isActive: true,
        centres: [1, 2, 3],
        userClientTableHiddenRows: ['Date Added'],
        email: 'test@test.test'
      }
    });
    expect(userStore.user).toStrictEqual({
      id: 1,
      username: 'testUser',
      displayName: 'Test User',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'nurse',
      groups: [{ id: 4, name: 'nurse' }],
      isActive: true,
      centres: [1, 2, 3],
      userClientTableHiddenRows: ['Date Added'],
      email: 'test@test.test'
    });
  });

  it('updateUser ignores centre if not passed', () => {
    userStore.user = exampleUser;
    userStore.users = {
      '1': exampleUser
    };
    userStore.updateUser({
      id: 1,
      username: 'testUser',
      displayName: 'Test User',
      accentColor: '#FFFFFF',
      darkTheme: false,
      isActive: true,
      userClientTableHiddenRows: ['Date Added']
    });
    expect(userStore.users).toStrictEqual({
      '1': {
        id: 1,
        username: 'testUser',
        displayName: 'Test User',
        accentColor: '#FFFFFF',
        darkTheme: false,
        group: 'nurse',
        groups: [{ id: 4, name: 'nurse' }],
        isActive: true,
        centres: [2, 4, 5],
        userClientTableHiddenRows: ['Date Added'],
        email: 'test@test.test'
      }
    });
    expect(userStore.user).toStrictEqual({
      id: 1,
      username: 'testUser',
      displayName: 'Test User',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'nurse',
      groups: [{ id: 4, name: 'nurse' }],
      isActive: true,
      centres: [2, 4, 5],
      userClientTableHiddenRows: ['Date Added'],
      email: 'test@test.test'
    });
  });

  it('updateUser handles JSON userClientTableHiddenRows', () => {
    userStore.user = exampleUser;
    userStore.users = {
      '1': exampleUser
    };
    userStore.updateUser({
      id: 1,
      userClientTableHiddenRows: JSON.stringify(['Date Added'])
    });
    expect(userStore.user).toStrictEqual({
      id: 1,
      username: 'testUser',
      displayName: 'Test User',
      accentColor: '#FFFFFF',
      darkTheme: false,
      group: 'nurse',
      groups: [{ id: 4, name: 'nurse' }],
      isActive: true,
      centres: [2, 4, 5],
      userClientTableHiddenRows: ['Date Added'],
      email: 'test@test.test'
    });
  });
  it('updateUser mutation should reconnect ws on group change', () => {
    const wsStore = useSocketStore();
    const basicStore = useBasicStore();
    const wsReconnect = vi.spyOn(wsStore, 'WS_RECONNECT');
    userStore.user = exampleUser;
    userStore.users = {};
    basicStore.appServerAddress = 'exampleserveraddress';
    basicStore.token = 'AAAAA';

    expect(wsReconnect).toHaveBeenCalledTimes(0);
    expect(wsReconnect).toHaveBeenCalledTimes(0);
    userStore.updateUser({
      id: 1,
      username: 'test',
      groups: [{ id: 2, name: 'admin' }]
    });
    expect(wsReconnect).toHaveBeenCalledTimes(1);
    expect(wsReconnect).toHaveBeenCalledWith('exampleserveraddress', 'AAAAA');
  });

  it('updateUser mutation updates snackbar on email change', () => {
    userStore.user = exampleUser;
    userStore.users = {};
    userStore.updateUser({
      id: 1,
      email: 'newemail@test.com'
    });
    expect(userStore.createNotification).toHaveBeenCalledWith({
      message:
        "Your email was changed! You'll receive an email at your new address shortly.",
      type: 'positive'
    });
  });

  it('updateUser mutation does not update snackbar if email same', () => {
    userStore.user = exampleUser;
    userStore.users = {};
    userStore.updateUser({
      id: 1,
      email: 'test@test.test'
    });
    expect(userStore.createNotification).toHaveBeenCalledTimes(0);
  });

  it('updateUser sets client centres if user is client', () => {
    userStore.user = exampleUser;
    userStore.users = {};
    clientStore.activeClient = clientStore.clients[4];
    userStore.updateUser({
      id: 1,
      groups: [{ id: 4, name: 'client' }]
    });
    expect(userStore.users[1].centres).toStrictEqual([3]);
  });

  it('updateUser mutation should not reconnect ws on group same', () => {
    const wsStore = useSocketStore();
    const wsReconnect = vi.spyOn(wsStore, 'WS_RECONNECT');
    userStore.user = exampleUser;
    userStore.users = {};
    userStore.updateUser({
      id: 1,
      username: 'test',
      groups: [{ id: 4, name: 'nurse' }]
    });
    expect(wsReconnect).toHaveBeenCalledTimes(0);
  });

  it('updateUserCentres can update user centres', async () => {
    userStore.users[3] = exampleUser;
    await userStore.updateUserCentres({
      userId: 3,
      centreIds: [2, 4]
    });
    expect(httpGet).toHaveBeenCalledTimes(0);
    expect(userStore.users[3].centres).toStrictEqual([2, 4]);
  });

  it('updateUserCentres does nothing when no client', async () => {
    await userStore.updateUserCentres({
      userId: 634,
      centreIds: [4]
    });
    expect(httpGet).not.toHaveBeenCalledWith('request-reset-totp', {
      params: { userId: 2 }
    });
  });

  it('updateUserCentres gets centre data from api when own user updates', async () => {
    httpGet.mockResolvedValueOnce({
      data: [
        { id: 3, name: 'test centre 3' },
        { id: 4, name: 'test centre 4' },
        { name: 'test centre 5' }
      ]
    });
    userStore.user = exampleUser;
    await userStore.updateUserCentres({
      userId: 1,
      centreIds: [4]
    });
    expect(centreStore.centres).toStrictEqual({
      '3': { id: 3, name: 'test centre 3' },
      '4': { id: 4, name: 'test centre 4' }
    });
    expect(httpGet).toHaveBeenCalledWith('centres/');
  });
});
