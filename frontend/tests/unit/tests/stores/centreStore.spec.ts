/* eslint-disable @typescript-eslint/no-explicit-any */
import { createPinia, setActivePinia } from 'pinia';
import { useUserStore } from 'src/stores/userStore';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import { useBasicStore } from 'src/stores/basicStore';
import { useCentreStore } from 'src/stores/centresStore';
import { flushPromises } from '@vue/test-utils';
import setup from '../../functions/setup';
import type Centre from 'src/types/Centre';
import type { SpyInstance } from 'vitest';

setup();
describe('Centre Store', () => {
  let centreStore: any;
  let basicStore: any;
  let userStore: any;
  let httpGet: SpyInstance;
  let exampleCentre: Centre;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/stores/websocketStore');
    setActivePinia(createPinia());
    centreStore = useCentreStore();
    basicStore = useBasicStore();
    userStore = useUserStore();
    exampleCentre = {
      id: 2,
      name: 'Test Centre',
      openingDays: [1, 2, 3],
      openingDaysFirstSetupComplete: true,
      closingDaysFirstSetupComplete: true,
      policies: [1],
      policiesFirstSetupComplete: true,
      needs: [],
      needsFirstSetupComplete: true,
      setupComplete: true,
      created: 'before',
      editingCentreName: false,
      nameValid: false,
      color: '#FFFFFF',
      closedOnPublicHolidays: false,
      country: 'JP',
      state: ''
    };

    httpGet = vi.spyOn(basicStore.http, 'get');
  });

  it('can update existing centre', () => {
    exampleCentre.state = 'QLD';
    exampleCentre.needs = [1];
    exampleCentre.country = 'AU';
    centreStore.centres = {
      2: exampleCentre
    };
    centreStore.updateCentre({
      id: 2,
      name: 'New Test Centre',
      openingDaysFirstSetupComplete: true,
      policiesFirstSetupComplete: true,
      setupComplete: true,
      created: 'after',
      needsFirstSetupComplete: true,
      openingDays: [1, 2, 4],
      state: null
    });
    expect(centreStore.centres).toStrictEqual({
      2: {
        id: 2,
        name: 'New Test Centre',
        openingDays: [1, 2, 4],
        openingDaysFirstSetupComplete: true,
        closingDaysFirstSetupComplete: true,
        policies: [1],
        policiesFirstSetupComplete: true,
        needs: [1],
        needsFirstSetupComplete: true,
        setupComplete: true,
        created: 'after',
        editingCentreName: false,
        nameValid: false,
        color: '#FFFFFF',
        closedOnPublicHolidays: false,
        country: 'AU',
        state: 'QLD'
      }
    });
  });

  it('can updateCentre with new policies', async () => {
    httpGet.mockResolvedValueOnce({
      data: [
        { id: 400, description: 'Example 400' },
        { description: 'Example' }
      ]
    });
    centreStore.centres = {
      2: exampleCentre
    };
    centreStore.updateCentre({
      id: 2,
      name: 'New Test Centre',
      openingDaysFirstSetupComplete: true,
      policiesFirstSetupComplete: true,
      setupComplete: true,
      created: 'after',
      policies: [1, 2],
      needsFirstSetupComplete: false,
      color: '#AAAAAA',
      closedOnPublicHolidays: true,
      country: 'RX',
      state: 'QLD'
    });
    await flushPromises();
    expect(httpGet).toHaveBeenCalledTimes(1);
    expect(httpGet).toHaveBeenCalledWith('centres/2/policies');
    expect(centreStore.policies[400]).toStrictEqual({
      id: 400,
      description: 'Example 400'
    });
    expect(centreStore.centres).toStrictEqual({
      2: {
        id: 2,
        name: 'New Test Centre',
        openingDays: [1, 2, 3],
        openingDaysFirstSetupComplete: true,
        closingDaysFirstSetupComplete: true,
        policies: [1, 2],
        policiesFirstSetupComplete: true,
        needsFirstSetupComplete: false,
        setupComplete: true,
        created: 'after',
        editingCentreName: false,
        nameValid: false,
        needs: [],
        color: '#AAAAAA',
        closedOnPublicHolidays: true,
        country: 'RX',
        state: 'QLD'
      }
    });
  });

  it('can updateCentre with new needs', async () => {
    httpGet.mockResolvedValueOnce({
      data: [
        { id: 100, description: 'Example 100' },
        { description: 'Example' }
      ]
    });
    centreStore.centres = {
      2: {
        id: 2,
        name: 'Test Centre',
        openingDays: [1, 2, 3],
        openingDaysFirstSetupComplete: true,
        policies: [],
        policiesFirstSetupComplete: true,
        needs: [1],
        needsFirstSetupComplete: true,
        setupComplete: true,
        created: 'before',
        editingCentreName: false,
        nameValid: false,
        color: '#FFFFFF',
        closedOnPublicHolidays: true,
        country: 'AU',
        state: 'QLD'
      }
    };
    centreStore.updateCentre({
      id: 2,
      name: 'New Test Centre',
      openingDaysFirstSetupComplete: true,
      policiesFirstSetupComplete: true,
      setupComplete: true,
      needsFirstSetupComplete: false,
      created: 'after',
      openingDays: [1, 2, 4],
      needs: [
        { id: 1, description: 'Example Need 1' },
        { id: 1, description: 'Example Need 2' }
      ],
      country: 'UK',
      state: ''
    });
    await flushPromises();
    expect(httpGet).toHaveBeenCalledTimes(1);
    expect(httpGet).toHaveBeenCalledWith('centres/2/needs');
    expect(centreStore.needs[100]).toStrictEqual({
      id: 100,
      description: 'Example 100'
    });
    expect(centreStore.centres).toStrictEqual({
      2: {
        id: 2,
        name: 'New Test Centre',
        openingDays: [1, 2, 4],
        openingDaysFirstSetupComplete: true,
        policies: [],
        needs: [
          { id: 1, description: 'Example Need 1' },
          { id: 1, description: 'Example Need 2' }
        ],
        needsFirstSetupComplete: false,
        policiesFirstSetupComplete: true,
        setupComplete: true,
        created: 'after',
        editingCentreName: false,
        nameValid: false,
        color: '#FFFFFF',
        closedOnPublicHolidays: true,
        country: 'UK',
        state: ''
      }
    });
  });

  it('deleteCentre should remove user sub to centre', () => {
    userStore.user.centres = [1, 2, 3];
    centreStore.centres = {
      2: exampleCentre
    };
    centreStore.deleteCentre({ id: 2 });
    expect(userStore.user.centres).toStrictEqual([1, 3]);
  });

  it('deleteCentre does nothing when no id passed', () => {
    centreStore.centres = {
      2: exampleCentre
    };
    centreStore.deleteCentre({});
    expect(centreStore.centres).toStrictEqual({
      2: exampleCentre
    });
  });
});
