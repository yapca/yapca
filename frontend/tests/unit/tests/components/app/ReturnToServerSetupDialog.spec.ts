import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ReturnToServerSetupDialog from 'src/components/app/ReturnToServerSetupDialog.vue';
import setup from 'tests/unit/functions/setup';

setup();

describe('ReturnToServerSetupDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(ReturnToServerSetupDialog);
  });

  it('goToServerSetup resets state and goes to serverSelect', async () => {
    const routerPush = vi.spyOn(wrapper.vm.router, 'push');
    wrapper.vm.basicStore.token = '1234';
    wrapper.vm.basicStore.appServerAddress = 'exampleAppServerAddress';
    await wrapper.vm.goToServerSetup();
    expect(wrapper.vm.basicStore.token).toStrictEqual('');
    expect(wrapper.vm.basicStore.appServerAddress).toStrictEqual('');
    expect(routerPush).toHaveBeenCalledWith({ name: 'serverSelect' });
  });

  it('goToServerSetup catches router error', () => {
    const routerPush = vi.spyOn(wrapper.vm.router, 'push');
    routerPush.mockRejectedValueOnce('Example Error');
    wrapper.vm.goToServerSetup();
    expect(routerPush).toHaveBeenCalledWith({ name: 'serverSelect' });
  });
});
