import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import Services from 'src/components/clientCreation/Services.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockClients } from '../../../mocks';

setup();

describe('Services.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = mount(Services, { props: { clientId: 4 } });
  });

  it('finishServicesSetup() sends patch to server', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.vm.finishServicesSetup();
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      servicesSetup: true
    });
  });

  it('finishServicesSetup() calls axiosFailed on server error', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce(null);
    await wrapper.vm.finishServicesSetup();
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('finishServicesSetup() returns null when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.finishServicesSetup()).toStrictEqual(null);
  });

  it('sets surgical history from input', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.find('#surgicalHistoryInput').setValue('new surgical input');
    await wrapper.vm.updateFieldFromRowBlur({
      target: { id: 'surgicalHistoryInput' }
    });
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      surgicalHistory: 'new surgical input'
    });
  });

  it('sets medicalHistory from input', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.find('#medicalHistoryInput').setValue('new medical input');
    await wrapper.vm.updateFieldFromRowBlur({
      target: { id: 'medicalHistoryInput' }
    });
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      medicalHistory: 'new medical input'
    });
  });

  it('sets doctor from input', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.find('#gpNameInput').setValue('new doctor');
    await wrapper.vm.updateFieldFromRowBlur({
      target: { id: 'gpNameInput' }
    });
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      gpName: 'new doctor'
    });
  });

  it('sets doctor phone number from input', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce(null);
    await wrapper.find('#gpPhoneNumberInput').setValue('12345');
    await wrapper.vm.updateFieldFromRowBlur({
      target: { id: 'gpPhoneNumberInput' }
    });
    expect(httpPatch).toHaveBeenCalledWith('clients/4/update/', {
      gpPhoneNumber: '12345'
    });
  });

  it('updateFieldFromRowBlur does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: undefined });
    const updateFieldSpy = vi.spyOn(wrapper.vm.context, 'updateField');

    await wrapper.vm.updateFieldFromRowBlur({
      target: { id: 'caseFormulationInput' }
    });

    expect(updateFieldSpy).toHaveBeenCalledTimes(0);
  });
});
