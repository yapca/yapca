import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import LocationSelect from 'src/components/centres/calendar/LocationSelect.vue';
import setup from 'tests/unit/functions/setup';
import { mockCentres } from 'tests/unit/mocks';
import { nextTick } from 'vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';

setup();

describe('LocationSelect.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    mockCentres();
    wrapper = shallowMount(LocationSelect, {
      props: {
        centreId: 2
      }
    });
  });

  it('countries returns list of countries', () => {
    expect(wrapper.vm.countries.length).toStrictEqual(65);

    expect(wrapper.vm.countries[18]).toStrictEqual({
      label: 'Ireland',
      value: 'IE'
    });
  });

  it('countries is empty when unformattedCountries are loading', () => {
    wrapper.vm.unformattedCountries = null;
    expect(wrapper.vm.countries).toStrictEqual([]);
  });

  it('centreCountry returns the store value for country', () => {
    expect(wrapper.vm.centreCountry).toStrictEqual('AU');
  });

  it('centreState returns the store value for state', () => {
    expect(wrapper.vm.centreState).toStrictEqual('QLD');
  });

  it('selectedCountry updates when centreCountry changes', async () => {
    const countrySelect = wrapper.find('#countrySelector');
    countrySelect.element.value = 'UK';
    wrapper.vm.centresStore.centres[2].country = 'US';
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.selectedCountry).toStrictEqual('US');
  });

  it('selectedState updates when centreState changes', async () => {
    wrapper.vm.selectedState = 'TX';
    wrapper.vm.centresStore.centres[2].state = 'AZ';
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.selectedState).toStrictEqual('AZ');
  });

  it('updateCentre() calls axiosFailed on failure', async () => {
    wrapper.vm.axiosFailed = vi.fn();
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce('Example Error');
    await wrapper.vm.updateCentre({ country: 'UK', state: '' });
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('states is empty array if country has no states', () => {
    wrapper.vm.basicStore.http.patch = vi
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    wrapper.vm.selectedCountry = 'UK';
    expect(wrapper.vm.states).toStrictEqual([]);
  });

  it('states returns states for country', () => {
    wrapper.vm.basicStore.http.patch = vi
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    wrapper.vm.selectedCountry = 'NZ';
    expect(wrapper.vm.states.length).toStrictEqual(11);
    expect(wrapper.vm.states[3]).toStrictEqual({
      label: "Hawke's Bay",
      value: 'HKB'
    });
  });

  it('readyToSubmit is true when country with no states is selected', async () => {
    wrapper.vm.basicStore.http.patch = vi
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.selectedCountry = 'UK';
    wrapper.vm.selectedState = '';
    await nextTick();
    expect(emit).toHaveBeenCalledTimes(1);
    expect(emit).toHaveBeenCalledWith('location-ready-to-submit', true);
  });

  it('readyToSubmit is false when country with states is selected', async () => {
    wrapper.vm.basicStore.http.patch = vi
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.selectedCountry = 'US';
    wrapper.vm.selectedState = '';
    await nextTick();

    expect(emit).toHaveBeenCalledTimes(1);
    expect(emit).toHaveBeenCalledWith('location-ready-to-submit', false);
  });

  it('readyToSubmit is true when state and country selected', async () => {
    wrapper.vm.basicStore.http.patch = vi
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.selectedCountry = 'US';
    wrapper.vm.selectedState = 'TX';
    await nextTick();
    expect(emit).toHaveBeenCalledTimes(1);
    expect(emit).toHaveBeenCalledWith('location-ready-to-submit', true);
  });

  it('setSelectedCountry sends patch to server', async () => {
    wrapper.vm.basicStore.http.patch = vi
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    await wrapper.vm.setSelectedCountry('ZA');
    expect(wrapper.vm.basicStore.http.patch).toHaveBeenCalledWith(
      'centres/2/update/',
      {
        country: 'ZA',
        state: ''
      }
    );
  });

  it('setSelectedState sends patch to server', async () => {
    wrapper.vm.basicStore.http.patch = vi
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    await wrapper.vm.setSelectedState('EX');
    expect(wrapper.vm.basicStore.http.patch).toHaveBeenCalledWith(
      'centres/2/update/',
      {
        state: 'EX'
      }
    );
  });
});
