import { flushPromises, shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CentreClientDayWarningDialog from 'src/components/centres/calendar/CentreClientDayWarningDialog.vue';
import setup from 'tests/unit/functions/setup';
import { mockCentres, mockClients } from 'tests/unit/mocks';
import { nextTick } from 'vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import type { SpyInstance } from 'vitest';

setup();

describe('CentreClientDayWarningDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpPatch: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(CentreClientDayWarningDialog, {
      props: {
        centreId: 4,
        clientIds: [2, 3],
        dayId: 3
      }
    });
    httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
  });

  it('openClientDialog() opens dialog and sets selected client', () => {
    wrapper.vm.showClientDialog = false;
    wrapper.vm.openClientDialog(6);
    expect(wrapper.vm.showClientDialog).toStrictEqual(true);
    expect(wrapper.vm.selectedClientId).toStrictEqual(6);
  });

  it('closeClientDialog() closes dialog', () => {
    wrapper.vm.showClientDialog = true;
    wrapper.vm.closeClientDialog();
    expect(wrapper.vm.showClientDialog).toStrictEqual(false);
  });

  it('closeDialogWhenNoClients() emits close-dialog', async () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.clientStore.clients[2].visitingDays = [1];
    wrapper.vm.clientStore.clients[3].visitingDays = [1];
    await nextTick();
    expect(emit).toBeCalledTimes(1);
    expect(emit).toHaveBeenCalledWith('close-dialog', 3);
  });

  it('automaticClientList() gets clients with one visiting day', () => {
    expect(wrapper.vm.automaticClientList).toStrictEqual([2]);
  });

  it('manualClientList() gets clients with multiple visiting day', () => {
    expect(wrapper.vm.manualClientList).toStrictEqual([3]);
  });

  it('removeClientsVisitingDays() removes days from automatic clients', async () => {
    await wrapper.vm.removeClientsVisitingDays();
    expect(httpPatch).toHaveBeenCalledTimes(1);
    expect(httpPatch).toHaveBeenCalledWith('clients/2/update/', {
      visitingDays: [
        { centre: 4, day: 5, client: 2 },
        { centre: 5, day: 6, client: 2 }
      ]
    });
  });

  it('removeClientsVisitingDays() calls axiosFailed() on failure', async () => {
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.removeClientsVisitingDays();
    await flushPromises();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('automaticClientList() is an empty array if centreId is undefined', async () => {
    await wrapper.setProps({ centreId: null });
    expect(wrapper.vm.automaticClientList).toStrictEqual([]);
  });

  it('manualClientList() is an empty array if centreId is undefined', async () => {
    await wrapper.setProps({ centreId: null });
    expect(wrapper.vm.manualClientList).toStrictEqual([]);
  });

  it('removeClientsVisitingDays() does nothing if no dayId', async () => {
    await wrapper.setProps({ dayId: null });
    await wrapper.vm.removeClientsVisitingDays();
    expect(httpPatch).toHaveBeenCalledTimes(0);
  });

  it('setClientDialog shows or hides dialog', () => {
    wrapper.vm.setClientDialog(false);
    expect(wrapper.vm.showClientDialog).toStrictEqual(false);

    wrapper.vm.setClientDialog(true);
    expect(wrapper.vm.showClientDialog).toStrictEqual(true);
  });
});
