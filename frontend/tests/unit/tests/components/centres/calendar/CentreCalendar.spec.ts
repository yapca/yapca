import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CentreCalendar from 'src/components/centres/calendar/CentreCalendar.vue';
import setup from 'tests/unit/functions/setup';
import { mockCentres } from 'tests/unit/mocks';
import { nextTick } from 'vue';
import updateCentre from 'src/functions/centre/updateCentre';
import type { SpyInstance } from 'vitest';

setup();

describe('LocationSelect.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpGet: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/centre/updateCentre');
    wrapper = shallowMount(CentreCalendar, {
      props: {
        centreId: 2
      }
    });
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    mockCentres();
    httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
  });

  it('changing centreId prop calls getEvents', async () => {
    wrapper.vm.context.getEvents = vi.fn();
    httpGet.mockResolvedValueOnce({ data: [{ id: 504 }] });
    await wrapper.setProps({ centreId: 3 });
    expect(wrapper.vm.context.getEvents).toHaveBeenCalledOnce();
  });

  it('updateRange sends get request to server', async () => {
    httpGet.mockResolvedValueOnce({ data: [{ id: 504 }] });
    await wrapper.vm.updateRange({
      newStart: '2020-10-15',
      newEnd: '2020-10-18'
    });
    expect(httpGet).toHaveBeenCalledWith('events/2020-10-08/2020-10-25/', {
      params: {
        centre: 2,
        eventType: 'User Close Event'
      }
    });
  });

  it('getEvents returns null if no start value', () => {
    wrapper.vm.start = null;
    expect(wrapper.vm.getEvents()).toStrictEqual(null);
    expect(httpGet).toHaveBeenCalledTimes(0);
  });

  it('storeEvents get events from store', () => {
    expect(wrapper.vm.events).toStrictEqual([
      {
        id: 1,
        name: 'event 1',
        color: '#4CAF50',
        start: '2022-01-08',
        end: '2022-01-10',
        centre: 2,
        eventType: 'User Close Event',
        timePeriod: 172800
      },
      {
        id: 4,
        name: 'event 4',
        color: '#B00020',
        start: '2022-10-02',
        end: '2022-10-03',
        centre: 2,
        eventType: 'Public Holiday',
        timePeriod: 86400
      }
    ]);
  });

  it('storeEvents removes country holidays if toggled', async () => {
    wrapper.vm.centreStore.centres[2].closedOnPublicHolidays = false;
    await nextTick();
    expect(wrapper.vm.events).toStrictEqual([
      {
        id: 1,
        name: 'event 1',
        color: '#4CAF50',
        start: '2022-01-08',
        end: '2022-01-10',
        centre: 2,
        eventType: 'User Close Event',
        timePeriod: 172800
      }
    ]);
  });

  it('deleteEvent sends delete request to server', () => {
    const httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
    httpDelete.mockResolvedValueOnce(true);
    wrapper.vm.deleteCloseEvent(3);
    expect(httpDelete).toHaveBeenCalledWith('events/3');
  });

  it('setClosedOnPublicHolidaysValue sets centre value', async () => {
    await wrapper.vm.setClosedOnPublicHolidaysValue();
    expect(updateCentre).toHaveBeenCalledWith({
      id: 2,
      closedOnPublicHolidays: false
    });

    wrapper.vm.centreStore.centres[2].closedOnPublicHolidays = false;

    await wrapper.vm.setClosedOnPublicHolidaysValue();
    expect(updateCentre).toHaveBeenCalledWith({
      id: 2,
      closedOnPublicHolidays: true
    });
  });

  it('setClosedOnPublicHolidaysValue does nothing if no centreId', async () => {
    await wrapper.setProps({ centreId: null });
    await wrapper.vm.setClosedOnPublicHolidaysValue();
    expect(updateCentre).toHaveBeenCalledTimes(0);
  });

  it('eventsNeedUpdating calls getEvents', async () => {
    wrapper.vm.context.getEvents = vi.fn();
    wrapper.vm.centreStore.eventsNeedUpdating = true;
    await nextTick();
    expect(wrapper.vm.context.getEvents).toHaveBeenCalledOnce();
    expect(wrapper.vm.centreStore.eventsNeedUpdating).toStrictEqual(false);
  });
});
