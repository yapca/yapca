import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ClientCardHeaderButtons from 'src/components/clients/ClientCardHeaderButtons.vue';
import setup from 'tests/unit/functions/setup';
import { mockClients } from 'tests/unit/mocks';

setup();

describe('ClientCardHeaderButtons.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    wrapper = shallowMount(ClientCardHeaderButtons, {
      props: {
        clientId: 4
      }
    });
  });

  it('gets client from store', () => {
    expect(wrapper.vm.client.firstName).toStrictEqual('test name 4');
  });

  it('client is undefined when not in store', async () => {
    await wrapper.setProps({ clientId: 738 });
    expect(wrapper.vm.client).toStrictEqual(undefined);
  });

  it('setCarePlanHistoryDialog sets dialog', () => {
    wrapper.vm.setCarePlanHistoryDialog(false);
    expect(wrapper.vm.showCarePlanHistoryDialog).toStrictEqual(false);

    wrapper.vm.setCarePlanHistoryDialog(true);
    expect(wrapper.vm.showCarePlanHistoryDialog).toStrictEqual(true);
  });

  it('setAbsenceHistoryDialog sets dialog', () => {
    wrapper.vm.setAbsenceHistoryDialog(false);
    expect(wrapper.vm.showAbsenceHistoryDialog).toStrictEqual(false);

    wrapper.vm.setAbsenceHistoryDialog(true);
    expect(wrapper.vm.showAbsenceHistoryDialog).toStrictEqual(true);
  });

  it('setClientUserPermissionsDialog sets dialog', () => {
    wrapper.vm.setClientUserPermissionsDialog(false);
    expect(wrapper.vm.showClientUserPermissionsDialog).toStrictEqual(false);

    wrapper.vm.setClientUserPermissionsDialog(true);
    expect(wrapper.vm.showClientUserPermissionsDialog).toStrictEqual(true);
  });

  it('closeDialog emits close-dialog', () => {
    wrapper.vm.setDeleteClientConfirmDialog(true);
    expect(wrapper.vm.showDeleteClientConfirmDialog).toStrictEqual(true);

    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.closeDialog();
    expect(wrapper.vm.showAbsenceHistoryDialog).toStrictEqual(false);
    expect(emit).toHaveBeenCalledWith('close-dialog');
  });

  it('showMakeClientUserDialogText changes based on account status', () => {
    expect(wrapper.vm.showMakeClientUserDialogText).toStrictEqual(
      'Allow test name 4 To Login'
    );
    wrapper.vm.clientStore.clients[4].accountStatus = 'Active';
    expect(wrapper.vm.showMakeClientUserDialogText).toStrictEqual(
      'Prevent test name 4 From Logging In'
    );
    wrapper.vm.clientStore.clients[4].accountStatus = 'abcd';
    expect(wrapper.vm.showMakeClientUserDialogText).toStrictEqual('');
  });
});
