import { mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import ClientDialogTable from 'src/components/clients/ClientDialogTable.vue';
import setup from 'tests/unit/functions/setup';
import { differenceInYears } from 'date-fns';
import updateField from 'src/functions/client/updateField';
import { mockClients } from '../../../mocks';

setup();

describe('ClientDialogTable.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    vi.mock('src/functions/client/updateField');
    wrapper = mount(ClientDialogTable, {
      props: {
        clientId: 4,
        clientData: [
          {
            text: 'Active?',
            booleanValue: 'isActive',
            editable: true,
            value: 'isActive'
          }
        ]
      }
    });
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('client() returns a formatted version of a client', () => {
    expect(wrapper.vm.formattedClient).toStrictEqual({
      id: 4,
      admittedBy: 'Unknown',
      isActive: true,
      gender: 'Male',
      createdDate: '26/06/20',
      agreedPolicies: [],
      centres: [3],
      visitingDays: [
        { day: 1, centre: 3, client: 4 },
        { day: 3, centre: 3, client: 4 },
        { day: 4, centre: 2, client: 4 },
        { day: 1, centre: 2, client: 4 },
        { day: 6, centre: 3, client: 4 }
      ],
      firstName: 'test name 4',
      surname: 'test surname 4',
      basicInfoSetup: true,
      additionalInfoSetup: true,
      servicesSetup: true,
      abilitySetup: true,
      setupComplete: false,
      visitingDaysSetup: true,
      address: 'Test Address 4',
      phoneNumber: '4444444',
      dateOfBirth: `15/07/91 (${differenceInYears(
        new Date(),
        new Date(1991, 6, 15)
      )})`,
      nextOfKin: [
        { id: 1, name: 'Test NoK 1', phoneNumber: '11111111' },
        { id: 2, name: 'Test NoK 2', phoneNumber: '22222222' },
        { id: 3, name: 'Test NoK 3', phoneNumber: '33333333' },
        { id: 4, name: 'Test NoK 4', phoneNumber: '44444444' }
      ],
      publicHealthNurseName: 'PHN 4',
      publicHealthNursePhoneNumber: 'PHN 4444444',
      homeHelpName: 'HH 4',
      homeHelpPhoneNumber: 'HH 4444444',
      gpName: 'GP 4',
      gpPhoneNumber: 'GP 4444444',
      chemistName: 'C 4',
      chemistPhoneNumber: 'C 4444444',
      medicalHistory: 'Client 4 Medical History',
      surgicalHistory: 'Client 4 Surgical History',
      medications: [
        {
          id: 1,
          name: 'Test Med 1',
          dosage: '1',
          frequency: '5',
          inactiveDate: '2020-03-12'
        },
        {
          id: 2,
          name: 'Test Med 2',
          dosage: '2',
          frequency: '6',
          inactiveDate: undefined
        },
        {
          id: 3,
          name: 'Test Med 3',
          dosage: '3',
          frequency: '7',
          inactiveDate: '3020-03-12'
        },
        {
          id: 4,
          name: 'Test Med 4',
          dosage: '4',
          frequency: '8',
          inactiveDate: undefined
        }
      ],
      mobility: 'Walks Unaided',
      toileting: 'Incontinent',
      hygiene: 'Self',
      sight: 'Glasses',
      hearing: 'Normal',
      usesDentures: true,
      caseFormulation: 'Example Case Formulation 4',
      dietSetup: true,
      allergies: 'Allergies List 4',
      intolerances: 'Intolerances List 4',
      requiresAssistanceEating: true,
      thicknerGrade: 2,
      feedingRisks: [
        {
          id: 1,
          feedingRisk: 'Poor Appetite'
        },
        {
          id: 2,
          feedingRisk: 'Chewing'
        }
      ],
      needs: [1, 2, 3],
      documents: [
        {
          id: 1,
          name: 'testFile1',
          originalName: 'testFile1.txt',
          document: 'testserver/testFile1.txt',
          client: 4,
          size: 400,
          createdDate: '2022-01-01'
        },
        {
          id: 2,
          name: 'testFile2',
          originalName: 'testFile2.png',
          document: 'testserver/testFile2.png',
          client: 4,
          size: 600,
          createdDate: '2022-01-02'
        },
        {
          id: 3,
          name: 'testFile3',
          originalName: 'testFile3.pdf',
          document: 'testserver/testFile3.pdf',
          client: 4,
          size: 302,
          createdDate: '2022-01-03'
        }
      ],
      storageSize: 20,
      reasonForInactivity: 'Deceased',
      accountStatus: 'No Account'
    });
  });

  it('client() returns unknown when admitted by is not linked to user', () => {
    wrapper.vm.clientStore.clients[4].admittedBy = 3;
    expect(wrapper.vm.formattedClient.admittedBy).toStrictEqual('Unknown');
  });

  it('clientRowValue() returns empty string if no row value', () => {
    const row = { value: 'test' };
    const client = {};
    const clientRowValue = wrapper.vm.clientRowValue(client, row);
    expect(clientRowValue).toStrictEqual('');
  });

  it('clientRowValue() returns row value when it exists', () => {
    const row = { value: 'test' };
    const client = { test: 'data' };
    const clientRowValue = wrapper.vm.clientRowValue(client, row);
    expect(clientRowValue).toStrictEqual('data');
  });

  it('client() returns empty null when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.client).toStrictEqual(undefined);
  });

  it('updateFieldFromRow calls updateField', async () => {
    await wrapper.findComponent('#isActiveInput').trigger('click');
    expect(updateField).toHaveBeenCalledWith(false, 'isActive', 4);
  });

  it('updateRow calls updateField', () => {
    wrapper.vm.updateRow(4, 'firstName', 'newFirstName');
    expect(updateField).toHaveBeenCalledWith('newFirstName', 'firstName', 4);
  });

  it('updateFieldNow sets value and calls updateField', () => {
    expect(wrapper.vm.clientStore.clients[4].firstName).toStrictEqual(
      'test name 4'
    );
    wrapper.vm.updateFieldNow('newFirstName', 'firstName', 4);
    expect(wrapper.vm.clientStore.clients[4].firstName).toStrictEqual(
      'newFirstName'
    );
    expect(updateField).toHaveBeenCalledWith('newFirstName', 'firstName', 4);
  });

  it('maxDateToday() returns false for date before 1910', () => {
    expect(wrapper.vm.maxDateToday('1909/01/03')).toStrictEqual(false);
  });

  it('maxDateToday() returns true for valid date', () => {
    expect(wrapper.vm.maxDateToday('2022/01/03')).toStrictEqual(true);
  });

  it('maxDateToday() returns false for date after today', () => {
    expect(wrapper.vm.maxDateToday('2100/01/03')).toStrictEqual(false);
  });

  it('updateCheckboxFromRow does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(
      await wrapper.vm.updateCheckboxFromRow(true, 'requiresAssistanceEating')
    ).toStrictEqual(null);
    expect(updateField).toHaveBeenCalledTimes(0);
  });

  it('updateFieldFromRow does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    await wrapper.vm.updateFieldFromRow('intolerances');
    expect(updateField).toHaveBeenCalledTimes(0);
  });

  it('updateFieldNow does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    await wrapper.vm.updateFieldNow('newValue', 'intolerances');
    expect(updateField).toHaveBeenCalledTimes(0);
  });
});
