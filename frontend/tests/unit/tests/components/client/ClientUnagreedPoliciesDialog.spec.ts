import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ClientUnagreedPoliciesDialog from 'src/components/clients/ClientUnagreedPoliciesDialog.vue';
import setup from 'tests/unit/functions/setup';
import updatePolicies from 'src/functions/policies/updatePolicies';

setup();

describe('ClientUnagreedPoliciesDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    vi.mock('src/functions/policies/updatePolicies', () => ({
      default: vi.fn(() => Promise.resolve())
    }));
    wrapper = shallowMount(ClientUnagreedPoliciesDialog, {
      props: {
        clientId: 4,
        unagreedPolicies: [
          {
            centre: 2,
            unagreedPolicies: [{ id: 1, description: 'Test Policy 1' }]
          },
          {
            centre: 3,
            unagreedPolicies: [
              { id: 2, description: 'Test Policy 2' },
              { id: 3, description: 'Test Policy 3' }
            ]
          }
        ]
      }
    });
  });

  it('totalUnagreedPolicies() returns the number of unagreed policies', () => {
    expect(wrapper.vm.totalUnagreedPolicies).toStrictEqual(3);
  });

  it('policyPlural() returns "policies" when there are multiple unagreed policies', () => {
    expect(wrapper.vm.policyPlural).toStrictEqual('policies');
  });

  it('cardText() returns a count of new policies when there are multiple unagreed policies', () => {
    expect(wrapper.vm.cardText).toStrictEqual(
      '3 new policies have been added.'
    );
  });

  it('acceptPolicies() calls updatePolicies', async () => {
    await wrapper.vm.acceptPolicies();
    expect(updatePolicies).toHaveBeenCalledTimes(1);
    expect(updatePolicies).toHaveBeenCalledWith(4);
  });

  it('totalUnagreedPolicies() is 0 when no unagreed policies are set', async () => {
    await wrapper.setProps({ unagreedPolicies: [] });
    expect(wrapper.vm.totalUnagreedPolicies).toStrictEqual(0);
  });

  it('policyPlural() returns "policy" when there are single unagreed policy', async () => {
    await wrapper.setProps({
      unagreedPolicies: [
        {
          centre: 2,
          unagreedPolicies: [{ id: 1, description: 'Test Policy 1' }]
        }
      ]
    });
    expect(wrapper.vm.policyPlural).toStrictEqual('policy');
  });

  it('cardText() returns "A new policy has been added" when there is a single unagreed policy', async () => {
    await wrapper.setProps({
      unagreedPolicies: [
        {
          centre: 2,
          unagreedPolicies: [{ id: 1, description: 'Test Policy 1' }]
        }
      ]
    });
    expect(wrapper.vm.cardText).toStrictEqual('A new policy has been added.');
  });

  it('acceptPolicies() does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    await wrapper.vm.acceptPolicies(4);
    expect(updatePolicies).toHaveBeenCalledTimes(0);
  });
});
