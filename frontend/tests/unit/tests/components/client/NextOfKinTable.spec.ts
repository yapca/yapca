import { mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import NextOfKinTable from 'src/components/clients/NextOfKinTable.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { nextTick } from 'vue';
import { mockClients } from '../../../mocks';

setup();

describe('NextOfKinTable.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = mount(NextOfKinTable, { props: { clientId: 4 } });
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('nextOfKin() returns store next of kin', () => {
    expect(wrapper.vm.nextOfKin).toStrictEqual([
      { id: 1, name: 'Test NoK 1', phoneNumber: '11111111' },
      { id: 2, name: 'Test NoK 2', phoneNumber: '22222222' },
      { id: 3, name: 'Test NoK 3', phoneNumber: '33333333' },
      { id: 4, name: 'Test NoK 4', phoneNumber: '44444444' }
    ]);
  });

  it('nextOfKin() is empty when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.nextOfKin).toStrictEqual([]);
  });

  it('createNextOfKin() sends a post request to server', async () => {
    wrapper.vm.showCreateNextOfKinDialog = true;
    await nextTick();
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce({});
    await wrapper
      .findComponent('#nextOfKinCreateSection')
      .find('#nextOfKinCreateNameField')
      .setValue('New NoK');
    await wrapper
      .findComponent('#nextOfKinCreateSection')
      .find('#nextOfKinCreatePhoneNumberField')
      .setValue('9999999');
    await wrapper.vm.createNextOfKin();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledWith('nextOfKin/', {
      name: 'New NoK',
      phoneNumber: '9999999',
      client: 4
    });
  });

  it('createNextOfKin() calls axiosFailed() on failure', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce({});
    await wrapper.vm.createNextOfKin();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('deleteNextOfKin() sends a delete request to server', async () => {
    const httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
    httpDelete.mockResolvedValueOnce({});
    await wrapper.find('.deleteNextOfKinButton').trigger('click');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpDelete).toHaveBeenCalledWith('nextOfKin/1/delete/');
  });

  it('deleteNextOfKin() calls axiosFailed() on failure', async () => {
    const httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
    httpDelete.mockRejectedValueOnce({});
    await wrapper.vm.deleteNextOfKin({
      id: 3,
      name: 'Test NoK 3',
      phoneNumber: '33333333'
    });
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('deleteNextOfKin() does nothing when only one nok exists', async () => {
    wrapper.vm.clientStore.clients[4].nextOfKin = [{ id: 3 }];
    const httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
    httpDelete.mockResolvedValueOnce({});
    await wrapper.find('.deleteNextOfKinButton').trigger('click');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpDelete).toHaveBeenCalledTimes(0);
  });

  it('updateNextOfKin() sends a patch request to server', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.updateNextOfKin(3, 'phoneNumber', '33333333');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('nextOfKin/3/update/', {
      phoneNumber: '33333333'
    });
  });

  it('updateNextOfKin() calls axiosFailed() on failure', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.updateNextOfKin(3, 'phoneNumber', '33333333');
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('openNextOfKinDialog() resets values and opens dialog', () => {
    wrapper.vm.newNOKName = 'New NoK';
    wrapper.vm.newNOKPhoneNumber = '9999999';
    wrapper.vm.createNextOfKinDialog = false;
    wrapper.vm.openNextOfKinDialog();
    expect(wrapper.vm.newNOKName).toStrictEqual('');
    expect(wrapper.vm.newNOKPhoneNumber).toStrictEqual('');
    expect(wrapper.vm.showCreateNextOfKinDialog).toStrictEqual(true);
  });

  it('nextOfKinValid is true when values are filled', () => {
    wrapper.vm.newNOKName = '';
    wrapper.vm.newNOKPhoneNumber = '';
    expect(wrapper.vm.nextOfKinValid).toStrictEqual(false);
    wrapper.vm.newNOKName = 'New NoK';
    expect(wrapper.vm.nextOfKinValid).toStrictEqual(false);
    wrapper.vm.newNOKPhoneNumber = '9999999';
    expect(wrapper.vm.nextOfKinValid).toStrictEqual(true);
  });

  it('deleteButtonColor changes when only 1 nok', async () => {
    expect(wrapper.vm.deleteButtonColor).toStrictEqual('negative');
    wrapper.vm.clientStore.clients[4].nextOfKin = [{ id: 3 }];
    await nextTick();
    expect(wrapper.vm.deleteButtonColor).toStrictEqual('grey');
  });
});
