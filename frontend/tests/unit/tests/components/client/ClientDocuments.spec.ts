import { mount, shallowMount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import ClientDocuments from 'src/components/clients/ClientDocuments.vue';
import setup from 'tests/unit/functions/setup';
import fileDownload from 'js-file-download';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import write_blob from 'capacitor-blob-writer';
import { Mediastore } from '@agorapulse/capacitor-mediastore';
import { Filesystem } from '@capacitor/filesystem';

import { mockClients } from '../../../mocks';

setup();

const exampleDocument = {
  id: 3,
  name: 'testFile3',
  originalName: 'testFile3.txt',
  document: 'testserver/testFile3.txt'
};

describe('ClientDocuments.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    vi.mock('@capacitor/filesystem', () => ({
      Filesystem: {
        getUri: () => {
          return { uri: 'filesystemPath' };
        }
      },
      Directory: {
        Cache: 'CACHE'
      }
    }));
    vi.mock('capacitor-blob-writer', () => ({
      default: vi.fn()
    }));
    vi.mock('js-file-download', () => ({
      default: vi.fn()
    }));
    vi.mock('@agorapulse/capacitor-mediastore', () => ({
      Mediastore: {
        saveToDownloads: vi.fn()
      }
    }));

    wrapper = shallowMount(ClientDocuments, { props: { clientId: 4 } });
    wrapper.vm.basicStore.platform.mobile = false;
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('formattedDate() returns date converted to readable format', () => {
    expect(wrapper.vm.formattedDate('2020-09-28T17:38:36Z')).toStrictEqual(
      '28/09/20'
    );
  });

  it('getFileSize() converts filesizes correctly', () => {
    expect(wrapper.vm.getFileSize(12345)).toStrictEqual('12.06 KB');
    expect(wrapper.vm.getFileSize(1234567)).toStrictEqual('1.18 MB');
    expect(wrapper.vm.getFileSize(12345678912)).toStrictEqual('11.5 GB');
    expect(wrapper.vm.getFileSize(1234567891234)).toStrictEqual('1.12 TB');
  });

  it('downloadDocument() calls fileDownload ', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockResolvedValueOnce({ data: 'example data' });
    await wrapper.vm.downloadDocument(exampleDocument);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpGet).toHaveBeenCalledWith('clientDocuments/3/download', {
      onDownloadProgress: expect.any(Function),
      responseType: 'blob',
      timeout: 60000
    });
    expect(fileDownload).toBeCalledWith('example data', 'testFile3.txt');
  });

  it('downloadDocument() calls axiosFailed on failure ', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockRejectedValueOnce({});
    await wrapper.vm.downloadDocument(exampleDocument);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('getFileNameWithoutExtension() returns file without extension', () => {
    const fileName = wrapper.vm.getFileNameWithoutExtension('test.file.txt');
    expect(fileName).toStrictEqual('test.file');
  });

  it('documents() returns client documents', () => {
    expect(wrapper.vm.documents).toStrictEqual([
      {
        id: 1,
        name: 'testFile1',
        originalName: 'testFile1.txt',
        document: 'testserver/testFile1.txt',
        client: 4,
        size: 400,
        createdDate: '2022-01-01'
      },
      {
        id: 2,
        name: 'testFile2',
        originalName: 'testFile2.png',
        document: 'testserver/testFile2.png',
        client: 4,
        size: 600,
        createdDate: '2022-01-02'
      },
      {
        id: 3,
        name: 'testFile3',
        originalName: 'testFile3.pdf',
        document: 'testserver/testFile3.pdf',
        client: 4,
        size: 302,
        createdDate: '2022-01-03'
      }
    ]);
  });

  it('documents() returns empty array when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.documents).toStrictEqual([]);
  });

  it('onDocumentAdded() sets selected documents', () => {
    const event = {
      target: {
        files: [
          { name: 'testFile.txt', size: 0 },
          { name: 'testFile2.txt', size: 20 },
          { name: 'testFile3.txt', size: 5 }
        ]
      }
    };
    wrapper.vm.onDocumentAdded(event);
    expect(wrapper.vm.selectedDocuments).toStrictEqual([
      {
        file: { name: 'testFile.txt', size: 0 },
        newName: 'testFile',
        uploaded: false,
        uploadStatus: 0,
        removed: false,
        error: 'File is empty and will not be uploaded'
      },
      {
        file: { name: 'testFile2.txt', size: 20 },
        newName: 'testFile2',
        uploaded: false,
        uploadStatus: 0,
        removed: false,
        error:
          "File is too large and won't be uploaded, file must be smaller than 10 B"
      },
      {
        file: { name: 'testFile3.txt', size: 5 },
        newName: 'testFile3',
        uploaded: false,
        uploadStatus: 0,
        removed: false,
        error: ''
      }
    ]);
  });

  it('onDocumentAdded() does nothing when no files', () => {
    const event = { target: { files: [] } };
    wrapper.vm.onDocumentAdded(event);
    expect(wrapper.vm.selectedDocuments).toStrictEqual([]);
  });

  it('onDocumentAdded() enabled add document modal', () => {
    const event = {
      target: {
        files: [{ name: 'testFile.txt' }, { name: 'testFile2.txt' }]
      }
    };
    expect(wrapper.vm.showAddDocumentDialog).toStrictEqual(false);
    wrapper.vm.onDocumentAdded(event);
    expect(wrapper.vm.showAddDocumentDialog).toStrictEqual(true);
  });

  it('isPreviewable() returns file type', () => {
    const isPreviewable = wrapper.vm.isPreviewable('text.webp');
    expect(isPreviewable).toStrictEqual({
      extension: 'webp',
      type: 'image'
    });
    const isPreviewablePdf = wrapper.vm.isPreviewable('text.pdf');
    expect(isPreviewablePdf).toStrictEqual({
      extension: 'pdf',
      type: 'pdf'
    });
  });

  it('isPreviewable() is false for pdfs on android', () => {
    wrapper.vm.basicStore.platform.mobile = true;
    const isPreviewable = wrapper.vm.isPreviewable('text.pdf');
    expect(isPreviewable).toStrictEqual(undefined);
  });

  it('isPreviewable() returns false if not previewable', () => {
    const isPreviewable = wrapper.vm.isPreviewable('text.txt');
    expect(isPreviewable).toStrictEqual(undefined);
  });

  it('previewDocument() calls axiosFailed on failure', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockRejectedValueOnce({});
    await wrapper.vm.previewDocument({
      id: 3,
      name: 'testFile3',
      originalName: 'testFile3.txt',
      document: 'testserver/testFile3.txt'
    });
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('closeDialog() closes dialog', () => {
    wrapper.vm.deleteDocumentConfirmDialog = true;
    wrapper.vm.closeDialog();
    expect(wrapper.vm.deleteDocumentConfirmDialog).toStrictEqual(false);
  });

  it('updateDocumentName() calls axiosFailed on failure ', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.updateDocumentName(3, '', 'testFile3');
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('updateDocumentName() sends patch to server', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.updateDocumentName(3, '', 'testFile3');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('clientDocuments/3/update/', {
      name: 'testFile3'
    });
  });

  it('downloadDocument() uses capacitor when downloading on android', async () => {
    wrapper.vm.basicStore.platform.mobile = true;
    wrapper.vm.basicStore.token = 'AAAAAAAA';
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    const blob = new Blob(['<html>…</html>'], { type: 'text/html' });
    httpGet.mockResolvedValueOnce({ data: blob });
    const saveToDownloads = vi.spyOn(Mediastore, 'saveToDownloads');
    const getUri = vi.spyOn(Filesystem, 'getUri');

    await wrapper.vm.downloadDocument(exampleDocument);
    expect(write_blob).toHaveBeenCalledTimes(1);
    expect(write_blob).toHaveBeenCalledWith({
      blob,
      directory: 'CACHE',
      fast_mode: true,
      path: 'testFile3.txt'
    });
    expect(getUri).toHaveBeenCalledTimes(1);
    expect(getUri).toHaveBeenCalledWith({
      directory: 'CACHE',
      path: 'testFile3.txt'
    });
    expect(saveToDownloads).toHaveBeenCalledWith({
      filename: 'testFile3.txt',
      path: 'filesystemPath'
    });
  });
});

describe('ClientDocuments.vue full mount', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.mock('js-file-download', () => ({
      default: vi.fn()
    }));
    wrapper = mount(ClientDocuments, { props: { clientId: 4 } });
    wrapper.vm.basicStore.platform.mobile = false;
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('uploadDocument() clicks uploader ref', () => {
    const uploaderClick = vi.spyOn(wrapper.vm.uploader, 'click');
    expect(uploaderClick).toHaveBeenCalledTimes(0);
    wrapper.vm.uploadDocument();
    expect(uploaderClick).toHaveBeenCalledTimes(1);
  });

  it('deleteDocument() opens dialog', async () => {
    wrapper.vm.deleteDocumentConfirmDialog = false;
    await wrapper.find('.deleteDocumentButton').trigger('click');
    expect(wrapper.vm.deleteDocumentConfirmDialog).toStrictEqual(true);
    expect(wrapper.vm.selectedDeletionDocumentId).toStrictEqual(1);
  });

  it('previewDocument() sets previewFileType to image if previewable', async () => {
    global.URL.createObjectURL = vi.fn(() => 'example_url');
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    const blob = new Blob(['<html>…</html>'], { type: 'png' });
    httpGet.mockResolvedValueOnce({ data: blob });
    await wrapper.findAll('.previewDocumentButton').at(1).trigger('click');
    expect(wrapper.vm.filePreviewSrc).toStrictEqual('example_url');
    expect(wrapper.vm.filePreviewName).toStrictEqual('testFile2');
    expect(wrapper.vm.previewModal).toStrictEqual(true);
    expect(wrapper.vm.previewFileType).toStrictEqual('image');
  });

  it('previewDocument() sets previewFileType to pdf if previewable', async () => {
    global.URL.createObjectURL = vi.fn(() => 'example_url');
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    const blob = new Blob(['<html>…</html>'], { type: 'png' });
    httpGet.mockResolvedValueOnce({ data: blob });
    await wrapper.findAll('.previewDocumentButton').at(2).trigger('click');
    expect(wrapper.vm.previewFileType).toStrictEqual('pdf');
  });

  it('previewDocument() sets file preview object', async () => {
    global.URL.createObjectURL = vi.fn(() => 'example_url');
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    const blob = new Blob(['<html>…</html>'], { type: 'text/html' });
    httpGet.mockResolvedValueOnce({ data: blob });
    await wrapper.find('.previewDocumentButton').trigger('click');

    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpGet).toHaveBeenCalledWith('clientDocuments/1/download', {
      onDownloadProgress: expect.any(Function),
      responseType: 'blob',
      timeout: 60000
    });
    expect(wrapper.vm.filePreviewSrc).toStrictEqual('example_url');
    expect(wrapper.vm.filePreviewName).toStrictEqual('testFile1');
    expect(wrapper.vm.previewModal).toStrictEqual(true);
    expect(wrapper.vm.previewFileType).toStrictEqual('');
  });

  it('getDocument sends progress to function', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    const setFileDownloadProgress = vi.spyOn(
      wrapper.vm.context,
      'setFileDownloadProgress'
    );
    httpGet.mockResolvedValueOnce({ data: [] });
    await wrapper.find('.downloadDocumentButton').trigger('click');
    const config = httpGet.mock.calls[0][1] as {
      // eslint-disable-next-line @typescript-eslint/ban-types
      onDownloadProgress: Function;
    };
    config.onDownloadProgress({ loaded: 214, total: 634 });
    expect(setFileDownloadProgress).toHaveBeenCalledWith({
      loaded: 214,
      total: 634
    });
  });
});
