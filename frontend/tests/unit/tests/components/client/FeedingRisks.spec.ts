import { shallowMount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import FeedingRisks from 'src/components/clients/FeedingRisks.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { nextTick } from 'vue';
import { mockClients } from '../../../mocks';

setup();

describe('FeedingRisks.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(FeedingRisks, { props: { clientId: 4 } });
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });
  it('storeFeedingRisks() returns store medications', () => {
    expect(wrapper.vm.storeFeedingRisks).toStrictEqual([
      {
        id: 1,
        feedingRisk: 'Poor Appetite'
      },
      {
        id: 2,
        feedingRisk: 'Chewing'
      }
    ]);
    wrapper.vm.clientStore.clients['4'].feedingRisks = [
      {
        id: 1,
        feedingRisk: 'Swallowing'
      }
    ];
    expect(wrapper.vm.storeFeedingRisks).toStrictEqual([
      {
        id: 1,
        feedingRisk: 'Swallowing'
      }
    ]);
  });

  it('updateFeedingRisksModel() updates the model for feeding risks', async () => {
    expect(wrapper.vm.feedingRisks).toStrictEqual(['Poor Appetite', 'Chewing']);
    wrapper.vm.clientStore.clients['4'].feedingRisks = [
      {
        id: 1,
        feedingRisk: 'Swallowing'
      }
    ];
    expect(wrapper.vm.storeFeedingRisks).toStrictEqual([
      {
        id: 1,
        feedingRisk: 'Swallowing'
      }
    ]);
    await nextTick();
    expect(wrapper.vm.feedingRisks).toStrictEqual(['Swallowing']);
    wrapper.vm.clientStore.clients['4'].feedingRisks = undefined;
    await nextTick();
    expect(wrapper.vm.feedingRisks).toStrictEqual([]);
  });

  it('updateFeedingRisks() sends post request to server', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce({});
    await wrapper.vm.updateFeedingRisks(['Chewing', 'Poor Appetite']);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledWith('feedingRisks/4/', [
      { client: 4, feedingRisk: 'Chewing' },
      { client: 4, feedingRisk: 'Poor Appetite' }
    ]);
    httpPost.mockResolvedValueOnce({});
    await wrapper.vm.updateFeedingRisks([]);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledWith('feedingRisks/4/', []);
  });

  it('updateFeedingRisks() calls axiosFailed() on failure', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce({});
    await wrapper.vm.updateFeedingRisks([1]);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('updateFeedingRisks() does nothing when no clientId', async () => {
    await wrapper.setProps({ clientId: undefined });
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    await wrapper.vm.updateFeedingRisks([1]);
    expect(httpPost).toHaveBeenCalledTimes(0);
  });

  it('storeFeedingRisks() is an empty array if clientId is undefined', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.storeFeedingRisks).toStrictEqual([]);
  });
});
