import { mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import VisitingCentresEditDialog from 'src/components/clients/VisitingCentresEditDialog.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { Notify } from 'quasar';
import { mockCentres, mockClients, mockUsers } from '../../../mocks';
import type { SpyInstance } from 'vitest';

setup();

describe('VisitingCentresEditDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockUsers();
    mockCentres();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = mount(VisitingCentresEditDialog, {
      props: { clientId: 4 }
    });
    wrapper.vm.userStore.user.centres = [1, 2, 3, 6];
    notifySpy = vi.spyOn(Notify, 'create');
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('centres() gets state centres as array', () => {
    wrapper.vm.userStore.user.centres = [3, 4];
    wrapper.vm.centreStore.centres = {
      2: { id: 2, setupComplete: true },
      3: { id: 3, setupComplete: false },
      4: { id: 4, setupComplete: true }
    };
    expect(wrapper.vm.centres).toStrictEqual([{ id: 4, setupComplete: true }]);
  });

  it('updateCentre() does not post/delete if not loaded', () => {
    wrapper.vm.loaded = false;
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    const httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
    wrapper.vm.updateCentre({ id: 3 });
    expect(httpPost).toHaveBeenCalledTimes(0);
    expect(httpDelete).toHaveBeenCalledTimes(0);
  });

  it('updateCentre() calls post if loaded and adding centre', async () => {
    wrapper.vm.loaded = true;
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce(true);
    wrapper.vm.selectedCentres = [{ id: 3 }, { id: 4 }, { id: 5 }];
    await wrapper.find('.centreName:nth(3)').trigger('click');
    expect(httpPost).toHaveBeenCalledTimes(1);
    expect(httpPost).toHaveBeenCalledWith('clients/centres/update/', {
      centre: 1,
      client: 4
    });
  });

  it('updateCentre() calls delete if loaded and removing centre', async () => {
    wrapper.vm.loaded = true;
    const httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
    httpDelete.mockResolvedValueOnce(true);
    wrapper.vm.selectedCentres = [{ id: 3 }, { id: 4 }, { id: 5 }];
    await wrapper.vm.updateCentre({ id: 3 });
    expect(httpDelete).toHaveBeenCalledTimes(1);
    expect(httpDelete).toHaveBeenCalledWith('clients/centres/update/', {
      data: {
        centre: 3,
        client: 4
      }
    });
  });

  it('updateCentre() calls axiosFailed on post server error', async () => {
    wrapper.vm.loaded = true;
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce(true);
    await wrapper.vm.updateCentre({ id: 60 });
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('updateCentre() calls axiosFailed on delete server error', async () => {
    wrapper.vm.loaded = true;
    wrapper.vm.selectedCentres = [{ id: 3 }, { id: 4 }];
    const httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
    httpDelete.mockRejectedValueOnce(true);
    await wrapper.vm.updateCentre({ id: 3 });
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('updateCentre() does not delete last centre', async () => {
    wrapper.vm.loaded = true;
    wrapper.vm.selectedCentres = [{ id: 3 }];
    const httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
    await wrapper.vm.updateCentre({ id: 3 });
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpDelete).toHaveBeenCalledTimes(0);
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Client must have at least one centre.',
      type: 'warning'
    });
  });

  it('updateSelectedCentres() is called on create', async () => {
    expect(wrapper.vm.selectedCentres.length).toStrictEqual(1);
    wrapper.vm.clientStore.clients[4].centres = [2, 3, 4, 5, 6];
    await nextTick();
    expect(wrapper.vm.selectedCentres.length).toStrictEqual(3);
  });

  it('updating clientCentres does nothing when no clientId', async () => {
    wrapper.vm.loaded = false;
    await wrapper.setProps({ clientId: null });
    wrapper.vm.clientStore.clients[4].centres = [2, 3, 4, 5, 6];
    await nextTick();
    expect(wrapper.vm.loaded).toStrictEqual(false);
    expect(wrapper.vm.selectedCentres.length).toStrictEqual(1);
  });
});
