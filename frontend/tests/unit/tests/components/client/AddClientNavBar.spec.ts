import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import AddClientNavBar from 'src/components/clients/AddClientNavBar.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import { Screen } from 'quasar';
import { mockClients } from '../../../mocks';

setup();

describe('AddClientNavBar.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(AddClientNavBar);
    Screen.width = 400;
  });

  it('closeDialog() closes dialog', () => {
    wrapper.vm.showDeleteClientConfirmDialog = true;
    wrapper.vm.closeDialog();
    expect(wrapper.vm.showDeleteClientConfirmDialog).toStrictEqual(false);
  });

  it('incompleteClients() returns incomplete store clients', () => {
    expect(wrapper.vm.incompleteClients.length).toStrictEqual(2);
    expect(wrapper.vm.incompleteClients[0].id).toStrictEqual(2);
    expect(wrapper.vm.incompleteClients[1].id).toStrictEqual(4);
  });

  it('goToClientCreation() calls setupClient store function with passed client', async () => {
    const setSetupClient = vi.spyOn(wrapper.vm.clientStore, 'setSetupClient');
    await wrapper.vm.goToClientCreation({ id: 8 });
    expect(setSetupClient).toHaveBeenCalledWith({ id: 8 });
  });

  it('goToClientCreation() calls router push', async () => {
    const routerPush = vi.spyOn(wrapper.vm.router, 'push');
    routerPush.mockResolvedValueOnce(true);
    await wrapper.vm.goToClientCreation({ id: 8 });
    expect(routerPush).toHaveBeenCalledWith({
      name: 'clientCreation',
      query: { nextUrl: '/' }
    });
  });

  it('sets nav bar clip when not xs', async () => {
    expect(wrapper.vm.clipClass).toStrictEqual('');
    Screen.width = 800;
    await nextTick();
    expect(wrapper.vm.clipClass).toStrictEqual('sidebarPush');
  });

  it('setDeleteClientConfirmDialog() can set client confim dialog', () => {
    wrapper.vm.showDeleteClientConfirmDialog = true;
    wrapper.vm.setDeleteClientConfirmDialog(false);
    expect(wrapper.vm.showDeleteClientConfirmDialog).toStrictEqual(false);
    wrapper.vm.setDeleteClientConfirmDialog(true);
    expect(wrapper.vm.showDeleteClientConfirmDialog).toStrictEqual(true);
  });
});
