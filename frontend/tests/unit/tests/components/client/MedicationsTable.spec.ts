import { mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import MedicationsTable from 'src/components/clients/MedicationsTable.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { nextTick } from 'vue';
import { formatISO } from 'date-fns';
import { mockClients } from '../../../mocks';

setup();

describe('MedicationsTable.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = mount(MedicationsTable, { props: { clientId: 4 } });
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('medications() returns store medications', () => {
    expect(wrapper.vm.medications).toStrictEqual([
      {
        id: 1,
        name: 'Test Med 1',
        dosage: '1',
        frequency: '5',
        inactiveDate: '2020-03-12'
      },
      {
        id: 2,
        name: 'Test Med 2',
        dosage: '2',
        frequency: '6',
        inactiveDate: undefined
      },
      {
        id: 3,
        name: 'Test Med 3',
        dosage: '3',
        frequency: '7',
        inactiveDate: '3020-03-12'
      },
      {
        id: 4,
        name: 'Test Med 4',
        dosage: '4',
        frequency: '8',
        inactiveDate: undefined
      }
    ]);
  });

  it('medications() is empty when no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    expect(wrapper.vm.medications).toStrictEqual([]);
  });

  it('activeMedications() returns medications with no inactive date', () => {
    expect(wrapper.vm.activeMedications).toStrictEqual([
      {
        id: 2,
        name: 'Test Med 2',
        dosage: '2',
        frequency: '6',
        inactiveDate: undefined
      },
      {
        id: 3,
        name: 'Test Med 3',
        dosage: '3',
        frequency: '7',
        inactiveDate: '3020-03-12'
      },
      {
        id: 4,
        name: 'Test Med 4',
        dosage: '4',
        frequency: '8',
        inactiveDate: undefined
      }
    ]);
  });

  it('archivedMedications() returns medications inactive before now', () => {
    expect(wrapper.vm.archivedMedications).toStrictEqual([
      {
        id: 1,
        name: 'Test Med 1',
        dosage: '1',
        frequency: '5',
        inactiveDate: '2020-03-12'
      }
    ]);
  });

  it('medicationArchiveHeaders() returns extra date header', () => {
    expect(wrapper.vm.medicationArchiveHeaders.length).toStrictEqual(5);
  });

  it('formatDate() converts ISO date to alternate format', () => {
    expect(wrapper.vm.formatDate('2020-11-09')).toStrictEqual('09/11/20');
  });

  it('createMedication() sends a post request to server', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce({});
    wrapper.vm.newMedicationName = 'New Med';
    wrapper.vm.newMedicationDosage = '9999999';
    wrapper.vm.newMedicationFrequency = '10';
    await wrapper.vm.createMedication();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPost).toHaveBeenCalledWith('medications/', {
      name: 'New Med',
      dosage: '9999999',
      frequency: '10',
      client: 4
    });
  });

  it('createMedication() calls axiosFailed() on failure', async () => {
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce({});
    await wrapper.vm.createMedication();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('deleteMedication() sends a delete request to server', async () => {
    const httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
    httpDelete.mockResolvedValueOnce({});
    await wrapper
      .findComponent('#medicationsTable')
      .find('.deleteMedicationButton')
      .trigger('click');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpDelete).toHaveBeenCalledWith('medications/2/delete/');
  });

  it('deleteMedication() calls axiosFailed() on failure', async () => {
    wrapper.vm.showArchiveDialog = true;
    await nextTick();

    const httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
    httpDelete.mockRejectedValueOnce({});
    await wrapper
      .findComponent('#archivedMedicationsTable')
      .find('.deleteMedicationButton')
      .trigger('click');
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('updateMedication() sends a patch request to server', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper.vm.updateMedication(3, 'dosage', '1234');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('medications/3/update/', {
      dosage: '1234'
    });
  });

  it('updateMedication() calls axiosFailed() on failure', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.updateMedication({
      id: 3,
      name: 'New Med Name',
      dosage: '1234'
    });
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('openMedicationDialog() resets values and opens dialog', () => {
    wrapper.vm.newMedicationName = 'New Med';
    wrapper.vm.newMedicationDosage = '9999999';
    wrapper.vm.newMedicationFrequency = '10';
    wrapper.vm.showCreateMedicationDialog = false;
    wrapper.vm.openMedicationDialog();
    expect(wrapper.vm.newMedicationName).toStrictEqual('');
    expect(wrapper.vm.newMedicationDosage).toStrictEqual('');
    expect(wrapper.vm.newMedicationFrequency).toStrictEqual('');
    expect(wrapper.vm.showCreateMedicationDialog).toStrictEqual(true);
  });

  it('openArchiveDialog() opens dialog', () => {
    wrapper.vm.showArchiveDialog = false;
    wrapper.vm.openArchiveDialog();
    expect(wrapper.vm.showArchiveDialog).toStrictEqual(true);
  });

  it('archiveMedication() sets inactive date to now when archive is true', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper
      .findComponent('#medicationsTable')
      .find('.archiveMedicationButton')
      .trigger('click');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('medications/2/update/', {
      inactiveDate: formatISO(new Date(), { representation: 'date' })
    });
  });

  it('archiveMedication() sets inactive date to null when archive false', async () => {
    wrapper.vm.showArchiveDialog = true;
    await nextTick();

    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockResolvedValueOnce({});
    await wrapper
      .findComponent('#archivedMedicationsTable')
      .find('.archiveMedicationButton')
      .trigger('click');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpPatch).toHaveBeenCalledWith('medications/1/update/', {
      inactiveDate: null
    });
  });

  it('archiveMedication() calls axiosFailed() on failure', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce({});
    await wrapper.vm.archiveMedication(3, false);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('validCreate is true when values are filled', async () => {
    wrapper.vm.showCreateMedicationDialog = true;
    await nextTick();

    wrapper.vm.newMedicationName = '';
    wrapper.vm.newMedicationDosage = '';
    wrapper.vm.newMedicationFrequency = '';

    expect(wrapper.vm.validCreate).toStrictEqual(false);
    await wrapper
      .findComponent('#medicationCreateSection')
      .find('#medicationCreateNameField')
      .setValue('new medication name');
    wrapper.vm.newMedicationName = 'New name';
    expect(wrapper.vm.validCreate).toStrictEqual(false);
    await wrapper
      .findComponent('#medicationCreateSection')
      .find('#medicationCreateDosageField')
      .setValue('new dosage');
    expect(wrapper.vm.validCreate).toStrictEqual(false);
    await wrapper
      .findComponent('#medicationCreateSection')
      .find('#medicationCreateFrequencyField')
      .setValue('new frequency');
    expect(wrapper.vm.validCreate).toStrictEqual(true);
  });
});
