import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import RegisterComponent from 'src/components/auth/RegisterComponent.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import loginSuccess from 'src/functions/auth/loginSuccess';

import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { setCssVar } from 'quasar';

setup();

describe('RegisterComponent.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    wrapper = shallowMount(RegisterComponent);
    vi.mock('src/functions/proxy/AxiosFailed');
    vi.mock('src/functions/auth/loginSuccess', () => ({
      default: vi.fn()
    }));
  });

  it('test passwordRules()', () => {
    expect(wrapper.vm.passwordRules).toStrictEqual([]);
    wrapper.vm.password = 'test1';
    wrapper.vm.confirmPassword = 'test';
    expect(typeof wrapper.vm.passwordRules).toStrictEqual('object');
    expect(typeof wrapper.vm.passwordRules[0]).toStrictEqual('function');
    expect(wrapper.vm.passwordRules[0](wrapper.vm.password)).toStrictEqual(
      'Values do not match'
    );
    wrapper.vm.confirmPassword = 'test1';
    expect(wrapper.vm.passwordRules[0](wrapper.vm.password)).toStrictEqual(
      true
    );
  });

  it('username errors reset on username change', async () => {
    wrapper.vm.userNameErrors = ['test1', 'test2'];
    wrapper.vm.username = 'newusername';
    await nextTick();
    expect(wrapper.vm.userNameErrors).toStrictEqual([]);
  });

  it('display name errors reset on display name change', async () => {
    wrapper.vm.displayNameErrors = ['test1', 'test2'];
    wrapper.vm.displayName = 'newdisplayname';
    await nextTick();
    expect(wrapper.vm.displayNameErrors).toStrictEqual([]);
  });

  it('validates password field on password change', async () => {
    wrapper.vm.passwordErrors = ['test1', 'test2'];
    wrapper.vm.password = 'newpassword';
    await nextTick();
    expect(wrapper.vm.passwordErrors).toStrictEqual([]);
  });

  it('validates confirm password field on password change', async () => {
    wrapper.vm.passwordErrors = ['test1', 'test2'];
    wrapper.vm.confirmPassword = 'newconfirmpassword';
    await nextTick();
    expect(wrapper.vm.passwordErrors).toStrictEqual([]);
  });

  it('test isValid()', () => {
    const register = vi.spyOn(wrapper.vm.context, 'register');

    // All data present
    wrapper.vm.username = 'aaa';
    wrapper.vm.validate();
    expect(register).toHaveBeenCalledTimes(0);
    wrapper.vm.email = 'aaa@test.com';
    wrapper.vm.validate();
    expect(register).toHaveBeenCalledTimes(0);
    wrapper.vm.password = 'aaaaaa';
    wrapper.vm.validate();
    expect(register).toHaveBeenCalledTimes(0);
    wrapper.vm.confirmPassword = 'aaaaaa';
    wrapper.vm.validate();
    expect(register).toHaveBeenCalledTimes(0);
    wrapper.vm.otpSecret = 'aaaaaa';
    wrapper.vm.validate();
    expect(register).toHaveBeenCalledTimes(1);

    // Invalid email
    wrapper.vm.email = 'aaaaa.aa';
    wrapper.vm.validate();
    expect(register).toHaveBeenCalledTimes(1);

    // No password match
    wrapper.vm.email = 'aaa@test.com';
    wrapper.vm.confirmPassword = 'bbbbbb';
    wrapper.vm.validate();
    expect(register).toHaveBeenCalledTimes(1);

    // Password too short
    wrapper.vm.confirmPassword = 'aaaa';
    wrapper.vm.password = 'aaaa';
    wrapper.vm.validate();
    expect(register).toHaveBeenCalledTimes(1);
  });

  it('isValid can be true when registerType is client-regisration and no Otp', async () => {
    wrapper.vm.username = 'aaa';
    wrapper.vm.email = 'aaa@test.com';
    wrapper.vm.password = 'aaaaaa';
    wrapper.vm.confirmPassword = 'aaaaaa';
    expect(wrapper.vm.isValid).toStrictEqual(false);

    await wrapper.setProps({ registerType: 'client-registration' });
    expect(wrapper.vm.isValid).toStrictEqual(true);
    wrapper.vm.useOtp = true;
    expect(wrapper.vm.isValid).toStrictEqual(false);
  });

  it('test register()', async () => {
    setCssVar('primary', '#FFFAAA');
    const registerFailed = vi.spyOn(wrapper.vm, 'registerFailed');
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockResolvedValueOnce('example register success');
    wrapper.vm.username = 'testUser';
    wrapper.vm.displayName = 'Test User';
    wrapper.vm.password = 'testPass';
    wrapper.vm.otpSecret = 'UA87AS7D8YM93JI0898';
    wrapper.vm.email = 'testemail@test.com';
    await wrapper.vm.register();
    expect(httpPost).toHaveBeenCalledWith('registration/', {
      username: 'testUser',
      displayName: 'Test User',
      password1: 'testPass',
      password2: 'testPass',
      tempCode: undefined,
      darkTheme: false,
      accentColor: '#FFFAAA',
      otpDeviceKey: 'UA87AS7D8YM93JI0898',
      email: 'testemail@test.com'
    });
    expect(registerFailed).toHaveBeenCalledTimes(0);
    expect(loginSuccess).toHaveBeenCalledWith(
      'example register success',
      expect.any(Object)
    );
  });

  it('test register failure', async () => {
    setCssVar('primary', '#FFFAAA');
    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce('example error');
    await wrapper.vm.register();
    expect(httpPost).toHaveBeenCalledTimes(1);
    expect(loginSuccess).toHaveBeenCalledTimes(0);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledWith('example error');
  });

  it('test registerFailed()', () => {
    const err = {
      response: {
        data: {
          username: ['test username error'],
          password1: ['test password error'],
          email: ['test email error']
        }
      }
    };
    wrapper.vm.registerFailed(err);
    expect(wrapper.vm.userNameErrors).toStrictEqual(['test username error']);
    expect(wrapper.vm.passwordErrors).toStrictEqual(['test password error']);
    expect(wrapper.vm.emailErrors).toStrictEqual(['test email error']);
    expect(axiosFailed).toHaveBeenCalledTimes(1);

    const expectedError = 'non field error';

    const nonFieldErr = {
      response: {
        data: {
          non_field_errors: [expectedError]
        }
      }
    };
    wrapper.vm.registerFailed(nonFieldErr);
    const noResponseErr = {};

    wrapper.vm.registerFailed(noResponseErr);
    expect(axiosFailed).toHaveBeenCalledTimes(3);
  });

  it('otpSetupComplete sets otp secret key from child component', () => {
    wrapper.vm.otpSecret = '1234';
    wrapper.vm.otpSetupComplete('5678');
    expect(wrapper.vm.otpSecret).toStrictEqual('5678');
  });

  it('sets displayName if value passed to component', async () => {
    await wrapper.setProps({ providedDisplayName: 'new display name' });
    expect(wrapper.vm.displayName).toStrictEqual('new display name');
  });

  it('password change calls validate on passwordInput', async () => {
    wrapper.vm.passwordErrors = ['test'];
    wrapper.vm.passwordInput = { validate: vi.fn() };
    wrapper.vm.password = 'newPassword';
    await nextTick();
    expect(wrapper.vm.passwordInput.validate).toHaveBeenCalledOnce();
  });
});
