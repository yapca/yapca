import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ThemeSelect from 'src/components/themes/ThemeSelect.vue';
import setup from 'tests/unit/functions/setup';
import { getCssVar, setCssVar } from 'quasar';
import { nextTick } from 'vue';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import type { DOMWrapper } from '@vue/test-utils';

setup();

describe('ThemeSelect.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let lightThemeButton: DOMWrapper<HTMLButtonElement>;
  let darkThemeButton: DOMWrapper<HTMLButtonElement>;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(ThemeSelect);
    const themeButtons = wrapper.findAll('.themeSelectButton');
    lightThemeButton = themeButtons.at(0);
    darkThemeButton = themeButtons.at(1);
  });

  it('updates selected theme on click', async () => {
    await lightThemeButton.trigger('click');
    expect(wrapper.vm.$q.dark.isActive).toStrictEqual(false);

    await darkThemeButton.trigger('click');
    expect(wrapper.vm.$q.dark.isActive).toStrictEqual(true);
  });

  it('clicking button while creating does not change', async () => {
    wrapper.vm.creating = true;
    expect(wrapper.vm.$q.dark.isActive).toStrictEqual(true);

    await darkThemeButton.trigger('click');
    expect(wrapper.vm.$q.dark.isActive).toStrictEqual(true);
  });

  it('test updateVuetifyTheme() while old and new match', async () => {
    wrapper.vm.$q.dark.set('#FFFFFF');
    await lightThemeButton.trigger('click');
    expect(wrapper.vm.$q.dark.isActive).toStrictEqual(false);
  });

  it('test updateQuasarAccent()', async () => {
    const updateUserSettings = vi.spyOn(
      wrapper.vm.context,
      'updateUserSettings'
    );
    expect(updateUserSettings).toHaveBeenCalledTimes(0);
    await nextTick();
    expect(wrapper.vm.selectedAccentColor).toStrictEqual('#1976D2');
    wrapper.vm.updateQuasarAccent('#444444');
    expect(getCssVar('primary')).toStrictEqual('#444444');
    expect(updateUserSettings).toHaveBeenCalledTimes(1);
  });

  it('updateQuasarAccent does nothing when no input', () => {
    const response = wrapper.vm.updateQuasarAccent();
    expect(response).toStrictEqual(null);
  });

  it('test updateUserSettings() sends patch request with theme', async () => {
    setCssVar('primary', '#352635');
    wrapper.vm.userStore.user.id = 3;
    wrapper.vm.basicStore.token = 'AAAAAAAA';

    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    httpPatch.mockRejectedValueOnce('Example Error');
    await wrapper.vm.updateUserSettings();

    expect(httpPatch).toHaveBeenCalledWith(
      'users/3/update/',
      {
        darkTheme: false,
        accentColor: '#352635'
      },
      { headers: { Authorization: 'Token AAAAAAAA' } }
    );
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledWith('Example Error');
    httpPatch.mockResolvedValueOnce({ data: {} });
    await wrapper.vm.updateUserSettings();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('test updateUserSettings() when no user exists', () => {
    wrapper.vm.userStore.user.id = null;
    const response = wrapper.vm.updateUserSettings();
    expect(response).toStrictEqual(null);
  });

  it('test darkTheme() function returns store value', () => {
    wrapper.vm.userStore.user.darkTheme = true;
    expect(wrapper.vm.darkTheme).toStrictEqual(true);
  });

  it('test accentColor() function returns store value', () => {
    wrapper.vm.userStore.user.accentColor = '#CCCCCC';
    expect(wrapper.vm.accentColor).toStrictEqual('#CCCCCC');
  });

  it('updating store dark theme sets selected theme to passed theme', async () => {
    wrapper.vm.userStore.user.darkTheme = true;
    await nextTick();
    expect(wrapper.vm.selectedTheme).toStrictEqual('#1E1E1E');

    wrapper.vm.userStore.user.darkTheme = false;
    await nextTick();
    expect(wrapper.vm.selectedTheme).toStrictEqual('#FFFFFF');
  });

  it('updating store accent color sets selected theme to passed theme', async () => {
    wrapper.vm.userStore.user.accentColor = '#E1E1E1';
    await nextTick();
    expect(wrapper.vm.selectedAccentColor).toStrictEqual('#E1E1E1');
  });
});
