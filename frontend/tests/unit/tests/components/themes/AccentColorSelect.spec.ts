import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import AccentColorSelect from 'src/components/themes/AccentColorSelect.vue';
import setup from 'tests/unit/functions/setup';

setup();

describe('AccentColorSelect.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    wrapper = shallowMount(AccentColorSelect, {
      props: {
        propColor: '#0097A7'
      }
    });
  });

  it('selected color is set on load', () => {
    expect(wrapper.vm.selectedColor).toStrictEqual('#0097A7');
  });

  it('updateSelectedColor() sets selectedColor on prop change', async () => {
    await wrapper.setProps({ propColor: '#FFFFFF' });
    expect(wrapper.vm.selectedColor).toStrictEqual('#FFFFFF');
  });

  it('update emit is called when selected color is updated', async () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.selectedColor = '#111111';
    wrapper.vm.updateColor(wrapper.vm.selectedColor);
    await wrapper.vm.$nextTick();
    expect(emit).toHaveBeenCalledTimes(1);
    expect(emit).toHaveBeenCalledWith('update-color', '#111111');
  });

  it('updateColorFromDialog calls emit', () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.centreMatchesColorDialog = true;
    wrapper.vm.conflictColor = '#CCCCCC';
    wrapper.vm.selectedColor = '#111111';
    wrapper.vm.updateColorFromDialog();
    expect(emit).toHaveBeenCalledTimes(1);
    expect(emit).toHaveBeenCalledWith('update-color', '#CCCCCC');
    expect(wrapper.vm.selectedColor).toStrictEqual('#CCCCCC');
    expect(wrapper.vm.centreMatchesColorDialog).toStrictEqual(false);
  });

  it('getConflictingCentres sets conflictingCentres to array of centres', async () => {
    await wrapper.setProps({ checkConflictingCentres: true });
    wrapper.vm.centresStore.centres = {
      '1': { color: '#CCCCCC', name: 'Test Centre' },
      '2': { color: '#111111', name: 'Test Centre 2' }
    };
    wrapper.vm.getConflictingCentres('#CCCCCC');
    expect(wrapper.vm.conflictingCentres).toStrictEqual(['Test Centre']);
  });

  it('getConflictingCentres returns empty array when prop is false', async () => {
    await wrapper.setProps({ checkConflictingCentres: false });
    wrapper.vm.centresStore.centres = {
      '1': { color: '#CCCCCC', name: 'Test Centre' }
    };
    wrapper.vm.getConflictingCentres('#CCCCCC');
    expect(wrapper.vm.conflictingCentres).toStrictEqual([]);
  });

  it('updateColor() opens dialog if conflicting centres exist', async () => {
    wrapper.vm.conflictColor = '#E64A19';
    await wrapper.setProps({ checkConflictingCentres: true });
    wrapper.vm.centreMatchesColorDialog = false;
    wrapper.vm.centresStore.centres = {
      '1': { color: '#E64A19', name: 'Test Centre' }
    };
    const accentSelectButton = wrapper.findAll('.accentSelectButton').at(13);
    await accentSelectButton.trigger('click');
    expect(wrapper.vm.centreMatchesColorDialog).toStrictEqual(true);
    expect(wrapper.vm.selectedColor).toStrictEqual('#0097A7');
    expect(wrapper.vm.conflictColor).toStrictEqual('#E64A19');
  });
});
