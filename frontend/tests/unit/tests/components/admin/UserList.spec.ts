import { flushPromises, mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import UserList from 'src/components/admin/UserList.vue';
import setup from 'tests/unit/functions/setup';
import { Notify } from 'quasar';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockClients, mockUsers } from 'tests/unit/mocks';
import { nextTick } from 'vue';
import { getRouter } from 'vue-router-mock';
import type { SpyInstance } from 'vitest';

setup();

process.env.VUE_APP_MAX_STORAGE = '830';
process.env.VUE_APP_MAX_FILE_SIZE = '10';

describe('UserList.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;
  let httpGet: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    mockClients();
    wrapper = mount(UserList);
    notifySpy = vi.spyOn(Notify, 'create');
    httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('test isCurrentUser()', () => {
    wrapper.vm.userStore.user.id = 2;
    let isCurrentUser = wrapper.vm.isCurrentUser(2);
    expect(isCurrentUser).toStrictEqual(true);
    isCurrentUser = wrapper.vm.isCurrentUser(5);
    expect(isCurrentUser).toStrictEqual(false);
  });

  it('openUserDialogOrNotify() opens edit user dialog', () => {
    const user = {
      id: 2,
      accentColor: '#FFFFFF',
      darkTheme: false,
      groups: [{ id: 2 }]
    };
    wrapper.vm.userStore.user.id = 3;
    wrapper.vm.showEditUserDialog = false;
    wrapper.vm.selectedUser = {};
    wrapper.vm.openUserDialogOrNotify(user);
    expect(wrapper.vm.showEditUserDialog).toStrictEqual(true);
    expect(wrapper.vm.selectedUser).toStrictEqual({
      id: 2,
      accentColor: '#FFFFFF',
      darkTheme: false,
      groups: [{ id: 2 }]
    });
  });

  it('openUserDialogOrNotify() opens snackbar for current user', () => {
    const user = {
      id: 3,
      accentColor: '#FFFFFF',
      darkTheme: false,
      groups: [{ id: 2 }]
    };
    wrapper.vm.showEditUserDialog = false;
    wrapper.vm.userStore.user.id = 3;
    wrapper.vm.selectedUser = {};
    wrapper.vm.openUserDialogOrNotify(user);
    expect(wrapper.vm.showEditUserDialog).toStrictEqual(false);
    expect(wrapper.vm.selectedUser).toStrictEqual({});
    expect(notifySpy).toHaveBeenCalledWith({
      message:
        'Cannot alter your own account here. Please go to the user page.',
      type: 'info',
      actions: [
        { color: 'white', handler: expect.any(Function), label: 'User Page' }
      ]
    });
  });

  it('gets users on create', async () => {
    httpGet.mockResolvedValue({ data: [{ id: 3, username: 'test' }] });
    wrapper.vm.userStore.users = {};
    await wrapper.vm.created();
    expect(httpGet).toHaveBeenCalledTimes(2);
    expect(httpGet).toHaveBeenCalledWith('users/display-names/');
    expect(httpGet).toHaveBeenCalledWith('groups/');
    await nextTick();
    expect(wrapper.vm.userStore.users).toStrictEqual({
      '3': {
        id: 3,
        username: null,
        displayName: '',
        accentColor: null,
        darkTheme: null,
        groups: [],
        group: '',
        isActive: false,
        permissions: [],
        centres: [],
        userClientTableHiddenRows: [],
        email: ''
      }
    });
    expect(axiosFailed).not.toHaveBeenCalled();
  });

  it('computes user table from store', () => {
    mockUsers();
    expect(wrapper.vm.users.length).toStrictEqual(4);
    expect(wrapper.vm.users[1]).toStrictEqual({
      id: 2,
      displayName: 'Test User 2',
      username: 'testUser2',
      email: 'test2@test.test',
      group: 'nurse',
      centres: 1,
      isActive: true,
      client: undefined
    });
    expect(wrapper.vm.users[3]).toStrictEqual({
      id: 4,
      displayName: 'test name 2 test surname 2',
      username: 'testClientUser1',
      email: 'test4@test.test',
      group: 'client',
      centres: 1,
      isActive: true,
      client: 2
    });
  });

  it('axiosFailed is called on create if server failure', async () => {
    httpGet.mockReset();
    httpGet.mockRejectedValue({});
    await wrapper.vm.created();
    await flushPromises();
    expect(axiosFailed).toHaveBeenCalledTimes(2);
  });

  it('go to user page button in notification does that', async () => {
    const routerPush = vi.spyOn(wrapper.vm.$router, 'push');
    const user = {
      id: 3,
      accentColor: '#FFFFFF',
      darkTheme: false,
      groups: [{ id: 2 }]
    };
    wrapper.vm.editUserDialog = false;
    wrapper.vm.userStore.user.id = 3;
    wrapper.vm.selectedUser = {};
    wrapper.vm.openUserDialogOrNotify(user);
    await notifySpy.mock.calls[0][0].actions[0].handler();
    expect(routerPush).toHaveBeenCalledWith({
      name: 'User Settings'
    });
  });

  it('leaving email change dialog via back button closes dialog', async () => {
    await wrapper.find('#addNewUserButton').trigger('click');
    expect(wrapper.vm.showAddNewUserDialog).toStrictEqual(true);
    await getRouter().setHash('#not-add-new-user');
    expect(wrapper.vm.showAddNewUserDialog).toStrictEqual(false);
  });

  it('close dialog passed from edit user dialog sets showEditUserDialog to false', () => {
    wrapper.vm.showEditUserDialog = true;
    wrapper.vm.setEditUserDialogFalse();
    expect(wrapper.vm.showEditUserDialog).toStrictEqual(false);
  });

  it('close dialog passed from edit user dialog sets showEditUserDialog to false', () => {
    wrapper.vm.showAddNewUserDialog = true;
    wrapper.vm.setAddNewUserDialogFalse();
    expect(wrapper.vm.showAddNewUserDialog).toStrictEqual(false);
  });
});
