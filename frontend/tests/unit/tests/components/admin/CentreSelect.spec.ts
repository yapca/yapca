import { mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import CentreSelect from 'src/components/admin/CentreSelect.vue';
import setup from 'tests/unit/functions/setup';
import { mockCentres, mockUsers } from 'tests/unit/mocks';
import { useUserStore } from 'src/stores/userStore';
import { nextTick } from 'vue';
import type { SpyInstance } from 'vitest';

setup();
describe('CentreSelect.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let emit: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    mockCentres();
    const userStore = useUserStore();
    userStore.user.centres = [1, 2, 3];
    wrapper = mount(CentreSelect, { props: { displayName: 'testname' } });
    emit = vi.spyOn(wrapper.vm.context, 'emit');
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('emits selectedCentres on updateCentre', async () => {
    // Removes centre 3
    wrapper.vm.selectedCentres = [3, 6];
    await nextTick();
    wrapper.vm.selectedWarningCentre = { centre: { id: 3 } };
    wrapper.vm.continueCentreUpdate();
    expect(emit).toHaveBeenCalledWith('update-selected-centres', 3, true);

    // Adds centre 2
    wrapper.vm.selectedCentres = [3, 6];
    await nextTick();
    wrapper.vm.updateCentre({ centre: { id: 2 } });
    expect(emit).toHaveBeenCalledWith('update-selected-centres', 2, true);
  });

  it('shows error code when no centres', async () => {
    expect(wrapper.vm.errorCode).toStrictEqual(
      'At least one centre must be selected'
    );
    wrapper.vm.selectedCentres = [{ id: 3 }, { id: 6 }];
    await nextTick();
    expect(wrapper.vm.errorCode).toStrictEqual('');
  });

  it('gets store centres', () => {
    expect(wrapper.vm.centres.length).toStrictEqual(2);
    expect(wrapper.vm.centres[0].active).toStrictEqual(false);
    expect(wrapper.vm.centres[0].centre.name).toStrictEqual('Test Centre 1');
  });

  it('clicking centre opens warning dialog', async () => {
    wrapper.vm.userChangeCentreWarning = false;
    const centreItem = wrapper.find('.addUserCentreSelectListItemCheckbox');
    await centreItem.trigger('click');
    expect(wrapper.vm.selectedWarningCentre.centre.id).toStrictEqual(1);
    expect(wrapper.vm.selectedWarningCentreSubscribe).toStrictEqual(true);
    expect(wrapper.vm.userChangeCentreWarning).toStrictEqual(true);
    expect(emit).toHaveBeenCalledTimes(0);
  });

  it('clicking centre does not open dialog when creating new user', async () => {
    wrapper.vm.userChangeCentreWarning = false;
    await wrapper.setProps({ creatingNewUser: true });
    const centreCheckbox = wrapper.find('.addUserCentreSelectListItemCheckbox');
    await centreCheckbox.trigger('click');
    expect(wrapper.vm.userChangeCentreWarning).toStrictEqual(false);
    expect(emit).toHaveBeenCalledWith('update-selected-centres', 1, true);
  });

  it('setUserChangeCentreWarningFalse', () => {
    wrapper.vm.userChangeCentreWarning = true;
    wrapper.vm.setUserChangeCentreWarningFalse();
    expect(wrapper.vm.userChangeCentreWarning).toStrictEqual(false);
  });

  it('continueCentreUpdate does nothing if no selectedWarningCentre', async () => {
    wrapper.vm.selectedCentres = [3, 6];
    await nextTick();
    wrapper.vm.continueCentreUpdate();
    expect(emit).toHaveBeenCalledTimes(0);
  });

  it('centreWarningHeaderText changes if subbed or not', () => {
    // Empty when no selectedWarningCentre
    wrapper.vm.selectedWarningCentreSubscribe = true;
    expect(wrapper.vm.centreWarningHeaderText).toStrictEqual('');

    wrapper.vm.selectedWarningCentre = { centre: { id: 1, name: 'centre 1' } };
    expect(wrapper.vm.centreWarningHeaderText).toStrictEqual(
      'Subscribe testname to centre 1?'
    );

    wrapper.vm.selectedWarningCentreSubscribe = false;
    expect(wrapper.vm.centreWarningHeaderText).toStrictEqual(
      'Unsubscribe testname from centre 1?'
    );
  });

  it('selectedUserCentres returns user centres', async () => {
    wrapper.vm.centreStore.centres = { 1: { id: 1 }, 3: { id: 3 } };
    mockUsers();
    await wrapper.setProps({ userId: 3 });
    expect(wrapper.vm.selectedUserCentres).toStrictEqual([1, 2]);
    await nextTick();
    expect(wrapper.vm.selectedCentres).toStrictEqual([1]);
  });
});
