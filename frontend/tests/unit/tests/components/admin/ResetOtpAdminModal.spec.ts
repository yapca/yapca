import { shallowMount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import ResetOtpAdminModal from 'src/components/admin/ResetOtpAdminModal.vue';
import setup from 'tests/unit/functions/setup';
import { mockClients } from 'tests/unit/mocks';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import type { SpyInstance } from 'vitest';

setup();

process.env.VUE_APP_MAX_STORAGE = '830';
process.env.VUE_APP_MAX_FILE_SIZE = '10';

describe('ResetOtpAdminModal.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpGet: SpyInstance;
  let emit: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    mockClients();
    wrapper = shallowMount(ResetOtpAdminModal, {
      props: {
        userId: 2
      }
    });
    emit = vi.spyOn(wrapper.vm.context, 'emit');
    httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('sendTotpResetEmail does nothing if no user id', async () => {
    await wrapper.setProps({ userId: null });
    wrapper.vm.sendTotpResetEmail();
    expect(httpGet).toHaveBeenCalledTimes(0);
  });

  it('sendTotpResetEmail calls email endpoint', async () => {
    httpGet.mockResolvedValueOnce(true);
    await wrapper.vm.sendTotpResetEmail();
    expect(httpGet).toHaveBeenCalledWith('request-reset-totp', {
      params: { userId: 2 }
    });
    expect(emit).toHaveBeenCalledWith('close-dialog');
  });

  it('sendTotpResetEmail calls axiosFailed on fail', async () => {
    httpGet.mockRejectedValueOnce(false);
    await wrapper.vm.sendTotpResetEmail();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(emit).toHaveBeenCalledTimes(1);
  });
});
