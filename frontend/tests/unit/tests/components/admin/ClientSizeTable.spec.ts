import { flushPromises, mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import ClientSizeTable from 'src/components/admin/ClientSizeTable.vue';
import setup from 'tests/unit/functions/setup';
import { Notify } from 'quasar';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockClients } from 'tests/unit/mocks';
import { nextTick } from 'vue';
import type { SpyInstance } from 'vitest';

setup();

process.env.VUE_APP_MAX_STORAGE = '830';
process.env.VUE_APP_MAX_FILE_SIZE = '10';

describe('ClientSizeTable.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let notifySpy: SpyInstance;
  let httpGet: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    mockClients();
    wrapper = mount(ClientSizeTable);

    notifySpy = vi.spyOn(Notify, 'create');
    httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
  });

  afterEach(() => {
    wrapper?.unmount();
    wrapper = undefined;
  });

  it('client loading watcher does nothing when loading', async () => {
    expect(axiosFailed).toHaveBeenCalledTimes(0);

    httpGet.mockRejectedValueOnce({});
    wrapper.vm.clientStore.clientsLoading = true;
    await nextTick();
    await flushPromises();
    expect(httpGet).toHaveBeenCalledTimes(0);
    expect(axiosFailed).toHaveBeenCalledTimes(0);
  });

  it('clicking row header calls custom sort', async () => {
    const tableWrapper = wrapper.findComponent('#clientSizeTableWrapper');
    tableWrapper.vm.tableContainerWidth = 900;
    tableWrapper.vm.$q.screen.lt.md = false;
    await nextTick();
    const sort = vi.spyOn(wrapper.vm.clientSizeHeaders[2], 'sort');
    const header = wrapper.find('#header-formattedStorageSize');
    await header.trigger('click');
    await header.trigger('click');
    await nextTick();
    expect(sort).toHaveBeenCalledTimes(1);
  });

  it('usageText returns readable usage text', async () => {
    wrapper.vm.exactFileValue = false;
    expect(wrapper.vm.usageText).toStrictEqual('12.0%');
    const progressBarLabel = wrapper.find('#storageUsageLabel');
    await progressBarLabel.trigger('click');
    expect(wrapper.vm.exactFileValue).toStrictEqual(true);
    expect(wrapper.vm.usageText).toStrictEqual('100 B/830 B');
  });

  it('totalUsage returns total document usage for clients', () => {
    expect(wrapper.vm.totalUsage).toStrictEqual(100);
  });

  it('totalUsagePercentage returns document usage as percentage of total', () => {
    expect(wrapper.vm.totalUsagePercentage).toStrictEqual(0.12048192771084337);
  });

  it('openClientDialog() does that', () => {
    wrapper.vm.clientDialog = false;
    wrapper.vm.userStore.user.centres = [1];
    const client = { id: 3 };
    wrapper.vm.openClientDialog(client);
    expect(wrapper.vm.selectedClientId).toStrictEqual(3);
    expect(wrapper.vm.showClientDialog).toStrictEqual(true);
    expect(notifySpy).not.toHaveBeenCalled();
  });

  it('openClientDialog() does not open if no centres overlap', () => {
    wrapper.vm.clientDialog = false;
    wrapper.vm.userStore.user.centres = [2, 3, 5];
    const client = { id: 3 };
    wrapper.vm.openClientDialog(client);
    expect(wrapper.vm.selectedClientId).toStrictEqual(null);
    expect(wrapper.vm.showClientDialog).toStrictEqual(false);
    expect(notifySpy).toHaveBeenCalledWith({
      type: 'info',
      message:
        'You cannot view this client, they are not in any of your centres'
    });
  });

  it('openClientDialog() handles client with no centres', () => {
    wrapper.vm.showClientDialog = false;
    wrapper.vm.userStore.user.centres = [2];
    const client = { id: 5 };
    wrapper.vm.openClientDialog(client);
    expect(wrapper.vm.showClientDialog).toStrictEqual(false);
  });

  it('closeClientDialog() does that', () => {
    wrapper.vm.showClientDialog = true;
    wrapper.vm.closeClientDialog();
    expect(wrapper.vm.showClientDialog).toStrictEqual(false);
  });

  it('clients() returns clients with storage', () => {
    expect(wrapper.vm.clients).toStrictEqual([
      {
        firstName: 'test name 1',
        formattedStorageSize: '60 B',
        id: 1,
        storageSize: 60,
        surname: 'test surname 1'
      },
      {
        firstName: 'test name 3',
        formattedStorageSize: '40 B',
        id: 3,
        storageSize: 40,
        surname: 'test surname 3'
      }
    ]);
  });

  it('gets clients storage usage on load', async () => {
    httpGet.mockResolvedValueOnce({ data: [{ id: 504 }] });
    wrapper.vm.clientStore.clientsLoading = false;
    await nextTick();
    await flushPromises();
    expect(wrapper.vm.clientStore.clients['504']).toStrictEqual({
      id: 504
    });
  });

  it('client loading watcher calls axiosFailed() on failure', async () => {
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    httpGet.mockRejectedValueOnce({});
    wrapper.vm.clientStore.clientsLoading = false;
    await nextTick();
    await flushPromises();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('maxStorage defaults to 0', () => {
    expect(wrapper.vm.maxStorage).toStrictEqual(830);
    process.env.VUE_APP_MAX_STORAGE = undefined;
    wrapper = mount(ClientSizeTable);
    expect(wrapper.vm.maxStorage).toStrictEqual(0);
  });
});
