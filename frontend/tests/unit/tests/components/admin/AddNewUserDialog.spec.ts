import { mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import AddNewUserDialog from 'src/components/admin/AddNewUserDialog.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { Notify } from 'quasar';
import { nextTick } from 'vue';
import type { SpyInstance } from 'vitest';

setup();

describe('AddNewUserDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpPost: SpyInstance;
  let emit: SpyInstance;
  let notifySpy: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = mount(AddNewUserDialog);
    httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    emit = vi.spyOn(wrapper.vm.context, 'emit');
    notifySpy = vi.spyOn(Notify, 'create');
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('changing email clears existing errors', async () => {
    wrapper.vm.emailErrors = [1, 2, 3];
    wrapper.vm.email = 'newemail';
    await nextTick();
    expect(wrapper.vm.emailErrors).toStrictEqual([]);
  });

  it('sendRegistrationEmail calls email endpoint', async () => {
    await wrapper.find('#addNewUserDisplayNameInput').setValue('Example');
    await wrapper.find('#addNewUserEmailInput').setValue('testemail@test.com');
    wrapper.vm.context.sendEmailSuccess = vi.fn();
    wrapper.vm.selectedCentres = [2, 3, 5];
    httpPost.mockResolvedValueOnce(true);
    await wrapper.vm.sendRegistrationEmail();
    expect(httpPost).toHaveBeenCalledWith('request-registration-email/', {
      email: 'testemail@test.com',
      displayName: 'Example',
      centres: [2, 3, 5]
    });
    expect(wrapper.vm.context.sendEmailSuccess).toHaveBeenCalledTimes(1);
  });

  it('sendEmailSuccess closes modal', () => {
    wrapper.vm.sendEmailSuccess();
    expect(emit).toHaveBeenCalledWith('close-dialog');
    expect(notifySpy).toHaveBeenCalledWith({
      message: 'Registration Email Sent!',
      type: 'positive'
    });
  });

  it('emailErrors are set on failure', async () => {
    const err = {
      response: {
        data: {
          email: ['test email error']
        }
      }
    };
    httpPost.mockRejectedValueOnce(err);

    await wrapper.vm.sendRegistrationEmail();
    expect(wrapper.vm.emailErrors).toStrictEqual(['test email error']);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledWith(err);
  });

  it('updateSelectedCentres adds and removes centres', () => {
    wrapper.vm.selectedCentres = [1, 3, 4];
    wrapper.vm.updateSelectedCentres(6, true);
    expect(wrapper.vm.selectedCentres).toStrictEqual([1, 3, 4, 6]);

    wrapper.vm.updateSelectedCentres(3, false);
    expect(wrapper.vm.selectedCentres).toStrictEqual([1, 4, 6]);
  });

  it('isValid returns true if form valid', () => {
    wrapper.vm.selectedCentres = [2, 3, 5];
    wrapper.vm.email = 'test@test.com';
    wrapper.vm.displayName = 'newDisplayName';

    expect(wrapper.vm.isValid).toStrictEqual(true);

    wrapper.vm.email = 'testest.com';
    expect(wrapper.vm.isValid).toStrictEqual(false);

    wrapper.vm.email = 'test@test.com';
    wrapper.vm.selectedCentres = [];
    expect(wrapper.vm.isValid).toStrictEqual(false);
  });

  it('display name rules handle empty value', async () => {
    const displayNameInput = wrapper.find('#addNewUserDisplayNameInput');
    await displayNameInput.setValue('');
    expect(
      wrapper.vm.displayNameRules[0](displayNameInput.element.value)
    ).toStrictEqual('Display name is required');
    await displayNameInput.setValue('test');
    expect(
      wrapper.vm.displayNameRules[0](displayNameInput.element.value)
    ).toStrictEqual(true);
  });

  it('email rules handle empty value and invalid email', async () => {
    const emailInput = wrapper.find('#addNewUserEmailInput');
    await emailInput.setValue('');
    expect(wrapper.vm.emailRules[0](emailInput.element.value)).toStrictEqual(
      'Email is required'
    );
    await emailInput.setValue('test');
    expect(wrapper.vm.emailRules[1](emailInput.element.value)).toStrictEqual(
      'Invalid Email Address'
    );
    await emailInput.setValue('asdasd@test.com');
    expect(wrapper.vm.emailRules[1](emailInput.element.value)).toStrictEqual(
      true
    );
  });

  it('validate does nothing if isValid is false', () => {
    const sendRegistrationEmail = vi.spyOn(
      wrapper.vm.context,
      'sendRegistrationEmail'
    );
    wrapper.vm.validate();
    expect(sendRegistrationEmail).toHaveBeenCalledTimes(0);
  });

  it('validate calls sendResistration when isValid is true', () => {
    const sendRegistrationEmail = vi.spyOn(
      wrapper.vm.context,
      'sendRegistrationEmail'
    );
    wrapper.vm.selectedCentres = [1];
    wrapper.vm.email = 'email@test.com';
    wrapper.vm.displayName = 'newDisplayName';

    wrapper.vm.validate();
    expect(sendRegistrationEmail).toHaveBeenCalledTimes(1);
  });
});
