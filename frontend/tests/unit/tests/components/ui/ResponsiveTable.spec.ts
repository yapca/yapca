import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import ResponsiveTable from 'src/components/ui/ResponsiveTable.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';

setup();

const exampleUser = {
  id: 1,
  username: 'test username',
  displayName: 'test displayname',
  email: 'exampleemail',
  group: 'testgroup',
  centres: 7,
  isActive: true
};

describe('ResponsiveTable.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    wrapper = mount(ResponsiveTable, {
      props: {
        headers: [
          {
            label: 'username',
            name: 'username',
            field: 'username',
            sortable: true
          }
        ],
        rows: [exampleUser],
        tableId: 'userTable',
        title: 'User Table',
        defaultSort: 'username'
      }
    });
  });

  it('clicking grid opens row', async () => {
    wrapper.vm.tableWidth = 90;
    await nextTick();
    expect(wrapper.vm.makeTableGrid).toStrictEqual(true);
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    const grid = wrapper.find('.responsiveTableGridCard');
    await grid.trigger('click');
    expect(emit).toHaveBeenCalledWith('row-clicked', exampleUser);
  });

  it('clicking header opens row', async () => {
    wrapper.vm.$q.screen.lt.md = false;
    wrapper.vm.tableContainerWidth = 900;
    await nextTick();
    expect(wrapper.vm.makeTableGrid).toStrictEqual(false);
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    const tableRow = wrapper.find('.responsiveTableRow');
    await tableRow.trigger('click');
    expect(emit).toHaveBeenCalledWith('row-clicked', exampleUser);
  });
});
