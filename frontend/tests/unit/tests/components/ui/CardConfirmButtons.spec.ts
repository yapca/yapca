import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CardConfirmButtons from 'src/components/ui/CardConfirmButtons.vue';
import setup from 'tests/unit/functions/setup';
import type { SpyInstance } from 'vitest';

setup();

describe('CardWithHeader.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let emit: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    wrapper = mount(CardConfirmButtons);
    emit = vi.spyOn(wrapper.vm, 'emit');
  });

  it('clicking go back emits event', async () => {
    await wrapper.setProps({ backButton: true });
    const goBackButton = wrapper.find('#goBackButton');
    await goBackButton.trigger('click');
    expect(emit).toHaveBeenCalledWith('go-back-button-clicked');
  });

  it('clicking confirm emits event', async () => {
    await wrapper.setProps({ confirmButtonValid: true });
    const confirmButton = wrapper.find('#confirmButton');
    await confirmButton.trigger('click');
    expect(emit).toHaveBeenCalledWith('confirm-button-clicked');
  });
});
