import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import MonthlyCalendar from 'src/components/ui/MonthlyCalendar.vue';
import setup from 'tests/unit/functions/setup';
import { mockCentres } from 'tests/unit/mocks';
import { useCentreStore } from 'src/stores/centresStore';
import { useUserStore } from 'src/stores/userStore';

setup();

describe('MonthlyCalendar.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed');
    mockCentres();
    const centreStore = useCentreStore();
    centreStore.events[2].end = undefined;
    wrapper = shallowMount(MonthlyCalendar, {
      props: {
        events: centreStore.events
      }
    });
  });

  it('creates events from prop.events', () => {
    expect(wrapper.vm.events).toStrictEqual([
      {
        id: 1,
        title: 'event 1',
        start: '2022-01-08T00:00:00.000Z',
        end: '2022-01-10T00:00:00.000Z',
        bgcolor: '#FAFAFA',
        timePeriod: 172800
      },
      {
        id: 2,
        title: 'event 2',
        start: '2022-05-03T00:00:00.000Z',
        end: '2022-05-04T00:00:00.000Z',
        bgcolor: '#AAAAAA',
        timePeriod: 86400
      },
      {
        id: 3,
        title: 'event 3',
        start: '2022-07-03T00:00:00.000Z',
        end: '',
        bgcolor: '#AAAAAA',
        timePeriod: 86400
      },
      {
        id: 4,
        title: 'event 4',
        start: '2022-10-02T00:00:00.000Z',
        end: '2022-10-03T00:00:00.000Z',
        bgcolor: '#FAFAFA',
        timePeriod: 86400
      }
    ]);
  });

  it('getWeekEvents gets events for a week', () => {
    expect(
      wrapper.vm.getWeekEvents([
        { date: '2022-01-08' },
        { date: '2022-01-09' },
        { date: '2022-01-10' }
      ])
    ).toStrictEqual([
      {
        size: 2,
        event: {
          id: 1,
          title: 'event 1',
          start: '2022-01-08T00:00:00.000Z',
          end: '2022-01-10T00:00:00.000Z',
          bgcolor: '#FAFAFA',
          timePeriod: 172800
        }
      },
      { size: 1 }
    ]);
  });

  it('badgeClasses are void when no events for date', () => {
    expect(
      wrapper.vm.badgeClasses({
        event: {
          bgcolor: '#FFFFFF'
        }
      })
    ).toStrictEqual({
      'my-event': true,
      'text-white': true,
      [`bg-#FFFFFF`]: true,
      'rounded-border': true,
      'q-calendar__ellipsis': true
    });

    expect(wrapper.vm.badgeClasses({})).toStrictEqual({
      'my-void-event': true
    });
  });

  it('badgeStyles are void when no events for date', () => {
    expect(
      wrapper.vm.badgeStyles(
        {
          event: {
            bgcolor: '#FFFFFF'
          },
          size: 5
        },
        4
      )
    ).toStrictEqual({
      'background-color': '#FFFFFF',
      width: '125%'
    });
    expect(wrapper.vm.badgeStyles({})).toStrictEqual({});
  });

  it('openEventWindow opens event if admin', () => {
    const userStore = useUserStore();
    userStore.user.group = 'admin';
    wrapper.vm.selectedCreateEventDate = '2020-03-02';
    wrapper.vm.showCreateCloseEventDialog = false;
    wrapper.vm.openEventWindow({
      scope: {
        timestamp: {
          date: '2022-10-01'
        }
      }
    });
    expect(wrapper.vm.selectedCreateEventDate).toStrictEqual('2022-10-01');
    expect(wrapper.vm.showCreateCloseEventDialog).toStrictEqual(true);
  });

  it('openEventWindow does not open event if admin', () => {
    const userStore = useUserStore();
    userStore.user.group = 'not-admin';
    wrapper.vm.selectedCreateEventDate = '2015-07-13';
    wrapper.vm.showCreateCloseEventDialog = false;
    wrapper.vm.openEventWindow({
      scope: {
        timestamp: {
          date: '2022-11-11'
        }
      }
    });
    expect(wrapper.vm.selectedCreateEventDate).toStrictEqual('2015-07-13');
    expect(wrapper.vm.showCreateCloseEventDialog).toStrictEqual(false);
  });

  it('setCreateCloseEventDialog shows or hides dialog', () => {
    wrapper.vm.setCreateCloseEventDialog(false);
    expect(wrapper.vm.showCreateCloseEventDialog).toStrictEqual(false);

    wrapper.vm.setCreateCloseEventDialog(true);
    expect(wrapper.vm.showCreateCloseEventDialog).toStrictEqual(true);
  });

  it('emitChange calls calendar-change emit', () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.emitChange({ start: '2022-01-05', end: '2022-01-07' });
    expect(emit).toHaveBeenCalledWith('calendar-change', {
      newStart: '2022-01-05',
      newEnd: '2022-01-07'
    });
  });

  it('goToToday sets calendar to today', () => {
    wrapper.vm.calendar = {
      moveToToday: vi.fn()
    };
    wrapper.vm.goToToday();
    expect(wrapper.vm.calendar.moveToToday).toHaveBeenCalledOnce();
  });

  it('goToToday does nothing if no calendar ref', () => {
    wrapper.vm.calendar = null;
    wrapper.vm.goToToday();
  });

  it('previous sets calendar to last month', () => {
    wrapper.vm.calendar = {
      prev: vi.fn()
    };
    wrapper.vm.previous();
    expect(wrapper.vm.calendar.prev).toHaveBeenCalledOnce();
  });

  it('previous does nothing if no calendar ref', () => {
    wrapper.vm.calendar = null;
    wrapper.vm.previous();
  });

  it('next sets calendar to next month', () => {
    wrapper.vm.calendar = {
      next: vi.fn()
    };
    wrapper.vm.next();
    expect(wrapper.vm.calendar.next).toHaveBeenCalledOnce();
  });

  it('next does nothing if no calendar ref', () => {
    wrapper.vm.calendar = null;
    wrapper.vm.next();
  });

  it('updating showCreateEventDialogProp sets showCreateCloseEventDialog', async () => {
    await wrapper.setProps({ showCreateEventDialogProp: false });
    expect(wrapper.vm.showCreateCloseEventDialog).toStrictEqual(false);

    await wrapper.setProps({ showCreateEventDialogProp: true });
    expect(wrapper.vm.showCreateCloseEventDialog).toStrictEqual(true);
  });

  it('deleteEvent emits delete-event if event id exists', () => {
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.deleteEvent(undefined);
    expect(emit).toHaveBeenCalledTimes(0);

    wrapper.vm.deleteEvent({ id: 3 });
    expect(emit).toHaveBeenCalledWith('delete-event', 3);
  });

  it('clickWeek opens a create event dialog for selected day', () => {
    const userStore = useUserStore();
    userStore.user.group = 'admin';
    wrapper.vm.clickWeek(
      {
        target: {
          className: 'example',
          clientWidth: 1024
        },
        layerX: 244
      },
      7,
      [{ date: '2022-02-08' }, { date: '2022-02-09' }, { date: '2022-02-10' }]
    );
    expect(wrapper.vm.selectedCreateEventDate).toStrictEqual('2022-02-09');
    expect(wrapper.vm.showCreateCloseEventDialog).toStrictEqual(true);
  });

  it('clickWeek does nothing with invalid class', () => {
    wrapper.vm.showCreateCloseEventDialog = false;
    const userStore = useUserStore();
    userStore.user.group = 'admin';
    wrapper.vm.clickWeek(
      {
        target: {
          className: 'event-button',
          clientWidth: 1024
        },
        layerX: 244
      },
      7,
      []
    );
    expect(wrapper.vm.selectedCreateEventDate).toStrictEqual('');
    expect(wrapper.vm.showCreateCloseEventDialog).toStrictEqual(false);
  });

  it('setselectedDate sets current date', () => {
    wrapper.vm.setSelectedDate('2022-07-07');
    expect(wrapper.vm.selectedDate).toStrictEqual('2022-07-07');
  });

  it('insertEvent updates selectedEvents', () => {
    const selectedEvents = [{ size: 2, event: { id: 3 } }];
    wrapper.vm.insertEvent(
      selectedEvents,
      7,
      [
        { id: 3, left: 4, size: 4, event: { id: 2 } },
        { id: 6, left: 2, size: 1, event: { id: 6 } }
      ],
      0,
      3,
      2
    );
    expect(selectedEvents).toStrictEqual([
      { size: 2, event: { id: 3 } },
      { size: 1 },
      { size: 4, event: { id: 2 } }
    ]);

    wrapper.vm.insertEvent(
      selectedEvents,
      7,
      [
        { id: 2, left: 4, size: 4, event: { id: 2 } },
        { id: 6, left: 3, size: 1, event: { id: 6 } }
      ],
      1,
      2,
      2
    );
    expect(selectedEvents).toStrictEqual([
      { size: 2, event: { id: 3 } },
      { size: 1 },
      { size: 4, event: { id: 2 } },
      { size: 1 },
      { size: 1, event: { id: 6 } },
      { size: 4, event: { id: 2 } }
    ]);

    wrapper.vm.insertEvent(
      selectedEvents,
      7,
      [
        { id: 6, left: 4, size: 4, event: { id: 2 } },
        { id: 6, left: 3, size: 1, event: { id: 6 } }
      ],
      1,
      2,
      2
    );
    expect(selectedEvents).toStrictEqual([
      { size: 2, event: { id: 3 } },
      { size: 1 },
      { size: 4, event: { id: 2 } },
      { size: 1 },
      { size: 1, event: { id: 6 } },
      { size: 4, event: { id: 2 } },
      { size: 1 },
      { size: 1, event: { id: 6 } },
      { size: 3 }
    ]);

    wrapper.vm.insertEvent(selectedEvents, 7, [], 1, 2, 2);
    expect(selectedEvents).toStrictEqual([
      { size: 2, event: { id: 3 } },
      { size: 1 },
      { size: 4, event: { id: 2 } },
      { size: 1 },
      { size: 1, event: { id: 6 } },
      { size: 4, event: { id: 2 } },
      { size: 1 },
      { size: 1, event: { id: 6 } },
      { size: 3 },
      { size: 5 }
    ]);
  });
});
