import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import DaySelector from 'src/components/ui/DaySelector.vue';
import setup from 'tests/unit/functions/setup';
import { nextTick } from 'vue';
import type { SpyInstance } from 'vitest';

setup();

describe('DaySelector.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let emit: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    wrapper = shallowMount(DaySelector, { props: { selectedDays: [2, 3, 5] } });
    emit = vi.spyOn(wrapper.vm.context, 'emit');
  });

  it('dayArray returns array of single letter days if breakpoint is xs', async () => {
    wrapper.vm.onResize({ width: 30 });
    await nextTick();
    const days = wrapper.vm.dayArray;
    expect(days).toStrictEqual([
      { label: 'M', value: 'monday' },
      { label: 'T', value: 'tuesday' },
      { label: 'W', value: 'wednesday' },
      { label: 'T', value: 'thursday' },
      { label: 'F', value: 'friday' },
      { label: 'S', value: 'saturday' },
      { label: 'S', value: 'sunday' }
    ]);
  });

  it('dayArray returns array of longer days if breakpoint is not xs', async () => {
    wrapper.vm.onResize({ width: 60 });
    await nextTick();
    const days = wrapper.vm.dayArray;
    expect(days).toStrictEqual([
      { label: 'Mon', value: 'monday' },
      { label: 'Tue', value: 'tuesday' },
      { label: 'Wed', value: 'wednesday' },
      { label: 'Thu', value: 'thursday' },
      { label: 'Fri', value: 'friday' },
      { label: 'Sat', value: 'saturday' },
      { label: 'Sun', value: 'sunday' }
    ]);
  });

  it('dayArray returns array of full days if breakpoint is xl', () => {
    wrapper.vm.buttonWidth = 100;
    const days = wrapper.vm.dayArray;
    expect(days).toStrictEqual([
      { label: 'Monday', value: 'monday' },
      { label: 'Tuesday', value: 'tuesday' },
      { label: 'Wednesday', value: 'wednesday' },
      { label: 'Thursday', value: 'thursday' },
      { label: 'Friday', value: 'friday' },
      { label: 'Saturday', value: 'saturday' },
      { label: 'Sunday', value: 'sunday' }
    ]);
  });

  it('setDayActive sends emit', () => {
    wrapper.vm.setDayActive(6);
    expect(emit).toHaveBeenCalledTimes(1);
    expect(emit).toHaveBeenCalledWith('update-selected-day', 6, true);

    wrapper.vm.setDayActive(2);
    expect(emit).toHaveBeenCalledWith('update-selected-day', 2, false);
  });

  it('setDayActive does not remove last selectedDay', async () => {
    await wrapper.setProps({ selectedDays: [3] });
    wrapper.vm.setDayActive(3);
    expect(emit).toHaveBeenCalledTimes(0);
  });

  it('dayButtonStyles changes for selected days', () => {
    expect(wrapper.vm.dayButtonStyles).toStrictEqual([
      { index: 0, textColor: 'black', buttonColor: '' },
      { index: 1, textColor: 'black', buttonColor: '' },
      { index: 2, textColor: 'white', buttonColor: 'primary' },
      { index: 3, textColor: 'white', buttonColor: 'primary' },
      { index: 4, textColor: 'black', buttonColor: '' },
      { index: 5, textColor: 'white', buttonColor: 'primary' },
      { index: 6, textColor: 'black', buttonColor: '' }
    ]);
    wrapper.vm.$q.dark.isActive = true;
    expect(wrapper.vm.dayButtonStyles).toStrictEqual([
      { index: 0, textColor: 'white', buttonColor: '' },
      { index: 1, textColor: 'white', buttonColor: '' },
      { index: 2, textColor: 'white', buttonColor: 'primary' },
      { index: 3, textColor: 'white', buttonColor: 'primary' },
      { index: 4, textColor: 'white', buttonColor: '' },
      { index: 5, textColor: 'white', buttonColor: 'primary' },
      { index: 6, textColor: 'white', buttonColor: '' }
    ]);
  });
});
