import { mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import DeleteConfirmDialog from 'src/components/ui/DeleteConfirmDialog.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import type { SpyInstance } from 'vitest';

setup();

describe('DeleteConfirmDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  let httpDelete: SpyInstance;

  beforeEach(() => {
    vi.resetAllMocks();
    vi.restoreAllMocks();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = mount(DeleteConfirmDialog, {
      props: { id: 8, name: 'centre', description: 'test' }
    });
    httpDelete = vi.spyOn(wrapper.vm.basicStore.http, 'delete');
  });

  it('deleteItem() sends patch request to server', async () => {
    httpDelete.mockResolvedValueOnce({});
    await wrapper.vm.deleteItem({
      id: 8,
      name: 'test centre'
    });
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpDelete).toHaveBeenCalledWith('centres/8/delete/');
  });

  it('deleteCentre() calls axiosFailed() on failure with id', async () => {
    httpDelete.mockRejectedValueOnce({});
    await wrapper.vm.deleteItem({
      id: 8,
      name: 'test centre'
    });
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('deleteCentre() does nothing if no id', async () => {
    await wrapper.setProps({ id: null });
    await wrapper.vm.deleteItem({
      id: 8,
      name: 'test centre'
    });
    expect(httpDelete).toHaveBeenCalledTimes(0);
  });
});
