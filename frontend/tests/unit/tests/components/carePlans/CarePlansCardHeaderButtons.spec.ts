import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import CarePlansCardHeaderButtons from 'src/components/carePlans/CarePlansCardHeaderButtons.vue';
import setup from 'tests/unit/functions/setup';
import { mockClients } from '../../../mocks';

setup();

describe('CarePlansCardHeaderButtons.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockClients();
    wrapper = shallowMount(CarePlansCardHeaderButtons, {
      props: {
        clientId: 4
      }
    });
  });

  it('closeClientDialog does that', () => {
    wrapper.vm.showClientDialog = true;
    wrapper.vm.closeClientDialog();
    expect(wrapper.vm.showClientDialog).toStrictEqual(false);
  });

  it('setCarePlanHistoryDialog does that', () => {
    wrapper.vm.showCarePlanHistoryDialog = false;
    wrapper.vm.setCarePlanHistoryDialog(true);
    expect(wrapper.vm.showCarePlanHistoryDialog).toStrictEqual(true);
    wrapper.vm.setCarePlanHistoryDialog(false);
    expect(wrapper.vm.showCarePlanHistoryDialog).toStrictEqual(false);
  });

  it('setAbsenceHistoryDialog does that', () => {
    wrapper.vm.showAbsenceHistoryDialog = false;
    wrapper.vm.setAbsenceHistoryDialog(true);
    expect(wrapper.vm.showAbsenceHistoryDialog).toStrictEqual(true);
    wrapper.vm.setAbsenceHistoryDialog(false);
    expect(wrapper.vm.showAbsenceHistoryDialog).toStrictEqual(false);
  });

  it('client is undefined if clientId is invalid', async () => {
    await wrapper.setProps({ clientId: 500 });
    expect(wrapper.vm.client).toStrictEqual(undefined);
  });

  it('client header color changes based on client active', () => {
    expect(wrapper.vm.selectedClientHeaderColor).toStrictEqual('primary');
    wrapper.vm.clientStore.clients[4].isActive = false;
    expect(wrapper.vm.selectedClientHeaderColor).toStrictEqual('grey');
  });
});
