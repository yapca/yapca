import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import AbsenceHistory from 'src/components/carePlans/AbsenceHistory.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockCentres, mockClients, mockUsers } from '../../../mocks';

setup();

describe('AbsenceHistory.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    mockClients();
    mockUsers();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(AbsenceHistory, { props: { clientId: 1 } });
    wrapper.vm.userStore.user.centres = [2, 3];
  });

  it('clientName returns the client name', () => {
    expect(wrapper.vm.clientName).toStrictEqual('test name 1');
  });

  it("clientName returns empty string if client doesn't exist", async () => {
    await wrapper.setProps({
      clientId: 7498
    });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.clientName).toStrictEqual('');
  });

  it('absences shows readable absences', () => {
    expect(wrapper.vm.absences).toStrictEqual([
      {
        date: '14/12/11',
        centre: 'Test Centre 2',
        active: false,
        reasonForInactivity: 'Deceased',
        reasonForAbsence: 'Inactive',
        addedBy: 'Test User'
      },
      {
        date: '16/12/11',
        centre: 'Test Centre 2',
        active: true,
        reasonForInactivity: 'N/A',
        reasonForAbsence: 'Busy',
        addedBy: 'Unknown'
      }
    ]);
  });

  it('getAbsences() gets absence values from store', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    wrapper.vm.clientStore.clients[4].needs.push({
      id: 4,
      need: 999,
      centre: 3,
      plan: 'Example Plan 4'
    });
    httpGet.mockResolvedValue({
      data: [
        {
          id: 11,
          date: '2011-12-19',
          centre: 1,
          client: 1,
          active: false,
          reasonForInactivity: 'Deceased',
          reasonForAbsence: '',
          addedBy: 1
        }
      ]
    });
    await wrapper.vm.getAbsences();
    expect(httpGet).toHaveBeenCalledWith('attendanceRecord/1/');
    expect(
      Object.keys(wrapper.vm.clientStore.absenceRecords).length
    ).toStrictEqual(5);
    expect(wrapper.vm.clientStore.absenceRecords[11]).toStrictEqual({
      id: 11,
      date: '2011-12-19',
      centre: 1,
      client: 1,
      active: false,
      reasonForInactivity: 'Deceased',
      reasonForAbsence: '',
      addedBy: 1
    });
  });

  it('getAbsences() throws axiosFailed on failure', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockRejectedValueOnce({});
    await wrapper.vm.getAbsences();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('getAbsences() does nothing if no clientId', async () => {
    await wrapper.setProps({ clientId: null });
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    await wrapper.vm.getAbsences();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpGet).toHaveBeenCalledTimes(0);
  });

  it('customSort() sorts absences by store date value descending', () => {
    const items = [{ id: 1 }, { id: 4 }, { id: 5 }, { id: 6 }];
    const sortedAbsences = wrapper.vm.customSort(items, 'date', false);
    expect(sortedAbsences).toStrictEqual([
      { id: 1 },
      { id: 4 },
      { id: 5 },
      { id: 6 }
    ]);
  });

  it('customSort() sorts absences by store date value ascending', () => {
    const items = [{ id: 1 }, { id: 4 }, { id: 5 }, { id: 6 }];
    const sortedAbsences = wrapper.vm.customSort(items, 'date', true);
    expect(sortedAbsences).toStrictEqual([
      { id: 6 },
      { id: 5 },
      { id: 4 },
      { id: 1 }
    ]);
  });

  it('customSort() sorts absences alphabetically if no date value', () => {
    const items = [
      { id: 1, reasonForAbsence: 'b' },
      { id: 4, reasonForAbsence: 'd' },
      { id: 5, reasonForAbsence: 'c' },
      { id: 6, reasonForAbsence: 'a' }
    ];
    const sortedAbsences = wrapper.vm.customSort(
      items,
      'reasonForAbsence',
      false
    );
    expect(sortedAbsences).toStrictEqual([
      { id: 6, reasonForAbsence: 'a' },
      { id: 1, reasonForAbsence: 'b' },
      { id: 5, reasonForAbsence: 'c' },
      { id: 4, reasonForAbsence: 'd' }
    ]);
  });

  it('customSort() sorts absences alphabetically descending if no date value', () => {
    const items = [
      { id: 1, reasonForAbsence: 'b' },
      { id: 4, reasonForAbsence: 'd' },
      { id: 5, reasonForAbsence: 'c' },
      { id: 6, reasonForAbsence: 'a' }
    ];
    const sortedAbsences = wrapper.vm.customSort(
      items,
      'reasonForAbsence',
      true
    );
    expect(sortedAbsences).toStrictEqual([
      { id: 4, reasonForAbsence: 'd' },
      { id: 5, reasonForAbsence: 'c' },
      { id: 1, reasonForAbsence: 'b' },
      { id: 6, reasonForAbsence: 'a' }
    ]);
  });
});
