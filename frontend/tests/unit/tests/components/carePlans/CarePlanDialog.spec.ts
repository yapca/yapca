import { shallowMount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import CarePlanDialog from 'src/components/carePlans/CarePlanDialog.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { nextTick } from 'vue';
import getClient from 'src/functions/client/getClient';
import {
  mockCarePlanAPIGet,
  mockCarePlans,
  mockCentres,
  mockClients,
  mockUsers
} from '../../../mocks';

setup();

describe('CarePlanDialog.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;
  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    mockClients();
    mockUsers();
    mockCarePlans();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    vi.mock('src/functions/client/getClient');
    wrapper = shallowMount(CarePlanDialog, {
      props: {
        carePlanAggregate: {
          clientId: 4,
          clientName: 'test client 4',
          date: '2011-11-18',
          totalNeeds: 4
        }
      }
    });
    wrapper.vm.userStore.user.centres = [3];
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = undefined;
  });

  it('readableCarePlans returns care plan data for table', () => {
    expect(wrapper.vm.readableCarePlans).toStrictEqual([
      {
        id: 20,
        centre: 'Test Centre 3',
        comment: 'example comment 20',
        completed: true,
        need: 'Test Need 4',
        plan: 'Example Plan 2'
      },
      {
        id: 21,
        centre: 'Test Centre 3',
        comment: 'example comment 21',
        completed: false,
        need: 'Test Need 1',
        plan: 'Example Plan 1'
      }
    ]);
  });

  it('carePlans is empty when no client needs', () => {
    wrapper.vm.clientStore.clientNeeds = {};
    expect(wrapper.vm.carePlans).toStrictEqual([]);
  });

  it('carePlans changes based on clientNeeds', () => {
    wrapper.vm.clientStore.clientNeeds[2].need = 400;
    expect(wrapper.vm.carePlans.length).toStrictEqual(1);
  });

  it('carePlans are empty when no carePlanAggregate', async () => {
    await wrapper.setProps({ carePlanAggregate: undefined });
    expect(wrapper.vm.carePlans).toStrictEqual([]);
  });

  it('getCarePlans() gets carePlan values from store', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    wrapper.vm.clientStore.clients[4].needs.push({
      id: 4,
      need: 999,
      centre: 3,
      plan: 'Example Plan 4'
    });
    httpGet.mockResolvedValue(mockCarePlanAPIGet);
    await wrapper.vm.getCarePlans();
    expect(httpGet).toHaveBeenCalledWith('carePlansDetail/4/2011-11-18/');
    expect(Object.keys(wrapper.vm.clientStore.carePlans).length).toStrictEqual(
      21
    );
    expect(wrapper.vm.clientStore.carePlans[10]).toStrictEqual({
      id: 10,
      date: '2011-12-12',
      need: 2,
      client: 4,
      inputted: true,
      comment: 'example comment 10',
      completed: false,
      clientNeed: {
        id: 5,
        centre: 3,
        need: 3,
        plan: 'example plan',
        isActive: true
      }
    });
    expect(wrapper.vm.centreStore.needs[3]).toStrictEqual({
      id: 3,
      description: 'test need 3'
    });
  });

  it('getCarePlans() throws axiosFailed on failure', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockRejectedValueOnce({});
    await wrapper.vm.getCarePlans();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('getCarePlans() does nothing if no carePlanAggregate', async () => {
    await wrapper.setProps({ carePlanAggregate: undefined });
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    await wrapper.vm.getCarePlans();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpGet).toHaveBeenCalledTimes(0);
  });

  it('updateClient() gets client needs and careplans', async () => {
    wrapper.vm.context.getCarePlans = vi.fn();
    wrapper.vm.getClientNeedsForClient = vi.fn();
    wrapper.vm.getCentreDataForClient = vi.fn();
    await wrapper.setProps({
      carePlanAggregate: {
        clientId: 4,
        date: '2011-11-19',
        totalNeeds: 4
      }
    });
    expect(wrapper.vm.context.getCarePlans).toHaveBeenCalledTimes(1);
  });

  it('updateClient() sets loading to false on complete', async () => {
    wrapper.vm.loading = true;
    wrapper.vm.context.getCarePlans = vi.fn();
    wrapper.vm.getClientNeedsForClient = vi.fn();
    wrapper.vm.getCentreNeedsForClient = vi.fn();
    await wrapper.vm.updateClient();
    expect(wrapper.vm.loading).toStrictEqual(false);
  });

  it('updateClient() does not get client data if client exists', async () => {
    wrapper.vm.context.getCarePlans = vi.fn();
    wrapper.vm.getClientNeedsForClient = vi.fn();
    await wrapper.vm.updateClient();
    expect(getClient).toHaveBeenCalledTimes(0);
  });

  it('updateClient() gets client data if no client exists', async () => {
    delete wrapper.vm.clientStore.clients[4];
    wrapper.vm.context.getCarePlans = vi.fn();
    wrapper.vm.getClientNeedsForClient = vi.fn();
    wrapper.vm.getCentreNeedsForClient = vi.fn();
    wrapper.vm.context.getClient = vi.fn();
    await wrapper.vm.updateClient();
    expect(wrapper.vm.context.getClient).toHaveBeenCalledTimes(1);
  });

  it('closeDialogIfNoCarePlans() does nothing if carePlans exist', async () => {
    wrapper.vm.context.emit = vi.fn();
    wrapper.vm.loading = false;
    delete wrapper.vm.clientStore.carePlans[20];
    await nextTick();
    expect(wrapper.vm.context.emit).toHaveBeenCalledTimes(0);
  });

  it('changing care plans closes dialog if no carePlans exist', async () => {
    wrapper.vm.context.emit = vi.fn();
    wrapper.vm.loading = false;
    wrapper.vm.clientStore.carePlans = [];
    await nextTick();
    expect(wrapper.vm.context.emit).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.context.emit).toHaveBeenCalledWith('close-dialog');
  });

  it('updateCarePlan() sends patch to server', async () => {
    wrapper.vm.basicStore.http.patch = vi
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    await wrapper.vm.updateCarePlan({ id: 4, comment: 'test' });
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(wrapper.vm.basicStore.http.patch).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.basicStore.http.patch).toHaveBeenCalledWith(
      'carePlans/4/update/',
      {
        id: 4,
        comment: 'test'
      }
    );
  });

  it('updateCarePlan() does nothing if no id', async () => {
    wrapper.vm.basicStore.http.patch = vi
      .fn()
      .mockImplementation(() => Promise.resolve({}));
    await wrapper.vm.updateCarePlan({ comment: 'test' });
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(wrapper.vm.basicStore.http.patch).toHaveBeenCalledTimes(0);
  });

  it('updateCarePlan() calls axiosFailed on failure', async () => {
    const httpPatch = vi.spyOn(wrapper.vm.basicStore.http, 'patch');
    httpPatch.mockRejectedValueOnce('Example Error');
    await wrapper.vm.updateCarePlan({ id: 4, comment: 'test' });
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('inputCarePlans() sends get request to server', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockResolvedValueOnce({});
    await wrapper.vm.inputCarePlans();
    expect(httpGet).toHaveBeenCalledWith('carePlans/4/2011-11-18/input/');
  });

  it('inputCarePlans() calls axiosFailed on server failure', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockRejectedValueOnce({});
    await wrapper.vm.inputCarePlans();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('inputCarePlans() does nothing if no carePlanAggregate', async () => {
    await wrapper.setProps({ carePlanAggregate: undefined });
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    await wrapper.vm.inputCarePlans();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpGet).toHaveBeenCalledTimes(0);
  });

  it('gets list of centres', () => {
    expect(wrapper.vm.centres).toStrictEqual([3]);
  });

  it('clientName returns the client name', () => {
    expect(wrapper.vm.clientName).toStrictEqual('test name 4');
  });

  it("clientName returns empty string if client doesn't exist", async () => {
    await wrapper.setProps({
      carePlanAggregate: {
        clientId: 354,
        date: '2011-11-19',
        totalNeeds: 4
      }
    });
    await nextTick();
    expect(wrapper.vm.clientName).toStrictEqual('');
  });

  it('setAbsenceSubmitDialog does that', () => {
    wrapper.vm.showAbsenceSubmitialog = false;
    wrapper.vm.setAbsenceSubmitDialog(true);
    expect(wrapper.vm.showAbsenceSubmitDialog).toStrictEqual(true);
    wrapper.vm.setAbsenceSubmitDialog(false);
    expect(wrapper.vm.showAbsenceSubmitDialog).toStrictEqual(false);
  });

  it('completeCarePlans() sends get request to server', async () => {
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockResolvedValueOnce({});
    await wrapper.vm.completeCarePlans();
    expect(httpGet).toHaveBeenCalledWith('carePlans/4/2011-11-18/complete/');
  });

  it('completeCarePlans() calls axiosFailed on server failure', async () => {
    wrapper.vm.readableCarePlans[0].completed = true;
    wrapper.vm.readableCarePlans[1].completed = true;
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    httpGet.mockRejectedValueOnce({});
    await wrapper.vm.completeCarePlans();
    expect(httpGet).toHaveBeenCalledWith('carePlans/4/2011-11-18/incomplete/');
    expect(axiosFailed).toHaveBeenCalledTimes(1);
  });

  it('completeCarePlans() does nothing if no carePlanAggregate', async () => {
    await wrapper.setProps({ carePlanAggregate: undefined });
    const httpGet = vi.spyOn(wrapper.vm.basicStore.http, 'get');
    await wrapper.vm.completeCarePlans();
    expect(axiosFailed).toHaveBeenCalledTimes(0);
    expect(httpGet).toHaveBeenCalledTimes(0);
  });
});
