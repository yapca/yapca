import { shallowMount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import Needs from 'src/components/centreSetup/Needs.vue';
import setup from 'tests/unit/functions/setup';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import { mockCentres } from 'tests/unit/mocks';

setup();

describe('Needs.vue', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let wrapper: any;

  beforeEach(() => {
    vi.useRealTimers();
    vi.resetAllMocks();
    vi.restoreAllMocks();
    mockCentres();
    vi.mock('src/functions/proxy/AxiosFailed', () => ({
      default: vi.fn()
    }));
    wrapper = shallowMount(Needs, {
      props: {
        centres: ['Centre 1']
      }
    });
  });

  it('test addNeedField()', () => {
    wrapper.vm.needInputs = [{ description: 'test 1' }];
    wrapper.vm.addNeedField();
    expect(wrapper.vm.needInputs).toStrictEqual([
      { description: 'test 1' },
      { description: '' }
    ]);
  });

  it('test removeNeedField()', () => {
    wrapper.vm.needInputs = [
      { description: 'test 1' },
      { description: 'test 2' },
      { description: 'test 3' }
    ];
    wrapper.vm.removeNeedField(1);
    expect(wrapper.vm.needInputs).toStrictEqual([
      { description: 'test 1' },
      { description: 'test 3' }
    ]);
  });

  it('test validateNeeds()', async () => {
    wrapper.vm.needInputs = [
      { description: 'test 1' },
      { description: '' },
      { description: 'test 3' }
    ];
    wrapper.vm.noNeedConfirmDialog = false;
    const createNeeds = vi.spyOn(wrapper.vm.context, 'createNeeds');

    await wrapper.vm.validateNeeds();
    expect(createNeeds).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.noNeedConfirmDialog).toStrictEqual(false);
    expect(wrapper.vm.needs).toStrictEqual([
      { description: 'test 1' },
      { description: 'test 3' }
    ]);

    wrapper.vm.needInputs = [
      { description: '' },
      { description: '' },
      { description: '' }
    ];

    wrapper.vm.validateNeeds();
    expect(wrapper.vm.noNeedConfirmDialog).toStrictEqual(true);
    expect(createNeeds).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.needs).toStrictEqual([]);
  });

  it('test createNeeds()', async () => {
    const consoleError = vi.spyOn(console, 'error');
    consoleError.mockImplementation(() => {
      return true;
    });
    wrapper.vm.needs = [{ description: 'test 1' }, { description: 'test 3' }];
    const createNeedsSuccessful = vi.spyOn(
      wrapper.vm.context,
      'createNeedsSuccessful'
    );

    const httpPost = vi.spyOn(wrapper.vm.basicStore.http, 'post');
    httpPost.mockRejectedValueOnce('Example Error');
    await wrapper.vm.createNeeds();
    expect(httpPost).toHaveBeenCalledWith('needs/', [
      { description: 'test 1' },
      { description: 'test 3' }
    ]);
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(axiosFailed).toHaveBeenCalledWith('Example Error');
    expect(createNeedsSuccessful).toHaveBeenCalledTimes(0);

    createNeedsSuccessful.mockResolvedValue({});
    httpPost.mockResolvedValue({ data: {} });
    await wrapper.vm.createNeeds();
    expect(axiosFailed).toHaveBeenCalledTimes(1);
    expect(createNeedsSuccessful).toHaveBeenCalledWith({});
  });

  it('test createNeedsSuccessful()', () => {
    wrapper.vm.centreStore.setupCentre.openingDays = [2, 3, 4];
    wrapper.vm.centreStore.setupCentre.policies = [2, 3, 4];

    const data = [
      { id: 1, description: 'Test Need 1' },
      { id: 2, description: 'Test Need 2' },
      { id: 3, description: 'Test Need 3' }
    ];
    const emit = vi.spyOn(wrapper.vm.context, 'emit');
    wrapper.vm.createNeedsSuccessful(data);
    expect(emit).toHaveBeenCalledWith('update-centre', {
      id: 2,
      name: 'Test Centre',
      needs: [1, 2, 3],
      policiesFirstSetupComplete: false,
      needsFirstSetupComplete: true,
      setupComplete: true,
      openingDaysFirstSetupComplete: false,
      closingDaysFirstSetupComplete: false,
      nameValid: true,
      editingCentreName: false,
      created: '',
      color: '#FFFFFF',
      closedOnPublicHolidays: true,
      country: 'AU',
      state: 'QLD'
    });
  });
});
