import { beforeEach } from 'vitest';
import {
  VueRouterMock,
  createRouterMock,
  injectRouterMock
} from 'vue-router-mock';
import { config } from '@vue/test-utils';
import type { RouterMockOptions } from 'vue-router-mock';

// https://github.com/posva/vue-router-mock
export function installRouter(options?: RouterMockOptions) {
  beforeEach(() => {
    const router = createRouterMock(options);
    injectRouterMock(router);
  });

  config.plugins.VueWrapper.install(VueRouterMock);
}
