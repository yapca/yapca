import type User from 'src/types/User';
import type BasicStore from './BasicStore';

export default interface GlobalProperties {
  $pinia: {
    state: {
      value: {
        user: {
          lockoutEndTime: Date;
          user: User;
        };
        basic: BasicStore;
        socket: {
          WS_RECONNECT: (
            appServerAddress: string,
            backendPath: string,
            token: string
          ) => void;
          clientVersion: string;
          clientType: string;
        };
      };
    };
  };
  $router: {
    currentRoute: {
      value: {
        query: {
          nextUrl: string;
        };
      };
    };
  };
}
