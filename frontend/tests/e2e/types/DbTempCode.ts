export default interface DbTempCode {
  id: number;
  startTime: Date;
  endTime: Date;
  action: string;
  code: string;
  email: string;
  associatedUserId: number | null;
  associatedClientId: number | null;
  centreName: string;
  permissionCodeName: string;
}
