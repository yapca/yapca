import type parse from 'postgres-interval';

export default interface DbEvent {
  allDay: boolean;
  centre_id: number;
  createdBy_id: number;
  description: string;
  eventType: 'User Close Event' | 'Public Holiday';
  id: number;
  link: string;
  name: string;
  recurrences: string;
  start: string;
  timePeriod: parse.IPostgresInterval;
}
