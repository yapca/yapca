export default interface DbUser {
  id: number;
  groupName: string;
  centreId: number;
  centreName: string;
  isActive: boolean;
  darkTheme: boolean;
  accentColor: string;
  displayName: string;
  userClientTableHiddenRows: string;
  otpDeviceKey: string | null;
  email: string;
  username: string;
  password: string;
  permission: string;
}
