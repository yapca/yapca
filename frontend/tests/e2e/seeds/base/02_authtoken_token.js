exports.seed = knex =>
  knex('authtoken_token').insert([
    {
      key: '8a6b5ac6bc6310fec020c2462db028996e856062',
      created: '2023-04-17T20:09:22.185Z',
      user_id: 4
    },
    {
      key: '7cdfa953a0a770d5f4e721b155fdc2bbef108017',
      created: '2023-04-17T20:09:22.664Z',
      user_id: 2
    },
    {
      key: '79dd84fe6210c7fe8eced30c371428df27b433f0',
      created: '2023-04-17T20:09:40.926Z',
      user_id: 1
    },
    {
      key: '74c56679433e942e967f3d51ad07fcb712232375',
      created: '2023-04-17T20:11:03.746Z',
      user_id: 3
    }
  ]);
