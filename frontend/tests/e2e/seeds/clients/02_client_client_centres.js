exports.seed = knex =>
  knex('client_client_centres').insert([
    {
      client_id: 1,
      centre_id: 1
    },
    {
      client_id: 2,
      centre_id: 3
    },
    {
      client_id: 3,
      centre_id: 1
    },
    {
      client_id: 3,
      centre_id: 3
    },
    {
      client_id: 3,
      centre_id: 4
    },
    {
      client_id: 4,
      centre_id: 1
    },
    {
      client_id: 4,
      centre_id: 3
    },
    {
      client_id: 5,
      centre_id: 2
    },
    {
      client_id: 5,
      centre_id: 3
    },
    {
      client_id: 5,
      centre_id: 4
    },
    {
      client_id: 6,
      centre_id: 1
    },
    {
      client_id: 7,
      centre_id: 1
    },
    {
      client_id: 8,
      centre_id: 1
    }
  ]);
