const makeBaseClient = index => {
  return {
    createdDate: '2023-04-16T20:37:22.685Z',
    firstName: 'Test Client',
    surname: index,
    isActive: true,
    setupComplete: true,
    basicInfoSetup: false,
    additionalInfoSetup: false,
    servicesSetup: false,
    abilitySetup: false,
    dietSetup: false,
    visitingDaysSetup: false,
    dateOfBirth: '1967-02-11',
    address: `Example Address ${index}`,
    phoneNumber: index.repeat(7),
    publicHealthNurseName: `PHN ${index}`,
    publicHealthNursePhoneNumber: `PHN ${index.repeat(7)}`,
    homeHelpName: `HH ${index}`,
    homeHelpPhoneNumber: `HH ${index.repeat(7)}`,
    gpName: `GP ${index}`,
    gpPhoneNumber: `GP ${index.repeat(7)}`,
    chemistName: `C ${index}`,
    chemistPhoneNumber: `C ${index.repeat(7)}`,
    medicalHistory: `Client ${index} Medical History`,
    surgicalHistory: `Client ${index} Surgical History`,
    mobility: 'Walks Unaided',
    toileting: 'Fully Continent',
    hygiene: 'Self',
    sight: 'Normal',
    hearing: 'Normal',
    usesDentures: true,
    caseFormulation: `Client ${index} Case Formulation`,
    allergies: '',
    intolerances: '',
    requiresAssistanceEating: false,
    thicknerGrade: 4,
    admittedBy_id: 1,
    storageSize: 0,
    reasonForInactivity: 'Deceased',
    gender: 'Unknown'
  };
};

exports.seed = knex =>
  knex('client_client').insert([
    {
      ...makeBaseClient('1'),
      mobility: 'Walks With Frame',
      hygiene: 'Full Hoist Shower',
      sight: 'Glasses',
      hearing: 'Impaired'
    },
    {
      ...makeBaseClient('2'),
      dateOfBirth: '1991-05-09',
      usesDentures: false,
      caseFormulation: '',
      thicknerGrade: 0
    },
    {
      ...makeBaseClient('3'),
      isActive: false,
      dateOfBirth: '1981-12-01',
      usesDentures: false,
      caseFormulation: '',
      thicknerGrade: 0
    },
    {
      ...makeBaseClient('4'),
      mobility: 'Walks With Frame',
      dateOfBirth: '1971-07-08',
      allergies: 'Allergies List 4',
      intolerances: 'Intolerances List 4',
      hygiene: 'Full Hoist Shower',
      sight: 'Glasses',
      hearing: 'Impaired',
      requiresAssistanceEating: true,
      admittedBy_id: 2
    },
    {
      ...makeBaseClient('5'),
      dateOfBirth: '1961-11-27',
      hygiene: 'Full Hoist Shower',
      sight: 'Glasses',
      hearing: 'Impaired',
      usesDentures: false,
      caseFormulation: '',
      thicknerGrade: 0
    },
    {
      ...makeBaseClient('6'),
      dateOfBirth: '2002-11-30',
      usesDentures: false,
      caseFormulation: '',
      thicknerGrade: 0
    },
    {
      ...makeBaseClient('7'),
      firstName: 'Batman',
      isActive: true,
      setupComplete: false,
      dateOfBirth: '1987-06-21',
      usesDentures: false,
      caseFormulation: '',
      thicknerGrade: 0
    },
    {
      ...makeBaseClient('8'),
      firstName: 'Shazam',
      isActive: true,
      setupComplete: false,
      basicInfoSetup: true,
      dateOfBirth: '1967-05-05',
      usesDentures: false,
      caseFormulation: '',
      thicknerGrade: 0
    }
  ]);
