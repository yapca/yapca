exports.seed = knex =>
  knex('client_medication').insert([
    {
      name: 'Test Med 1',
      dosage: '10mg',
      frequency: 'weekly',
      inactiveDate: null,
      client_id: 4
    },
    {
      name: 'Test Med 2',
      dosage: '100mg',
      frequency: 'daily',
      inactiveDate: null,
      client_id: 4
    },
    {
      name: 'Test Med 3',
      dosage: '5mg',
      frequency: 'hourly',
      inactiveDate: '2011-03-02',
      client_id: 4
    }
  ]);
