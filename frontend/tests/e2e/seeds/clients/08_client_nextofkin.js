exports.seed = knex =>
  knex('client_nextofkin').insert([
    {
      name: 'Test NoK 1',
      phoneNumber: '111111',
      client_id: 4
    },
    {
      name: 'Test NoK 2',
      phoneNumber: '222222',
      client_id: 4
    }
  ]);
