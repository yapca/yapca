exports.seed = knex =>
  knex('client_clientagreedpolicies').insert([
    {
      centre_id: 1,
      client_id: 1,
      policy_id: 1
    },
    {
      centre_id: 1,
      client_id: 1,
      policy_id: 3
    },
    {
      centre_id: 1,
      client_id: 1,
      policy_id: 5
    },
    {
      centre_id: 1,
      client_id: 3,
      policy_id: 1
    },
    {
      centre_id: 1,
      client_id: 3,
      policy_id: 3
    },
    {
      centre_id: 1,
      client_id: 3,
      policy_id: 5
    },
    {
      centre_id: 3,
      client_id: 3,
      policy_id: 6
    },
    {
      centre_id: 4,
      client_id: 3,
      policy_id: 5
    },
    {
      centre_id: 1,
      client_id: 4,
      policy_id: 1
    },
    {
      centre_id: 1,
      client_id: 4,
      policy_id: 3
    },
    {
      centre_id: 1,
      client_id: 4,
      policy_id: 5
    },
    {
      centre_id: 3,
      client_id: 4,
      policy_id: 6
    }
  ]);
