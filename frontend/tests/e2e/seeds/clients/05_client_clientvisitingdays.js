exports.seed = knex =>
  knex('client_clientvisitingdays').insert([
    {
      centre_id: 1,
      client_id: 1,
      day_id: 2
    },
    {
      centre_id: 1,
      client_id: 1,
      day_id: 3
    },
    {
      centre_id: 3,
      client_id: 2,
      day_id: 4
    },
    {
      centre_id: 3,
      client_id: 2,
      day_id: 1
    },
    {
      centre_id: 1,
      client_id: 3,
      day_id: 2
    },
    {
      centre_id: 1,
      client_id: 3,
      day_id: 5
    },
    {
      centre_id: 4,
      client_id: 3,
      day_id: 2
    },
    {
      centre_id: 1,
      client_id: 4,
      day_id: 2
    },
    {
      centre_id: 1,
      client_id: 4,
      day_id: 5
    },
    {
      centre_id: 3,
      client_id: 4,
      day_id: 3
    },
    {
      centre_id: 2,
      client_id: 5,
      day_id: 1
    },
    {
      centre_id: 2,
      client_id: 5,
      day_id: 4
    },
    {
      centre_id: 1,
      client_id: 6,
      day_id: 3
    },
    {
      centre_id: 1,
      client_id: 7,
      day_id: 3
    },
    {
      centre_id: 1,
      client_id: 8,
      day_id: 3
    }
  ]);
