exports.seed = knex =>
  knex('authentication_customuser').insert([
    {
      password:
        'pbkdf2_sha256$120000$ug2WOCLCuKse$1TINlkBwEfviiHfvgcXes1zGK5O+5YBpe0+8dXWJpeA=',
      last_login: '2023-04-16T20:37:24.202Z',
      is_superuser: false,
      username: 'clientUser',
      first_name: '',
      last_name: '',
      is_staff: false,
      is_active: true,
      date_joined: '2023-04-16T20:37:24.012Z',
      displayName: null,
      darkTheme: true,
      accentColor: '#00796B',
      userClientTableHiddenRows: '',
      client_id: 1,
      email: 'testclientuser@test.com',
      otpDeviceKey: ''
    }
  ]);
