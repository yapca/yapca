const baseCentre = {
  name: 'Example Centre',
  policiesFirstSetupComplete: true,
  needsFirstSetupComplete: true,
  openingDaysFirstSetupComplete: true,
  setupComplete: true,
  created: '2023-04-15T19:52:55.406Z',
  color: '#0097A7',
  closedOnPublicHolidays: false,
  country: 'IE',
  state: '',
  closingDaysFirstSetupComplete: false
};

exports.seed = knex =>
  knex('centre_centre').insert([
    baseCentre,
    {
      ...baseCentre,
      name: 'Example Centre 2',
      color: '#D32F2F'
    },
    {
      ...baseCentre,
      name: 'Example Centre 3',
      color: '#1976D2'
    },
    {
      ...baseCentre,
      name: 'Example Centre 4',
      color: '#C2185B'
    }
  ]);
