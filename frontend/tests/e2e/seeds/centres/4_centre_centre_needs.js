exports.seed = knex =>
  knex('centre_centre_needs').insert([
    {
      centre_id: 1,
      need_id: 5
    },
    {
      centre_id: 1,
      need_id: 1
    },
    {
      centre_id: 1,
      need_id: 3
    },
    {
      centre_id: 2,
      need_id: 4
    },
    {
      centre_id: 2,
      need_id: 2
    },
    {
      centre_id: 3,
      need_id: 6
    },
    {
      centre_id: 4,
      need_id: 5
    }
  ]);
