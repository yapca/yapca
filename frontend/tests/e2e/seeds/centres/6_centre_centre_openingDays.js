exports.seed = knex =>
  knex('centre_centre_openingDays').insert([
    {
      centre_id: 1,
      day_id: 2
    },
    {
      centre_id: 1,
      day_id: 3
    },
    {
      centre_id: 1,
      day_id: 5
    },
    {
      centre_id: 2,
      day_id: 1
    },
    {
      centre_id: 2,
      day_id: 4
    },
    {
      centre_id: 2,
      day_id: 6
    },
    {
      centre_id: 3,
      day_id: 1
    },
    {
      centre_id: 3,
      day_id: 3
    },
    {
      centre_id: 3,
      day_id: 4
    },
    {
      centre_id: 4,
      day_id: 2
    },
    {
      centre_id: 4,
      day_id: 5
    },
    {
      centre_id: 4,
      day_id: 6
    }
  ]);
