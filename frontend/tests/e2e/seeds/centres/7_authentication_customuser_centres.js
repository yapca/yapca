exports.seed = knex =>
  knex('authentication_customuser_centres').insert([
    {
      customuser_id: 1,
      centre_id: 1
    },
    {
      customuser_id: 2,
      centre_id: 1
    },
    {
      customuser_id: 2,
      centre_id: 2
    },
    {
      customuser_id: 2,
      centre_id: 4
    },
    {
      customuser_id: 4,
      centre_id: 1
    },
    {
      customuser_id: 4,
      centre_id: 2
    },
    {
      customuser_id: 4,
      centre_id: 3
    },
    {
      customuser_id: 4,
      centre_id: 4
    }
  ]);
