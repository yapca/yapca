import { database } from '../globals';
import type { Knex } from 'knex';

const setOtpDeviceKey = (username: string, newKey: string): Knex.QueryBuilder =>
  database()
    .where('username', '=', username)
    .table('authentication_customuser')
    .update({
      otpDeviceKey: newKey
    });
export default setOtpDeviceKey;
