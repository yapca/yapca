import { database } from '../globals';

const createCentreWithOpeningDaysSetup = (): Promise<void> =>
  database()
    .table('centre_centre')
    .insert({
      name: 'Example Centre OD Setup',
      openingDaysFirstSetupComplete: true,
      closingDaysFirstSetupComplete: true,
      policiesFirstSetupComplete: false,
      setupComplete: false,
      needsFirstSetupComplete: false,
      created: '2020-02-26 21:17:56',
      color: '#FFFFFF',
      closedOnPublicHolidays: true,
      country: 'IE',
      state: ''
    })
    .returning('id')
    .then((centre: { id: number }[]) =>
      database().table('centre_centre_openingDays').insert({
        centre_id: centre[0].id,
        day_id: 4
      })
    );

export default createCentreWithOpeningDaysSetup;
