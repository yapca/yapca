import { database } from '../globals';
import type DbTempCode from '../types/DbTempCode';

const getTempCodesByAction = (action: string): Promise<DbTempCode[]> =>
  database()
    .select()
    .from('authentication_tempcode')
    .leftJoin(
      'authentication_tempcode_centres',
      'authentication_tempcode.id',
      'authentication_tempcode_centres.tempcode_id'
    )
    .leftJoin(
      'centre_centre',
      'authentication_tempcode_centres.centre_id',
      'centre_centre.id'
    )
    .leftJoin(
      'authentication_tempcode_permissions',
      'authentication_tempcode.id',
      'authentication_tempcode_permissions.tempcode_id'
    )
    .leftJoin(
      'authentication_userpermission',
      'authentication_tempcode_permissions.userpermission_id',
      'authentication_userpermission.id'
    )
    .columns([
      'authentication_tempcode.id AS id',
      'authentication_tempcode.startTime AS startTime',
      'authentication_tempcode.endTime AS endTime',
      'authentication_tempcode.action AS action',
      'authentication_tempcode.code AS code',
      'authentication_tempcode.email AS email',
      'authentication_tempcode.associatedUser_id AS associatedUserId',
      'authentication_tempcode.associatedClient_id AS associatedClientId',
      'authentication_userpermission.codename AS permissionCodeName',
      'centre_centre.name AS centreName'
    ])
    .where('action', action);
export default getTempCodesByAction;
