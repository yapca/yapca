import { database } from '../globals';
import type DbClient from '../types/DbClient';

const getClientByName = (surname: string): Promise<DbClient[]> =>
  database()
    .select()
    .from('client_client')
    .leftJoin(
      'authentication_customuser',
      'client_client.admittedBy_id',
      'authentication_customuser.id'
    )
    .leftJoin(
      'client_clientvisitingdays',
      'client_client.id',
      'client_clientvisitingdays.client_id'
    )
    .leftJoin('centre_day', 'client_clientvisitingdays.day_id', 'centre_day.id')
    .leftJoin(
      'client_clientagreedpolicies',
      'client_client.id',
      'client_clientagreedpolicies.client_id'
    )
    .leftJoin(
      'centre_policy',
      'client_clientagreedpolicies.policy_id',
      'centre_policy.id'
    )
    .leftJoin(
      'client_clientneed',
      'client_client.id',
      'client_clientneed.client_id'
    )
    .leftJoin('centre_need', 'client_clientneed.need_id', 'centre_need.id')
    .leftJoin(
      'client_client_centres',
      'client_client.id',
      'client_client_centres.client_id'
    )
    .leftJoin(
      'client_nextofkin',
      'client_client.id',
      'client_nextofkin.client_id'
    )
    .leftJoin(
      'client_medication',
      'client_client.id',
      'client_medication.client_id'
    )
    .leftJoin(
      'client_clientdocument',
      'client_client.id',
      'client_clientdocument.client_id'
    )
    .leftJoin(
      'client_feedingrisk',
      'client_client.id',
      'client_feedingrisk.client_id'
    )
    .where('surname', surname)
    .columns([
      'client_client.id AS id',
      'client_clientvisitingdays.day_id AS dayId',
      'client_client.address AS address',
      'client_client.dateOfBirth AS dateOfBirth',
      'client_client.gender AS gender',
      'client_client.phoneNumber AS phoneNumber',
      'client_client.additionalInfoSetup AS additionalInfoSetup',
      'client_nextofkin.id AS nextOfKinId',
      'client_nextofkin.name AS nextOfKinName',
      'client_nextofkin.phoneNumber AS nextOfKinPhoneNumber',
      'client_client_centres.centre_id AS centreId',
      'client_client.firstName AS firstName',
      'client_client.surname AS surname',
      'client_client.basicInfoSetup AS basicInfoSetup',
      'client_client.gpName AS gpName',
      'client_client.gpPhoneNumber AS gpPhoneNumber',
      'client_client.servicesSetup AS servicesSetup',
      'client_client.isActive AS isActive',
      'client_client.medicalHistory AS medicalHistory',
      'client_client.surgicalHistory AS surgicalHistory',
      'client_client.homeHelpName AS homeHelpName',
      'centre_policy.description AS policyDescription',
      'client_medication.id AS medicationId',
      'client_medication.name AS medicationName',
      'client_medication.dosage AS medicationDosage',
      'client_medication.frequency AS medicationFrequency',
      'client_medication.inactiveDate AS medicationInactiveDate',
      'centre_day.day AS day',
      'client_client.setupComplete AS setupComplete',
      'client_client.mobility AS mobility',
      'client_client.toileting AS toileting',
      'client_client.hygiene AS hygiene',
      'client_client.sight AS sight',
      'client_client.hearing AS hearing',
      'client_client.usesDentures AS usesDentures',
      'client_client.caseFormulation AS caseFormulation',
      'client_client.thicknerGrade AS thicknerGrade',
      'client_feedingrisk.feedingRisk AS feedingRisk',
      'client_client.allergies AS allergies',
      'client_client.intolerances AS intolerances',
      'client_client.requiresAssistanceEating AS requiresAssistanceEating',
      'centre_need.description AS need',
      'client_clientneed.plan AS needPlan',
      'client_clientneed.isActive AS clientNeedActive',
      'client_clientneed.id AS clientNeedId',
      'client_clientdocument.name AS documentName',
      'client_clientdocument.originalName AS documentOriginalName',
      'client_client.reasonForInactivity AS reasonForInactivity'
    ]);
export default getClientByName;
