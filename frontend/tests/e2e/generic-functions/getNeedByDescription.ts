import { database } from '../globals';
import type Need from 'src/types/Need';

const getNeedByDescription = (needDescription: string): Promise<Need[]> =>
  database().select().from('centre_need').where('description', needDescription);
export default getNeedByDescription;
