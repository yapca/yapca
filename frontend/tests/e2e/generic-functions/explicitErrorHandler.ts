const explicitErrorHandler = () => {
  window.addEventListener('error', e => {
    if (
      e.message ===
        'ResizeObserver loop completed with undelivered notifications.' ||
      e.message === 'ResizeObserver loop limit exceeded'
    ) {
      e.stopImmediatePropagation();
    }
  });
};

export default explicitErrorHandler;
