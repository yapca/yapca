import { ClientFunction } from 'testcafe';
import type ClientApp from '../types/ClientApp';

const changeClientVersion = ClientFunction((clientVersion: string) => {
  const app = document.querySelector('#q-app') as unknown as ClientApp;
  const store = app.__vue_app__!.config.globalProperties.$pinia.state.value;
  store.basic.clientVersion = clientVersion;
});

export default changeClientVersion;
