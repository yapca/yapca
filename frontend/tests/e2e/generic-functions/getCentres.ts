import { database } from '../globals';
import type DbCentre from '../types/DbCentre';

const getCentres = (): Promise<DbCentre[]> =>
  database()
    .select()
    .from('centre_centre')
    .leftJoin(
      'centre_centre_policies',
      'centre_centre.id',
      'centre_centre_policies.centre_id'
    )
    .leftJoin(
      'centre_policy',
      'centre_centre_policies.policy_id',
      'centre_policy.id'
    )
    .leftJoin(
      'centre_centre_needs',
      'centre_centre.id',
      'centre_centre_needs.centre_id'
    )
    .leftJoin('centre_need', 'centre_centre_needs.need_id', 'centre_need.id')
    .leftJoin(
      'centre_centre_openingDays',
      'centre_centre.id',
      'centre_centre_openingDays.centre_id'
    )
    .columns([
      'centre_centre.id AS id',
      'centre_centre.name AS name',
      'centre_centre_openingDays.day_id AS dayId',
      'centre_policy.description AS policyDescription',
      'centre_need.description AS needDescription',
      'centre_centre.color AS color'
    ]);

export default getCentres;
