import { ClientFunction } from 'testcafe';
import getStore from './getStore';

const changeBackendPath = ClientFunction(async (backendPath: string) => {
  const store = await getStore();
  store.basic.backendPath = backendPath;
  return store.socket.WS_RECONNECT(
    store.basic.appServerAddress,
    store.basic.backendPath,
    store.basic.token
  );
});

export default changeBackendPath;
