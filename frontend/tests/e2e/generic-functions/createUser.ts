import axios from 'axios';
import * as dotenv from 'dotenv';
import createTempCode from './createTempCode';
import type { AxiosResponse } from 'axios';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});
const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const createUser = async (): Promise<AxiosResponse> => {
  await createTempCode(
    new Date(Date.now() + 5 * 60000),
    'registration',
    'ci6TbI7VDKXA1wIIRAtyyPUc9JT7lrq5',
    null,
    'newemail@test.com'
  );
  return http.post('registration/', {
    username: 'newTestUser',
    displayName: 'New Test User',
    email: 'newemail@test.com',
    password1: 'testPassword',
    password2: 'testPassword',
    accentColor: '#FFFFFF',
    darkTheme: false,
    tempCode: 'ci6TbI7VDKXA1wIIRAtyyPUc9JT7lrq5'
  });
};

export default createUser;
