import axios from 'axios';

import * as dotenv from 'dotenv';
import FormData from 'form-data';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type { AxiosResponse } from 'axios';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});

const createClientDocument = async (
  clientId: number,
  fileName: string,
  name: string,
  fileContents: string
): Promise<AxiosResponse> => {
  const http = axios.create({
    baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
      process.env.VUE_APP_API_HOST ?? ''
    }:${process.env.VUE_APP_API_PORT ?? ''}${
      process.env.VUE_APP_API_PATH ?? ''
    }/api/`
  });
  const data = new FormData();
  data.append('client', clientId.toString());
  data.append('document', fileContents, fileName);
  data.append('name', name);
  data.append('originalName', fileName);
  const contentLength = data.getLengthSync();
  const response: LoginResponse = await http.post('login/', {
    username: 'everyCentreUser',
    password: 'testPassword1'
  });
  return http.post('clientDocuments/upload/', data, {
    headers: {
      Authorization: `Token ${response.data.key}`,
      ...data.getHeaders(),
      'content-length': contentLength.toString()
    }
  });
};

export default createClientDocument;
