import { database } from '../globals';
import type ClientDocument from 'src/types/ClientDocument';

const getClientDocumentByName = (name: string): Promise<ClientDocument[]> =>
  database().select().from('client_clientdocument').where('name', name);
export default getClientDocumentByName;
