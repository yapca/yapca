import { ClientFunction } from 'testcafe';

const goForward = ClientFunction(() => window.history.forward());

export default goForward;
