import axios from 'axios';

import * as dotenv from 'dotenv';
import getClientDocumentByName from './getClientDocumentByName';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type { AxiosResponse } from 'axios';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});
const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const deleteClientDocument = (name: string): Promise<AxiosResponse> =>
  getClientDocumentByName(name).then((document: { id: number }[]) =>
    http
      .post('login/', {
        username: 'everyCentreUser',
        password: 'testPassword1'
      })
      .then((response: LoginResponse) =>
        http.delete(`clientDocuments/${document[0].id}/delete/`, {
          headers: {
            Authorization: `Token ${response.data.key}`
          }
        })
      )
  );

export default deleteClientDocument;
