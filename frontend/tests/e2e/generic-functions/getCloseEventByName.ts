import { database } from '../globals';
import type DbEvent from '../types/DbEvent';

const getCloseEventByName = (name: string): Promise<DbEvent[]> =>
  database().select().from('centre_event').where('name', name);
export default getCloseEventByName;
