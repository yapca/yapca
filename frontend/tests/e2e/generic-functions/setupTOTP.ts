import { TOTP } from 'otpauth';
import {
  otpInput1,
  otpInput2,
  otpInput3,
  otpInput4,
  otpInput5,
  otpInput6,
  otpSecretKeyText
} from '../generic-selectors/Auth';

const setupTOTP = async (browser: TestController): Promise<void> => {
  const secretKey = await otpSecretKeyText.innerText;
  const totp = new TOTP({
    issuer: 'Yapca',
    label: 'YAPCA TOTP Authentication Code',
    algorithm: 'SHA1',
    digits: 6,
    period: 30,
    secret: secretKey
  });
  const token = totp.generate();
  return browser
    .typeText(otpInput1, token.charAt(0))
    .typeText(otpInput2, token.charAt(1))
    .typeText(otpInput3, token.charAt(2))
    .typeText(otpInput4, token.charAt(3))
    .typeText(otpInput5, token.charAt(4))
    .typeText(otpInput6, token.charAt(5));
};
export default setupTOTP;
