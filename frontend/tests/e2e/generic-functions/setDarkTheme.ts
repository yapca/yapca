import { database } from '../globals';
import type { Knex } from 'knex';

const setDarkTheme = (): Knex.QueryBuilder =>
  database()
    .where('is_active', '=', true)
    .table('authentication_customuser')
    .update({
      darkTheme: true
    });
export default setDarkTheme;
