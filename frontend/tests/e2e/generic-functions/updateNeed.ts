import axios from 'axios';

import * as dotenv from 'dotenv';
import getNeedByDescription from './getNeedByDescription';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type { AxiosResponse } from 'axios';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});
const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const updateNeed = (
  needDescription: string,
  newNeedDescription: string
): Promise<AxiosResponse> =>
  getNeedByDescription(needDescription).then((need: { id: number }[]) =>
    http
      .post('login/', {
        username: 'testAdmin',
        password: 'testPassword1'
      })
      .then((response: LoginResponse) =>
        http.patch(
          `needs/${need[0].id}/update/`,
          { description: newNeedDescription },
          {
            headers: {
              Authorization: `Token ${response.data.key}`
            }
          }
        )
      )
  );

export default updateNeed;
