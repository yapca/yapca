import axios from 'axios';

import * as dotenv from 'dotenv';
import getClientByName from './getClientByName';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type { AxiosResponse } from 'axios';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});
const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const deleteMedication = (
  clientSurname: string,
  medicationName: string
): Promise<AxiosResponse> =>
  getClientByName(clientSurname).then(
    (client: { medicationId: number; medicationName: string }[]) =>
      http
        .post('login/', {
          username: 'testAdmin',
          password: 'testPassword1'
        })
        .then((response: LoginResponse) =>
          http.delete(
            `medications/${
              client.find(
                clientData => clientData.medicationName === medicationName
              )!.medicationId
            }/delete/`,
            {
              headers: {
                Authorization: `Token ${response.data.key}`
              }
            }
          )
        )
  );

export default deleteMedication;
