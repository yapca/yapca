import { database } from '../globals';

const createCentreWithPoliciesSetup = async (): Promise<void> =>
  database().table('centre_centre').insert({
    name: 'Example Centre Policies Setup',
    openingDaysFirstSetupComplete: true,
    closingDaysFirstSetupComplete: true,
    policiesFirstSetupComplete: true,
    setupComplete: false,
    needsFirstSetupComplete: false,
    created: '2020-02-26 21:17:56',
    color: '#FFFFFF',
    closedOnPublicHolidays: true,
    country: 'IE',
    state: ''
  });

export default createCentreWithPoliciesSetup;
