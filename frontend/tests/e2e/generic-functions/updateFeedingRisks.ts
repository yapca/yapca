import axios from 'axios';
import * as dotenv from 'dotenv';
import getClientByName from './getClientByName';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type { AxiosResponse } from 'axios';

dotenv.config({
  path: `../../../config/${process.env.YAPCA_ENV ?? 'local'}.env`
});
const http = axios.create({
  baseURL: `${process.env.VUE_APP_API_PROTOCOL ?? ''}://${
    process.env.VUE_APP_API_HOST ?? ''
  }:${process.env.VUE_APP_API_PORT ?? ''}${
    process.env.VUE_APP_API_PATH ?? ''
  }/api/`
});

const updateFeedingRisks = (): Promise<AxiosResponse> =>
  getClientByName('4').then((client: { id: number }[]) =>
    http
      .post('login/', {
        username: 'testAdmin',
        password: 'testPassword1'
      })
      .then((response: LoginResponse) =>
        http.post(
          `feedingRisks/${client[0].id}/`,
          [
            {
              client: client[0].id,
              feedingRisk: 'Swallowing'
            }
          ],
          {
            headers: {
              Authorization: `Token ${response.data.key}`
            }
          }
        )
      )
  );

export default updateFeedingRisks;
