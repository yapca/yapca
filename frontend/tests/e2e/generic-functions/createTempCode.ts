import { database } from '../globals';

const createTempCode = async (
  endTime: Date,
  action: string,
  code: string,
  userId: number | null,
  email: string | null = null,
  clientId: number | null = null,
  permissions: string[] | null = null
) => {
  const tempCode = (await database()
    .table('authentication_tempcode')
    .returning('id')
    .insert({
      startTime: '2020-02-26 21:17:56',
      endTime,
      action,
      code,
      associatedUser_id: userId,
      associatedClient_id: clientId,
      email
    })) as [{ id: number }];
  if (permissions) {
    for (const permissionCode of permissions) {
      const permission = (
        await database()
          .select()
          .from('authentication_userpermission')
          .where('codename', permissionCode)
          .columns(['authentication_userpermission.id'])
      )[0] as { id: number };
      await database().table('authentication_tempcode_permissions').insert({
        tempcode_id: tempCode[0].id,
        userpermission_id: permission.id
      });
    }
  }
  return tempCode;
};

export default createTempCode;
