import { Selector } from 'testcafe';

export const needOptions = Selector('.v-menu__content').find('.v-list-item');
export const noNeedsText = Selector('#noNeedsText');
export const needPlanInput = Selector('.needPlanInput');
export const centreNeedsCentreHeader = Selector('.centreNeedsCentreHeader');
export const needsList = Selector('.needsCentresList').find(
  '.q-select__dropdown-icon'
);
export const planEditButton = Selector('.planEditButton');
export const editPlanDescriptionField = Selector('.editPlanDescriptionField');
