/* eslint-disable import/prefer-default-export */
import { Selector } from 'testcafe';

export const absenceHistoryButton = Selector('#absenceHistoryButton');
export const historyTable = Selector('#absenceHistoryTable');
