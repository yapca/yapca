import { Selector } from 'testcafe';

export const infoSnackbar = Selector('.q-notification');
export const snackbarText = Selector('.q-notification__message');
export const snackbarCountBadge = Selector('.q-notification__badge');
export const snackbarButton = Selector('.q-notification').find('.q-btn');
export const registerTab = Selector('a').withAttribute('href', '#register-tab');
export const returnToServerSetupButton = Selector('#returnToServerSetupButton');
export const returnToServerSetupDialog = Selector('#returnToServerSetupDialog');
export const otpSecretKeyText = Selector('#otpSecretKeyText');
export const otpInput1 = Selector('#otpInput--0');
export const otpInput2 = Selector('#otpInput--1');
export const otpInput3 = Selector('#otpInput--2');
export const otpInput4 = Selector('#otpInput--3');
export const otpInput5 = Selector('#otpInput--4');
export const otpInput6 = Selector('#otpInput--5');
export const usernameInput = Selector('#registerUsernameInput');
export const displayNameInput = Selector('#registerDisplayNameInput');
export const passwordInput = Selector('#registerPasswordInput');
export const confirmPasswordInput = Selector('#registerConfirmPasswordInput');
export const registerButton = Selector('#registerButton');
export const emailInput = Selector('#registerEmailInput');
export const emailWarningText = Selector('#emailWarningText');
export const openChangeEmailDialogButton = Selector(
  '#openChangeEmailDialogButton'
);
export const emailChangeForm = Selector('#emailChangeForm');
export const newEmailInput = Selector('#emailInput');
export const changeEmailButton = Selector(
  '.card-confirm-button'
).filterVisible();
export const loginForm = Selector('#loginForm');
export const passwordChangeForm = Selector('#passwordChangeForm');
export const openChangePasswordDialogButton = Selector(
  '#openChangePasswordDialogButton'
);
export const openDisableTOTPDialog = Selector('#openDisableTOTPDialog');
export const otpTitleRow = Selector('#otpTitleRow');
