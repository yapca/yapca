import { Selector } from 'testcafe';

export const topBar = Selector('#topBar');
export const topBarText = Selector('#topBarText');
