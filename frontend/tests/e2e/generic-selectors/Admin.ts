import { Selector } from 'testcafe';

export const userTable = Selector('#userListTable');
export const userTableRows = userTable.find('tr');
export const userTableRow2 = userTableRows.nth(2);
export const userTableRow3 = userTableRows.nth(3);
export const userTableRow4 = userTableRows.nth(4);
export const userTableRow3Col3 = userTableRow3.child('td').nth(2);
export const userTableRow3Col4 = userTableRow3.child('td').nth(3);
export const userTableRow3Col5 = userTableRow3.child('td').nth(4);
export const editUserDialogGroupInput = Selector('#editUserDialogGroupInput');
export const editUserDialogGroupInputText =
  editUserDialogGroupInput.child('span');
export const centreSelectListItem = Selector('.addUserCentreSelectListItem');
