import { Selector } from 'testcafe';

export const newPasswordInput = Selector('#passwordInput');
export const confirmPasswordInput = Selector('#confirmPasswordInput');
export const changePasswordButton = Selector('#confirmButton');
