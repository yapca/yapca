import { Selector } from 'testcafe';

export const mySettingsLink = Selector('#sidebar-title-user-settings');
export const logoutIcon = Selector('#sidebar-icon-logout');
export const sidebar = Selector('#sidebar');
