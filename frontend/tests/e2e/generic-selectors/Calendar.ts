import { Selector } from 'testcafe';

export const headerEvents = Selector('.q-calendar-day__head--day__event');
export const tueHeaderEvents = headerEvents
  .nth(1)
  .find('.centre-visitors-banner');
