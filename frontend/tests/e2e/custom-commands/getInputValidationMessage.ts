export default (input: Selector): Promise<string> => {
  return input.parent().parent().parent().find('.q-field__messages > div')
    .textContent;
};
