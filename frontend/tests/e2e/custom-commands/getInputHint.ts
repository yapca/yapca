export default (input: Selector): Promise<string> => {
  return input
    .parent()
    .parent()
    .parent()
    .child('.q-field__bottom')
    .child('.q-field__messages').innerText;
};
