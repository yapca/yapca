export default (listItem: Selector): Promise<string> => {
  return listItem.find('.q-item__label').nth(0).textContent;
};
