export default (input: Selector): Promise<string> => {
  return input.find('.q-field__label').textContent;
};
