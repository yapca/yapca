import { ClientFunction } from 'testcafe';

export default ClientFunction(() => document.location.href);
