export default (input: Selector): Promise<string> => {
  return input.parent().child('.q-field__label').innerText;
};
