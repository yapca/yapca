export default (input: Selector): Promise<string> => {
  return input.child('.q-btn__content').textContent;
};
