import { Selector, t } from 'testcafe';

const firstDialogSelector = Selector('.q-select__dialog')
  .find('.q-item')
  .nth(0);
const firstMenuSelector = Selector('.q-menu').find('.q-item').nth(0);

const setSelector = async (
  selectorIndex: number,
  menu = true
): Promise<TestController> => {
  let firstSelectorActive = false;
  while (!firstSelectorActive) {
    await t.pressKey('pageup');
    if (menu) {
      firstSelectorActive = await firstMenuSelector.hasClass(
        'q-manual-focusable--focused'
      );
    } else {
      firstSelectorActive = await firstDialogSelector.hasClass(
        'q-manual-focusable--focused'
      );
    }
  }
  for (let i = 0; i < selectorIndex; i++) {
    await t.pressKey('down');
  }
  return t.pressKey('enter');
};

export default setSelector;
