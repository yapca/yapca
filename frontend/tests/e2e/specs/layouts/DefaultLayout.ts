import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../globals';
import {
  deleteAllClients,
  deleteClient,
  setDarkTheme,
  updateClient,
  updateClientPermissions,
  updateUser
} from '../../generic-functions';
import { admin, clientUser, nurse, url } from '../../custom-commands';

import { infoSnackbar, snackbarText } from '../../generic-selectors/Auth';
import {
  logoutIcon,
  mySettingsLink,
  sidebar
} from '../../generic-selectors/Sidebar';

const adminLink = Selector('#sidebar-title-admin');
const calendarLink = Selector('#sidebar-title-calendar');

const myMedicalInfoLink = Selector('#sidebar-title-my-medical-info');
const clientsLink = Selector('#sidebar-title-clients');
const clientsIcon = Selector('#sidebar-icon-clients');
const carePlansLink = Selector('#sidebar-title-carePlans');
const carePlansIcon = Selector('#sidebar-icon-carePlans');
const calendarIcon = Selector('#sidebar-icon-calendar');
const centresIcon = Selector('#sidebar-icon-centres');
const mySettingsIcon = Selector('#sidebar-icon-user-settings');
const mySettingsIconText = mySettingsIcon.child();
const adminIcon = Selector('#sidebar-icon-admin');
const sidebarGroup = Selector('#sidebar-group').find('.q-item');
const clientsPage = Selector('#clientsPage');
const calendarPage = Selector('#calendar');
const centresPage = Selector('#centresPage');
const mySettingsPage = Selector('#mySettingsPage');
const adminPage = Selector('#adminPage');
const carePlansPage = Selector('#carePlansPage');
const sidebarContainer = Selector('.q-drawer');

fixture`layouts/Default Layout`
  .page(baseUrl)
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    return browser.resizeWindow(1280, 720).navigateTo(baseUrl);
  })
  .after(destroyDb);

test('admin can view admin link in sidebar', async browser => {
  await admin();
  return browser
    .expect(sidebarGroup.count)
    .eql(6)
    .expect(adminLink.exists)
    .ok();
});

test('nurse can not view admin link in sidebar', async browser => {
  await nurse();
  return browser
    .expect(sidebarGroup.count)
    .eql(5)
    .expect(adminLink.exists)
    .notOk();
});

test('sidebar text is visible', async browser => {
  await admin();
  return browser
    .expect(mySettingsLink.visible)
    .ok()
    .expect(mySettingsLink.textContent)
    .eql('Test Admin')
    .expect(adminLink.visible)
    .ok()
    .expect(adminLink.textContent)
    .eql('Admin')
    .expect(clientsLink.visible)
    .ok()
    .expect(clientsLink.textContent)
    .eql('Clients')
    .expect(carePlansLink.visible)
    .ok()
    .expect(carePlansLink.textContent)
    .eql('Care Plans')
    .expect(myMedicalInfoLink.visible)
    .notOk();
});

test('sidebar icons are visible', async browser => {
  await admin();
  return browser
    .expect(mySettingsIcon.visible)
    .ok()
    .expect(adminIcon.visible)
    .ok()
    .expect(clientsIcon.visible)
    .ok()
    .expect(carePlansIcon.visible)
    .ok();
});

test('sidebar background color is correct with light theme', async browser => {
  await admin();
  return browser
    .expect(sidebarContainer.getStyleProperty('background-color'))
    .eql('rgb(255, 255, 255)');
});

test('sidebar text color is correct with light theme', async browser => {
  await admin();
  return browser
    .expect(adminLink.getStyleProperty('color'))
    .eql('rgb(0, 0, 0)');
});

test('sidebar icon color is correct with light theme', async browser => {
  await admin();
  return browser
    .expect(adminIcon.getStyleProperty('color'))
    .eql('rgb(0, 0, 0)');
});

test('sidebar background color is correct with dark theme', browser =>
  Promise.all([setDarkTheme(), admin()]).then(() =>
    browser
      .expect(sidebarContainer.getStyleProperty('background-color'))
      .eql('rgb(29, 29, 29)')
  ));

test('sidebar text color is correct with dark theme', browser =>
  Promise.all([setDarkTheme(), admin()]).then(() =>
    browser
      .expect(adminLink.getStyleProperty('color'))
      .eql('rgb(255, 255, 255)')
  ));

test('sidebar icon color is correct with dark theme', browser =>
  Promise.all([setDarkTheme(), admin()]).then(() =>
    browser
      .expect(adminIcon.getStyleProperty('color'))
      .eql('rgb(255, 255, 255)')
  ));

test('current page is highlighted in correct accent color', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/admin`)
    .expect(adminIcon.getStyleProperty('color'))
    .eql('rgb(211, 47, 47)')
    .expect(adminLink.getStyleProperty('color'))
    .eql('rgb(211, 47, 47)');
});

test('avatar icon is highlighted in correct accent color', async browser => {
  await admin();
  return browser
    .expect(mySettingsIcon.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)');
});

test('avatar icon uses first letter of username', async browser => {
  await admin();
  return browser.expect(mySettingsIconText.textContent).eql('T');
});

test('clicking the clients link opens the clients page', async browser => {
  await admin();
  return browser
    .click(clientsIcon)
    .expect(url())
    .eql(`${baseUrl}/clients`)
    .expect(clientsPage.visible)
    .ok();
});

test('clicking the care plans link opens the care plans page', async browser => {
  await admin();
  return browser
    .click(carePlansIcon)
    .expect(url())
    .eql(`${baseUrl}/carePlans`)
    .expect(carePlansPage.visible)
    .ok();
});

test('clicking the my settings link opens the my settings page', async browser => {
  await admin();
  return browser
    .click(mySettingsIcon)
    .expect(url())
    .eql(`${baseUrl}/user-settings`)
    .expect(mySettingsPage.visible)
    .ok();
});

test('clicking the admin link opens the admin page', async browser => {
  await admin();
  return browser
    .click(adminIcon)
    .expect(url())
    .eql(`${baseUrl}/admin`)
    .expect(adminPage.visible)
    .ok();
});

test('clicking the centres link opens the centres page', async browser => {
  await admin();
  return browser
    .click(centresIcon)
    .expect(url())
    .eql(`${baseUrl}/centres`)
    .expect(centresPage.visible)
    .ok();
});

test('clicking the logout link opens logs user out', async browser => {
  await admin();
  return browser.click(logoutIcon).expect(url()).eql(`${baseUrl}/auth`);
});

test('notification is shown when user group updated by admin', async browser => {
  await admin();
  await browser.expect(infoSnackbar.exists).notOk();
  await updateUser('testAdmin', { groups: [2] });
  await browser
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .eql('An admin has changed your group to nurse');
});

test('multiple notifications for group changes can be visible', async browser => {
  await admin();
  await browser.expect(infoSnackbar.exists).notOk();
  await updateUser('testAdmin', { groups: [2] });
  await updateUser('testAdmin', { groups: [1] });
  await browser.expect(infoSnackbar.count).eql(2);
});

test('admin option disappears when user group changed to nurse', async browser => {
  await admin();
  await browser.expect(sidebarGroup.count).eql(6).expect(adminLink.exists).ok();
  await updateUser('testAdmin', { groups: [2] });
  await browser
    .expect(sidebarGroup.count)
    .eql(5)
    .expect(adminLink.exists)
    .notOk();
});

test('admin option appears when user group changed to admin', async browser => {
  await nurse();
  await browser
    .expect(sidebarGroup.count)
    .eql(5)
    .expect(adminLink.exists)
    .notOk();
  await updateUser('testNurse', { groups: [1] });
  await browser.expect(sidebarGroup.count).eql(6).expect(adminLink.exists).ok();
});

test('user redirected to clients page from admin page if group changed', async browser => {
  await admin();
  await browser.click(adminIcon);
  await updateUser('testAdmin', { groups: [2] });
  await browser.expect(url()).eql(`${baseUrl}/clients`);
});

test('user not redirected on group change if not on admin page', async browser => {
  await admin();
  await browser.click(mySettingsIcon);
  await updateUser('testAdmin', { groups: [2] });
  await browser.expect(url()).eql(`${baseUrl}/user-settings`);
});

test('sidebar is not visible in xs breakpoint', async browser => {
  await admin();
  return browser
    .expect(sidebar.visible)
    .ok()
    .resizeWindow(480, 640)
    .expect(sidebar.visible)
    .notOk();
});

test('clicking the calendar link opens the calendar page', async browser => {
  await seedDb(['clients']);
  await nurse();
  return browser
    .expect(calendarLink.visible)
    .ok()
    .expect(calendarLink.textContent)
    .eql('Calendar')
    .click(calendarIcon)
    .expect(url())
    .eql(`${baseUrl}/calendar`)
    .expect(calendarPage.visible)
    .ok();
});

test('notification is shown when last client removed', async browser => {
  await seedDb(['clients']);
  await nurse();
  await browser.expect(infoSnackbar.exists).notOk();
  await deleteAllClients();
  return browser
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .eql('The only client was removed');
});

test('calendar option is removed when all clients are removed elsewhere', async browser => {
  await seedDb(['clients']);
  await nurse();
  await browser.expect(calendarLink.visible).ok();
  await deleteClient('1');
  await deleteClient('2');
  await deleteClient('3');
  await deleteClient('4');
  await deleteClient('5');
  await deleteClient('6');
  await deleteClient('7');
  await deleteClient('8');
  return browser.expect(calendarLink.visible).notOk();
});

test('calendar option appears when added from elsewhere', async browser => {
  await seedDb(['clients']);
  await deleteClient('1');
  await deleteClient('2');
  await deleteClient('4');
  await deleteClient('5');
  await deleteClient('6');
  await deleteClient('7');
  await deleteClient('8');
  await nurse();
  await browser.expect(calendarLink.visible).notOk();
  await updateClient('3', { isActive: true });
  return browser.expect(calendarLink.visible).ok();
});

test('sidebar is visible for clients', async browser => {
  await seedDb(['clients']);
  await clientUser();
  return browser
    .expect(sidebarGroup.count)
    .eql(4)
    .expect(adminLink.exists)
    .notOk()
    .expect(calendarLink.exists)
    .ok()
    .expect(mySettingsLink.visible)
    .ok()
    .expect(mySettingsLink.textContent)
    .eql('Test Client')
    .expect(adminLink.visible)
    .notOk()
    .expect(clientsLink.visible)
    .notOk()
    .expect(myMedicalInfoLink.visible)
    .ok()
    .expect(myMedicalInfoLink.textContent)
    .eql('My Medical Info');
});

test('removing calendar permission from client changes their page', async browser => {
  await seedDb(['clients']);
  await clientUser();
  await browser.navigateTo(`${baseUrl}/calendar`);
  await updateClientPermissions('1', ['client_can_view_own_data']);
  return browser
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .eql('Your access to the calendar has been restricted by an admin.')
    .expect(calendarLink.exists)
    .notOk()
    .expect(myMedicalInfoLink.visible)
    .ok()
    .expect(url())
    .eql(`${baseUrl}/my-medical-info`);
});

test('removing medical info permission from client changes their page', async browser => {
  await seedDb(['clients']);
  await clientUser();
  await browser.navigateTo(`${baseUrl}/my-medical-info`);
  await updateClientPermissions('1', ['client_can_view_calendar']);
  return browser
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .eql('Your access to your medical info has been restricted by an admin.')
    .expect(calendarLink.visible)
    .ok()
    .expect(myMedicalInfoLink.exists)
    .notOk()
    .expect(url())
    .eql(`${baseUrl}/calendar`);
});
