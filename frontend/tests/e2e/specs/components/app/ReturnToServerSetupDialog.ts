import { Selector } from 'testcafe';
import { url } from '../../../custom-commands';
import { baseUrl } from '../../../globals';
import {
  registerButton,
  returnToServerSetupButton,
  returnToServerSetupDialog
} from '../../../generic-selectors/Auth';
import { changeClientType } from '../../../generic-functions';

const returnToServerSetupDialogGoBackButton = Selector(
  '#returnToServerSetupDialogGoBackButton'
);
const returnToServerSetupDialogContinueButton = Selector(
  '#returnToServerSetupDialogContinueButton'
);

fixture`components/app/ReturnToServerSetupDialog`
  .page(baseUrl)
  .beforeEach(async browser => {
    await browser.expect(registerButton.visible).ok();
    await changeClientType('electron');
    return browser.click(returnToServerSetupButton);
  });

test('clicking go back closes dialog', browser =>
  browser
    .click(returnToServerSetupDialogGoBackButton)
    .expect(returnToServerSetupDialog.visible)
    .notOk());

test('clicking continue goes to server select page', browser =>
  browser
    .click(returnToServerSetupDialogContinueButton)
    .expect(url())
    .eql(`${baseUrl}/server-select`));
