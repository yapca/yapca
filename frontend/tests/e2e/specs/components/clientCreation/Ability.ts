import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  getItemCaption,
  getItemLabel,
  nurse,
  selectorInputLabel,
  selectorValue,
  textAreaLabel
} from '../../../custom-commands';
import { finishClientButtons } from '../../../generic-selectors/Clients';
import { clientAbilitySubmit } from '../../../generic-selectors/ClientCreation';
import {
  centreNeedsCentreHeader,
  editPlanDescriptionField,
  needsList,
  noNeedsText,
  planEditButton
} from '../../../generic-selectors/Needs';

import {
  createClientNeed,
  createClientServicesSetup,
  getCentreByName,
  getClientByName,
  getNeedByDescription,
  updateCentre,
  updateClient,
  updateClientNeed
} from '../../../generic-functions';
import {
  menuSelector,
  popupEditSaveButton,
  selectedStepHighlight,
  selectorMenu
} from '../../../generic-selectors/Layout';
import { centreCards } from '../../../generic-selectors/Centres';
import setSelector from '../../../custom-commands/setSelectorMenuValue';
import clickPlanEditButton from '../../../custom-commands/clickPlanEditButton';

const viewCentreButton = Selector('#needsViewCentreButton');
const clientFieldEdit = Selector('.clientFieldEdit');
const sightInput = Selector('#sightInput');

fixture`components/ClientCreation/Ability`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await createClientServicesSetup();
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(finishClientButtons.nth(0));
  })
  .after(destroyDb);

test('continue button should be enabled', async browser =>
  browser.expect(clientAbilitySubmit.hasAttribute('disabled')).notOk());

test('all text is correctly rendered', browser =>
  browser
    .expect(clientFieldEdit.count)
    .eql(7)
    .expect(selectorInputLabel(clientFieldEdit.nth(0)))
    .eql('Mobility')
    .expect(selectorInputLabel(clientFieldEdit.nth(1)))
    .eql('Toileting')
    .expect(selectorInputLabel(clientFieldEdit.nth(2)))
    .eql('Hygiene')
    .expect(selectorInputLabel(clientFieldEdit.nth(3)))
    .eql('Sight')
    .expect(selectorInputLabel(clientFieldEdit.nth(4)))
    .eql('Hearing')
    .expect(getItemLabel(clientFieldEdit.nth(5)))
    .eql('Dentures')
    .expect(getItemCaption(clientFieldEdit.nth(5)))
    .eql('Does Test Client wear dentures?')
    .expect(selectorInputLabel(clientFieldEdit.nth(6)))
    .eql('Case Formulation'));

test('inputting abilities saves client to database', async browser => {
  await browser
    .click(clientFieldEdit.nth(0))
    .click(menuSelector.nth(3))
    .click(clientFieldEdit.nth(1))
    .click(menuSelector.nth(1))
    .click(clientFieldEdit.nth(2))
    .click(menuSelector.nth(1))
    .click(clientFieldEdit.nth(3))
    .click(menuSelector.nth(2))
    .click(clientFieldEdit.nth(4))
    .click(menuSelector.nth(1))
    .click(clientFieldEdit.nth(5))
    .scrollIntoView(clientFieldEdit.nth(6))
    .typeText(clientFieldEdit.nth(6), 'TEST CASE FORMULATION')
    .click(clientAbilitySubmit)
    .expect(selectedStepHighlight.textContent)
    .eql('5');
  const clients = await getClientByName('12');
  return browser
    .expect(
      clients.filter(
        (client: { mobility: string }) =>
          client.mobility === 'Walks With Walker'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { toileting: string }) =>
          client.toileting === 'Occasionally Incontinent'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { hygiene: string }) => client.hygiene === 'Needs Assistance'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter((client: { sight: string }) => client.sight === 'Impaired')
        .length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { hearing: string }) => client.hearing === 'Impaired'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter((client: { usesDentures: boolean }) => client.usesDentures)
        .length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { caseFormulation: string }) =>
          client.caseFormulation === 'TEST CASE FORMULATION'
      ).length
    )
    .gte(1);
});

test('data appears if set elsewhere', async browser => {
  await updateClient('12', {
    sight: 'Glasses'
  });
  return browser.expect(selectorValue(sightInput)).eql('Glasses');
});

test('mobility selections are correct', async browser => {
  await browser
    .click(clientFieldEdit.nth(0))
    .expect(selectorMenu.count)
    .eql(5)
    .expect(selectorMenu.nth(0).textContent)
    .eql('Walks Unaided')
    .expect(selectorMenu.nth(1).textContent)
    .eql('Walks With Stick')
    .expect(selectorMenu.nth(2).textContent)
    .eql('Walks With Frame')
    .expect(selectorMenu.nth(3).textContent)
    .eql('Walks With Walker')
    .expect(selectorMenu.nth(4).textContent)
    .eql('Uses Wheelchair');
});

test('toileting selections are correct', async browser => {
  await browser
    .click(clientFieldEdit.nth(1))
    .expect(selectorMenu.count)
    .eql(3)
    .expect(selectorMenu.nth(0).textContent)
    .eql('Fully Continent')
    .expect(selectorMenu.nth(1).textContent)
    .eql('Occasionally Incontinent')
    .expect(selectorMenu.nth(2).textContent)
    .eql('Incontinent');
});

test('hygiene selections are correct', async browser => {
  await browser
    .click(clientFieldEdit.nth(2))
    .expect(selectorMenu.count)
    .eql(3)
    .expect(selectorMenu.nth(0).textContent)
    .eql('Self')
    .expect(selectorMenu.nth(1).textContent)
    .eql('Needs Assistance')
    .expect(selectorMenu.nth(2).textContent)
    .eql('Full Hoist Shower');
});

test('sight selections are correct', async browser => {
  await browser
    .click(clientFieldEdit.nth(3))
    .expect(selectorMenu.count)
    .eql(3)
    .expect(selectorMenu.nth(0).textContent)
    .eql('Normal')
    .expect(selectorMenu.nth(1).textContent)
    .eql('Glasses')
    .expect(selectorMenu.nth(2).textContent)
    .eql('Impaired');
});

test('hearing selections are correct', async browser => {
  await browser
    .click(clientFieldEdit.nth(4))
    .expect(selectorMenu.count)
    .eql(2)
    .expect(selectorMenu.nth(0).textContent)
    .eql('Normal')
    .expect(selectorMenu.nth(1).textContent)
    .eql('Impaired');
});

test('needs list shows clients centres', async browser => {
  await browser
    .expect(centreNeedsCentreHeader.count)
    .eql(1)
    .expect(centreNeedsCentreHeader.nth(0).textContent)
    .eql('Example Centre');
});

test('needs list shows lists of needs for a centre', async browser => {
  await browser
    .click(needsList)
    .expect(selectorMenu.count)
    .eql(3)
    .expect(selectorMenu.nth(0).textContent)
    .eql('Example Need 1')
    .expect(selectorMenu.nth(1).textContent)
    .eql('Example Need 3')
    .expect(selectorMenu.nth(2).textContent)
    .eql('Example Need 5');
});

test('cannot select less than one need', async browser => {
  await browser.click(needsList);
  await setSelector(0);
  await setSelector(0);
  await browser
    .expect(selectorMenu.nth(0).getAttribute('aria-selected'))
    .eql('true');
});

test('selecting a need updates database', async browser => {
  await browser.click(needsList);
  await setSelector(2);
  const clients = await getClientByName('12');
  return browser
    .expect(
      clients.filter(
        (client: { need: string }) => client.need === 'Example Need 1'
      ).length
    )
    .eql(0)
    .expect(
      clients.filter(
        (client: { need: string }) => client.need === 'Example Need 3'
      ).length
    )
    .eql(0)
    .expect(
      clients.filter(
        (client: { need: string }) => client.need === 'Example Need 5'
      ).length
    )
    .gte(1);
});

test('setting no needs does not update database', async browser => {
  await browser.click(needsList);
  await setSelector(0);
  await setSelector(0);
  const clients = await getClientByName('12');
  return browser
    .expect(
      clients.filter(
        (client: { need: string }) => client.need === 'Example Need 1'
      ).length
    )
    .gte(1);
});

test('needs list updates when updated elsewhere', async browser => {
  const [exampleNeed4] = await getNeedByDescription('Example Need 4');
  await updateCentre('Example Centre', { needs: [exampleNeed4.id] });
  return browser
    .click(needsList)
    .expect(selectorMenu.count)
    .eql(1)
    .expect(selectorMenu.nth(0).textContent)
    .eql('Example Need 4');
});

test('client needs updates when updated elsewhere', async browser => {
  const [client] = await getClientByName('12');
  const [centre] = await getCentreByName('Example Centre');
  const [exampleNeed5] = await getNeedByDescription('Example Need 5');
  await createClientNeed({
    client: client.id,
    centre: centre.id,
    need: exampleNeed5.id,
    plan: 'Example Plan 3'
  });
  return browser
    .click(needsList)
    .expect(selectorMenu.nth(0).getAttribute('aria-selected'))
    .eql('false')
    .expect(selectorMenu.nth(1).getAttribute('aria-selected'))
    .eql('false')
    .expect(selectorMenu.nth(2).getAttribute('aria-selected'))
    .eql('true');
});

test('centre shows message when no needs', async browser => {
  await updateCentre('Example Centre', { needs: [] });
  return browser
    .expect(noNeedsText.visible)
    .ok()
    .expect(noNeedsText.textContent)
    .eql(' There are no needs for this centre, edit the centre and add some ');
});

test('plan edit button shows plan text area', async browser => {
  await browser.click(needsList).expect(planEditButton.count).eql(0);
  await setSelector(1);
  await setSelector(2);
  await clickPlanEditButton(browser, 1);
  return browser
    .expect(editPlanDescriptionField.visible)
    .ok()
    .expect(textAreaLabel(editPlanDescriptionField))
    .eql("Add Test Client's plan for 'Example Need 5'");
});

test('entering plan saves it in the database', async browser => {
  await browser.click(needsList);
  await setSelector(2);
  await clickPlanEditButton(browser, 0);
  await browser
    .typeText(editPlanDescriptionField, 'Example Plan')
    .click(popupEditSaveButton)
    .pressKey('esc')
    .click(clientAbilitySubmit)
    .expect(selectedStepHighlight.textContent)
    .eql('5');
  const clients = await getClientByName('12');
  return browser
    .expect(
      clients.filter(
        (client: { needPlan: string }) => client.needPlan === 'Example Plan'
      ).length
    )
    .gte(1);
});

test('need plans are shown on load', async browser => {
  const [client] = await getClientByName('12');
  const [centre] = await getCentreByName('Example Centre');
  const [exampleNeed1] = await getNeedByDescription('Example Need 1');
  await createClientNeed({
    client: client.id,
    centre: centre.id,
    need: exampleNeed1.id,
    plan: 'Example Plan 3'
  });
  await browser.eval(() => document.location.reload());
  await browser.click(needsList).expect(planEditButton.nth(0).count).eql(1);
  await clickPlanEditButton(browser, 0);
  return browser
    .expect(editPlanDescriptionField.find('textarea').value)
    .eql('Example Plan 3');
});

test('need plans update when changed elsewhere', async browser => {
  const [client] = await getClientByName('12');
  const [centre] = await getCentreByName('Example Centre');
  const [exampleNeed1] = await getNeedByDescription('Example Need 1');
  await createClientNeed({
    client: client.id,
    centre: centre.id,
    need: exampleNeed1.id,
    plan: 'Example Plan 3'
  });
  await browser.eval(() => document.location.reload());
  await updateClientNeed('Example Plan 3', '12', { plan: 'Test Need Plan' });
  await browser.click(needsList);
  await clickPlanEditButton(browser, 0);
  return browser
    .expect(editPlanDescriptionField.find('textarea').value)
    .eql('Test Need Plan');
});

test('clicking view opens centre dialog', async browser =>
  browser.click(viewCentreButton).expect(centreCards.visible).ok());

test('view centre option is visible when no needs exist', async browser => {
  await updateCentre('Example Centre', { needs: [] });
  return browser.click(viewCentreButton).expect(centreCards.visible).ok();
});
