import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { inputLabel, nurse } from '../../../custom-commands';
import {
  addMedicationButton,
  finishClientButtons,
  medicationCreateDosageField,
  medicationCreateFrequencyField,
  medicationCreateNameField,
  medicationsTable
} from '../../../generic-selectors/Clients';
import { clientServicesSubmit } from '../../../generic-selectors/ClientCreation';

import {
  createClientAdditionalInfoSetup,
  getClientByName,
  updateClient
} from '../../../generic-functions';

const doctorNameInput = Selector('#gpNameInput');
const doctorNumberInput = Selector('#gpPhoneNumberInput');
const publicHealthNurseNameInput = Selector('#publicHealthNurseNameInput');
const publicHealthNurseNumberInput = Selector(
  '#publicHealthNursePhoneNumberInput'
);
const homeHelpNameInput = Selector('#homeHelpNameInput');
const homeHelpNumberInput = Selector('#homeHelpPhoneNumberInput');
const chemistNameInput = Selector('#chemistNameInput');
const chemistNumberInput = Selector('#chemistPhoneNumberInput');
const medicalHistoryInput = Selector('#medicalHistoryInput');
const surgicalHistoryInput = Selector('#surgicalHistoryInput');
const createMedicationSubmitButton = Selector('#createMedicationSubmitButton');

fixture`components/clientCreation/Services`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await createClientAdditionalInfoSetup();
    await browser.navigateTo(baseUrl);
    await nurse();
    return browser.resizeWindow(1280, 720).click(finishClientButtons.nth(0));
  })
  .after(destroyDb);

test('doctor field should be focused', async browser =>
  browser.pressKey('t e s t').expect(doctorNameInput.value).eql('test'));

test('continue button should be enabled', async browser =>
  browser.expect(clientServicesSubmit.hasAttribute('disabled')).notOk());

test('all text is correctly rendered', browser =>
  browser
    .expect(inputLabel(doctorNameInput))
    .eql('Doctor')
    .expect(inputLabel(doctorNumberInput))
    .eql('Doctor Phone Number')
    .expect(inputLabel(publicHealthNurseNameInput))
    .eql('Public Health Nurse')
    .expect(inputLabel(publicHealthNurseNumberInput))
    .eql('Public Health Nurse Phone Number')
    .expect(inputLabel(homeHelpNameInput))
    .eql('Home Help')
    .expect(inputLabel(homeHelpNumberInput))
    .eql('Home Help Phone Number')
    .expect(inputLabel(chemistNameInput))
    .eql('Chemist')
    .expect(inputLabel(chemistNumberInput))
    .eql('Chemist Phone Number')
    .expect(inputLabel(medicalHistoryInput))
    .eql('Medical History')
    .expect(inputLabel(surgicalHistoryInput))
    .eql('Surgical History'));

test('inputting services saves client to database', async browser => {
  await browser
    .typeText(doctorNameInput, 'Dr. House')
    .selectText(doctorNumberInput)
    .typeText(doctorNumberInput, '1234567')
    .scrollIntoView(medicalHistoryInput)
    .selectText(medicalHistoryInput)
    .typeText(medicalHistoryInput, 'Example Medical History')
    .selectText(surgicalHistoryInput)
    .typeText(surgicalHistoryInput, 'Example Surgical History')
    .click(clientServicesSubmit);
  const clients = await getClientByName('11');
  return browser
    .expect(
      clients.filter(
        (client: { gpName: string }) => client.gpName === 'Dr. House'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { gpPhoneNumber: string }) =>
          client.gpPhoneNumber === '1234567'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { servicesSetup: boolean }) => client.servicesSetup
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { medicalHistory: string }) =>
          client.medicalHistory === 'Example Medical History'
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { surgicalHistory: string }) =>
          client.surgicalHistory === 'Example Surgical History'
      ).length
    )
    .gte(1);
});

test('data appears if set elsewhere', async browser => {
  await updateClient('11', {
    homeHelpName: 'New HH Name'
  });
  return browser.expect(homeHelpNameInput.value).eql('New HH Name');
});

test('medications table is visible', browser =>
  browser
    .click(addMedicationButton)
    .typeText(medicationCreateNameField, 'Example Name')
    .typeText(medicationCreateDosageField, '1234567')
    .typeText(medicationCreateFrequencyField, '7654321')
    .click(createMedicationSubmitButton)
    .expect(medicationsTable.visible)
    .ok());
