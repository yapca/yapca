import { Selector } from 'testcafe';
import { confirmButton, goBackButton } from '../../../generic-selectors/Layout';
import { cardTitle, closeDialogIcon } from '../../../generic-selectors/Centres';
import { infoSnackbar, snackbarText } from '../../../generic-selectors/Auth';
import {
  centreSelectListItem,
  editUserDialogGroupInput,
  userTableRow3,
  userTableRow4,
  userTableRows
} from '../../../generic-selectors/Admin';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { admin } from '../../../custom-commands';
import { getUserByName, updateUser } from '../../../generic-functions';
import { clientDialogHeader } from '../../../generic-selectors/Clients';
const options = Selector('.q-item--clickable')
  .find('span')
  .withText('admin')
  .parent('.q-virtual-scroll__content')
  .child();

const groupSelectAdminOption = options.nth(0);
const loginForm = Selector('#loginForm');
const deleteUserButton = Selector('#deleteUserButton');
const deleteUserConfirmDialog = Selector('#deleteUserDialog');
const deleteUserHeader = deleteUserConfirmDialog.find('.cardTitle');
const deleteUserDialogText = Selector('#deleteUserDialogText');
const goToClientPageButton = Selector('#goToClientPageButton');

fixture`components/Admin/EditUserDialog`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(`${baseUrl}/admin`).resizeWindow(1280, 720);
    await admin();
    await browser.click(userTableRow4);
  })
  .after(destroyDb);

test('editing a users group updates in the database', async browser => {
  await browser.click(editUserDialogGroupInput).click(groupSelectAdminOption);
  const users = await getUserByName('testNurse');
  return browser
    .expect(
      users.filter((user: { groupName: string }) => user.groupName === 'admin')
        .length
    )
    .gte(1);
});

test('dialog is shown when clicking delete user', browser =>
  browser
    .click(deleteUserButton)
    .expect(deleteUserHeader.textContent)
    .eql('Delete Test Nurse')
    .expect(deleteUserDialogText.textContent)
    .eql(
      ' Deleting Test Nurse will make their account PERMANENTLY unrecoverable '
    )
    .expect(confirmButton.textContent)
    .eql('Delete Test Nurse'));

test('delete user go back button does not delete user', async browser => {
  await browser
    .click(deleteUserButton)
    .click(goBackButton)
    .expect(deleteUserConfirmDialog.visible)
    .notOk();
  return getUserByName('testNurse').then(user =>
    browser.expect(user[0].isActive).eql(true)
  );
});

test('delete user confirm button does delete user', async browser => {
  await browser
    .click(deleteUserButton)
    .click(confirmButton)
    .expect(deleteUserConfirmDialog.visible)
    .notOk();
  return getUserByName('testNurse').then(user =>
    browser.expect(user[0].isActive).eql(false)
  );
});

test('notification is shown on user deletion', browser =>
  browser
    .click(deleteUserButton)
    .click(confirmButton)
    .expect(infoSnackbar.visible)
    .ok()
    .expect(snackbarText.textContent)
    .contains('Test Nurse has been deleted'));

test('deleting user removes them from user list', browser =>
  browser
    .click(deleteUserButton)
    .click(confirmButton)
    .expect(userTableRows.count)
    .eql(5));

test('deleting a user logs them out', async browser => {
  await updateUser('testAdmin', { isActive: false });
  await browser
    .expect(loginForm.visible)
    .ok()
    .expect(infoSnackbar.visible)
    .ok()
    .expect(snackbarText.textContent)
    .contains('An admin has disabled your account');
});

test('clicking close on user dialog closes dialog', browser =>
  browser.click(closeDialogIcon).expect(cardTitle.visible).ok());

test('centres are not visible for client user', browser =>
  browser
    .click(closeDialogIcon)
    .click(userTableRow3)
    .expect(centreSelectListItem.visible)
    .notOk()
    .click(goToClientPageButton)
    .expect(clientDialogHeader.visible)
    .ok());
