import { Selector } from 'testcafe';
import { userTableRow3 } from '../../../generic-selectors/Admin';
import { admin } from '../../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { snackbarText } from '../../../generic-selectors/Auth';

import { confirmButton } from '../../../generic-selectors/Layout';

const resetOtpButton = Selector('#resetOtpButton');
const adminRequestResetTotpModal = Selector('#adminRequestResetTotpModal');
const adminTotpWarning = Selector('#adminTotpWarning');

fixture`components/admin/ResetOtpAdminModal`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await browser.navigateTo(`${baseUrl}/admin`).resizeWindow(1280, 720);
    await admin();
    return browser.click(userTableRow3).click(resetOtpButton);
  })
  .after(destroyDb);

test('admin warning text should be visible', browser =>
  browser.expect(adminTotpWarning.textContent).contains('WARNING'));

test('error is shown when temp-code already exists', browser =>
  browser
    .click(confirmButton)
    .expect(adminRequestResetTotpModal.visible)
    .notOk()
    .expect(snackbarText.nth(0).textContent)
    .contains('Email successfully sent!')
    .click(resetOtpButton)
    .click(confirmButton)
    .expect(snackbarText.textContent)
    .contains('User was already sent email in last 24 hours')
    .expect(adminRequestResetTotpModal.visible)
    .notOk());
