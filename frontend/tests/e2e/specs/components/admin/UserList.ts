import {
  infoSnackbar,
  snackbarButton,
  snackbarText
} from '../../../generic-selectors/Auth';
import {
  editUserDialogGroupInput,
  editUserDialogGroupInputText,
  userTable,
  userTableRow2,
  userTableRow3,
  userTableRow3Col3,
  userTableRow3Col4,
  userTableRow3Col5,
  userTableRow4,
  userTableRows
} from '../../../generic-selectors/Admin';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { admin, url } from '../../../custom-commands';
import { createUser, updateUser } from '../../../generic-functions';
import { cardTitle } from '../../../generic-selectors/Centres';

const headers = userTable.find('th');
const userTableHeader1 = headers.nth(0);
const userTableHeader2 = headers.nth(1);
const userTableHeader3 = headers.nth(2);
const userTableHeader4 = headers.nth(3);
const userTableHeader5 = headers.nth(4);
const userTableRow1 = userTableRows.nth(1);
const userTableRow1Col1 = userTableRow1.child('td').nth(0);
const userTableRow1Col5 = userTableRow1.child('td').nth(4);
const userTableRow2Col1 = userTableRow2.child('td').nth(0);
const userTableRow2Col5 = userTableRow2.child('td').nth(4);
const userTableRow3Col1 = userTableRow3.child('td').nth(0);
const userTableRow3Col2 = userTableRow3.child('td').nth(1);
const userTableRow4Col1 = userTableRow4.child('td').nth(0);
const userTableRow4Col2 = userTableRow4.child('td').nth(1);
const userTableRow4Col3 = userTableRow4.child('td').nth(2);
const userTableRow4Col4 = userTableRow4.child('td').nth(3);
const userTableRow4Col5 = userTableRow4.child('td').nth(4);

fixture`components/Admin/UserList`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(`${baseUrl}/admin`).resizeWindow(1280, 720);
    await admin();
  })
  .after(destroyDb);

test('user table headers are visible', browser =>
  browser
    .expect(userTableHeader1.textContent)
    .eql('Name')
    .expect(userTableHeader2.textContent)
    .eql('Username')
    .expect(userTableHeader3.textContent)
    .eql('Email')
    .expect(userTableHeader4.textContent)
    .eql('Group')
    .expect(userTableHeader5.textContent)
    .eql('Subscribed Centres'));

test('user table data is visible', browser =>
  browser
    .expect(userTableRows.count)
    .eql(6)
    .expect(userTableRow3Col1.textContent)
    .eql('Test Client 1')
    .expect(userTableRow4Col1.textContent)
    .eql('Test Nurse')
    .expect(userTableRow3Col2.textContent)
    .eql('clientUser')
    .expect(userTableRow4Col2.textContent)
    .eql('testNurse')
    .expect(userTableRow3Col3.textContent)
    .eql('testclientuser@test.com')
    .expect(userTableRow3Col4.textContent)
    .eql('client')
    .expect(userTableRow4Col3.textContent)
    .eql('testemail@test.com')
    .expect(userTableRow4Col4.textContent)
    .eql('nurse')
    .expect(userTableRow3Col5.textContent)
    .eql('1')
    .expect(userTableRow4Col5.textContent)
    .eql('3'));

test('clicking row for user opens edit dialog', browser =>
  browser
    .click(userTableRow4)
    .expect(cardTitle.withText('Edit Test Nurse').visible)
    .ok()
    .expect(editUserDialogGroupInput.visible)
    .ok()
    .expect(editUserDialogGroupInputText.textContent)
    .eql('nurse'));

test('user list updates when a new user is created', async browser => {
  await browser.expect(userTableRows.count).eql(6);
  await createUser();
  await browser
    .expect(userTableRows.count)
    .eql(7)
    .expect(userTableRow2Col1.textContent)
    .eql('New Test User')
    .expect(userTableRow2Col5.textContent)
    .eql('0');
});

test('user list updates when a users group is updated', async browser => {
  await browser.expect(userTableRow4Col4.textContent).eql('nurse');
  await updateUser('testNurse', { groups: [1] });
  await browser.expect(userTableRow4Col4.textContent).eql('admin');
});

test('user list updates when user deleted elsewhere', async browser => {
  await updateUser('testNurse', { isActive: false });
  await browser.expect(userTableRows.count).eql(5);
});

test('email updates when changed elsewhere', async browser => {
  await updateUser('testNurse', { email: 'newemail@test.com' });
  await browser.expect(userTableRow4Col3.textContent).eql('newemail@test.com');
});

test('table can be sorted by subscribed centres', browser =>
  browser
    .click(userTableHeader5)
    .expect(userTableRow1Col1.textContent)
    .eql('Test OTP User')
    .expect(userTableRow1Col5.textContent)
    .eql('0')
    .click(userTableHeader5)
    .expect(userTableRow1Col1.textContent)
    .eql('Every Centre User')
    .expect(userTableRow1Col5.textContent)
    .eql('4'));

test('clicking row of own user shows notification', browser =>
  browser
    .click(userTableRow2)
    .expect(infoSnackbar.visible)
    .ok()
    .expect(snackbarText.textContent)
    .eql('Cannot alter your own account here. Please go to the user page.')
    .expect(snackbarButton.visible)
    .ok()
    .expect(snackbarButton.textContent)
    .eql('User Page')
    .click(snackbarButton)
    .expect(url())
    .eql(`${baseUrl}/user-settings`));
