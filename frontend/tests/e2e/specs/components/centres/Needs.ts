import { Selector } from 'testcafe';
import { confirmButton } from '../../../generic-selectors/Layout';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  getCentreByName,
  updateCentre,
  updateNeed
} from '../../../generic-functions';
import {
  centresTableRow1,
  centresTableRow3,
  needsExpander
} from '../../../generic-selectors/Centres';
import { inputValidationMessage, nurse } from '../../../custom-commands';

const centreNeedsTable = Selector('#needsTable');
const centreNeedsRows = centreNeedsTable.find('tbody').find('tr');
const addNeedButton = Selector('#addNeedButton');
const noNeedMessage = centreNeedsTable.find('.q-table__bottom--nodata');
const deleteButtons = Selector('.deleteNeedButton');
const needEditNameField = Selector('#needEditNameField');
const needCreateDescriptionField = Selector('#needCreateDescriptionField');
const needsSearch = Selector('#needsSearch');
const needsPopupEdit = Selector('#needPopupEdit');
const editPopupButtons = needsPopupEdit.find('.q-btn');
const saveButton = editPopupButtons.nth(1);
const headers = centreNeedsTable.find('.responsiveTableHeader');

const checkNeedDescription = (
  centreName: string,
  needDescription: string,
  browser: TestController
) =>
  getCentreByName(centreName).then(centres =>
    browser
      .expect(
        centres.filter(
          (centre: { needDescription: string }) =>
            centre.needDescription === needDescription
        ).length
      )
      .gt(1)
  );

fixture`components/centres/CentreItemTable Needs`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await browser.navigateTo(`${baseUrl}/centres`);
  })
  .after(destroyDb);

test('list of needs is correct', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(needsExpander)
    .expect(centreNeedsRows.count)
    .eql(3)
    .expect(centreNeedsRows.nth(0).find('td').nth(0).textContent)
    .eql('Example Need 1')
    .expect(centreNeedsRows.nth(1).find('td').nth(0).textContent)
    .eql('Example Need 3')
    .expect(centreNeedsRows.nth(2).find('td').nth(0).textContent)
    .eql('Example Need 5');
});

test('needs update in database on change', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(needsExpander)
    .click(centreNeedsRows.nth(0).find('td').nth(0))
    .selectText(needEditNameField)
    .pressKey('delete')
    .typeText(needEditNameField, 'New Need')
    .click(saveButton)
    .then(() => checkNeedDescription('Example Centre', 'New Need', browser));
});

test('needs update when updated elsewhere', async browser => {
  await nurse();
  await browser.click(centresTableRow1).click(needsExpander);
  const inputValue = (
    await centreNeedsRows.nth(0).find('td').nth(0).textContent
  ).trim();
  await updateNeed(inputValue, 'New Need Name');
  return browser
    .expect(centreNeedsRows.nth(2).find('td').nth(0).textContent)
    .eql('New Need Name');
});

test('added need is saved in database', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(needsExpander)
    .click(addNeedButton)
    .typeText(needCreateDescriptionField, 'New Need')
    .click(confirmButton)
    .expect(centreNeedsRows.count)
    .eql(4)
    .then(() => checkNeedDescription('Example Centre', 'New Need', browser));
});

test('message is displayed when no needs exist', async browser => {
  await nurse();
  await browser.click(centresTableRow3).click(needsExpander);
  await updateCentre('Example Centre 4', { needs: [] });
  return browser
    .expect(noNeedMessage.textContent)
    .eql('No needs found in centre');
});

test('delete need button removes need from list', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(needsExpander)
    .click(deleteButtons.nth(0))
    .expect(centreNeedsRows.count)
    .eql(2);
});

test('delete need button removes need from database', async browser => {
  await nurse();
  await browser.click(centresTableRow1).click(needsExpander);
  const inputValue = await centreNeedsRows.nth(0).value;
  await browser
    .click(deleteButtons.nth(0))
    .then(() =>
      getCentreByName('Example Centre').then(centres =>
        browser
          .expect(
            centres.filter(
              (centre: { needDescription: string }) =>
                centre.needDescription === inputValue
            ).length
          )
          .eql(0)
      )
    );
});

test('error is shown when need name is not unique', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(needsExpander)
    .click(addNeedButton)
    .typeText(needCreateDescriptionField, 'Example Need 3')
    .expect(inputValidationMessage(needCreateDescriptionField))
    .eql('Need description must be unique');
});

test('need does not update in database when not unique', async browser => {
  await nurse();
  const existingCentreNeeds = await getCentreByName('Example Centre').then(
    centres =>
      centres.filter(
        (centre: { needDescription: string }) =>
          centre.needDescription === 'Example Need 3'
      ).length
  );
  await browser
    .click(centresTableRow1)
    .click(needsExpander)
    .click(addNeedButton)
    .typeText(needCreateDescriptionField, 'Example Need 3')
    .click(confirmButton)
    .then(() =>
      getCentreByName('Example Centre').then(centres =>
        browser
          .expect(
            centres.filter(
              (centre: { needDescription: string }) =>
                centre.needDescription === 'Example Need 3'
            ).length
          )
          .eql(existingCentreNeeds)
      )
    );
});

test('needs table shows correct headers', async browser => {
  await nurse();
  await browser
    .click(centresTableRow1)
    .click(needsExpander)
    .expect(headers.count)
    .eql(2)
    .expect(headers.nth(0).textContent)
    .eql('Description')
    .expect(headers.nth(1).textContent)
    .eql('Actions');
});

test("table can be sorted by 'description'", async browser => {
  await nurse();
  await browser
    .click(centresTableRow1)
    .click(needsExpander)
    .click(headers.nth(0))
    .expect(centreNeedsRows.nth(0).find('td').nth(0).textContent)
    .eql('Example Need 5')
    .expect(centreNeedsRows.nth(2).find('td').nth(0).textContent)
    .eql('Example Need 1')
    .click(headers.nth(0))
    .expect(centreNeedsRows.nth(0).find('td').nth(0).textContent)
    .eql('Example Need 1')
    .expect(centreNeedsRows.nth(2).find('td').nth(0).textContent)
    .eql('Example Need 5');
});

test('search bar filters table', async browser => {
  await nurse();
  await browser
    .click(centresTableRow1)
    .click(needsExpander)
    .typeText(needsSearch, '3')
    .expect(centreNeedsRows.count)
    .eql(1)
    .expect(centreNeedsRows.nth(0).find('td').nth(0).textContent)
    .eql('Example Need 3');
});

test('message is shown when no needs found in search', async browser => {
  await nurse();
  await browser
    .click(centresTableRow1)
    .click(needsExpander)
    .typeText(needsSearch, '999')
    .expect(noNeedMessage.textContent)
    .eql('No matching needs found in centre');
});

test('dialog does not closes on pressing save if not unique', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(needsExpander)
    .click(addNeedButton)
    .typeText(needCreateDescriptionField, 'Example Need 3')
    .click(confirmButton)
    .expect(needCreateDescriptionField.visible)
    .ok();
});
