import { Selector } from 'testcafe';
import { confirmButton } from '../../../generic-selectors/Layout';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  getCentreByName,
  updateCentre,
  updatePolicy
} from '../../../generic-functions';
import {
  centresTableRow1,
  centresTableRow3,
  policiesExpander
} from '../../../generic-selectors/Centres';
import { inputValidationMessage, nurse } from '../../../custom-commands';

const centrePoliciesTable = Selector('#policiesTable');
const centrePoliciesRows = centrePoliciesTable.find('tbody').find('tr');
const addPolicyButton = Selector('#addPolicyButton');
const noPolicyMessage = centrePoliciesTable.find('.q-table__bottom--nodata');
const deleteButtons = Selector('.deletePolicyButton');
const policyEditNameField = Selector('#policyEditNameField');
const policyCreateDescriptionField = Selector('#policyCreateDescriptionField');
const policiesSearch = Selector('#policiesSearch');
const policiesPopupEdit = Selector('#policyPopupEdit');
const editPopupButtons = policiesPopupEdit.find('.q-btn');
const saveButton = editPopupButtons.nth(1);
const headers = centrePoliciesTable.find('.responsiveTableHeader');

const checkPolicyDescription = (
  centreName: string,
  policyDescription: string,
  browser: TestController
) =>
  getCentreByName(centreName).then(centres =>
    browser
      .expect(
        centres.filter(
          (centre: { policyDescription: string }) =>
            centre.policyDescription === policyDescription
        ).length
      )
      .gt(1)
  );

fixture`components/centres/CentreItemTable Policies`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await browser.navigateTo(`${baseUrl}/centres`);
  })
  .after(destroyDb);

test('list of policies is correct', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .expect(centrePoliciesRows.count)
    .eql(3)
    .expect(centrePoliciesRows.nth(0).find('td').nth(0).textContent)
    .eql('Example Policy 1')
    .expect(centrePoliciesRows.nth(1).find('td').nth(0).textContent)
    .eql('Example Policy 3')
    .expect(centrePoliciesRows.nth(2).find('td').nth(0).textContent)
    .eql('Example Policy 5');
});

test('policies update in database on change', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .click(centrePoliciesRows.nth(0).find('td').nth(0))
    .selectText(policyEditNameField)
    .pressKey('delete')
    .typeText(policyEditNameField, 'New Policy')
    .click(saveButton)
    .then(() =>
      checkPolicyDescription('Example Centre', 'New Policy', browser)
    );
});

test('policies update when updated elsewhere', async browser => {
  await nurse();
  await browser.click(centresTableRow1).click(policiesExpander);
  const inputValue = (
    await centrePoliciesRows.nth(0).find('td').nth(0).textContent
  ).trim();
  await updatePolicy(inputValue, 'New Policy Name');
  return browser
    .expect(centrePoliciesRows.nth(2).find('td').nth(0).textContent)
    .eql('New Policy Name');
});

test('added policy is saved in database', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .click(addPolicyButton)
    .typeText(policyCreateDescriptionField, 'New Policy')
    .click(confirmButton)
    .expect(confirmButton.exists)
    .notOk()
    .then(() =>
      checkPolicyDescription('Example Centre', 'New Policy', browser)
    );
});

test('field is added on button click', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .click(addPolicyButton)
    .typeText(policyCreateDescriptionField, 'New Policy')
    .click(confirmButton)
    .expect(centrePoliciesRows.count)
    .eql(4);
});

test('message is displayed when no policies exist', async browser => {
  await nurse();
  await browser.click(centresTableRow3).click(policiesExpander);
  await updateCentre('Example Centre 4', { policies: [] });
  return browser
    .expect(noPolicyMessage.textContent)
    .eql('No policies found in centre');
});

test('delete policy button removes policy from list', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .click(deleteButtons.nth(0))
    .expect(centrePoliciesRows.count)
    .eql(2);
});

test('delete policy button removes policy from database', async browser => {
  await nurse();
  await browser.click(centresTableRow1).click(policiesExpander);
  const inputValue = await centrePoliciesRows.nth(0).value;
  await browser
    .click(deleteButtons.nth(0))
    .then(() =>
      getCentreByName('Example Centre').then(centres =>
        browser
          .expect(
            centres.filter(
              (centre: { policyDescription: string }) =>
                centre.policyDescription === inputValue
            ).length
          )
          .eql(0)
      )
    );
});

test('error is shown when policy name is not unique', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .click(addPolicyButton)
    .typeText(policyCreateDescriptionField, 'Example Policy 3')
    .expect(inputValidationMessage(policyCreateDescriptionField))
    .eql('Policy description must be unique');
});

test('policy does not update in database when not unique', async browser => {
  await nurse();
  const existingCentrePolicies = await getCentreByName('Example Centre').then(
    centres =>
      centres.filter(
        (centre: { policyDescription: string }) =>
          centre.policyDescription === 'Example Policy 3'
      ).length
  );
  await browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .click(addPolicyButton)
    .typeText(policyCreateDescriptionField, 'Example Policy 3')
    .click(confirmButton)
    .then(() =>
      getCentreByName('Example Centre').then(centres =>
        browser
          .expect(
            centres.filter(
              (centre: { policyDescription: string }) =>
                centre.policyDescription === 'Example Policy 3'
            ).length
          )
          .eql(existingCentrePolicies)
      )
    );
});

test('table shows correct headers', async browser => {
  await nurse();
  await browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .expect(headers.count)
    .eql(2)
    .expect(headers.nth(0).textContent)
    .eql('Description')
    .expect(headers.nth(1).textContent)
    .eql('Actions');
});

test("table can be sorted by 'description'", async browser => {
  await nurse();
  await browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .click(headers.nth(0))
    .expect(centrePoliciesRows.nth(0).find('td').nth(0).textContent)
    .eql('Example Policy 5')
    .expect(centrePoliciesRows.nth(2).find('td').nth(0).textContent)
    .eql('Example Policy 1')
    .click(headers.nth(0))
    .expect(centrePoliciesRows.nth(0).find('td').nth(0).textContent)
    .eql('Example Policy 1')
    .expect(centrePoliciesRows.nth(2).find('td').nth(0).textContent)
    .eql('Example Policy 5');
});

test('search bar filters table', async browser => {
  await nurse();
  await browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .typeText(policiesSearch, '3')
    .expect(centrePoliciesRows.count)
    .eql(1)
    .expect(centrePoliciesRows.nth(0).find('td').nth(0).textContent)
    .eql('Example Policy 3');
});

test('message is shown when no policies found in search', async browser => {
  await nurse();
  await browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .typeText(policiesSearch, '999')
    .expect(noPolicyMessage.textContent)
    .eql('No matching policies found in centre');
});

test('dialog does not closes on pressing save if not unique', async browser => {
  await nurse();
  return browser
    .click(centresTableRow1)
    .click(policiesExpander)
    .click(addPolicyButton)
    .typeText(policyCreateDescriptionField, 'Example Policy 3')
    .click(confirmButton)
    .expect(policyCreateDescriptionField.visible)
    .ok();
});
