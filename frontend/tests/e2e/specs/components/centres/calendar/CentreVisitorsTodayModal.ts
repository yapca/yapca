import { Selector } from 'testcafe';
import { add, format, isPast, startOfWeek } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../../../globals';
import {
  deleteClient,
  explicitErrorHandler,
  getCentreByName,
  goBack,
  goForward,
  updateClient
} from '../../../../generic-functions';
import { nurse } from '../../../../custom-commands';

import { tueHeaderEvents } from '../../../../generic-selectors/Calendar';
import { clientDialogHeader } from '../../../../generic-selectors/Clients';

const clientVisitingToday = Selector('.clientVisitingToday');
const clientsVisitingDialog = Selector('#showCentreVisitorsTodayModal');
const cardHeader = clientsVisitingDialog.find('.cardTitle');
const clientVisitingDetailText = Selector('#clientVisitingDetailText');
const tuesday = add(startOfWeek(new Date(), { weekStartsOn: 1 }), {
  days: 1
});

fixture`components/Centres/calendar/CentreVisitorsTodayModal`
  .clientScripts({ content: `(${explicitErrorHandler.toString()})()` })
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(`${baseUrl}/calendar`).resizeWindow(1280, 720);
    await nurse();
    return browser.click(tueHeaderEvents.nth(0));
  })
  .after(destroyDb);

test('shows list of clients', async browser => {
  return browser
    .expect(clientVisitingToday.count)
    .eql(2)
    .expect(clientVisitingToday.nth(0).textContent)
    .eql('Test Client 1')
    .expect(clientVisitingToday.nth(1).textContent)
    .eql('Test Client 4');
});

test('client list updates if changed elsewhere', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('1', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  return browser
    .expect(clientVisitingToday.count)
    .eql(1)
    .expect(clientVisitingToday.nth(0).textContent)
    .eql('Test Client 4');
});

test('clicking client opens dialog', async browser => {
  return browser
    .expect(clientDialogHeader.visible)
    .notOk()
    .click(clientVisitingToday.nth(0))
    .expect(clientDialogHeader.visible)
    .ok();
});

test('dialog text is correct', async browser => {
  const pastOrPresent = isPast(tuesday) ? 'were' : 'are';
  return browser
    .expect(cardHeader.textContent)
    .eql(`Example Centre | ${format(tuesday, 'MMM do')}`)
    .expect(clientVisitingDetailText.textContent)
    .eql(
      `2 clients ${pastOrPresent} scheduled to visit Example Centre on ${format(
        tuesday,
        'MMMM do'
      )}`
    );
});

test('dialog text changes if client list changes', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('1', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  const pastOrPresent = isPast(tuesday) ? 'was' : 'is';

  return browser
    .expect(clientVisitingDetailText.textContent)
    .eql(
      `1 client ${pastOrPresent} scheduled to visit Example Centre on ${format(
        tuesday,
        'MMMM do'
      )}`
    );
});

test('deleting client closes client dialog', async browser => {
  await browser.click(clientVisitingToday.nth(0));
  await deleteClient('1');
  await browser.expect(clientDialogHeader.visible).notOk();
});

test('pressing back while in client dialog closes dialog', async browser => {
  await browser.click(clientVisitingToday.nth(0));
  await goBack();
  await browser.expect(clientDialogHeader.visible).notOk();
  await goForward();
  return browser.expect(clientDialogHeader.visible).ok();
});
