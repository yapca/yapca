import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../../globals';
import {
  getCentreByName,
  getClientByName,
  updateClient
} from '../../../../generic-functions';
import {
  centreExpanderOpeningDays,
  centresTableRow1
} from '../../../../generic-selectors/Centres';
import { admin } from '../../../../custom-commands';
import { confirmButton } from '../../../../generic-selectors/Layout';

const daySelectorOne = Selector('.daySelector').nth(0);
const daySelectorOneWed = daySelectorOne.find('button').nth(2);
const manualHelpText = Selector('#manualHelpText');
const autoHelpText = Selector('#autoHelpText');
const autoClientList = Selector('.autoClientList');
const manualClientList = Selector('.manualClientList');
const clientDialog = Selector('#centreClientDayWarningDialog');
const centreClientDayWarningDialog = Selector('#CentreClientDayWarningDialog');

const checkDayIdForClient = async (
  clientId: string,
  dayId: number,
  browser: TestController
) => {
  await getClientByName(clientId).then(clients =>
    browser
      .expect(
        clients.filter((client: { dayId: number }) => client.dayId === dayId)
          .length
      )
      .eql(0)
  );
};
fixture`components/Centres/Calendar/CentreClientDayWarningDialog`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(`${baseUrl}/centres`).resizeWindow(1280, 720);
    await admin();
    return browser
      .click(centresTableRow1)
      .click(centreExpanderOpeningDays)
      .click(daySelectorOneWed);
  })
  .after(destroyDb);

test('dialog shows help text when clients exists', browser =>
  browser
    .expect(autoHelpText.textContent)
    .eql(
      ' The clients below will automatically have Wedednesday removed from their ' +
        'visiting days. Select their names if you wish to manually change ' +
        'their days anyway. '
    )
    .expect(manualHelpText.textContent)
    .eql(
      ' The clients below cannot be automatically updated because their only ' +
        'visiting day would be removed. Select them and change them manually ' +
        'before you can continue. '
    ));

test('dialog shows list of clients with visiting days in selected day', browser =>
  browser
    .expect(autoClientList.count)
    .eql(1)
    .expect(manualClientList.count)
    .eql(3)
    .expect(autoClientList.nth(0).textContent)
    .eql('Test Client 1')
    .expect(manualClientList.nth(0).textContent)
    .eql('Test Client 6'));

test('clicking a client opens the client dialog', browser =>
  browser.click(autoClientList.nth(0)).expect(clientDialog.visible).ok());

test('accept button is disabled when manual clients exists', browser =>
  browser.expect(confirmButton.hasAttribute('disabled')).ok());

test('accept button is enabled when manual clients are removed', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('6', {
    visitingDays: [
      { centre: centre1.id, day: 3 },
      { centre: centre1.id, day: 5 }
    ]
  });
  await updateClient('7', {
    visitingDays: [
      { centre: centre1.id, day: 3 },
      { centre: centre1.id, day: 5 }
    ]
  });
  await updateClient('8', {
    visitingDays: [
      { centre: centre1.id, day: 3 },
      { centre: centre1.id, day: 5 }
    ]
  });
  return browser.expect(confirmButton.hasAttribute('disabled')).notOk();
});

test('clients are removed from manual list when visiting days added', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('6', {
    visitingDays: [
      { centre: centre1.id, day: 3 },
      { centre: centre1.id, day: 5 }
    ]
  });
  await updateClient('7', {
    visitingDays: [
      { centre: centre1.id, day: 3 },
      { centre: centre1.id, day: 5 }
    ]
  });
  await updateClient('8', {
    visitingDays: [
      { centre: centre1.id, day: 3 },
      { centre: centre1.id, day: 5 }
    ]
  });
  return browser
    .expect(autoClientList.count)
    .eql(4)
    .expect(manualClientList.count)
    .eql(0);
});

test('clients are removed from auto list when visiting days changed', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('1', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  return browser
    .expect(autoClientList.count)
    .eql(0)
    .expect(manualClientList.count)
    .eql(3);
});

test('manual help text vanishes when when visiting days added', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('6', {
    visitingDays: [
      { centre: centre1.id, day: 3 },
      { centre: centre1.id, day: 5 }
    ]
  });
  await updateClient('7', {
    visitingDays: [
      { centre: centre1.id, day: 3 },
      { centre: centre1.id, day: 5 }
    ]
  });
  await updateClient('8', {
    visitingDays: [
      { centre: centre1.id, day: 3 },
      { centre: centre1.id, day: 5 }
    ]
  });
  return browser.expect(manualHelpText.visible).notOk();
});

test('auto help text vanishes when visiting days changed', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('1', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  return browser.expect(autoHelpText.visible).notOk();
});

test('dialog closes when no auto or manual clients', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('6', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  await updateClient('7', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  await updateClient('8', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  await updateClient('1', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  return browser.expect(centreClientDayWarningDialog.visible).notOk();
});

test('pressing accept closes dialog', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('6', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  await updateClient('7', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  await updateClient('8', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  return browser
    .click(confirmButton)
    .expect(centreClientDayWarningDialog.visible)
    .notOk();
});

test('pressing accept updates day selector', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('6', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  await updateClient('7', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  await updateClient('8', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  return browser
    .click(confirmButton)
    .expect(daySelectorOneWed.hasClass('v-btn--active'))
    .notOk();
});

test('pressing accept updates database', async browser => {
  const [centre1] = await getCentreByName('Example Centre');
  await updateClient('6', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  await updateClient('7', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  await updateClient('8', {
    visitingDays: [{ centre: centre1.id, day: 5 }]
  });
  await browser.click(confirmButton);
  await checkDayIdForClient('2', 3, browser);
  await checkDayIdForClient('6', 3, browser);
});
