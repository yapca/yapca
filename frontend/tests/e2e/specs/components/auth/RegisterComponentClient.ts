import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  confirmPasswordInput,
  emailInput,
  otpInput1,
  passwordInput,
  registerButton,
  usernameInput
} from '../../../generic-selectors/Auth';
import {
  buttonLabel,
  inputLabel,
  inputValidationMessage,
  url
} from '../../../custom-commands';
import {
  createRegistrationTempCode,
  getClientByName,
  getTempCodesByAction,
  getUserByName,
  setupTOTP
} from '../../../generic-functions';

const enableTOTPToggle = Selector('#enableTOTPToggle');
const registrationTempCode = 'ci6TbI7VDKXA1wIIRAtyyPUc9JT7lrq5';

fixture`components/auth/Register (Client User)`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    const client3Id = (await getClientByName('3'))[0].id;
    await createRegistrationTempCode(registrationTempCode, client3Id, [
      'client_can_view_own_data'
    ]);
    return browser
      .navigateTo(
        `${baseUrl}/temp-code?action=client-registration&email=newuseremail@test.com&code=${registrationTempCode}`
      )
      .expect(usernameInput.visible)
      .ok({ timeout: 10000 })
      .resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('registering as client user should go to my medical info', async browser => {
  await browser
    .typeText(usernameInput, 'clientUser1')
    .typeText(passwordInput, 'testPassword1')
    .typeText(confirmPasswordInput, 'testPassword1')
    .click(registerButton)
    .expect(url())
    .eql(`${baseUrl}/my-medical-info`);
  const user = (await getUserByName('clientUser1'))[0];
  return browser
    .expect(user.username)
    .eql('clientUser1')
    .expect(user.email)
    .eql('newuseremail@test.com');
});

test('validation error should occur when attempting to register as existing user', async browser => {
  return browser
    .typeText(usernameInput, 'clientUser')
    .typeText(passwordInput, 'testPassword1')
    .typeText(confirmPasswordInput, 'testPassword1')
    .click(registerButton)
    .expect(inputValidationMessage(usernameInput))
    .eql('A user with that username already exists.');
});

test('all text is correctly rendered', browser =>
  browser
    .expect(inputLabel(usernameInput))
    .eql('Username')
    .expect(inputLabel(emailInput))
    .eql('Email')
    .expect(inputLabel(passwordInput))
    .eql('Password')
    .expect(inputLabel(confirmPasswordInput))
    .eql('Confirm Password')
    .expect(buttonLabel(registerButton))
    .eql('Register'));

test('register button is disabled until all valid data is entered', async browser =>
  browser
    .expect(registerButton.hasAttribute('disabled'))
    .ok()
    .typeText(usernameInput, 'testUser')
    .expect(registerButton.hasAttribute('disabled'))
    .ok()
    .typeText(passwordInput, 'testPassword')
    .expect(registerButton.hasAttribute('disabled'))
    .ok()
    .typeText(confirmPasswordInput, 'testPassword')
    .expect(registerButton.hasAttribute('disabled'))
    .notOk());

test('register button disables after being enabled if data becomes invalid', async browser =>
  browser
    .typeText(usernameInput, 'testUser')
    .typeText(passwordInput, 'testPassword')
    .typeText(confirmPasswordInput, 'testPassword')
    .expect(registerButton.hasAttribute('disabled'))
    .notOk()
    .selectText(usernameInput)
    .pressKey('delete')
    .expect(registerButton.hasAttribute('disabled'))
    .ok());

test('username field should be auto focused', browser => {
  return browser
    .expect(usernameInput.visible)
    .ok()
    .pressKey('t e s t')
    .expect(usernameInput.value)
    .eql('test');
});

test('finishing registration deletes related tempCode', async browser => {
  const previousRegistrationTempCodes = await getTempCodesByAction(
    'client-registration'
  );
  await browser
    .expect(previousRegistrationTempCodes.length)
    .eql(1)
    .typeText(usernameInput, 'clientUser1')
    .typeText(passwordInput, 'testPassword1')
    .typeText(confirmPasswordInput, 'testPassword1');
  await browser
    .click(registerButton)
    .expect(url())
    .eql(`${baseUrl}/my-medical-info`);
  const newRegistrationTempCodes = await getTempCodesByAction(
    'client-registration'
  );
  return browser.expect(newRegistrationTempCodes.length).eql(0);
});

test('email input is disabled', browser => {
  return browser.expect(emailInput.hasAttribute('disabled')).ok();
});

test('totp input is hidden while not toggled', browser =>
  browser
    .expect(otpInput1.visible)
    .notOk()
    .click(enableTOTPToggle)
    .expect(otpInput1.visible)
    .ok());

test('register button is disabled until all valid data is entered', async browser => {
  await browser
    .typeText(usernameInput, 'testUser')
    .typeText(passwordInput, 'testPassword')
    .typeText(confirmPasswordInput, 'testPassword')
    .click(enableTOTPToggle)
    .expect(registerButton.hasAttribute('disabled'))
    .ok()
    .click(enableTOTPToggle)
    .expect(registerButton.hasAttribute('disabled'))
    .notOk()
    .click(enableTOTPToggle);
  await setupTOTP(browser);
  return browser.expect(registerButton.hasAttribute('disabled')).notOk();
});
