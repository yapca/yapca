import { Selector } from 'testcafe';
import { TOTP } from 'otpauth';
import {
  otpInput1,
  otpInput2,
  otpInput3,
  otpInput4,
  otpInput5,
  otpInput6
} from '../../../generic-selectors/Auth';
import { createCentre } from '../../../generic-functions';
import { url } from '../../../custom-commands';
import {
  loginButton,
  passwordInput,
  usernameInput
} from '../../../generic-selectors/Login';
import { baseUrl, destroyDb, seedDb } from '../../../globals';

const otpInputModal = Selector('#otpInputModal');
const otpIncorrectMessage = Selector('#otpIncorrectMessage');
const cancelOtpInputButton = Selector('#cancelOtpInputButton');
const adminResetTempCodeButton = Selector('#adminResetTempCodeButton');
const tempCodeUsernameInput = Selector('#usernameInput');
const totp = new TOTP({
  issuer: 'Yapca',
  label: 'YAPCA TOTP Authentication Code',
  algorithm: 'SHA1',
  digits: 6,
  period: 30,
  secret: 'JBSWY3DPEHPK3PXP'
});

fixture`components/auth/OtpInputModal`
  .beforeEach(async browser => {
    await seedDb();
    await createCentre();
    return browser.navigateTo(baseUrl).resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('otp input appears for users with OTP enabled and is auto focused', browser =>
  browser
    .expect(otpInputModal.visible)
    .notOk()
    .typeText(usernameInput, 'testOTPUser')
    .typeText(passwordInput, 'testPassword1')
    .click(loginButton)
    .expect(otpInputModal.visible)
    .ok()
    .expect(url())
    .eql(`${baseUrl}/auth?nextUrl=/clients`)
    .pressKey('4 9 3 2 1')
    .expect(otpInput1.value)
    .eql('4')
    .expect(otpInput2.value)
    .eql('9')
    .expect(otpInput3.value)
    .eql('3')
    .expect(otpInput4.value)
    .eql('2')
    .expect(otpInput5.value)
    .eql('1')
    .expect(otpInput6.value)
    .eql(''));

test('inputting correct OTP opens next page', async browser => {
  await browser
    .typeText(usernameInput, 'testOTPUser')
    .typeText(passwordInput, 'testPassword1')
    .click(loginButton);
  const token = totp.generate();
  return browser
    .expect(otpInputModal.visible)
    .ok()
    .pressKey(
      `${token.charAt(0)} ${token.charAt(1)} ${token.charAt(2)} ${token.charAt(
        3
      )} ${token.charAt(4)} ${token.charAt(5)}`
    )
    .expect(url())
    .eql(`${baseUrl}/error`);
});

test('inputting incorrect OTP shows error and resets code', browser =>
  browser
    .typeText(usernameInput, 'testOTPUser')
    .typeText(passwordInput, 'testPassword1')
    .click(loginButton)
    .expect(otpInputModal.visible)
    .ok()
    .expect(otpIncorrectMessage.visible)
    .notOk()
    .pressKey('4 9 3 2 1 5')
    .expect(otpInputModal.visible)
    .ok()
    .expect(url())
    .eql(`${baseUrl}/auth?nextUrl=/clients`)
    .expect(otpIncorrectMessage.visible)
    .ok()
    .expect(otpIncorrectMessage.textContent)
    .eql('The code you entered is incorrect. Please enter the code again.')
    .pressKey('4')
    .expect(otpIncorrectMessage.visible)
    .notOk());

test('closing the OTP input and reopening resets the input', browser =>
  browser
    .typeText(usernameInput, 'testOTPUser')
    .typeText(passwordInput, 'testPassword1')
    .click(loginButton)
    .pressKey('4 9 3')
    .click(cancelOtpInputButton)
    .expect(otpIncorrectMessage.visible)
    .notOk()
    .click(loginButton)
    .expect(otpInput1.value)
    .eql('')
    .expect(otpInput2.value)
    .eql('')
    .expect(otpInput3.value)
    .eql('')
    .expect(otpInput4.value)
    .eql('')
    .expect(otpInput5.value)
    .eql('')
    .expect(otpInput6.value)
    .eql(''));

test('admin reset button redirects to temp-code page', browser =>
  browser
    .typeText(usernameInput, 'testOTPUser')
    .typeText(passwordInput, 'testPassword1')
    .click(loginButton)
    .click(adminResetTempCodeButton)
    .expect(tempCodeUsernameInput.value)
    .eql('testOTPUser')
    .expect(url())
    .eql(`${baseUrl}/temp-code?action=reset-totp&username=testOTPUser`));
