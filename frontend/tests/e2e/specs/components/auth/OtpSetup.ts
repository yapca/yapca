import { Selector } from 'testcafe';
import {
  createCentre,
  createRegistrationTempCode,
  getUserByName,
  setupTOTP
} from '../../../generic-functions';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  confirmPasswordInput,
  otpInput1,
  otpInput2,
  otpInput3,
  otpInput4,
  otpInput5,
  otpInput6,
  otpSecretKeyText,
  otpTitleRow,
  passwordInput,
  registerButton,
  usernameInput
} from '../../../generic-selectors/Auth';
import { url } from '../../../custom-commands';

const otpRegisterIncorrectMessage = Selector('#otpRegisterIncorrectMessage');
const otpRegisterSuccessMessage = Selector('#otpRegisterSuccessMessage');
const registrationTempCode = 'ci6TbI7VDKXA1wIIRAtyyPUc9JT7lrq5';

fixture`components/auth/OtpSetup`
  .beforeEach(async browser => {
    await seedDb();
    await createRegistrationTempCode(registrationTempCode);
    await createCentre();
    return browser
      .navigateTo(
        `${baseUrl}/temp-code?action=registration&email=newuseremail@test.com&displayName=New%20User&code=${registrationTempCode}`
      )
      .resizeWindow(1280, 720)
      .typeText(usernameInput, 'newUser')
      .typeText(passwordInput, 'testPassword1')
      .typeText(confirmPasswordInput, 'testPassword1');
  })
  .after(destroyDb);

test('inputting incorrect token resets input and shows error', browser =>
  browser
    .expect(otpTitleRow.textContent)
    .contains("YAPCA requires that users with access to multiple clients' data")
    .expect(otpTitleRow.textContent)
    .notContains('YAPCA supports optional multi-factor authentication')
    .expect(otpRegisterIncorrectMessage.visible)
    .notOk()
    .typeText(otpInput1, '1')
    .typeText(otpInput2, '2')
    .typeText(otpInput3, '3')
    .typeText(otpInput4, '4')
    .typeText(otpInput5, '5')
    .typeText(otpInput6, '6')
    .expect(otpRegisterIncorrectMessage.visible)
    .ok()
    .expect(otpRegisterIncorrectMessage.textContent)
    .contains('The code you entered is incorrect')
    .expect(otpInput1.value)
    .eql('')
    .expect(otpInput2.value)
    .eql('')
    .expect(otpInput3.value)
    .eql('')
    .expect(otpInput4.value)
    .eql('')
    .expect(otpInput5.value)
    .eql('')
    .expect(otpInput6.value)
    .eql(''));

test('inputting incorrect token does not enable register button', browser =>
  browser
    .typeText(otpInput1, '1')
    .typeText(otpInput2, '2')
    .typeText(otpInput3, '3')
    .typeText(otpInput4, '4')
    .typeText(otpInput5, '5')
    .typeText(otpInput6, '6')
    .expect(registerButton.hasAttribute('disabled'))
    .ok());

test('inputting any text makes existing error go away', browser =>
  browser
    .typeText(otpInput1, '1')
    .typeText(otpInput2, '2')
    .typeText(otpInput3, '3')
    .typeText(otpInput4, '4')
    .typeText(otpInput5, '5')
    .typeText(otpInput6, '6')
    .typeText(otpInput1, '1')
    .expect(otpRegisterIncorrectMessage.visible)
    .notOk());

test('inputting correct token disables input and shows success message', async browser => {
  await browser.expect(otpRegisterSuccessMessage.visible).notOk();
  await setupTOTP(browser);
  return browser
    .expect(otpInput1.hasAttribute('disabled'))
    .ok()
    .expect(otpInput2.hasAttribute('disabled'))
    .ok()
    .expect(otpInput3.hasAttribute('disabled'))
    .ok()
    .expect(otpInput4.hasAttribute('disabled'))
    .ok()
    .expect(otpInput5.hasAttribute('disabled'))
    .ok()
    .expect(otpInput6.hasAttribute('disabled'))
    .ok()
    .expect(otpRegisterSuccessMessage.visible)
    .ok()
    .expect(otpRegisterSuccessMessage.textContent)
    .eql('Your second device is setup correctly.');
});

test('secret key is saved to database', async browser => {
  const secretKey = await otpSecretKeyText.innerText;
  await setupTOTP(browser);
  await browser.click(registerButton).expect(url()).eql(`${baseUrl}/error`);
  const user = (await getUserByName('newUser'))[0];
  return browser
    .expect(user.otpDeviceKey?.length)
    .eql(32)
    .expect(user.otpDeviceKey)
    .eql(secretKey);
});
