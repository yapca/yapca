import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  createAbsenceRecords,
  createCarePlans,
  getCentreByName,
  getClientByName,
  getUserByName,
  inputAbsence,
  removeUserCentre
} from '../../../generic-functions';
import {
  absenceHistoryDialog,
  aggregateCarePlansRows,
  carePlanDialog,
  carePlanMenuIcon
} from '../../../generic-selectors/CarePlans';
import {
  absenceHistoryButton,
  historyTable
} from '../../../generic-selectors/AbsenceRecords';
import { inputLabel, nurse } from '../../../custom-commands';
import {
  cardTitle,
  closeDialogButton
} from '../../../generic-selectors/Layout';

const headers = historyTable.find('.responsiveTableHeader');
const rows = historyTable.find('.grid-data-row');
const data = historyTable.find('.grid-data-cell');
const noDataMessage = historyTable.find('.q-table__bottom');
const historySearch = Selector('#historySearch');
const closeModalButton = absenceHistoryDialog.find('#closeDialogButton');

fixture`components/carePlans/AbsenceHistory`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await createCarePlans();
    await createAbsenceRecords();
    await browser.navigateTo(`${baseUrl}/carePlans`).resizeWindow(1280, 720);
    await nurse();
    return browser
      .click(aggregateCarePlansRows.nth(0))
      .click(carePlanMenuIcon)
      .click(absenceHistoryButton);
  })
  .after(destroyDb);

test('title is correct', async browser =>
  browser
    .expect(cardTitle.nth(2).textContent)
    .eql("Test Client's Absence History"));

test('table has correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(6)
    .expect(headers.nth(0).textContent)
    .eql('Date')
    .expect(headers.nth(1).textContent)
    .eql('Centre')
    .expect(headers.nth(2).textContent)
    .eql('Reason For Absence')
    .expect(headers.nth(3).textContent)
    .eql('Client Was Active')
    .expect(headers.nth(4).textContent)
    .eql('Reason For Inactivity')
    .expect(headers.nth(5).textContent)
    .eql('Added By'));

test('table shows message when no data', async browser =>
  browser
    .click(closeModalButton)
    .click(closeDialogButton)
    .expect(carePlanDialog.visible)
    .notOk()
    .click(aggregateCarePlansRows.nth(2))
    .click(carePlanMenuIcon)
    .click(absenceHistoryButton)
    .expect(noDataMessage.textContent)
    .eql('Test Client has no previous absence records'));

test('table has correct data', async browser =>
  browser
    .expect(data.count)
    .eql(12)
    .expect(data.nth(0).textContent)
    .eql('04/01/20')
    .expect(data.nth(1).textContent)
    .eql('Example Centre 2')
    .expect(data.nth(2).textContent)
    .eql('Sick')
    .expect(data.nth(3).find('.activeIcon').visible)
    .ok()
    .expect(data.nth(4).textContent)
    .eql('N/A')
    .expect(data.nth(5).textContent)
    .eql('Test Nurse')
    .expect(data.nth(6).textContent)
    .eql('03/01/20')
    .expect(data.nth(7).textContent)
    .eql('Example Centre')
    .expect(data.nth(8).textContent)
    .eql('Inactive')
    .expect(data.nth(9).find('.inactiveIcon').visible)
    .ok()
    .expect(data.nth(10).textContent)
    .eql('Deceased')
    .expect(data.nth(11).textContent)
    .eql('Auto Generated'));

test('search bar has correct text', async browser =>
  browser
    .expect(inputLabel(historySearch))
    .eql("Search Test Client's History"));

test('search bar filters data', async browser =>
  browser
    .typeText(historySearch, '04/01/20')
    .expect(rows.count)
    .eql(6)
    .selectText(historySearch)
    .pressKey('delete')
    .typeText(historySearch, 'Example Centre')
    .expect(rows.count)
    .eql(12));

test('absence appears when added elsewhere', async browser => {
  const nurseId = (await getUserByName('testNurse'))[0].id;
  const client4Id = (await getClientByName('4'))[0].id;
  const centre1Id = (await getCentreByName('Example Centre'))[0].id;
  await inputAbsence([
    {
      date: '2020-01-06',
      reasonForAbsence: 'Busy',
      addedBy: nurseId,
      centre: centre1Id,
      client: client4Id
    }
  ]);
  return browser
    .expect(rows.count)
    .eql(18)
    .expect(data.nth(0).textContent)
    .eql('06/01/20')
    .expect(data.nth(2).textContent)
    .eql('Busy');
});

test('close button closes modal', async browser =>
  browser.click(closeModalButton).expect(headers.visible).notOk());

test('absences disappear when user unsubs from centre', async browser => {
  await removeUserCentre('testNurse', 'Example Centre 4');
  await removeUserCentre('testNurse', 'Example Centre 2');
  return browser.expect(rows.count).eql(6);
});
