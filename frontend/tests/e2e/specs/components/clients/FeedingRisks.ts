import { Selector } from 'testcafe';
import setSelector from '../../../custom-commands/setSelectorMenuValue';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  getClientByName,
  updateFeedingRisks
} from '../../../generic-functions';

import { nurse } from '../../../custom-commands';
import { clientsTableRow2 } from '../../../generic-selectors/Clients';
import { dialogSelectorMenu } from '../../../generic-selectors/Layout';

const chips = Selector('#feedingRisksInput').find('.q-chip__content');
const activeDialogSelectors =
  Selector('.q-select__dialog').find('.q-item--active');

fixture`components/clients/feedingRisks`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(clientsTableRow2);
  })
  .after(destroyDb);

test('chips text are correct', async browser =>
  browser
    .expect(chips.count)
    .eql(2)
    .expect(chips.nth(0).textContent)
    .eql('Poor Appetite')
    .expect(chips.nth(1).textContent)
    .eql('Chewing'));

test('select menu has correct text', browser =>
  browser
    .click(chips)
    .click(chips)
    .expect(dialogSelectorMenu.count)
    .eql(3)
    .expect(dialogSelectorMenu.nth(0).textContent)
    .eql('Chewing')
    .expect(dialogSelectorMenu.nth(1).textContent)
    .eql('Swallowing')
    .expect(dialogSelectorMenu.nth(2).textContent)
    .eql('Poor Appetite'));

test('select menu has correct values selected on load', async browser =>
  browser
    .click(chips)
    .click(chips)
    .expect(activeDialogSelectors.count)
    .eql(2)
    .expect(activeDialogSelectors.nth(0).textContent)
    .eql('Chewing')
    .expect(activeDialogSelectors.nth(1).textContent)
    .eql('Poor Appetite'));

test('selecting value updates database', async browser => {
  await browser.click(chips).click(chips);
  await setSelector(1, false);
  const clients = await getClientByName('4');
  return browser
    .expect(
      clients.filter(
        (client: { feedingRisk: string }) => client.feedingRisk === 'Chewing'
      ).length
    )
    .gt(0)
    .expect(
      clients.filter(
        (client: { feedingRisk: string }) => client.feedingRisk === 'Swallowing'
      ).length
    )
    .gt(0)
    .expect(
      clients.filter(
        (client: { feedingRisk: string }) =>
          client.feedingRisk === 'Poor Appetite'
      ).length
    )
    .gt(0);
});

test('selecting value updates chips', async browser => {
  await browser.click(chips).click(chips);
  await setSelector(1, false);
  await browser
    .expect(chips.count)
    .eql(3)
    .expect(chips.nth(2).textContent)
    .eql('Swallowing');
});

test('unselecting value updates chips', async browser => {
  await browser.click(chips).click(chips);
  await setSelector(0, false);
  return browser.expect(chips.count).eql(1);
});

test('chips update when updated elsewhere', async browser => {
  await updateFeedingRisks();
  return browser
    .expect(chips.count)
    .eql(1)
    .expect(chips.nth(0).textContent)
    .eql('Swallowing');
});

test('selected menu items are correct when updated from elsewhere', async browser => {
  await updateFeedingRisks();
  return browser
    .click(chips)
    .click(chips)
    .expect(activeDialogSelectors.count)
    .eql(1)
    .expect(activeDialogSelectors.nth(0).textContent)
    .eql('Swallowing');
});
