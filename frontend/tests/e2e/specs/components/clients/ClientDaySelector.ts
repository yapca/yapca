import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  getCentreByName,
  getClientByName,
  updateClient
} from '../../../generic-functions';
import { nurse } from '../../../custom-commands';
import {
  clientVisitingCentreHeaders,
  clientsTableRow2,
  daySelector,
  daySelectorFri,
  daySelectorTue,
  daySelectorWed
} from '../../../generic-selectors/Clients';

fixture`components/clients/ClientDaySelector`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser
      .click(clientsTableRow2)
      .click(clientVisitingCentreHeaders.nth(0));
  })
  .after(destroyDb);

test('day selector is visible', async browser =>
  browser.expect(daySelector.visible).ok());

test('day selector shows correct days', async browser =>
  browser
    .expect(daySelectorTue.textContent)
    .eql('Tuesday')
    .expect(daySelectorWed.textContent)
    .eql('Wednesday')
    .expect(daySelectorFri.textContent)
    .eql('Friday'));

test('day selector has correct days selected', async browser =>
  browser
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .expect(daySelectorWed.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorFri.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)'));

test('day active status updates on click', async browser =>
  browser
    .click(daySelectorTue)
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .click(daySelectorTue)
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .click(daySelectorWed)
    .expect(daySelectorWed.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)'));

test('selecting a day changes it in database', async browser =>
  browser
    .click(daySelectorWed)
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter((client: { dayId: number }) => client.dayId === 3)
              .length
          )
          .gte(1)
      )
    ));

test('cannot select less than one day', async browser =>
  browser
    .click(daySelectorTue)
    .click(daySelectorFri)
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorFri.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)'));

test('selected days update in real time', async browser => {
  const centreOneId = (await getCentreByName('Example Centre'))[0].id;
  await updateClient('4', {
    visitingDays: [
      { centre: centreOneId, day: 3 },
      { centre: centreOneId, day: 5 }
    ]
  });
  await browser
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorWed.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .expect(daySelectorFri.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)');
  await updateClient('4', {
    visitingDays: [{ centre: centreOneId, day: 3 }]
  });
  await browser
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(daySelectorWed.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .expect(daySelectorFri.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)');
});
