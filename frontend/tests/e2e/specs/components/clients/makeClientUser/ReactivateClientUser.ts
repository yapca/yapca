import { Selector } from 'testcafe';
import {
  clientHamburgerMenuIcon,
  clientsTableRow1
} from '../../../../generic-selectors/Clients';
import { nurse } from '../../../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../../../globals';
import { snackbarText } from '../../../../generic-selectors/Auth';

import { getUserByName, updateUser } from '../../../../generic-functions';

const addNewClientCard = Selector('#makeClientUserDialog');
const makeClientUserButton = Selector('#makeClientUserButton');
const confirmButton = Selector('#makeClientUserConfirmButton');
const cardTitle = addNewClientCard.find('.cardTitle');
const addNewClientWarning = Selector('#addNewClientWarning');

fixture`components/clients/MakeClientUser (Reactivate User)`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(baseUrl);
    await nurse();
    await updateUser('clientUser', { isActive: false });
    return browser
      .click(clientsTableRow1)
      .click(clientHamburgerMenuIcon)
      .click(makeClientUserButton);
  })
  .after(destroyDb);

test('text is correct', browser =>
  browser
    .expect(cardTitle.textContent)
    .eql('Allow Test Client To Login')
    .expect(addNewClientWarning.textContent)
    .contains('Test Client already has an account but it was disabled.')
    .expect(confirmButton.textContent)
    .eql('Reactivate Account'));

test('pressing submit enables user', async browser => {
  await browser
    .click(confirmButton)
    .expect(snackbarText.textContent)
    .contains('Account Reactivated!')
    .expect(addNewClientCard.visible)
    .notOk();
  const clientUser = (await getUserByName('clientUser'))[0];
  await browser.expect(clientUser.isActive).eql(true);
});
