import { Selector } from 'testcafe';
import {
  clientHamburgerMenuIcon,
  clientsTableRow2
} from '../../../../generic-selectors/Clients';
import { inputValidationMessage, nurse } from '../../../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../../../globals';
import { snackbarText } from '../../../../generic-selectors/Auth';

import {
  getClientByName,
  getTempCodesByAction
} from '../../../../generic-functions';

const addNewClientCard = Selector('#makeClientUserDialog');
const emailInput = Selector('#addNewClientEmailInput');
const makeClientUserButton = Selector('#makeClientUserButton');
const confirmButton = Selector('#makeClientUserConfirmButton');
const canViewCalendarToggle = Selector('#canViewCalendarToggle');
const canViewOwnDataToggle = Selector('#canViewOwnDataToggle');

fixture`components/clients/MakeClientUser (New User)`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(baseUrl);
    await nurse();
    return browser
      .click(clientsTableRow2)
      .click(clientHamburgerMenuIcon)
      .click(makeClientUserButton);
  })
  .after(destroyDb);

test('input shows error when invalid email', browser =>
  browser
    .typeText(emailInput, 'invalidemail')
    .selectText(emailInput)
    .expect(inputValidationMessage(emailInput))
    .eql('Invalid Email Address'));

test('shows snackbar and closes dialog on success', browser =>
  browser
    .expect(addNewClientCard.visible)
    .ok()
    .typeText(emailInput, 'testemail2@test.com')
    .click(confirmButton)
    .expect(snackbarText.textContent)
    .contains('Invite Sent!')
    .expect(addNewClientCard.visible)
    .notOk());

test('pressing enter to submit does not work when data is invalid', browser =>
  browser
    .typeText(emailInput, 'testemail')
    .pressKey('enter')
    .expect(addNewClientCard.visible)
    .ok());

test('can submit client user by pressing enter', browser =>
  browser
    .typeText(emailInput, 'testemail2@test.com')
    .pressKey('enter')
    .expect(addNewClientCard.visible)
    .notOk());

test('submit button is disabled until all data is entered', browser =>
  browser
    .typeText(emailInput, 'testemail')
    .expect(confirmButton.hasAttribute('disabled'))
    .ok()
    .typeText(emailInput, '@test.com')
    .expect(confirmButton.hasAttribute('disabled'))
    .notOk());

test('submitting adds tempCode to database', async browser => {
  const client4Id = (await getClientByName('4'))[0].id;
  await browser
    .typeText(emailInput, 'testemail2@test.com')
    .click(confirmButton)
    .expect(addNewClientCard.visible)
    .notOk();
  const registrationTempCodes = await getTempCodesByAction(
    'client-registration'
  );
  return browser
    .expect(registrationTempCodes.length)
    .eql(1)
    .expect(registrationTempCodes[0].email)
    .eql('testemail2@test.com')
    .expect(registrationTempCodes[0].associatedUserId)
    .eql(null)
    .expect(registrationTempCodes[0].associatedClientId)
    .eql(client4Id)
    .expect(registrationTempCodes[0].permissionCodeName)
    .eql('client_can_view_calendar');
});

test('can set different permissions when making client', async browser => {
  await browser
    .typeText(emailInput, 'testemail2@test.com')
    .expect(canViewCalendarToggle.hasAttribute('aria-disabled'))
    .ok()
    .expect(canViewOwnDataToggle.hasAttribute('aria-disabled'))
    .notOk()
    .click(canViewOwnDataToggle)
    .expect(canViewCalendarToggle.hasAttribute('aria-disabled'))
    .notOk()
    .expect(canViewOwnDataToggle.hasAttribute('aria-disabled'))
    .notOk()
    .click(canViewCalendarToggle)
    .expect(canViewCalendarToggle.hasAttribute('aria-disabled'))
    .notOk()
    .expect(canViewOwnDataToggle.hasAttribute('aria-disabled'))
    .ok()
    .click(confirmButton)
    .expect(addNewClientCard.visible)
    .notOk();
  const registrationTempCodes = await getTempCodesByAction(
    'client-registration'
  );
  return browser
    .expect(registrationTempCodes.length)
    .eql(1)
    .expect(registrationTempCodes[0].permissionCodeName)
    .eql('client_can_view_own_data');
});

test('input shows error when email taken by user', browser =>
  browser
    .typeText(emailInput, 'testemail@test.com')
    .click(confirmButton)
    .expect(inputValidationMessage(emailInput))
    .eql('Email is already in use.')
    .expect(addNewClientCard.visible)
    .ok());

test('make client user input shows error when email already sent', browser =>
  browser
    .typeText(emailInput, 'testemail2@test.com')
    .click(confirmButton)
    .expect(addNewClientCard.visible)
    .notOk()
    .click(makeClientUserButton)
    .typeText(emailInput, 'testemail2@test.com')
    .click(confirmButton)
    .expect(inputValidationMessage(emailInput))
    .eql(
      'Registration was already requested for this email address in the last 12 hours'
    )
    .expect(addNewClientCard.visible)
    .ok());
