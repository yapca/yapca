import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  getClientByName,
  getNeedByDescription,
  updateCentre,
  updateClientNeed
} from '../../../generic-functions';
import {
  editPlanDescriptionField,
  needsList,
  noNeedsText,
  planEditButton
} from '../../../generic-selectors/Needs';

import { nurse, textAreaLabel } from '../../../custom-commands';
import {
  clientVisitingCentreHeaders,
  clientsGridRows
} from '../../../generic-selectors/Clients';
import {
  dialogSelectorMenu,
  popupEditSaveButton
} from '../../../generic-selectors/Layout';
import { centreCards } from '../../../generic-selectors/Centres';

const needsEditIcon = Selector('#needsEditIcon');
const viewCentreButton = Selector('#needsViewCentreButton');

fixture`components/clients/NeedList`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(baseUrl).resizeWindow(400, 720);
    await nurse();
    return browser
      .click(clientsGridRows.nth(1))
      .click(clientVisitingCentreHeaders.nth(0))
      .click(needsEditIcon);
  })
  .after(destroyDb);

test('needs list shows lists of needs for a centre', async browser => {
  await browser
    .click(needsList)
    .expect(dialogSelectorMenu.count)
    .eql(3)
    .expect(
      dialogSelectorMenu.nth(0).find('.q-item__section').child('div').nth(0)
        .textContent
    )
    .eql('Example Need 1')
    .expect(
      dialogSelectorMenu.nth(0).find('.q-item__section').child('div').nth(1)
        .textContent
    )
    .eql('Example Plan 1')
    .expect(
      dialogSelectorMenu.nth(1).find('.q-item__section').child('div').nth(0)
        .textContent
    )
    .eql('Example Need 3')
    .expect(
      dialogSelectorMenu.nth(1).find('.q-item__section').child('div').nth(1)
        .textContent
    )
    .eql('Example Plan 3')
    .expect(
      dialogSelectorMenu.nth(2).find('.q-item__section').child('div').nth(0)
        .textContent
    )
    .eql('Example Need 5')
    .expect(
      dialogSelectorMenu.nth(2).find('.q-item__section').child('div').nth(1)
        .textContent
    )
    .eql('');
});

test('cannot select less than one need', async browser => {
  return browser
    .click(needsList)
    .click(
      dialogSelectorMenu.nth(0).find('.q-item__section').child('div').nth(0)
    )
    .click(
      dialogSelectorMenu.nth(1).find('.q-item__section').child('div').nth(0)
    )
    .click(
      dialogSelectorMenu.nth(2).find('.q-item__section').child('div').nth(0)
    )
    .expect(dialogSelectorMenu.nth(0).getAttribute('aria-selected'))
    .eql('false')
    .expect(dialogSelectorMenu.nth(1).getAttribute('aria-selected'))
    .eql('false')
    .expect(dialogSelectorMenu.nth(2).getAttribute('aria-selected'))
    .eql('true');
});

test('selecting a need in needs list updates database', async browser => {
  await browser
    .click(needsList)
    .click(
      dialogSelectorMenu.nth(2).find('.q-item__section').child('div').nth(0)
    )
    .expect(dialogSelectorMenu.nth(2).getAttribute('aria-selected'))
    .eql('false');
  const clients = await getClientByName('4');
  return browser
    .expect(
      clients.filter(
        (client: { need: string; clientNeedActive: boolean }) =>
          client.need === 'Example Need 1' && client.clientNeedActive
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { need: string; clientNeedActive: boolean }) =>
          client.need === 'Example Need 3' && client.clientNeedActive
      ).length
    )
    .gte(1)
    .expect(
      clients.filter(
        (client: { need: string; clientNeedActive: boolean }) =>
          client.need === 'Example Need 5' && client.clientNeedActive
      ).length
    )
    .eql(0);
});

test('setting no needs in needs list does not update database', async browser => {
  await browser
    .click(needsList)
    .click(
      dialogSelectorMenu.nth(0).find('.q-item__section').child('div').nth(0)
    )
    .click(
      dialogSelectorMenu.nth(1).find('.q-item__section').child('div').nth(0)
    )
    .click(
      dialogSelectorMenu.nth(2).find('.q-item__section').child('div').nth(0)
    );
  const clients = await getClientByName('4');
  return browser
    .expect(
      clients.filter(
        (client: { need: string; clientNeedActive: boolean }) =>
          client.need === 'Example Need 1' && client.clientNeedActive
      ).length
    )
    .eql(0)
    .expect(
      clients.filter(
        (client: { need: string; clientNeedActive: boolean }) =>
          client.need === 'Example Need 3' && client.clientNeedActive
      ).length
    )
    .eql(0)
    .expect(
      clients.filter(
        (client: { need: string; clientNeedActive: boolean }) =>
          client.need === 'Example Need 5' && client.clientNeedActive
      ).length
    )
    .gte(1);
});

test('needs list updates when updated elsewhere', async browser => {
  const [exampleNeed4] = await getNeedByDescription('Example Need 4');
  await updateCentre('Example Centre', { needs: [exampleNeed4.id] });
  return browser
    .click(needsList)
    .expect(dialogSelectorMenu.count)
    .eql(1)
    .expect(dialogSelectorMenu.nth(0).textContent)
    .eql('Example Need 4');
});

test('client needs updates when updated elsewhere', async browser => {
  await updateClientNeed('Example Plan 1', '4', { isActive: false });
  await updateClientNeed('', '4', { isActive: false });
  return browser
    .click(needsList)
    .expect(dialogSelectorMenu.nth(0).getAttribute('aria-selected'))
    .eql('false')
    .expect(dialogSelectorMenu.nth(1).getAttribute('aria-selected'))
    .eql('true')
    .expect(dialogSelectorMenu.nth(2).getAttribute('aria-selected'))
    .eql('false');
});

test('centre shows message when no needs', async browser => {
  await updateCentre('Example Centre', { needs: [] });
  return browser
    .expect(noNeedsText.visible)
    .ok()
    .expect(noNeedsText.textContent)
    .eql(' There are no needs for this centre, edit the centre and add some ');
});

test('plan edit button shows plan text area', async browser =>
  browser
    .click(needsList)
    .expect(planEditButton.count)
    .eql(3)
    .click(planEditButton.nth(2))
    .expect(editPlanDescriptionField.visible)
    .ok()
    .expect(textAreaLabel(editPlanDescriptionField))
    .eql("Add Test Client's plan for 'Example Need 5'"));

test('entering plan saves it in the database', async browser => {
  await browser
    .click(needsList)
    .click(planEditButton.nth(2))
    .typeText(editPlanDescriptionField, 'Example Plan')
    .click(popupEditSaveButton);
  const clients = await getClientByName('4');
  return browser
    .expect(
      clients.filter(
        (client: { needPlan: string }) => client.needPlan === 'Example Plan'
      ).length
    )
    .gte(1);
});

test('need plans are shown on load', async browser => {
  await updateClientNeed('Example Plan 3', '4', { plan: 'Test Need Plan' });
  await browser.eval(() => document.location.reload());
  await browser
    .click(clientsGridRows.nth(1))
    .click(clientVisitingCentreHeaders.nth(0))
    .click(needsEditIcon)
    .click(needsList)
    .expect(planEditButton.nth(0).count)
    .eql(1)
    .click(planEditButton.nth(1))
    .expect(editPlanDescriptionField.find('textarea').value)
    .eql('Test Need Plan');
});

test('need plans update when changed elsewhere', async browser => {
  await updateClientNeed('Example Plan 3', '4', { plan: 'Test Need Plan' });
  await browser
    .click(needsList)
    .click(planEditButton.nth(1))
    .expect(editPlanDescriptionField.find('textarea').value)
    .eql('Test Need Plan');
});

test('clicking view opens centre dialog', async browser =>
  browser.click(viewCentreButton).expect(centreCards.visible).ok());

test('view centre option is visible when no needs exist', async browser => {
  await updateCentre('Example Centre', { needs: [] });
  return browser.click(viewCentreButton).expect(centreCards.visible).ok();
});
