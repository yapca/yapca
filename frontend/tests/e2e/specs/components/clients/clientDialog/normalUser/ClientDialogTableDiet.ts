import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../../../globals';
import {
  getClientByName,
  updateClient
} from '../../../../../generic-functions';

import { nurse } from '../../../../../custom-commands';
import { clientsTableRow2 } from '../../../../../generic-selectors/Clients';

const tableWrapper = Selector('.clientDialogTableWrapper').nth(3);
const headers = tableWrapper.find('.clientDialogTableHeader');
const data = tableWrapper.find('.clientDialogTableData');
const clientDialogTableMainHeader = tableWrapper.find(
  '.clientDialogTableMainHeader'
);
const slider = Selector('.clientEditSlider').find('.q-slider__thumb');
const sliderLabel = Selector('.clientEditSlider').find('.q-slider__text');
const feedingRisksInput = Selector('#feedingRisksInput');

fixture`components/clients/clientDialogTableDiet (Normal User)`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(clientsTableRow2);
  })
  .after(destroyDb);

test('dialog shows correct header', async browser =>
  browser.expect(clientDialogTableMainHeader.textContent).eql('Diet'));

test('table shows correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(5)
    .expect(headers.nth(0).textContent)
    .eql('Allergies')
    .expect(headers.nth(1).textContent)
    .eql('Intolerances')
    .expect(headers.nth(2).textContent)
    .eql('Requires Assistance Eating?')
    .expect(headers.nth(3).textContent)
    .eql('Thickner Grade')
    .expect(headers.nth(4).textContent)
    .eql('Feeding Risks'));

test('table shows correct data', async browser =>
  browser
    .expect(data.count)
    .eql(4)
    .expect(data.nth(0).textContent)
    .eql('Allergies List 4')
    .expect(data.nth(1).textContent)
    .eql('Intolerances List 4')
    .expect(data.nth(2).getAttribute('aria-checked'))
    .eql('true')
    .expect(data.nth(3).textContent)
    .eql('4')
    .expect(feedingRisksInput.find('.q-chip__content').count)
    .eql(2)
    .expect(feedingRisksInput.find('.q-chip__content').nth(0).textContent)
    .eql('Poor Appetite')
    .expect(feedingRisksInput.find('.q-chip__content').nth(1).textContent)
    .eql('Chewing'));

test('table updates when data changed elsewhere', async browser => {
  await updateClient('4', {
    thicknerGrade: 2,
    requiresAssistanceEating: false
  });
  return browser
    .expect(data.nth(2).getAttribute('aria-checked'))
    .eql('false')
    .expect(data.nth(3).textContent)
    .eql('2');
});

test('thickner grade is editable', async browser =>
  browser
    .click(data.nth(3))
    .expect(slider.visible)
    .ok()
    .expect(sliderLabel.textContent)
    .eql('4')
    .drag(slider, -150, 0)
    .expect(data.nth(3).textContent)
    .eql('2')
    .expect(slider.visible)
    .notOk()
    .click(data.nth(3))
    .expect(sliderLabel.textContent)
    .eql('2'));

test('updating thickner grade updates in database', async browser =>
  browser
    .click(data.nth(3))
    .drag(slider, -150, 0)
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { thicknerGrade: number }) => client.thicknerGrade === 2
            ).length
          )
          .gt(0)
      )
    ));
