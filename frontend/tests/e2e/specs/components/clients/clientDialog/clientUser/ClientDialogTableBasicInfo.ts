import { Selector } from 'testcafe';
import { differenceInYears } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../../../../globals';
import { updateClient } from '../../../../../generic-functions';

import { clientUser } from '../../../../../custom-commands';

const tableWrapper = Selector('.clientDialogTableWrapper').nth(0);
const headers = tableWrapper.find('.clientDialogTableHeader');
const data = tableWrapper.find('.clientDialogTableData');
const clientFieldEdit = Selector('.clientFieldEdit')
  .filterVisible()
  .find('input');
const dateFieldEdit = Selector('.clientDateFieldEdit');
const clientDialogTableMainHeader = tableWrapper.find(
  '.clientDialogTableMainHeader'
);

fixture`components/clients/clientDialogTableBasicInfo (Client User)`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    return clientUser();
  })
  .after(destroyDb);

test('dialog shows correct header', async browser =>
  browser.expect(clientDialogTableMainHeader.textContent).eql('Basic Info'));

test('table shows correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(10)
    .expect(headers.nth(0).textContent)
    .eql('First Name')
    .expect(headers.nth(1).textContent)
    .eql('Surname')
    .expect(headers.nth(2).textContent)
    .eql('Gender')
    .expect(headers.nth(3).textContent)
    .eql('Active?')
    .expect(headers.nth(4).textContent)
    .eql('Account Status')
    .expect(headers.nth(5).textContent)
    .eql('Date Of Birth')
    .expect(headers.nth(6).textContent)
    .eql('Address')
    .expect(headers.nth(7).textContent)
    .eql('Phone Number')
    .expect(headers.nth(8).textContent)
    .eql('Admitted By')
    .expect(headers.nth(9).textContent)
    .eql('Created Date'));

test('table shows correct data', async browser => {
  await browser
    .expect(data.count)
    .eql(10)
    .expect(data.nth(0).textContent)
    .eql('Test Client')
    .expect(data.nth(1).textContent)
    .eql('1')
    .expect(data.nth(2).textContent)
    .eql('Unknown')
    .expect(data.nth(3).getAttribute('aria-checked'))
    .eql('true')
    .expect(data.nth(4).textContent)
    .eql('Active');
  const age = differenceInYears(Date.now(), new Date('1967-02-11'));
  return browser
    .expect(data.nth(5).textContent)
    .eql(`11/02/67 (${age})`)
    .expect(data.nth(6).textContent)
    .eql('Example Address 1')
    .expect(data.nth(7).textContent)
    .eql('1111111')
    .expect(data.nth(8).textContent)
    .eql('Test Admin')
    .expect(data.nth(9).textContent)
    .eql('16/04/23');
});

test('table updates when data changed elsewhere', async browser => {
  await updateClient('1', {
    firstName: 'New First Name',
    surname: 'New Surname'
  });
  return browser
    .expect(data.nth(0).textContent)
    .eql('New First Name')
    .expect(data.nth(1).textContent)
    .eql('New Surname');
});

test('first name is not editable', async browser =>
  browser.click(data.nth(0)).expect(clientFieldEdit.visible).notOk());

test('date of birth is not editable', async browser =>
  browser.click(data.nth(5)).expect(dateFieldEdit.visible).notOk());

test('admitted by is not editable', async browser =>
  browser.click(data.nth(8)).expect(clientFieldEdit.visible).notOk());

test('created date is not editable', async browser =>
  browser.click(data.nth(9)).expect(clientFieldEdit.visible).notOk());

test('clicking active switch does not update it', async browser =>
  browser
    .click(data.nth(3))
    .expect(data.nth(3).getAttribute('aria-checked'))
    .eql('true'));
