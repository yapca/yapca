import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { deleteNextOfKin, getClientByName } from '../../../generic-functions';

import {
  buttonLabel,
  inputLabel,
  inputValidationMessage,
  nurse
} from '../../../custom-commands';
import {
  addNextOfKinButton,
  clientsTableRow2,
  createNextOfKinSubmitButton,
  nextOfKinCreateNameField,
  nextOfKinCreatePhoneNumberField
} from '../../../generic-selectors/Clients';

const nextOfKinTable = Selector('#nextOfKinTable');
const headers = nextOfKinTable.find('th');
const data = nextOfKinTable.find('.grid-data-cell');
const nextOfKinEditNameField = Selector('#nextOfKinEditNameField');
const nextOfKinEditPhoneNumberField = Selector(
  '#nextOfKinEditPhoneNumberField'
);
const createNextOfKinDialog = Selector('#createNextOfKinDialog');
const deleteNextOfKinButtons = Selector('.deleteNextOfKinButton');
const deleteTooltip = Selector('.nokWarningTooltip');
const nextOfKinExpander = Selector('#nextOfKinExpander');

fixture`components/clients/nextOfKinTable`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(clientsTableRow2).click(nextOfKinExpander);
  })
  .after(destroyDb);

test('table shows correct headers', async browser =>
  browser
    .expect(headers.count)
    .eql(3) // 3 headers + 2 sort arrows
    .expect(headers.nth(0).textContent)
    .eql('Name')
    .expect(headers.nth(1).textContent)
    .eql('Phone Number')
    .expect(headers.nth(2).textContent)
    .eql('Actions'));

test('table shows correct data', browser =>
  browser
    .expect(data.count)
    .eql(6)
    .expect(data.nth(0).textContent)
    .eql('Test NoK 1')
    .expect(data.nth(1).textContent)
    .eql('111111')
    .expect(data.nth(3).textContent)
    .eql('Test NoK 2')
    .expect(data.nth(4).textContent)
    .eql('222222'));

test('name is editable', async browser =>
  browser
    .click(data.nth(0))
    .expect(nextOfKinEditNameField.visible)
    .ok()
    .expect(nextOfKinEditNameField.value)
    .eql('Test NoK 1')
    .typeText(nextOfKinEditNameField, '234')
    .expect(nextOfKinEditNameField.value)
    .eql('Test NoK 1234')
    .click(nextOfKinTable)
    .expect(data.nth(0).textContent)
    .eql('Test NoK 1234'));

test('pressing enter closes edit dialog', async browser =>
  browser
    .click(data.nth(0))
    .typeText(nextOfKinEditNameField, '234')
    .pressKey('enter')
    .expect(data.nth(0).textContent)
    .eql('Test NoK 1234'));

test('name edit label is correct', async browser =>
  browser
    .click(data.nth(0))
    .selectText(nextOfKinEditNameField)
    .pressKey('delete')
    .expect(inputLabel(nextOfKinEditNameField))
    .eql('Name'));

test('updating name updates in database', async browser =>
  browser
    .click(data.nth(0))
    .typeText(nextOfKinEditNameField, '234')
    .pressKey('enter')
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { nextOfKinName: string }) =>
                client.nextOfKinName === 'Test NoK 1234'
            ).length
          )
          .gt(0)
      )
    ));

test('empty name field shows error', async browser =>
  browser
    .click(data.nth(0))
    .selectText(nextOfKinEditNameField)
    .pressKey('delete')
    .expect(inputValidationMessage(nextOfKinEditNameField))
    .eql('Name is required'));

test('database does not update if field name empty', async browser =>
  browser
    .click(data.nth(0))
    .selectText(nextOfKinEditNameField)
    .pressKey('delete')
    .pressKey('enter')
    .expect(nextOfKinEditNameField.visible)
    .notOk()
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { nextOfKinName: string }) =>
                client.nextOfKinName === 'Test NoK 1'
            ).length
          )
          .gt(0)
      )
    ));

test('phone number is editable', async browser =>
  browser
    .click(data.nth(1))
    .expect(nextOfKinEditPhoneNumberField.visible)
    .ok()
    .expect(nextOfKinEditPhoneNumberField.value)
    .eql('111111')
    .selectText(nextOfKinEditPhoneNumberField)
    .typeText(nextOfKinEditPhoneNumberField, '234')
    .expect(nextOfKinEditPhoneNumberField.value)
    .eql('234')
    .click(nextOfKinTable)
    .expect(data.nth(1).textContent)
    .eql('234'));

test('phone number edit label is correct', async browser =>
  browser
    .click(data.nth(1))
    .selectText(nextOfKinEditPhoneNumberField)
    .pressKey('delete')
    .expect(inputLabel(nextOfKinEditPhoneNumberField))
    .eql('Phone Number'));

test('updating phone number updates in database', async browser =>
  browser
    .click(data.nth(1))
    .selectText(nextOfKinEditPhoneNumberField)
    .typeText(nextOfKinEditPhoneNumberField, ' 1234')
    .pressKey('enter')
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { nextOfKinPhoneNumber: string }) =>
                client.nextOfKinPhoneNumber === '1234'
            ).length
          )
          .gt(0)
      )
    ));

test('empty phone number field shows error', async browser =>
  browser
    .click(data.nth(1))
    .selectText(nextOfKinEditPhoneNumberField)
    .pressKey('delete')
    .expect(inputValidationMessage(nextOfKinEditPhoneNumberField))
    .eql('Phone Number is required'));

test('clicking add button opens dialog', browser =>
  browser
    .expect(createNextOfKinDialog.visible)
    .notOk()
    .click(addNextOfKinButton)
    .expect(createNextOfKinDialog.visible)
    .ok());

test('create next of kin dialog labels are correct', browser =>
  browser
    .click(addNextOfKinButton)
    .expect(inputLabel(nextOfKinCreateNameField))
    .eql('Name')
    .expect(inputLabel(nextOfKinCreatePhoneNumberField))
    .eql('Phone Number'));

test('add next of kin button has correct label', browser =>
  browser.expect(buttonLabel(addNextOfKinButton)).eql('Add Next Of Kin'));

test('create next of kin button has correct label', browser =>
  browser
    .click(addNextOfKinButton)
    .expect(buttonLabel(createNextOfKinSubmitButton))
    .eql('Create Next Of Kin'));

test('create button is disabled when no name and phone number', browser =>
  browser
    .click(addNextOfKinButton)
    .expect(createNextOfKinSubmitButton.hasAttribute('disabled'))
    .ok()
    .typeText(nextOfKinCreateNameField, 'Example Name')
    .expect(createNextOfKinSubmitButton.hasAttribute('disabled'))
    .ok()
    .typeText(nextOfKinCreatePhoneNumberField, '1234567')
    .expect(createNextOfKinSubmitButton.hasAttribute('disabled'))
    .notOk()
    .selectText(nextOfKinCreateNameField)
    .pressKey('delete')
    .expect(createNextOfKinSubmitButton.hasAttribute('disabled'))
    .ok());

test('clicking create button closes dialog', browser =>
  browser
    .click(addNextOfKinButton)
    .typeText(nextOfKinCreateNameField, 'Example Name')
    .typeText(nextOfKinCreatePhoneNumberField, '1234567')
    .click(createNextOfKinSubmitButton)
    .expect(createNextOfKinDialog.visible)
    .notOk());

test('clicking create adds next of kin to database', browser =>
  browser
    .click(addNextOfKinButton)
    .typeText(nextOfKinCreateNameField, 'Example Name')
    .typeText(nextOfKinCreatePhoneNumberField, '1234567')
    .click(createNextOfKinSubmitButton)
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { nextOfKinPhoneNumber: string }) =>
                client.nextOfKinPhoneNumber === '1234567'
            ).length
          )
          .gt(0)
          .expect(
            clients.filter(
              (client: { nextOfKinName: string }) =>
                client.nextOfKinName === 'Example Name'
            ).length
          )
          .gt(0)
      )
    ));

test('clicking create adds next of kin to table', browser =>
  browser
    .click(addNextOfKinButton)
    .typeText(nextOfKinCreateNameField, 'Example Name')
    .typeText(nextOfKinCreatePhoneNumberField, '1234567')
    .click(createNextOfKinSubmitButton)
    .expect(data.count)
    .eql(9)
    .expect(data.nth(0).textContent)
    .eql('Example Name')
    .expect(data.nth(1).textContent)
    .eql('1234567'));

test('delete button updates table', async browser =>
  browser
    .expect(data.count)
    .eql(6)
    .click(deleteNextOfKinButtons.nth(1))
    .expect(data.count)
    .eql(3));

test('delete button updates database', async browser =>
  browser
    .click(deleteNextOfKinButtons.nth(1))
    .then(() =>
      getClientByName('4').then(clients =>
        browser
          .expect(
            clients.filter(
              (client: { nextOfKinName: string }) =>
                client.nextOfKinName === 'Test NoK 2'
            ).length
          )
          .eql(0)
      )
    ));

test('table is not visible when there are no next of kin', async browser => {
  await browser.expect(nextOfKinTable.visible).ok();
  await deleteNextOfKin('4', 'Test NoK 1');
  await deleteNextOfKin('4', 'Test NoK 2');
  return browser.expect(nextOfKinTable.visible).notOk();
});

test('delete button is disabled when only one next of kin exists', async browser => {
  await browser
    .expect(deleteNextOfKinButtons.nth(0).find('svg').getStyleProperty('color'))
    .eql('rgb(193, 0, 21)');
  await deleteNextOfKin('4', 'Test NoK 2');
  return browser
    .expect(deleteNextOfKinButtons.count)
    .eql(1)
    .expect(deleteNextOfKinButtons.nth(0).find('svg').getStyleProperty('color'))
    .eql('rgb(158, 158, 158)')
    .click(deleteNextOfKinButtons.nth(0))
    .expect(data.count)
    .eql(3);
});

test('tooltip is shown on delete button when one next of kin exists', async browser => {
  await browser
    .expect(deleteTooltip.visible)
    .notOk()
    .hover(deleteNextOfKinButtons.nth(0))
    .expect(deleteTooltip.visible)
    .notOk()
    .hover(data.nth(0));
  await deleteNextOfKin('4', 'Test NoK 2');
  return browser
    .hover(deleteNextOfKinButtons.nth(0))
    .expect(deleteTooltip.visible)
    .ok()
    .expect(deleteTooltip.textContent)
    .eql('At least one next of kin must exist')
    .hover(data.nth(0))
    .expect(deleteTooltip.visible)
    .notOk();
});
