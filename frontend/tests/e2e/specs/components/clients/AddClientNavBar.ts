import { Selector } from 'testcafe';
import { nurse, url } from '../../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { getCentreByName, updateClient } from '../../../generic-functions';
import { finishClientButtons } from '../../../generic-selectors/Clients';
import {
  deleteConfirmButton,
  deleteConfirmDialog,
  selectedStepHighlight
} from '../../../generic-selectors/Layout';

const clientNavBar = Selector('#clientNavBar');
const createClientButton = Selector('#createClientButton');
const clientSetupStepper = Selector('#clientSetupStepper');

fixture`components/clients/AddClientNavBar`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.navigateTo(baseUrl);
    await nurse();
    return browser.resizeWindow(1280, 720).navigateTo(`${baseUrl}/clients`);
  })
  .after(destroyDb);

test('nav bar is visible for nurse', browser =>
  browser.expect(clientNavBar.visible).ok());

test('nav bar shows client create button', browser =>
  browser
    .expect(createClientButton.visible)
    .ok()
    .expect(createClientButton.textContent)
    .eql('Add New Client'));

test('create client button opens client setup page', browser =>
  browser.click(createClientButton).expect(clientSetupStepper.visible).ok());

test('create client button passes current page url to client setup page', browser =>
  browser
    .click(createClientButton)
    .expect(url())
    .eql(`${baseUrl}/client-creation?nextUrl=/clients`));

test('nav bar shows no incomplete client buttons if none exist', async browser => {
  await updateClient('7', { setupComplete: true });
  await updateClient('8', { setupComplete: true });
  return browser.expect(finishClientButtons.exists).notOk();
});

test('nav bar shows incomplete client buttons if some exist', async browser =>
  browser
    .navigateTo(`${baseUrl}/test`)
    .expect(finishClientButtons.count)
    .eql(4));

test('incomplete client buttons show name of incomplete clients', browser =>
  browser
    .navigateTo(`${baseUrl}/test`)
    .expect(finishClientButtons.nth(0).textContent)
    .eql("Finish Creating Batman's Profile")
    .expect(finishClientButtons.nth(1).textContent)
    .eql("Cancel Creating Batman's Profile")
    .expect(finishClientButtons.nth(2).textContent)
    .eql("Finish Creating Shazam's Profile")
    .expect(finishClientButtons.nth(3).textContent)
    .eql("Cancel Creating Shazam's Profile"));

test('incomplete client buttons appear when added from elsewhere', async browser => {
  await browser
    .resizeWindow(1920, 1080)
    .expect(finishClientButtons.count)
    .eql(4);
  await updateClient('6', { setupComplete: false });
  return browser.expect(finishClientButtons.count).eql(6);
});

test('finish client buttons disappear when updated from elsewhere', async browser => {
  await browser.expect(finishClientButtons.count).eql(4);
  await updateClient('7', { setupComplete: true });
  await browser.expect(finishClientButtons.count).eql(2);
});

test('incomplete client button opens client setup page', browser =>
  browser
    .click(finishClientButtons.nth(2))
    .expect(clientSetupStepper.visible)
    .ok());

test('incomplete client button passes current page url to client setup page', browser =>
  browser
    .click(finishClientButtons.nth(2))
    .expect(url())
    .eql(`${baseUrl}/client-creation?nextUrl=/clients`));

test('incomplete client button opens client setup on correct section', browser =>
  browser
    .click(finishClientButtons.nth(2))
    .expect(selectedStepHighlight.textContent)
    .eql('2'));

test('delete client button opens confirm dialog', async browser =>
  browser
    .expect(deleteConfirmDialog.visible)
    .notOk()
    .click(finishClientButtons.nth(1))
    .expect(deleteConfirmDialog.visible)
    .ok());

test('delete client confirm button updates database', async browser =>
  browser
    .click(finishClientButtons.nth(1))
    .click(deleteConfirmButton)
    .then(() =>
      getCentreByName('Example Centre 5').then(centres =>
        browser.expect(centres.length).eql(0)
      )
    ));

test('delete client confirm button removes client from page', async browser =>
  browser
    .click(finishClientButtons.nth(1))
    .click(deleteConfirmButton)
    .expect(finishClientButtons.count)
    .eql(2));

test('delete client confirm button closes dialog', async browser =>
  browser
    .click(finishClientButtons.nth(1))
    .click(deleteConfirmButton)
    .expect(deleteConfirmDialog.visible)
    .notOk());
