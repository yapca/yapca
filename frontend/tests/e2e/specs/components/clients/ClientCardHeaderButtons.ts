import { baseUrl, destroyDb, seedDb } from '../../../globals';
import { nurse } from '../../../custom-commands';
import {
  clientHamburgerMenuIcon,
  clientUserPermissionsButton,
  clientsTableRow1
} from '../../../generic-selectors/Clients';
import setClientUserActivation from '../../../generic-functions/setClientUserActivation';

fixture`components/clients/ClientCardHeaderButtons`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    await browser.resizeWindow(1280, 720).navigateTo(baseUrl);
    await nurse();
    return browser.click(clientsTableRow1).click(clientHamburgerMenuIcon);
  })
  .after(destroyDb);

test('edit client permissions option is only visible when client active', async browser => {
  await browser
    .expect(clientUserPermissionsButton.visible)
    .ok()
    .expect(clientUserPermissionsButton.textContent)
    .eql("Edit Test Client's Permissions");
  await setClientUserActivation('1', false);
  return browser.expect(clientUserPermissionsButton.exists).notOk();
});
