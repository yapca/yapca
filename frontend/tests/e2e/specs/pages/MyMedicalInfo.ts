import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../globals';
import { clientUser, nurse, url } from '../../custom-commands';
import {
  explicitErrorHandler,
  removeUserPermission
} from '../../generic-functions';

const clientDialogSection = Selector('#clientDialogSection');

fixture`pages/MyMedicalInfo`
  .clientScripts({ content: `(${explicitErrorHandler.toString()})()` })
  .beforeEach(async browser => {
    await seedDb(['base', 'centres', 'clients']);
    return browser.resizeWindow(1280, 720).navigateTo(baseUrl);
  })
  .after(destroyDb);

test('page shows client info', async browser => {
  await clientUser();
  return browser
    .expect(url())
    .eql(`${baseUrl}/my-medical-info`)
    .expect(clientDialogSection.visible)
    .ok();
});

test('cannot view page as nurse', async browser => {
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/my-medical-info`)
    .expect(url())
    .eql(`${baseUrl}/clients`);
});

test('medical info page cannot be accessed by clientUser without permission', async browser => {
  await removeUserPermission('clientUser', 'client_can_view_own_data');
  await clientUser();
  return browser
    .navigateTo(`${baseUrl}/my-medical-info`)
    .expect(url())
    .eql(`${baseUrl}/calendar`);
});
