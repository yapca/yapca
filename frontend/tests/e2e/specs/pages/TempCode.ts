import { Selector } from 'testcafe';
import { loginButton } from '../../generic-selectors/Login';
import {
  createTempCode,
  getTempCode,
  getUserByName,
  setupTOTP
} from '../../generic-functions';
import {
  infoSnackbar,
  otpSecretKeyText,
  snackbarText
} from '../../generic-selectors/Auth';
import { inputValidationMessage, url } from '../../custom-commands';
import { baseUrl, destroyDb, seedDb } from '../../globals';
import {
  changePasswordButton,
  confirmPasswordInput,
  newPasswordInput
} from '../../generic-selectors/ChangePassword';

fixture`pages/TempCode`
  .beforeEach(async browser => {
    await seedDb();
    return browser.navigateTo(baseUrl).expect(loginButton.visible).ok();
  })
  .after(destroyDb);

const tempCodeInputForm = Selector('#tempCodeInputForm');
const tempCodeInput = Selector('#tempCodeInput');
const usernameInput = Selector('#usernameInput');
const emailInput = Selector('#emailInput');
const tempCodeValidateButton = Selector('#tempCodeValidateButton');
const tempCodeReturnToLogin = Selector('#tempCodeReturnToLogin');
const tempCodeEyeIcon = Selector('#tempCodeEyeIcon');

const totpResetTempCode = 'FYCF5luA0NMbfwFVdOgI2R8L5mQ31c1c';
const passwordResetTempCode = '5M0nKZNS68IKNnen3IzTMIaDZv0s14fL';

const createExampleTempCode = async (): Promise<[{ id: number }]> => {
  const userId = (await getUserByName('testNurse'))[0].id;
  await createTempCode(
    new Date(Date.now() + 5 * 60000),
    'reset-totp',
    totpResetTempCode,
    userId
  );
  return createTempCode(
    new Date(Date.now() + 5 * 60000),
    'reset-password',
    passwordResetTempCode,
    userId
  );
};

const resetPassword = async (browser: TestController): Promise<void> => {
  return browser
    .expect(newPasswordInput.visible)
    .ok({ timeout: 10000 })
    .typeText(newPasswordInput, 'newPassword')
    .typeText(confirmPasswordInput, 'newPassword')
    .click(changePasswordButton);
};

test('return to login button does that', async browser =>
  browser
    .navigateTo(`${baseUrl}/temp-code?action=reset-password`)
    .click(tempCodeReturnToLogin)
    .expect(loginButton.visible)
    .ok()
    .expect(url())
    .eql(`${baseUrl}/auth`));

test('does not render without action query', async browser =>
  browser
    .navigateTo(`${baseUrl}/temp-code`)
    .expect(tempCodeInputForm.visible)
    .notOk());

test('can pass code as query', async browser =>
  browser
    .navigateTo(`${baseUrl}/temp-code?action=reset-totp&code=123345`)
    .expect(tempCodeInput.visible)
    .ok({ timeout: 10000 })
    .expect(tempCodeInput.value)
    .eql('123345'));

test('can pass username as query', async browser =>
  browser
    .navigateTo(`${baseUrl}/temp-code?action=reset-totp&username=testUser`)
    .expect(usernameInput.value)
    .eql('testUser')
    .expect(emailInput.exists)
    .notOk());

test('shows validation error if code is wrong length', async browser =>
  browser
    .navigateTo(`${baseUrl}/temp-code?action=reset-totp&username=testUser`)
    .typeText(tempCodeInput, '12345')
    .expect(inputValidationMessage(tempCodeInput))
    .eql('Code must be 32 characters'));

test('button is disabled until valid data is entered', async browser =>
  browser
    .navigateTo(`${baseUrl}/temp-code?action=reset-totp`)
    .expect(tempCodeValidateButton.hasAttribute('disabled'))
    .ok()
    .typeText(usernameInput, 'user')
    .expect(tempCodeValidateButton.hasAttribute('disabled'))
    .ok()
    .typeText(tempCodeInput, '12345')
    .expect(tempCodeValidateButton.hasAttribute('disabled'))
    .ok()
    .typeText(tempCodeInput, 'FYCF5luA0NMbfwFVdOgI2R8L5mQ')
    .expect(tempCodeValidateButton.hasAttribute('disabled'))
    .notOk()
    .selectText(tempCodeInput)
    .pressKey('delete')
    .expect(tempCodeValidateButton.hasAttribute('disabled'))
    .ok());

test('passing username and code auto validates', async browser =>
  browser
    .navigateTo(
      `${baseUrl}/temp-code?action=reset-totp&username=testUser&code=${totpResetTempCode}`
    )
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .contains('Code/User is invalid or outdated'));

test('TOTP setup is shown when code is correct for "reset-totp" action', async browser => {
  await createExampleTempCode();
  await browser
    .navigateTo(
      `${baseUrl}/temp-code?action=reset-totp&username=testNurse&code=${totpResetTempCode}`
    )
    .expect(otpSecretKeyText.visible)
    .ok();
  const tempCode = await getTempCode(totpResetTempCode);
  return browser.expect(tempCode.length).gte(1);
});

test('inputting TOTP code returns to login and shows snackbar message', async browser => {
  await createExampleTempCode();
  await browser.navigateTo(
    `${baseUrl}/temp-code?action=reset-totp&username=testNurse&code=${totpResetTempCode}`
  );
  await setupTOTP(browser);
  return browser
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .contains('TOTP Device Successfully Reset')
    .expect(url())
    .eql(`${baseUrl}/auth`);
});

test('inputting TOTP code deletes tempCode from database', async browser => {
  await createExampleTempCode();
  await browser.navigateTo(
    `${baseUrl}/temp-code?action=reset-totp&username=testNurse&code=${totpResetTempCode}`
  );
  await setupTOTP(browser);
  const tempCode = await getTempCode(totpResetTempCode);
  return browser.expect(tempCode.length).eql(0);
});

test('inputting TOTP code resets associated user otpDeviceKey', async browser => {
  await createExampleTempCode();
  await browser.navigateTo(
    `${baseUrl}/temp-code?action=reset-totp&username=testNurse&code=${totpResetTempCode}`
  );
  const secretKey = await otpSecretKeyText.innerText;
  await setupTOTP(browser);
  const newOtpDeviceKey = (await getUserByName('testNurse'))[0].otpDeviceKey;
  return browser.expect(newOtpDeviceKey).eql(secretKey);
});

test('Password Reset is shown when code is correct for "reset-password" action', async browser => {
  await createExampleTempCode();
  await browser
    .navigateTo(
      `${baseUrl}/temp-code?action=reset-password&username=testNurse&code=${passwordResetTempCode}`
    )
    .expect(newPasswordInput.visible)
    .ok();
  const tempCode = await getTempCode(passwordResetTempCode);
  return browser.expect(tempCode.length).gte(1);
});

test('Fails when using correct for code with wrong action "reset-password" action', async browser => {
  await createExampleTempCode();
  await browser
    .navigateTo(
      `${baseUrl}/temp-code?action=reset-password&username=testNurse&code=${totpResetTempCode}`
    )
    .expect(newPasswordInput.visible)
    .notOk()
    .expect(snackbarText.textContent)
    .contains('Code/User is invalid or outdated');
  const tempCode = await getTempCode(passwordResetTempCode);
  return browser.expect(tempCode.length).gte(1);
});

test('resetting password returns to login and shows snackbar message + deletes code', async browser => {
  await createExampleTempCode();
  await browser.navigateTo(
    `${baseUrl}/temp-code?action=reset-password&username=testNurse&code=${passwordResetTempCode}`
  );
  await resetPassword(browser);
  await browser
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .contains('Password Changed!')
    .expect(url())
    .eql(`${baseUrl}/auth`);
  const tempCode = await getTempCode(passwordResetTempCode);
  return browser.expect(tempCode.length).eql(0);
});

test('changing password changes associated user password', async browser => {
  await createExampleTempCode();
  const oldPassword = (await getUserByName('testNurse'))[0].password;
  await browser.navigateTo(
    `${baseUrl}/temp-code?action=reset-password&username=testNurse&code=${passwordResetTempCode}`
  );
  await resetPassword(browser);
  const newPassword = (await getUserByName('testNurse'))[0].password;
  return browser.expect(oldPassword).notEql(newPassword);
});

test('passing login as action return to login page', browser =>
  browser
    .navigateTo(`${baseUrl}/temp-code?action=login`)
    .expect(url())
    .eql(`${baseUrl}/auth`));

test('changing query while already on page updates username', browser =>
  browser
    .navigateTo(
      `${baseUrl}/temp-code?action=reset-password&username=testNurse&code=1234`
    )
    .expect(usernameInput.value)
    .eql('testNurse')
    .expect(tempCodeInput.value)
    .eql('1234')
    .navigateTo(
      `${baseUrl}/temp-code?action=reset-password&username=newUser&code=5678`
    )
    .expect(usernameInput.value)
    .eql('newUser')
    .expect(tempCodeInput.value)
    .eql('5678'));

test('username input becomes email when action is registration', browser =>
  browser
    .navigateTo(
      `${baseUrl}/temp-code?action=registration&email=test@test.com&code=1234`
    )
    .expect(usernameInput.exists)
    .notOk()
    .expect(emailInput.value)
    .eql('test@test.com'));

test('input shows error when invalid email', browser =>
  browser
    .navigateTo(`${baseUrl}/temp-code?action=registration`)
    .typeText(emailInput, 'invalidemail')
    .selectText(emailInput)
    .expect(inputValidationMessage(emailInput))
    .eql('Invalid Email Address'));

test('temp code input can be shown and hidden with icon', browser =>
  browser
    .navigateTo(`${baseUrl}/temp-code?action=registration`)
    .expect(tempCodeInput.getAttribute('type'))
    .eql('password')
    .click(tempCodeEyeIcon)
    .expect(tempCodeInput.getAttribute('type'))
    .eql('text'));

test('username input becomes email when action is client registration', browser =>
  browser
    .navigateTo(
      `${baseUrl}/temp-code?action=client-registration&email=test@test.com&code=1234`
    )
    .expect(usernameInput.exists)
    .notOk()
    .expect(emailInput.value)
    .eql('test@test.com'));
