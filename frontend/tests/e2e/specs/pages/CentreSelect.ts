import { baseUrl, destroyDb, seedDb } from '../../globals';
import {
  addUserCentre,
  createCentreWithOpeningDaysSetup,
  deleteCentre,
  getCentreByName,
  getUserByName,
  removeUserCentre,
  updateCentre
} from '../../generic-functions';
import { admin, url } from '../../custom-commands';
import { infoSnackbar, snackbarText } from '../../generic-selectors/Auth';
import {
  centreSelectButton,
  centresList
} from '../../generic-selectors/Centres';

fixture`pages/CentreSelect Setup Database`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    await removeUserCentre('testAdmin', 'Example Centre');
    await browser.navigateTo(baseUrl);
  })
  .after(destroyDb);

test('user gets redirected to centre-select when not subbed', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .expect(url())
    .eql(`${baseUrl}/centre-select?nextUrl=/clients`);
});

test('page shows list of centres', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .expect(centresList.count)
    .eql(4)
    .expect(centresList.nth(0).textContent)
    .eql('Example Centre')
    .expect(centresList.nth(1).textContent)
    .eql('Example Centre 2')
    .expect(centresList.nth(2).textContent)
    .eql('Example Centre 3');
});

test('page should not show incomplete centre', async browser => {
  await createCentreWithOpeningDaysSetup();
  await admin();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .expect(centresList.count)
    .eql(4);
});

test('centres in centre select should update when added from elsewhere', async browser => {
  await createCentreWithOpeningDaysSetup();
  await admin();
  await browser.navigateTo(`${baseUrl}/centres`);
  await updateCentre('Example Centre OD Setup', { setupComplete: true });
  return browser.expect(centresList.count).eql(5);
});

test('button disabled when no centres selected', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .expect(centreSelectButton.hasAttribute('disabled'))
    .ok()
    .click(centresList.nth(0))
    .expect(centreSelectButton.hasAttribute('disabled'))
    .notOk()
    .click(centresList.nth(0))
    .expect(centreSelectButton.hasAttribute('disabled'))
    .ok();
});

test('clicking button goes to next page', async browser => {
  await admin();
  await browser
    .navigateTo(`${baseUrl}/clients`)
    .click(centresList.nth(1))
    .expect(centreSelectButton.hasAttribute('disabled'))
    .notOk()
    .click(centreSelectButton)
    .expect(centreSelectButton.visible)
    .notOk()
    .expect(url())
    .eql(`${baseUrl}/clients`);
});

test('clicking button updates database', async browser => {
  await admin();
  const centreOneId = (await getCentreByName('Example Centre'))[0].id;
  const centreThreeId = (await getCentreByName('Example Centre 3'))[0].id;
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(centresList.nth(0))
    .click(centresList.nth(2))
    .click(centreSelectButton)
    .then(() =>
      getUserByName('testAdmin').then(users =>
        browser
          .expect(
            users.filter(
              (user: { centreId: number }) => user.centreId === centreOneId
            ).length
          )
          .gte(1)
          .expect(
            users.filter(
              (user: { centreId: number }) => user.centreId === centreThreeId
            ).length
          )
          .gte(1)
      )
    );
});

test('user redirects to centre-select when only centre deleted', async browser => {
  await admin();
  await addUserCentre('testAdmin', 'Example Centre');
  await browser.navigateTo(`${baseUrl}/clients`);
  await deleteCentre('Example Centre');
  return browser
    .expect(centresList.count)
    .eql(3)
    .expect(url())
    .eql(`${baseUrl}/centre-select?nextUrl=/clients`);
});

test('notification shown when only subbed centre deleted', async browser => {
  await admin();
  await addUserCentre('testAdmin', 'Example Centre');
  await browser.navigateTo(`${baseUrl}/user-settings`);
  await deleteCentre('Example Centre');
  return browser
    .expect(infoSnackbar.exists)
    .ok()
    .expect(snackbarText.textContent)
    .eql(
      'Your only subscribed centre was removed, please select new centres to subscribe to'
    )
    .expect(infoSnackbar.getStyleProperty('background-color'))
    .eql('rgb(49, 204, 236)');
});
