import { Selector } from 'testcafe';
import { differenceInYears } from 'date-fns';
import { baseUrl, destroyDb, seedDb } from '../../../globals';
import {
  deleteClient,
  getCentreByName,
  goBack,
  goForward,
  setClientHiddenHeaders,
  updateClient,
  updateClientCentre,
  updateUser
} from '../../../generic-functions';
import { nurse } from '../../../custom-commands';
import {
  clientFilterButton,
  clientsGridRows,
  clientsTableHeader1,
  clientsTableHeader2,
  clientsTableHeaders,
  clientsTableRow1,
  clientsTableRow4,
  clientsTableRows,
  noClientsMessage
} from '../../../generic-selectors/Clients';
import {
  sidebar,
  topNavBarDrawerOpenButton
} from '../../../generic-selectors/Layout';

const clientsTableRow1Col = clientsTableRow1.child('td');

const clientsGridRow1Cols = clientsGridRows.nth(0).find('.grid-data-cell');

const clientsTableRow4Col = clientsTableRow4.child('td');

const clientsTableRow4Col1 = clientsTableRow4.child('td').nth(0);
const clientsTableRow4Col2 = clientsTableRow4.child('td').nth(1);
const clientSearch = Selector('#clientSearch');
const clientDialog = Selector('#clientDialog');
const clientsHiddenSwitch = Selector('#clientsHiddenSwitch');
const clientsHiddenSwitchLabel = clientsHiddenSwitch.find('.q-toggle__label');
const ageFilterSliders = Selector('.ageFilter').find('.q-slider__thumb');

fixture`pages/clients Clients Table Setup Database`
  .page(baseUrl)
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    return browser.navigateTo(`${baseUrl}/clients`).resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('table shows message when no clients exist', async browser => {
  await nurse();
  return browser
    .expect(noClientsMessage.textContent)
    .eql('No clients have been added to your subscribed centres');
});

test('table shows 3 headers by default', async browser => {
  await seedDb(['clients']);
  await nurse();
  return browser
    .expect(clientsTableHeaders.count)
    .eql(3)
    .expect(clientsTableHeaders.nth(0).textContent)
    .eql('First Name')
    .expect(clientsTableHeaders.nth(1).textContent)
    .eql('Surname')
    .expect(clientsTableHeaders.nth(2).textContent)
    .eql('Address');
});

test('table shows correct headers', async browser => {
  await seedDb(['clients']);
  await setClientHiddenHeaders('testNurse', []);
  await nurse();
  return browser
    .expect(clientsTableHeaders.count)
    .eql(23)
    .expect(clientsTableHeaders.nth(0).textContent)
    .eql('First Name')
    .expect(clientsTableHeaders.nth(1).textContent)
    .eql('Surname')
    .expect(clientsTableHeaders.nth(2).textContent)
    .eql('Admitted By')
    .expect(clientsTableHeaders.nth(3).textContent)
    .eql('Gender')
    .expect(clientsTableHeaders.nth(4).textContent)
    .eql('Date Added')
    .expect(clientsTableHeaders.nth(5).textContent)
    .eql('Date Of Birth')
    .expect(clientsTableHeaders.nth(6).textContent)
    .eql('Address')
    .expect(clientsTableHeaders.nth(7).textContent)
    .eql('Phone Number')
    .expect(clientsTableHeaders.nth(8).textContent)
    .eql('Home Help')
    .expect(clientsTableHeaders.nth(9).textContent)
    .eql('Home Help Number')
    .expect(clientsTableHeaders.nth(10).textContent)
    .eql('Public Health Nurse')
    .expect(clientsTableHeaders.nth(11).textContent)
    .eql('Public Health Nurse Number')
    .expect(clientsTableHeaders.nth(12).textContent)
    .eql('GP')
    .expect(clientsTableHeaders.nth(13).textContent)
    .eql('GP Number')
    .expect(clientsTableHeaders.nth(14).textContent)
    .eql('Chemist')
    .expect(clientsTableHeaders.nth(15).textContent)
    .eql('Chemist Number')
    .expect(clientsTableHeaders.nth(16).textContent)
    .eql('Mobility')
    .expect(clientsTableHeaders.nth(17).textContent)
    .eql('Toileting')
    .expect(clientsTableHeaders.nth(18).textContent)
    .eql('Hygiene')
    .expect(clientsTableHeaders.nth(19).textContent)
    .eql('Sight')
    .expect(clientsTableHeaders.nth(20).textContent)
    .eql('Hearing')
    .expect(clientsTableHeaders.nth(21).textContent)
    .eql('Thickner Grade')
    .expect(clientsTableHeaders.nth(22).textContent)
    .eql('Account Status');
});

test('table shows active clients', async browser => {
  await seedDb(['clients']);
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .expect(clientsTableRows.count)
    .eql(4);
});

test('table formats data correctly', async browser => {
  await seedDb(['clients']);
  await setClientHiddenHeaders('testNurse', []);
  await nurse();
  const age = differenceInYears(new Date(), new Date('1967-02-11'));

  return browser
    .expect(clientsGridRow1Cols.count)
    .eql(23)
    .expect(clientsGridRow1Cols.nth(0).textContent)
    .eql('Test Client')
    .expect(clientsGridRow1Cols.nth(1).textContent)
    .eql('1')
    .expect(clientsGridRow1Cols.nth(2).textContent)
    .eql('Test Admin')
    .expect(clientsGridRow1Cols.nth(3).textContent)
    .eql('Unknown')
    .expect(clientsGridRow1Cols.nth(4).textContent)
    .eql('16/04/23')
    .expect(clientsGridRow1Cols.nth(5).textContent)
    .eql(`11/02/67 (${age})`)
    .expect(clientsGridRow1Cols.nth(6).textContent)
    .eql('Example Address 1')
    .expect(clientsGridRow1Cols.nth(7).textContent)
    .eql('1111111')
    .expect(clientsGridRow1Cols.nth(8).textContent)
    .eql('HH 1')
    .expect(clientsGridRow1Cols.nth(9).textContent)
    .eql('HH 1111111')
    .expect(clientsGridRow1Cols.nth(10).textContent)
    .eql('PHN 1')
    .expect(clientsGridRow1Cols.nth(11).textContent)
    .eql('PHN 1111111')
    .expect(clientsGridRow1Cols.nth(12).textContent)
    .eql('GP 1')
    .expect(clientsGridRow1Cols.nth(13).textContent)
    .eql('GP 1111111')
    .expect(clientsGridRow1Cols.nth(14).textContent)
    .eql('C 1')
    .expect(clientsGridRow1Cols.nth(15).textContent)
    .eql('C 1111111')
    .expect(clientsGridRow1Cols.nth(16).textContent)
    .eql('Walks With Frame')
    .expect(clientsGridRow1Cols.nth(17).textContent)
    .eql('Fully Continent')
    .expect(clientsGridRow1Cols.nth(18).textContent)
    .eql('Full Hoist Shower')
    .expect(clientsGridRow1Cols.nth(19).textContent)
    .eql('Glasses')
    .expect(clientsGridRow1Cols.nth(20).textContent)
    .eql('Impaired')
    .expect(clientsGridRow1Cols.nth(21).textContent)
    .eql('4')
    .expect(clientsGridRow1Cols.nth(22).textContent)
    .eql('Active');
});

test('client data updates when updated elsewhere', async browser => {
  await seedDb(['clients']);
  await nurse();
  await updateClient('4', {
    firstName: 'New First Name',
    surname: 'New Surname'
  });
  return browser
    .expect(clientsTableRow4Col1.textContent)
    .eql('New First Name')
    .expect(clientsTableRow4Col2.textContent)
    .eql('New Surname');
});

test("'admitted by' updates when updated elsewhere", async browser => {
  await seedDb(['clients']);
  await setClientHiddenHeaders('testNurse', []);
  await nurse();
  await updateUser('testAdmin', { displayName: 'New Admin Name' });
  return browser
    .expect(clientsGridRow1Cols.nth(2).textContent)
    .eql('New Admin Name');
});

test('client row disappears when client made inactive elsewhere', async browser => {
  await seedDb(['clients']);
  await nurse();
  await updateClient('4', { isActive: false });
  return browser.expect(clientsTableRows.count).eql(3);
});

test('client row appears when client made active elsewhere', async browser => {
  await seedDb(['clients']);
  await updateClient('4', { isActive: false });
  await nurse();
  await browser.expect(clientsTableRows.count).eql(3);
  await updateClient('4', { isActive: true });
  return browser.expect(clientsTableRows.count).eql(4);
});

test("table can be sorted by 'surname'", async browser => {
  await seedDb(['clients']);
  await nurse();
  await browser
    .expect(clientsTableRow1Col.nth(1).textContent)
    .eql('1')
    .expect(clientsTableRow4Col2.textContent)
    .eql('6')
    .click(clientsTableHeader2)
    .expect(clientsTableRow1Col.nth(1).textContent)
    .eql('6')
    .expect(clientsTableRow4Col2.textContent)
    .eql('1');
});

test("table can be sorted by 'admitted by'", async browser => {
  await seedDb(['clients']);
  await setClientHiddenHeaders('testNurse', ['Admitted By'], true);
  await nurse();
  await browser
    .click(clientsTableHeader1)
    .expect(clientsTableRow1Col.nth(0).textContent)
    .eql('Test Admin')
    .expect(clientsTableRow4Col.nth(0).textContent)
    .eql('Test Nurse');
});

test('search bar filters table', async browser => {
  await seedDb(['clients']);
  await setClientHiddenHeaders('testNurse', []);
  await nurse();
  await browser
    .expect(clientsGridRows.count)
    .eql(4)
    .typeText(clientSearch, 'nurse')
    .expect(clientsGridRows.count)
    .eql(1)
    .expect(clientsGridRow1Cols.nth(0).textContent)
    .eql('Test Client');
});

test('client dialog opens on row click', async browser => {
  await seedDb(['clients']);
  await nurse();
  await browser
    .navigateTo(`${baseUrl}/clients`)
    .click(clientsTableRow4)
    .expect(clientDialog.visible)
    .ok();
});

test('hidden clients switch label updates when toggled', async browser => {
  await seedDb(['clients']);
  await setClientHiddenHeaders('testNurse', []);
  await nurse();
  await browser
    .expect(clientsGridRows.count)
    .eql(4)
    .click(clientFilterButton)
    .expect(clientsHiddenSwitchLabel.textContent)
    .eql('Inactive clients are hidden')
    .click(clientsHiddenSwitch)
    .expect(clientsHiddenSwitchLabel.textContent)
    .eql('Inactive clients are shown')
    .expect(clientsGridRows.count)
    .eql(5)
    .click(clientsHiddenSwitch)
    .expect(clientsHiddenSwitchLabel.textContent)
    .eql('Inactive clients are hidden')
    .expect(clientsGridRows.count)
    .eql(4); // 4 * 22
});

test('clients update if deleted elsewhere', async browser => {
  await seedDb(['clients']);
  await nurse();
  await deleteClient('4');
  return browser.expect(clientsTableRows.count).eql(3);
});

test('clients can be filtered by age', async browser => {
  await seedDb(['clients']);
  await nurse();
  return browser
    .click(clientFilterButton)
    .drag(ageFilterSliders.nth(1), -100, 0)
    .expect(clientsTableRows.count)
    .eql(2)
    .expect(clientsTableRow1Col.nth(1).textContent)
    .eql('4');
});

test('filtering all clients shows filtered client message', async browser => {
  await seedDb(['clients']);
  await nurse();
  return browser
    .click(clientFilterButton)
    .drag(ageFilterSliders.nth(1), -250, 0)
    .expect(noClientsMessage.textContent)
    .eql('No clients visible due to filters');
});

test('top nav bar button opens sidebar', async browser => {
  await nurse();
  return browser
    .resizeWindow(480, 640)
    .click(topNavBarDrawerOpenButton)
    .expect(sidebar.visible)
    .ok();
});

test('pressing back while in client dialog closes dialog', async browser => {
  await seedDb(['clients']);
  await nurse();
  await browser.click(clientsTableRow4);
  await goBack();
  await browser.expect(clientDialog.visible).notOk();
  await goForward();
  return browser.expect(clientDialog.visible).ok();
});

test('clients update if removed from all users centres', async browser => {
  // Nurse user is subbed to centre 1, 2 and 4
  // If client is subbed to centre 3 then user should no longer see
  await seedDb(['clients']);
  await nurse();
  await browser.expect(clientsTableRows.count).eql(4);
  const centre1Id = (await getCentreByName('Example Centre'))[0].id;
  await updateClientCentre('everyCentreUser', '4', centre1Id, false);
  return browser.expect(clientsTableRows.count).eql(3);
});

test('date of birth is sorted correctly', async browser => {
  await seedDb(['clients']);
  await nurse();
  await setClientHiddenHeaders('testNurse', ['Date Of Birth'], true);
  const ageSince1961 = differenceInYears(new Date(), new Date('1961-11-27'));
  const ageSince1967 = differenceInYears(new Date(), new Date('1967-02-11'));
  const ageSince2002 = differenceInYears(new Date(), new Date('2002-11-30'));

  await browser
    //Unsorted
    .expect(clientsTableRow1Col.nth(0).textContent)
    .eql(`11/02/67 (${ageSince1967})`)
    .expect(clientsTableRow4Col.nth(0).textContent)
    .eql(`30/11/02 (${ageSince2002})`)
    // Sorted oldest first
    .click(clientsTableHeader1)
    .expect(clientsTableRow1Col.nth(0).textContent)
    .eql(`27/11/61 (${ageSince1961})`)
    .expect(clientsTableRow4Col.nth(0).textContent)
    .eql(`30/11/02 (${ageSince2002})`)
    // Sorted youngest first
    .click(clientsTableHeader1)
    .expect(clientsTableRow1Col.nth(0).textContent)
    .eql(`30/11/02 (${ageSince2002})`)
    .expect(clientsTableRow4Col.nth(0).textContent)
    .eql(`27/11/61 (${ageSince1961})`);
});
