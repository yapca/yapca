import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../globals';
import {
  addUserCentre,
  createCentreWithOpeningDaysSetup,
  deleteCentre,
  goBack,
  goForward,
  updateCentre
} from '../../generic-functions';
import { admin, clientUser, nurse, url } from '../../custom-commands';
import {
  centreCards,
  centresTable,
  centresTableRows
} from '../../generic-selectors/Centres';
import { tableHeaderExpander } from '../../generic-selectors/Layout';

const headers = centresTable.find('th');
const centreSearch = Selector('#centreSearch');
const nameGridDataCells = Selector('.data-cell-name');

fixture`pages/CentresPage`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    return browser.resizeWindow(1280, 720).navigateTo(`${baseUrl}/centres`);
  })
  .after(destroyDb);

test('page cannot be accessed by clientUser', async browser => {
  await seedDb(['clients']);
  await clientUser();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .expect(url())
    .eql(`${baseUrl}/my-medical-info`);
});

test('centres page should show a list of all centres for admin', async browser => {
  await admin();
  return browser.expect(centresTableRows.count).eql(4);
});

test('centres page should show a list of all subbed centres for nurse', async browser => {
  await nurse();
  return browser.expect(centresTableRows.count).eql(3);
});
test('centres page should not show imcomplete centre', async browser => {
  await createCentreWithOpeningDaysSetup();
  await admin();
  return browser.expect(centresTableRows.count).eql(4);
});

test('centres should update when added from elsewhere', async browser => {
  await createCentreWithOpeningDaysSetup();
  await admin();
  await updateCentre('Example Centre OD Setup', { setupComplete: true });
  return browser.expect(centresTableRows.count).eql(5);
});

test('centres update if deleted elsewhere', async browser => {
  await admin();
  await deleteCentre('Example Centre 2');
  await browser.expect(centresTableRows.count).eql(3);
});

test('table shows name for nuse', async browser => {
  await nurse();
  return browser
    .expect(headers.count)
    .eql(1)
    .expect(headers.nth(0).textContent)
    .eql('Name');
});

test('table shows name and subbed status for admin', async browser => {
  await admin();
  return browser
    .expect(headers.count)
    .eql(2)
    .expect(headers.nth(0).textContent)
    .eql('Name')
    .expect(headers.nth(1).textContent)
    .eql('Subscribed?');
});

test('table shows centre names', async browser => {
  await nurse();
  return browser
    .expect(centresTableRows.nth(0).find('td').nth(0).textContent)
    .eql('Example Centre')
    .expect(centresTableRows.nth(1).find('td').nth(0).textContent)
    .eql('Example Centre 2')
    .expect(centresTableRows.nth(2).find('td').nth(0).textContent)
    .eql('Example Centre 4');
});

test("table can be sorted by 'name'", async browser => {
  await admin();
  await browser
    .click(headers.nth(0))
    .expect(centresTableRows.nth(0).find('td').nth(0).textContent)
    .eql('Example Centre 4')
    .expect(centresTableRows.nth(3).find('td').nth(0).textContent)
    .eql('Example Centre');
});

test('centre search bar filters table', async browser => {
  await nurse();
  await browser
    .typeText(centreSearch, '2')
    .expect(centresTableRows.count)
    .eql(1)
    .expect(centresTableRows.nth(0).find('td').nth(0).textContent)
    .eql('Example Centre 2');
});

test('name updates when changed elsewhere', async browser => {
  await nurse();
  await updateCentre('Example Centre', { name: 'A New Name' });
  return browser
    .expect(centresTableRows.nth(0).find('td').nth(0).textContent)
    .eql('A New Name');
});

test('table shows users subscribed status for admins', async browser => {
  await admin();
  return browser
    .expect(centresTableRows.nth(0).find('.notSubscribedIcon').visible)
    .notOk()
    .expect(centresTableRows.nth(0).find('.subscribedIcon').visible)
    .ok()
    .expect(centresTableRows.nth(1).find('.notSubscribedIcon').visible)
    .ok()
    .expect(centresTableRows.nth(1).find('.subscribedIcon').visible)
    .notOk()
    .expect(centresTableRows.nth(2).find('.subscribedIcon').visible)
    .notOk()
    .expect(centresTableRows.nth(2).find('.notSubscribedIcon').visible)
    .ok()
    .expect(centresTableRows.nth(3).find('.notSubscribedIcon').visible)
    .ok()
    .expect(centresTableRows.nth(3).find('.subscribedIcon').visible)
    .notOk();
});

test('admin subscribed status changes when updated elsewhere', async browser => {
  await admin();
  await addUserCentre('testAdmin', 'Example Centre 3');
  return browser
    .expect(centresTableRows.nth(2).find('.subscribedIcon').visible)
    .ok()
    .expect(centresTableRows.nth(2).find('.notSubscribedIcon').visible)
    .notOk();
});

test('pressing back while in centre dialog closes dialog', async browser => {
  await nurse();
  await browser
    .click(centresTableRows.nth(0))
    .expect(centreCards.visible)
    .ok()
    .expect(url())
    .eql(`${baseUrl}/centres#centre-dialog`);
  await goBack();
  await browser
    .expect(centreCards.visible)
    .notOk()
    .expect(url())
    .eql(`${baseUrl}/centres`);
  await goForward();
  return browser
    .expect(centreCards.visible)
    .ok()
    .expect(url())
    .eql(`${baseUrl}/centres#centre-dialog`);
});

test("table can be sorted by 'name' in grid view", async browser => {
  await admin();
  await browser
    .resizeWindow(480, 720)
    .click(tableHeaderExpander)
    .click(headers.nth(0))
    .expect(nameGridDataCells.nth(0).textContent)
    .eql('Example Centre 4')
    .expect(nameGridDataCells.nth(3).textContent)
    .eql('Example Centre')
    .click(headers.nth(0))
    .expect(nameGridDataCells.nth(0).textContent)
    .eql('Example Centre')
    .expect(nameGridDataCells.nth(3).textContent)
    .eql('Example Centre 4');
});
