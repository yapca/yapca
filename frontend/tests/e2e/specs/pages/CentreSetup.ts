import { Selector } from 'testcafe';
import {
  createCentre,
  createCentreWithClosingDaysSetup,
  createCentreWithOpeningDaysSetup,
  createCentreWithPoliciesSetup,
  getCentreByName,
  getCentres,
  getUserByName,
  updateCentre
} from '../../generic-functions';
import { baseUrl, database, destroyDb, seedDb } from '../../globals';
import {
  admin,
  clientUser,
  inputValidationMessage,
  nurse,
  url
} from '../../custom-commands';
import {
  firstNeedInput,
  needsSubmitButton,
  subscribeToCentreDialog
} from '../../generic-selectors/CentreSetup';
import {
  centreMatchingColorDialog,
  countrySelector,
  stateSelector
} from '../../generic-selectors/Centres';
import {
  accentColorSelectors,
  confirmButton,
  goBackButton,
  selectedStepHighlight
} from '../../generic-selectors/Layout';
import setSelector from '../../custom-commands/setSelectorMenuValue';

const window = Selector('body');
const stepper = Selector('#centreSetupStepper');

const centreNameInput = Selector('#centreInput');
const centreNameContinueButton = Selector('#centreForm').child('button');
const openDaysContinueButton = Selector('#openDaysSubmitButton');
const closingDaysSubmitButton = Selector('#closingDaysSubmitButton');
const daySelector = Selector('#daySelector');
const tuesdayButton = daySelector.find('.q-btn').nth(1);
const thursdayButton = daySelector.find('.q-btn').nth(3);
const activeStrpStepDescription = Selector('.q-stepper__tab--active').find(
  '.q-stepper__caption'
);
const finishCentreButtons = Selector('.finishCentreButton');

const centreGoBackButton = Selector('#centreGoBackButton');

fixture`pages/CentreSetup`
  .skipJsErrors({
    message: /.*resizeobserver.*/gi
  })
  .beforeEach(async browser => {
    await seedDb();
    return browser.navigateTo(baseUrl).resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('page can not be accessed by a non-logged in user', async browser =>
  browser
    .navigateTo(`${baseUrl}/centre-setup`)
    .expect(url())
    .eql(`${baseUrl}/auth?nextUrl=/centre-setup`));

test('page can not be accessed by nurse user', async browser => {
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/centre-setup`)
    .expect(url())
    .eql(`${baseUrl}/error`);
});

test('page cannot be accessed by clientUser', async browser => {
  await seedDb(['centres', 'clients']);
  await clientUser();
  return browser
    .navigateTo(`${baseUrl}/centre-setup`)
    .expect(url())
    .eql(`${baseUrl}/my-medical-info`);
});

test('page can be accessed by admin user', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/`)
    .expect(url())
    .eql(`${baseUrl}/centre-setup?nextUrl=/clients`);
});

test('window color is correct when user has light theme set', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/`)
    .expect(window.getStyleProperty('background-color'))
    .eql('rgba(0, 0, 0, 0)')
    .expect(stepper.getStyleProperty('background-color'))
    .eql('rgb(255, 255, 255)');
});

test('window color is correct when user has dark theme set', browser =>
  database()
    .table('authentication_customuser')
    .where('username', '=', 'testAdmin')
    .update({ darkTheme: true })
    .then(async () => {
      await admin();
      return browser
        .navigateTo(`${baseUrl}/`)
        .expect(window.getStyleProperty('background-color'))
        .eql('rgb(18, 18, 18)')
        .expect(stepper.getStyleProperty('background-color'))
        .eql('rgb(29, 29, 29)');
    }));

test('stepper accent color is correct when user has accent set', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/`)
    .expect(selectedStepHighlight.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)');
});

test('goes to step 1 when no unfinished test-centre exists', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/`)
    .expect(selectedStepHighlight.textContent)
    .eql('1');
});

test('goes to step 2 when name already set for centre', browser =>
  createCentre().then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/`)
      .expect(selectedStepHighlight.textContent)
      .eql('2');
  }));

test('goes to step 3 when opening days already set for centre', browser =>
  createCentreWithOpeningDaysSetup().then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/`)
      .expect(selectedStepHighlight.textContent)
      .eql('3');
  }));

test('goes to step 4 when closing days already set for centre', browser =>
  createCentreWithClosingDaysSetup().then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/`)
      .expect(selectedStepHighlight.textContent)
      .eql('4');
  }));

test('goes to step 4 policies already set for centre', browser =>
  createCentreWithPoliciesSetup().then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/`)
      .expect(selectedStepHighlight.textContent)
      .eql('5');
  }));

test('inputting centre name and color progresses to step 2', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/`)
    .typeText(centreNameInput, 'Test Centre')
    .click(accentColorSelectors.nth(6))
    .click(centreNameContinueButton)
    .expect(selectedStepHighlight.textContent)
    .eql('2');
});

test('inputting centre name and color saves new centre to database', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/`)
    .typeText(centreNameInput, 'Test Centre')
    .click(accentColorSelectors.nth(6))
    .click(centreNameContinueButton)
    .then(() =>
      getCentres().then(centres =>
        browser
          .expect(centres.length)
          .eql(1)
          .expect(centres[0].name)
          .eql('Test Centre')
          .expect(centres[0].color)
          .eql('#0288D1')
      )
    );
});

test('centre name field should be auto focused', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/`)
    .pressKey('t e s t')
    .expect(centreNameInput.value)
    .eql('test');
});

test('pressing enter submit centre only works when name entered', async browser => {
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centre-setup`)
    .click(accentColorSelectors.nth(6))
    .pressKey('enter')
    .expect(selectedStepHighlight.textContent)
    .notEql('2')
    .typeText(centreNameInput, 'Test Centre')
    .pressKey('enter')
    .expect(selectedStepHighlight.textContent)
    .eql('2');
});

test('centre submit button only enabled when name and color entered', async browser => {
  await admin();
  return browser
    .expect(centreNameContinueButton.hasAttribute('disabled'))
    .ok()
    .typeText(centreNameInput, 'testUser1')
    .expect(centreNameContinueButton.hasAttribute('disabled'))
    .ok()
    .click(accentColorSelectors.nth(6))
    .expect(centreNameContinueButton.hasAttribute('disabled'))
    .notOk()
    .selectText(centreNameInput)
    .pressKey('delete')
    .expect(centreNameContinueButton.hasAttribute('disabled'))
    .ok();
});

test('goes to step 3 when opening days are set', browser =>
  createCentre().then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/`)
      .click(tuesdayButton)
      .click(openDaysContinueButton)
      .expect(selectedStepHighlight.textContent)
      .eql('3');
  }));

test('error is shown when centre exists with same name', browser =>
  seedDb(['centres']).then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/centre-setup`)
      .typeText(centreNameInput, 'Example Centre')
      .click(accentColorSelectors.nth(6))
      .click(centreNameContinueButton)
      .expect(inputValidationMessage(centreNameInput))
      .eql('A centre with that name already exists.')
      .expect(centreNameContinueButton.hasAttribute('disabled'))
      .ok()
      .typeText(centreNameInput, '1')
      .expect(centreNameContinueButton.hasAttribute('disabled'))
      .notOk();
  }));

test('centre name gets rendered in description of step 2', browser =>
  createCentre().then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/centre-setup`)
      .expect(activeStrpStepDescription.innerText)
      .eql('Select which days of the week Example Centre opens');
  }));

test('description of step 3  is correct', browser =>
  createCentreWithOpeningDaysSetup().then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/centre-setup`)
      .expect(activeStrpStepDescription.innerText)
      .eql(
        'Select where the centre is located (this data is used to calculate public holidays). Then select specific closing days.'
      );
  }));

test('inputting weekdays saves them to the database', browser =>
  createCentre().then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/`)
      .click(tuesdayButton)
      .click(thursdayButton)
      .click(openDaysContinueButton)
      .then(() =>
        getCentres().then(centres =>
          browser
            .expect(centres.length)
            .eql(2)
            .expect(
              centres.find((centre: { dayId: number }) => centre.dayId === 2)
            )
            .ok()
            .expect(
              centres.find((centre: { dayId: number }) => centre.dayId === 4)
            )
            .ok()
        )
      );
  }));

test('centre sub dialog does not appear if only 1 centre exists', browser =>
  createCentreWithPoliciesSetup().then(async () => {
    await admin();
    return browser
      .typeText(firstNeedInput, 'Example Need')
      .expect(subscribeToCentreDialog.visible)
      .notOk()
      .click(needsSubmitButton)
      .expect(needsSubmitButton.visible)
      .notOk()
      .expect(url())
      .eql(`${baseUrl}/clients`);
  }));

test("selecting don't subscribe in dialog loads next page", async browser => {
  await seedDb(['centres']);
  await createCentreWithPoliciesSetup();
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(finishCentreButtons.nth(0))
    .typeText(firstNeedInput, 'Example Need')
    .click(needsSubmitButton)
    .click(goBackButton)
    .expect(url())
    .eql(`${baseUrl}/centres`);
});

test('selecting subscribe in dialog loads next page', async browser => {
  await seedDb(['centres']);
  await createCentreWithPoliciesSetup();
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(finishCentreButtons.nth(0))
    .typeText(firstNeedInput, 'Example Need')
    .click(needsSubmitButton)
    .click(confirmButton)
    .expect(url())
    .eql(`${baseUrl}/centres`);
});

test('selecting subscribe in dialog updates database', async browser => {
  await seedDb(['centres']);
  await createCentreWithPoliciesSetup();
  await admin();
  const centreId = (await getCentreByName('Example Centre Policies Setup'))[0]
    .id;
  await browser
    .navigateTo(`${baseUrl}/centres`)
    .click(finishCentreButtons.nth(0))
    .typeText(firstNeedInput, 'Example Need')
    .click(needsSubmitButton)
    .click(confirmButton);
  const userObjects = await getUserByName('testAdmin');
  return browser
    .expect(
      userObjects.filter(
        (user: { centreId: number }) => user.centreId === centreId
      ).length
    )
    .gte(1);
});

test("selecting don't subscribe in dialog does not update database", async browser => {
  await seedDb(['centres']);
  await createCentreWithPoliciesSetup();
  await admin();
  const centreId = (await getCentreByName('Example Centre Policies Setup'))[0]
    .id;
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(finishCentreButtons.nth(0))
    .typeText(firstNeedInput, 'Example Need')
    .click(needsSubmitButton)
    .click(goBackButton)
    .then(() =>
      getUserByName('testAdmin').then(userObjects =>
        browser
          .expect(
            userObjects.filter(
              (user: { centreId: number }) => user.centreId === centreId
            ).length
          )
          .eql(0)
      )
    );
});

test('days resize at different breakpoints', async browser =>
  createCentre().then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/`)
      .expect(tuesdayButton.textContent)
      .eql('Tuesday')
      .expect(thursdayButton.textContent)
      .eql('Thursday')
      .resizeWindow(600, 720)
      .expect(tuesdayButton.textContent)
      .eql('Tue')
      .expect(thursdayButton.textContent)
      .eql('Thu')
      .resizeWindow(400, 600)
      .expect(tuesdayButton.textContent)
      .eql('T')
      .expect(thursdayButton.textContent)
      .eql('T');
  }));

test('clicking a non conflicting color does not open dialog', async browser => {
  await seedDb(['centres']);
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centre-setup`)
    .click(accentColorSelectors.nth(15))
    .expect(centreMatchingColorDialog.visible)
    .notOk();
});

test('clicking conflicting color opens dialog', async browser => {
  await seedDb(['centres']);
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centre-setup`)
    .click(accentColorSelectors.nth(2))
    .expect(centreMatchingColorDialog.visible)
    .ok();
});

test('opening days submit button disabled when no opening days selected', browser =>
  createCentre().then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/`)
      .expect(openDaysContinueButton.hasAttribute('disabled'))
      .ok()
      .click(tuesdayButton)
      .expect(openDaysContinueButton.hasAttribute('disabled'))
      .notOk();
  }));

test('closing days submit button disabled when country but no state', async browser => {
  await createCentreWithOpeningDaysSetup();
  await admin();
  await browser.navigateTo(`${baseUrl}/`).click(countrySelector);
  await setSelector(3);
  await browser
    .expect(closingDaysSubmitButton.hasAttribute('disabled'))
    .ok()
    .click(stateSelector);
  await setSelector(4);
  return browser
    .expect(closingDaysSubmitButton.hasAttribute('disabled'))
    .notOk();
});

test('go back button is not visible for first centre', browser =>
  createCentre().then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/centre-setup`)
      .expect(centreGoBackButton.visible)
      .notOk();
  }));

test('go back button returns to next page', browser =>
  seedDb(['centres']).then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/centre-setup?nextUrl=clients`)
      .expect(centreGoBackButton.visible)
      .ok()
      .click(centreGoBackButton)
      .expect(url())
      .eql(`${baseUrl}/clients`);
  }));

test('go back button says "go back" when step is 1', browser =>
  seedDb(['centres']).then(async () => {
    await admin();
    return browser
      .navigateTo(`${baseUrl}/centre-setup?nextUrl=centres`)
      .expect(centreGoBackButton.textContent)
      .eql('Go Back');
  }));

test('go back button says user can return when step is > 1', async browser => {
  await seedDb(['centres']);
  await createCentreWithPoliciesSetup();
  await admin();
  return browser
    .navigateTo(`${baseUrl}/centres`)
    .click(finishCentreButtons.nth(0))
    .expect(centreGoBackButton.textContent)
    .eql('Go Back (You can return later)');
});

test('step updates when changed by another user', async browser => {
  await createCentreWithOpeningDaysSetup();
  await admin();
  await browser.expect(selectedStepHighlight.textContent).eql('3');
  await updateCentre('Example Centre OD Setup', {
    closingDaysFirstSetupComplete: true
  });
  return browser.expect(selectedStepHighlight.textContent).eql('4');
});
