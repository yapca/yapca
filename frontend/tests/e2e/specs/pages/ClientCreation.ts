import { Selector } from 'testcafe';
import { baseUrl, destroyDb, seedDb } from '../../globals';
import { clientUser, nurse, url } from '../../custom-commands';
import {
  addressInput,
  clientAdditionalInfoSubmit,
  clientBasicInfoSubmit,
  clientDietSubmit,
  clientServicesSubmit,
  createNextOfKinSubmitButton,
  dateOfBirthInput,
  firstNameInput,
  phoneNumberInput,
  surnameInput
} from '../../generic-selectors/ClientCreation';
import {
  createClientAbilitySetup,
  createClientAdditionalInfoSetup,
  getCentreByName,
  getClientByName,
  updateClient,
  updateClientCentre
} from '../../generic-functions';
import {
  addNextOfKinButton,
  clientVisitingCentreHeaders,
  daySelectorFri,
  daySelectorTue,
  daySelectorTwoMon,
  daySelectorWed,
  finishClientButtons,
  nextOfKinCreateNameField,
  nextOfKinCreatePhoneNumberField
} from '../../generic-selectors/Clients';
import { centreCards, centresList } from '../../generic-selectors/Centres';
import {
  closeDialogButton,
  datePickerDay,
  selectedStepHighlight
} from '../../generic-selectors/Layout';

const clientVisitingDaysSubmit = Selector('#clientVisitingDaysSubmit');
const clientPoliciesSubmit = Selector('#clientPoliciesSubmit');
const unagreedPoliciesCentreHeader = Selector('.unagreedPoliciesCentreHeader');
const unagreedPoliciesCentrePolicyText = Selector(
  '.unagreedPoliciesCentrePolicyText'
);
const clientsPage = Selector('#clientsPage');
const goBackButton = Selector('#clientCreationGoBackButton');
const viewCentreButton = Selector('#viewCentreButton');

fixture`views/ClientCreation Setup Database`
  .beforeEach(async browser => {
    await seedDb(['base', 'centres']);
    return browser
      .navigateTo(`${baseUrl}/client-creation`)
      .resizeWindow(1280, 720);
  })
  .after(destroyDb);

test('page can not be accessed by a non-logged in user', async browser =>
  browser.expect(url()).eql(`${baseUrl}/auth?nextUrl=/client-creation`));

test('page can be accessed by a nurse', async browser => {
  await nurse();
  return browser.expect(url()).eql(`${baseUrl}/client-creation`);
});

test('page cannot be accessed by clientUser', async browser => {
  await seedDb(['clients']);
  await clientUser();
  return browser
    .navigateTo(`${baseUrl}/client-creation`)
    .expect(url())
    .eql(`${baseUrl}/my-medical-info`);
});

test('inputting client data loads step 2', async browser => {
  await nurse();
  return browser
    .typeText(firstNameInput, 'First Name')
    .typeText(surnameInput, 'Surname')
    .click(centresList.nth(2))
    .click(clientBasicInfoSubmit)
    .expect(selectedStepHighlight.textContent)
    .eql('2');
});

test('inputting additional data loads step 3', async browser => {
  await nurse();
  return browser
    .typeText(firstNameInput, 'First Name')
    .typeText(surnameInput, 'Surname')
    .click(centresList.nth(2))
    .click(clientBasicInfoSubmit)
    .typeText(addressInput, 'Example Address')
    .typeText(phoneNumberInput, '01111111')
    .click(dateOfBirthInput)
    .click(datePickerDay.nth(0))
    .pressKey('esc')
    .click(addNextOfKinButton)
    .typeText(nextOfKinCreateNameField, 'Example Name')
    .typeText(nextOfKinCreatePhoneNumberField, '1234567')
    .click(createNextOfKinSubmitButton)
    .click(clientAdditionalInfoSubmit)
    .expect(selectedStepHighlight.textContent)
    .eql('3');
});

test('inputting services data loads step 4', async browser => {
  await createClientAdditionalInfoSetup();
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .click(clientServicesSubmit)
    .expect(selectedStepHighlight.textContent)
    .eql('4');
});

test('inputting diet data loads step 6', async browser => {
  await createClientAbilitySetup();
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .click(clientDietSubmit)
    .expect(selectedStepHighlight.textContent)
    .eql('6');
});

test('selecting policies loads next page', async browser => {
  await createClientAbilitySetup();
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients?nextUrl=`)
    .click(finishClientButtons.nth(0))
    .click(clientDietSubmit)
    .click(clientVisitingCentreHeaders.nth(0))
    .click(daySelectorTue)
    .click(clientVisitingDaysSubmit)
    .click(clientPoliciesSubmit)
    .expect(clientsPage.visible)
    .ok()
    .expect(url())
    .eql(`${baseUrl}/clients?nextUrl=`);
});

test('selecting visiting days updates client in database', async browser => {
  await createClientAbilitySetup();
  await nurse();
  await browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .click(clientDietSubmit)
    .click(clientVisitingCentreHeaders.nth(0))
    .click(daySelectorTue)
    .click(clientVisitingDaysSubmit);
  const clients = await getClientByName('12');
  return browser
    .expect(
      clients.filter((client: { day: string }) => client.day === 'Tuesday')
        .length
    )
    .gt(0)
    .expect(
      clients.filter(
        (client: { setupComplete: boolean }) => client.setupComplete
      ).length
    )
    .eql(0);
});

test('no visiting days are selected by default', async browser => {
  await createClientAbilitySetup();
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .click(clientDietSubmit)
    .click(clientVisitingCentreHeaders.nth(0))
    .expect(daySelectorTue.hasClass('v-btn--active'))
    .notOk()
    .expect(daySelectorWed.hasClass('v-btn--active'))
    .notOk()
    .expect(daySelectorFri.hasClass('v-btn--active'))
    .notOk();
});

test('cannot select less than one day', async browser => {
  await createClientAbilitySetup();
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .click(clientDietSubmit)
    .click(clientVisitingCentreHeaders.nth(0))
    .click(daySelectorTue)
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)')
    .click(daySelectorTue)
    .expect(daySelectorTue.getStyleProperty('background-color'))
    .eql('rgb(211, 47, 47)');
});

test('continue disabled until a day is selected for each centre', async browser => {
  await createClientAbilitySetup();
  const centre2Id = (await getCentreByName('Example Centre 2'))[0].id;
  await updateClientCentre('testNurse', '12', centre2Id, true);
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .click(clientDietSubmit)
    .click(clientVisitingCentreHeaders.nth(0))
    .expect(clientVisitingDaysSubmit.hasAttribute('disabled'))
    .ok()
    .click(daySelectorTue)
    .expect(clientVisitingDaysSubmit.hasAttribute('disabled'))
    .ok()
    .click(clientVisitingCentreHeaders.nth(1))
    .click(daySelectorTwoMon)
    .expect(clientVisitingDaysSubmit.hasAttribute('disabled'))
    .notOk();
});

test('goes to step 2 when basic info already set for client', async browser => {
  await seedDb(['clients']);
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(2))
    .expect(selectedStepHighlight.textContent)
    .eql('2');
});

test('goes to step 3 when additional info already set for client', async browser => {
  await seedDb(['clients']);
  await updateClient('7', { additionalInfoSetup: true });
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .expect(selectedStepHighlight.textContent)
    .eql('3');
});

test('goes to step 4 when services info already set for client', async browser => {
  await seedDb(['clients']);
  await updateClient('7', { additionalInfoSetup: true, servicesSetup: true });
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .expect(selectedStepHighlight.textContent)
    .eql('4');
});

test('goes to step 5 when abilities info already set for client', async browser => {
  await seedDb(['clients']);
  await updateClient('7', {
    additionalInfoSetup: true,
    servicesSetup: true,
    abilitySetup: true
  });
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .expect(selectedStepHighlight.textContent)
    .eql('5');
});

test('goes to step 7 when visiting days already set for client', async browser => {
  await seedDb(['clients']);
  await updateClient('7', {
    additionalInfoSetup: true,
    visitingDaysSetup: true,
    servicesSetup: true,
    abilitySetup: true,
    dietSetup: true
  });
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .expect(selectedStepHighlight.textContent)
    .eql('7');
});

test('inputting visiting days loads step 7', async browser => {
  await createClientAbilitySetup();
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .click(clientDietSubmit)
    .click(clientVisitingCentreHeaders.nth(0))
    .click(daySelectorTue)
    .click(clientVisitingDaysSubmit)
    .expect(selectedStepHighlight.textContent)
    .eql('7');
});

test('policy step shows list of centres', async browser => {
  await seedDb(['clients']);
  const centre2Id = (await getCentreByName('Example Centre 2'))[0].id;
  await updateClientCentre('testNurse', '7', centre2Id, true);
  await updateClient('7', {
    additionalInfoSetup: true,
    visitingDaysSetup: true,
    servicesSetup: true,
    abilitySetup: true
  });
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .expect(unagreedPoliciesCentreHeader.count)
    .eql(2)
    .expect(unagreedPoliciesCentreHeader.nth(0).textContent)
    .eql('Example Centre')
    .expect(unagreedPoliciesCentreHeader.nth(1).textContent)
    .eql('Example Centre 2');
});

test('policy step shows list of policies for each centre', async browser => {
  await seedDb(['clients']);
  const centre2Id = (await getCentreByName('Example Centre 2'))[0].id;
  await updateClientCentre('testNurse', '7', centre2Id, true);
  await updateClient('7', {
    additionalInfoSetup: true,
    visitingDaysSetup: true,
    servicesSetup: true,
    abilitySetup: true
  });
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .expect(unagreedPoliciesCentrePolicyText.count)
    .eql(5)
    .expect(unagreedPoliciesCentrePolicyText.nth(0).textContent)
    .eql('Example Policy 1')
    .expect(unagreedPoliciesCentrePolicyText.nth(1).textContent)
    .eql('Example Policy 3')
    .expect(unagreedPoliciesCentrePolicyText.nth(2).textContent)
    .eql('Example Policy 5')
    .expect(unagreedPoliciesCentrePolicyText.nth(3).textContent)
    .eql('Example Policy 2')
    .expect(unagreedPoliciesCentrePolicyText.nth(4).textContent)
    .eql('Example Policy 4');
});

test('clicking accept in policy step updates database', async browser => {
  await seedDb(['clients']);
  const centre2Id = (await getCentreByName('Example Centre 2'))[0].id;
  await updateClientCentre('testNurse', '7', centre2Id, true);
  await updateClient('7', {
    additionalInfoSetup: true,
    visitingDaysSetup: true,
    servicesSetup: true,
    abilitySetup: true
  });
  await nurse();
  await browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .click(clientPoliciesSubmit)
    .expect(url())
    .eql(`${baseUrl}/clients`);
  const clients = await getClientByName('7');
  return browser
    .expect(
      clients.filter(
        (client: { setupComplete: boolean }) => client.setupComplete
      ).length
    )
    .gt(0)
    .expect(
      clients.filter(
        (client: { policyDescription: string }) =>
          client.policyDescription === 'Example Policy 1'
      ).length
    )
    .gt(0)
    .expect(
      clients.filter(
        (client: { policyDescription: string }) =>
          client.policyDescription === 'Example Policy 2'
      ).length
    )
    .gt(0)
    .expect(
      clients.filter(
        (client: { policyDescription: string }) =>
          client.policyDescription === 'Example Policy 3'
      ).length
    )
    .gt(0)
    .expect(
      clients.filter(
        (client: { policyDescription: string }) =>
          client.policyDescription === 'Example Policy 4'
      ).length
    )
    .gt(0)
    .expect(
      clients.filter(
        (client: { policyDescription: string }) =>
          client.policyDescription === 'Example Policy 5'
      ).length
    )
    .gt(0);
});

test('go back button returns to next page', async browser => {
  await browser.navigateTo(`${baseUrl}/clients`);
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/client-creation?nextUrl=user-settings`)
    .expect(goBackButton.visible)
    .ok()
    .click(goBackButton)
    .expect(url())
    .eql(`${baseUrl}/user-settings`);
});

test('go back button says "go back" when step is 1', async browser => {
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/client-creation?nextUrl=user-settings`)
    .expect(goBackButton.textContent)
    .eql('Go Back');
});

test('go back button says user can return when step is > 1', async browser => {
  await createClientAbilitySetup();
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients`)
    .click(finishClientButtons.nth(0))
    .expect(goBackButton.textContent)
    .eql('Go Back (You can return later)');
});

test('can close centre view with x button', async browser => {
  await createClientAbilitySetup();
  await nurse();
  return browser
    .navigateTo(`${baseUrl}/clients?nextUrl=`)
    .click(finishClientButtons.nth(0))
    .click(clientDietSubmit)
    .click(clientVisitingCentreHeaders.nth(0))
    .click(viewCentreButton)
    .expect(centreCards.visible)
    .ok()
    .click(closeDialogButton)
    .expect(centreCards.visible)
    .notOk();
});
