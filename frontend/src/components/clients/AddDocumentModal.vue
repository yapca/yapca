<template>
  <q-card-section>
    <responsive-table
      table-id="addDocumentTable"
      :rows="incompleteDocuments"
      :headers="headers"
      default-sort="name"
    >
      <template #size="{ cellData }">
        {{ getFileSize(cellData.size) }}
      </template>
      <template #name="{ cellData }">
        <q-input
          v-model="cellData.tempName"
          label="Name"
          single-line
          :maxlength="72"
          class="addDocumentNameInput"
          :rules="[v => !!v || 'Document name is required']"
          @blur="cellData.name = cellData.tempName"
        />
      </template>
      <template #actions="{ cellData }">
        <q-circular-progress
          v-if="uploading"
          show-value
          :value="cellData.document.uploadStatus"
          size="50px"
          color="primary"
          class="addDocumentUploadProgress"
        >
          {{ cellData.document.uploadStatus }}
        </q-circular-progress>
        <div v-else>
          <div v-if="cellData.document.error">
            <q-tooltip class="fileSizeWarningTooltip">{{
              cellData.document.error
            }}</q-tooltip>
            <q-icon
              size="md"
              color="negative"
              class="fileSizeWarningIcon"
              :name="mdiAlert"
            />
          </div>
          <q-btn
            v-else
            class="removeFileButton"
            round
            flat
            :icon="mdiClose"
            @click="removeDocument(cellData.document.file)"
          />
        </div>
      </template>
    </responsive-table>

    <q-banner
      v-if="documentsOverMaxLimit && !uploading"
      id="fileOverStorageLimitAlert"
      class="text-white bg-red"
    >
      <template #avatar>
        <q-icon :name="mdiAlert" />
      </template>
      There is only {{ getFileSize(maxStorage - totalUsage) }} of space left on
      the server. The uploaded files total {{ getFileSize(documentUsage) }}.
      Please reduce this upload by
      {{ getFileSize(documentUsage + totalUsage - maxStorage) }}
    </q-banner>
  </q-card-section>
  <card-confirm-buttons
    v-if="!uploading"
    confirm-button-text="Begin Upload"
    confirm-button-id="documentUploadButton"
    confirm-button-color="positive"
    :confirm-button-valid="
      !namelessDocumentsExist && !documentsOverMaxLimit && !noValidUploads
    "
    @confirm-button-clicked="uploadDocuments"
  />
</template>

<script lang="ts" setup>
import { mdiAlert, mdiClose } from '@mdi/js';
import { computed, ref, watch } from 'vue';
import { useClientStore } from 'src/stores/clientsStore';
import getClient from 'src/functions/client/getClient';
import { useBasicStore } from 'src/stores/basicStore';
import axiosFailed from 'src/functions/proxy/AxiosFailed';
import getFileSize from 'src/functions/common/getFileSize';
import ResponsiveTable from 'src/components/ui/ResponsiveTable.vue';
import CardConfirmButtons from 'src/components/ui/CardConfirmButtons.vue';
import { Notify } from 'quasar';
import type { AxiosProgressEvent } from 'axios';
import type Client from 'src/types/Client';
import type DocumentUpload from 'src/types/DocumentUpload';

interface Props {
  clientId: number | undefined;
  documents: DocumentUpload[];
}
const props = withDefaults(defineProps<Props>(), {
  clientId: undefined,
  documents: () => []
});
const emit = defineEmits(['close-dialog']);

const basicStore = useBasicStore();
const clientStore = useClientStore();

const uploading = ref(false);
const incompleteDocuments = ref<
  {
    name: string;
    tempName: string;
    originalFileName: string;
    size: number;
    document: DocumentUpload;
  }[]
>([]);

const headers = ref([
  {
    label: 'Name',
    name: 'name',
    field: 'name',
    sortable: true
  },
  {
    label: 'Original File Name',
    name: 'originalFileName',
    field: 'originalFileName',
    sortable: false
  },
  {
    label: 'Size',
    name: 'size',
    field: 'size',
    sortable: true
  },
  {
    label: 'Actions',
    name: 'actions',
    field: 'actions',
    sortable: false
  }
]);

const maxStorage = computed(() => {
  return process.env.VUE_APP_MAX_STORAGE &&
    process.env.VUE_APP_MAX_STORAGE !== 'undefined'
    ? Number.parseInt(process.env.VUE_APP_MAX_STORAGE, 10)
    : 0;
});

const namelessDocumentsExist = computed(() => {
  return incompleteDocuments.value.some(document => !document.tempName);
});

const noValidUploads = computed(() => {
  return incompleteDocuments.value.every(
    incompleteDocument =>
      incompleteDocument.document.removed || incompleteDocument.document.error
  );
});

const documentsOverMaxLimit = computed(() => {
  return totalUsage.value + documentUsage.value >= maxStorage.value;
});

const totalUsage = computed(() => {
  return clients.value.reduce((total, client) => total + client.storageSize, 0);
});

const documentUsage = computed(() => {
  return incompleteDocuments.value
    .filter(
      incompleteDocument =>
        !incompleteDocument.document.removed &&
        !incompleteDocument.document.error
    )
    .reduce(
      (total, incompleteDocument) =>
        total + incompleteDocument.document.file.size,
      0
    );
});

const clients = computed(() => {
  return Object.keys(clientStore.clients)
    .map(clientKey => {
      return { ...clientStore.clients[clientKey] };
    })
    .filter((client: Client) => client.setupComplete)
    .filter((client: Client) => client.storageSize > 0);
});

watch(
  () => props.documents,
  () => {
    incompleteDocuments.value = props.documents
      .filter(document => !document.uploaded && !document.removed)
      .map(selectedDocument => {
        return {
          name: selectedDocument.newName,
          tempName: selectedDocument.newName,
          originalFileName: selectedDocument.file.name,
          size: selectedDocument.file.size,
          document: selectedDocument
        };
      });
  },
  { immediate: true }
);

watch(incompleteDocuments, () => {
  if (incompleteDocuments.value.length === 0) {
    Notify.create({
      type: 'positive',
      message: 'All files uploaded successfully'
    });
    closeDialog();
  }
});

const closeDialog = (): void => {
  uploading.value = false;
  context.emit('close-dialog');
};

const setDocumentProgress = (
  progressEvent: AxiosProgressEvent,
  document: DocumentUpload
): void => {
  if (progressEvent.total !== undefined) {
    const percentCompleted = Math.round(
      (progressEvent.loaded * 100) / progressEvent.total
    );
    document.uploadStatus = percentCompleted;
  }
};

const uploadDocuments = (): Promise<void> | null => {
  if (props.clientId) {
    const clientId = props.clientId;
    uploading.value = true;
    const promises: Promise<void>[] = [];
    incompleteDocuments.value.forEach(incompleteDocument => {
      const document = incompleteDocument.document;
      if (!document.removed && !document.error) {
        promises.push(uploadDocument(clientId, document));
      } else {
        // eslint-disable-next-line no-param-reassign
        document.removed = true;
        removeDocument(document.file);
      }
    });
    // Client documents return client data out of order
    // so after uploads, get fresh client data
    return Promise.all(promises).then(() => getClient(clientId));
  }
  return null;
};

const removeDocument = (file: File): void => {
  incompleteDocuments.value = incompleteDocuments.value.filter(
    incompleteDocument => incompleteDocument.document.file !== file
  );
};

const uploadDocument = (
  clientId: number,
  document: DocumentUpload
): Promise<void> => {
  const data = new FormData();
  data.append('client', clientId.toString());
  data.append('document', document.file);
  data.append('name', document.newName);
  data.append('originalName', document.file.name);
  return basicStore.http
    .post('clientDocuments/upload/', data, {
      headers: {
        Authorization: `Token ${basicStore.token}`,
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress: (progressEvent: AxiosProgressEvent) =>
        context.setDocumentProgress(progressEvent, document)
    })
    .then(() => {
      incompleteDocuments.value = incompleteDocuments.value.filter(
        incompleteDocument => incompleteDocument.document.file !== document.file
      );
    })
    .catch(axiosFailed);
};

const context = {
  emit,
  setDocumentProgress
};
</script>
