export default interface CustomAxiosError {
  response?: {
    data?: {
      non_field_errors?: string[];
      error?: string | string[];
      detail?: string;
      email?: string[];
    };
  };
}
