export default interface Need {
  id: number;
  description: string;
  plan?: string;
}
