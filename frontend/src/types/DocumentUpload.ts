export default interface DocumentUpload {
  file: File;
  newName: string;
  uploaded: boolean;
  removed: boolean;
  uploadStatus: number;
  error?: string;
}
