export default interface RecurrenceOption {
  label: 'Never' | 'Weekly' | 'Monthly' | 'Yearly';
  value: 'never' | 'weekly' | 'monthly' | 'yearly';
}
