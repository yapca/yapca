export default interface DeleteEventWs {
  action: 'deleteEvent';
  data: {
    id: number;
  };
}
