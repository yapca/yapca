import type Centre from '../Centre';

export default interface DeleteCentreWs {
  action: 'deleteCentre';
  data: Centre;
}
