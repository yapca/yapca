import type ClientNeed from '../ClientNeed';

export default interface UpdateClientNeedWs {
  action: 'updateClientNeed';
  data: ClientNeed;
}
