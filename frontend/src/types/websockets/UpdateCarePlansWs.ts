import type CarePlan from '../CarePlan';

export default interface UpdateCarePlansWs {
  action: 'updateCarePlans';
  data: CarePlan[];
}
