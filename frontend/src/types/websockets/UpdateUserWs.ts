import type User from '../User';

export default interface UpdateUserWs {
  action: 'updateUser';
  data: User;
}
