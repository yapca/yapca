import type Need from '../Need';

export default interface UpdateNeedWs {
  action: 'updateNeed';
  data: Need;
}
