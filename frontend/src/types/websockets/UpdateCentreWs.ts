import type Centre from '../Centre';

export default interface UpdateCentreWs {
  action: 'updateCentre';
  data: Centre;
}
