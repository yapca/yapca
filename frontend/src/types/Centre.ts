import type IncludedCountries from './IncludedCountries';

export default interface Centre {
  id: number;
  name: string;
  openingDays?: number[];
  openingDaysFirstSetupComplete: boolean;
  closingDaysFirstSetupComplete: boolean;
  policies?: number[];
  needs?: number[];
  policiesFirstSetupComplete: boolean;
  needsFirstSetupComplete: boolean;
  setupComplete: boolean;
  created: string;
  editingCentreName: boolean;
  nameValid: boolean;
  color: string;
  closedOnPublicHolidays: boolean;
  country: keyof IncludedCountries;
  state: string;
}
