import type Group from './Group';

export default interface User {
  id: null | number;
  username: null | string;
  displayName: string;
  accentColor: null | string;
  darkTheme: null | boolean;
  groups?: (Group | number)[];
  group: string;
  isActive: boolean;
  centres?: number[];
  userClientTableHiddenRows: string | string[];
  email: string;
  client?: number;
  hasTOTP?: boolean;
  permissions?: string[];
}
