export default interface Policy {
  id: number;
  description: string;
}
