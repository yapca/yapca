export default interface ReadableAbsenceRecord {
  date: string;
  centre: string;
  reasonForAbsence: string;
  reasonForInactivity: string;
  active: boolean;
  addedBy: string;
}
