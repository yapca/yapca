export default interface ResponsiveTableHeader {
  label: string;
  name: string;
  field: string;
  sortable: boolean;
  align?: 'center' | 'left' | 'right';
}
