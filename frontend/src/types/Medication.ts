export default interface Medication {
  id: number;
  name: string;
  dosage: string;
  frequency: string;
  inactiveDate: string | undefined;
}
