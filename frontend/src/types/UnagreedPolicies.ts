import type Policy from './Policy';

export default interface UnagreedPolicies {
  centre: number;
  unagreedPolicies: Policy[];
}
