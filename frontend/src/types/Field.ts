import type Client from './Client';

export default interface Field {
  text: string;
  value: keyof Client;
  editable?: boolean;
  fieldType?: 'tel' | 'text' | 'number' | 'textarea';
  choices?: string[];
  min?: number;
  max?: number;
  maxlength?: number;
  inputValue?:
    | 'firstName'
    | 'surname'
    | 'address'
    | 'phoneNumber'
    | 'publicHealthNurseName'
    | 'publicHealthNursePhoneNumber'
    | 'homeHelpName'
    | 'homeHelpPhoneNumber'
    | 'gpName'
    | 'gpPhoneNumber'
    | 'chemistName'
    | 'chemistPhoneNumber'
    | 'medicalHistory'
    | 'surgicalHistory'
    | 'caseFormulation'
    | 'allergies'
    | 'intolerances';
  booleanValue?: 'isActive' | 'usesDentures' | 'requiresAssistanceEating';
  dateValue?: 'dateOfBirth';
  choiceValue?:
    | 'gender'
    | 'mobility'
    | 'toileting'
    | 'hygiene'
    | 'hearing'
    | 'reasonForInactivity'
    | 'sight';
  sliderValue?: 'thicknerGrade';
  icon?: string;
  iconUnfilled?: string;
}
