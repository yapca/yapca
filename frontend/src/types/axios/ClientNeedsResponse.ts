import type ClientNeed from '../ClientNeed';

export default interface ClientNeedsResponse {
  data: ClientNeed[];
}
