import type Centre from '../Centre';

export default interface CentresResponse {
  data: Centre[];
}
