import { defineStore } from 'pinia';
import getBackendPath from 'src/functions/backend/getBackendPath';
import { Platform } from 'quasar';
import http from 'src/functions/backend/http';

export const useBasicStore = defineStore('basic', {
  state: () => ({
    registerAbility: false,
    token: '',
    backendPath: getBackendPath(),
    appServerAddress: '',
    http: http(''),
    navDrawer: true,
    clientVersion: '',
    topBar: {
      active: false,
      color: 'info',
      indeterminate: false,
      value: 0,
      text: ''
    },
    platform: Platform.is,
    navBarText: 'Welcome',
    backendDataLoaded: false
  })
});
