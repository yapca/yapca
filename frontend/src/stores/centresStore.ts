import { defineStore } from 'pinia';
import { useBasicStore } from './basicStore';
import { useUserStore } from './userStore';
import type CalendarEvent from 'src/types/CalendarEvent';
import type Need from 'src/types/Need';
import type CentresResponse from 'src/types/axios/CentresResponse';
import type Centre from 'src/types/Centre';
import type Policy from 'src/types/Policy';
import type NeedsResponse from 'src/types/axios/NeedsResponse';

interface CentreItem extends Need, Policy {}

export const useCentreStore = defineStore('centres', {
  state: () => ({
    centres: {} as Record<string, Centre>,
    needs: {} as Record<string, Need>,
    policies: {} as Record<string, Policy>,
    setupCentre: null as Centre | null,
    eventsNeedUpdating: false,
    centresLoading: false,
    events: [] as CalendarEvent[]
  }),
  actions: {
    setSetupCentre(centre: Centre | null) {
      this.setupCentre = centre;
    },
    getCentres() {
      const basicStore = useBasicStore();
      return basicStore.http
        .get('centres/')
        .then((response: CentresResponse) => {
          const initialValue = {};
          this.centres = response.data.reduce(
            (centreObject: Record<string, Centre>, centre: Centre) => {
              if (centre.id) {
                return {
                  ...centreObject,
                  [centre.id.toString()]: centre
                };
              }
              return centreObject;
            },
            initialValue
          );
        });
    },
    deleteCentre(centre: Centre) {
      const userStore = useUserStore();
      if (centre.id) {
        // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
        delete this.centres[centre.id];
        if (userStore.user.centres) {
          userStore.user.centres = userStore.user.centres.filter(
            centreId => centreId !== centre.id
          );
        }
      }
    },
    async updateCentre(updatedCentreData: Centre) {
      // Not updating the entire centre at once prevents issues arising where
      // the websocket may not send a policies/openingDays object due to being an
      // m2m relation.
      if (updatedCentreData.id) {
        const centreId = updatedCentreData.id;
        if (!(centreId in this.centres)) {
          this.centres[centreId] = updatedCentreData;
        } else {
          const centre = this.centres[centreId];
          const keys = Object.keys(updatedCentreData) as (keyof Centre)[];
          keys.forEach((key: keyof Centre) => {
            const centreKeyValue = updatedCentreData[key] as
              | string
              | number
              | boolean
              | number[]
              | undefined
              | null;
            if (centreKeyValue !== null) {
              (centre[key] as typeof key) = updatedCentreData[
                key
              ] as typeof key;
            }
          });
          await this.updateCentreAdditionalData(updatedCentreData, centreId);
          centre.editingCentreName = false;
        }
      }
    },
    updateCentreAdditionalData(centre: Partial<Centre>, centreId: number) {
      if (centre.country || centre.state) {
        this.eventsNeedUpdating = true;
      }
      if (centre.policies) {
        return this.getCentreRelatedData(centreId, 'policies');
      }
      if (centre.needs) {
        return this.getCentreRelatedData(centreId, 'needs');
      }
      return null;
    },
    getCentreRelatedData(centreId: number, itemName: 'policies' | 'needs') {
      const basicStore = useBasicStore();
      return basicStore.http
        .get(`centres/${centreId}/${itemName}`)
        .then((response: NeedsResponse) => {
          const initialValue = {};
          const newItems = response.data.reduce(
            (itemObject: Record<string, CentreItem>, item: CentreItem) => {
              if (item.id) {
                return {
                  ...itemObject,
                  [item.id.toString()]: item
                };
              }
              return itemObject;
            },
            initialValue
          );
          this[itemName] = {
            ...this[itemName],
            ...newItems
          };
        });
    },
    updateNeed(need: Need) {
      this.needs[need.id] = need;
    },
    updatePolicy(policy: Policy) {
      this.policies[policy.id] = policy;
    },
    updateEvent() {
      this.eventsNeedUpdating = true;
    }
  }
});
