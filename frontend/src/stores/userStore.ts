import { defineStore } from 'pinia';
import { useSocketStore } from 'src/stores/websocketStore';
import { useBasicStore } from 'src/stores/basicStore';
import { Notify } from 'quasar';
import { useCentreStore } from './centresStore';
import { useClientStore } from './clientsStore';
import type User from 'src/types/User';

const convertUserClientTableHiddenRows = (
  userClientTableHiddenRows: string | string[]
): string[] => {
  // Convert userClientTableHiddenRows to array dependind on whether
  // object or string
  if (typeof userClientTableHiddenRows === 'string') {
    return JSON.parse(userClientTableHiddenRows) as string[];
  }
  return userClientTableHiddenRows;
};

const formatUser = (user: User): User => {
  const clientStore = useClientStore();

  const formattedUser = user;
  if (user.userClientTableHiddenRows) {
    formattedUser.userClientTableHiddenRows = convertUserClientTableHiddenRows(
      user.userClientTableHiddenRows
    );
  }
  if (
    user.groups &&
    user.groups.length > 0 &&
    typeof user.groups[0] === 'object'
  ) {
    formattedUser.group = user.groups[0].name;
  }
  if (formattedUser.group === 'client' && clientStore.activeClient) {
    formattedUser.centres = clientStore.activeClient.centres;
  }
  return formattedUser;
};

export const useUserStore = defineStore('user', {
  state: () => ({
    user: {
      id: null,
      username: null,
      displayName: '',
      accentColor: null,
      darkTheme: null,
      groups: [],
      group: '',
      isActive: false,
      centres: [],
      userClientTableHiddenRows: [],
      email: '',
      permissions: []
    } as User,
    users: {} as Record<number, User>,
    lockoutEndTime: undefined as undefined | Date,
    createNotification: Notify.create
  }),
  actions: {
    updateUser(user: User) {
      if (user.id) {
        const formattedUser = formatUser(user);

        const userId = user.id;
        if (this.user.id === userId) {
          this.handleCurrentUserChange(formattedUser, user.email);
          const keys = Object.keys(formattedUser) as (keyof User)[];
          keys.forEach((key: keyof User) => {
            if (key in this.user && formattedUser[key] !== null) {
              (this.user[key] as typeof key) = formattedUser[key] as typeof key;
            }
          });
          this.users[userId] = this.user;
        } else if (!(userId in this.users)) {
          this.users[userId] = formattedUser;
        } else {
          const keys = Object.keys(formattedUser) as (keyof User)[];
          keys.forEach((key: keyof User) => {
            if (key in this.users[userId] && formattedUser[key] !== null) {
              (this.users[userId][key] as typeof key) = formattedUser[
                key
              ] as typeof key;
            }
          });
        }
      }
    },
    async updateUserCentres(userCentreData: {
      userId: number;
      centreIds: number[];
    }) {
      if (userCentreData.userId in this.users) {
        this.users[userCentreData.userId].centres = userCentreData.centreIds;
      }
      if (this.user.id === userCentreData.userId) {
        this.user.centres = userCentreData.centreIds;
        const centreStore = useCentreStore();
        await centreStore.getCentres();
      }
    },
    handleCurrentUserChange(formattedUser: User, email: string): void {
      const basicStore = useBasicStore();
      const wsStore = useSocketStore();
      if (formattedUser.group && formattedUser.group !== this.user.group) {
        wsStore.WS_RECONNECT(basicStore.appServerAddress, basicStore.token);
      }
      if (this.user.email !== email) {
        this.createNotification({
          type: 'positive',
          message:
            "Your email was changed! You'll receive an email at your new address shortly."
        });
      }
    },
    updateLockoutEndTime(newTime: Date) {
      this.lockoutEndTime = newTime;
    }
  }
});
