import ServerSelect from '../pages/ServerSelect.vue';
import AuthPage from '../pages/AuthPage.vue';
import ErrorPage from '../pages/ErrorPage.vue';
import CentreSelect from '../pages/CentreSelect.vue';
import CentreSetup from '../pages/CentreSetup.vue';
import TempCode from '../pages/TempCode.vue';
import DefaultLayout from '../layouts/DefaultLayout.vue';
import ClientsPage from '../pages/ClientsPage.vue';
import CentresPage from '../pages/CentresPage.vue';
import UserPage from '../pages/UserPage.vue';
import AdminPage from '../pages/AdminPage.vue';
import CarePlansPage from '../pages/CarePlansPage.vue';
import ClientCreation from '../pages/ClientCreation.vue';
import CalendarPage from '../pages/CalendarPage.vue';
import MyMedicalInfo from '../pages/MyMedicalInfo.vue';

import type { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/server-select',
    name: 'serverSelect',
    component: ServerSelect
  },
  {
    path: '/auth',
    name: 'auth',
    component: AuthPage
  },
  {
    path: '/error',
    name: 'errorPage',
    component: ErrorPage,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/centre-select',
    name: 'centreSelect',
    component: CentreSelect,
    meta: {
      requiresAuth: true,
      medicalUser: true
    }
  },
  {
    path: '/centre-setup',
    name: 'centreSetup',
    component: CentreSetup,
    meta: {
      requiresAuth: true,
      requiresAdmin: true
    }
  },
  {
    path: '/client-creation',
    name: 'clientCreation',
    component: ClientCreation,
    meta: {
      requiresAuth: true,
      checkCentres: true,
      medicalUser: true
    }
  },
  {
    path: '/temp-code',
    name: 'tempCode',
    component: TempCode
  },
  {
    path: '/',
    name: 'main',
    component: DefaultLayout,
    redirect: '/clients',
    meta: {
      requiresAuth: true,
      checkCentres: true
    },
    children: [
      {
        path: 'admin',
        name: 'admin',
        component: AdminPage,
        meta: {
          requiresAdmin: true
        }
      },
      {
        path: '/clients',
        name: 'Clients',
        component: ClientsPage,
        meta: {
          medicalUser: true
        }
      },
      {
        path: '/my-medical-info',
        name: 'My Medical Info',
        component: MyMedicalInfo,
        meta: {
          canViewMedicalInfo: true
        }
      },
      {
        path: 'user-settings',
        name: 'User Settings',
        component: UserPage
      },
      {
        path: 'centres',
        name: 'Centres',
        component: CentresPage,
        meta: {
          medicalUser: true
        }
      },
      {
        path: 'carePlans',
        name: 'carePlans',
        component: CarePlansPage,
        meta: {
          medicalUser: true
        }
      },
      {
        path: 'calendar',
        name: 'calendar',
        component: CalendarPage,
        meta: {
          canViewCalendar: true
        }
      }
    ]
  },
  { path: '/:catchAll(.*)*', redirect: '/clients' }
];

export default routes;
