import { useClientStore } from 'src/stores/clientsStore';
import { useBasicStore } from 'src/stores/basicStore';
import axiosFailed from './AxiosFailed';
import type CustomAxiosError from 'src/types/CustomAxiosError';
import type Client from 'src/types/Client';
import type ClientsResponse from 'src/types/axios/ClientsResponse';

const getClients = async (): Promise<void> => {
  const basicStore = useBasicStore();
  const clientStore = useClientStore();
  clientStore.clientsLoading = true;
  return basicStore.http
    .get('clients/')
    .then((response: ClientsResponse) => {
      const initialValue = {};
      clientStore.clients = response.data.reduce(
        (clientObject: Record<string, Client>, client: Client) => {
          if (client.id) {
            return {
              ...clientObject,
              [client.id.toString()]: client
            };
          }
          return clientObject;
        },
        initialValue
      );
      clientStore.clientsLoading = false;
    })
    .catch((err: CustomAxiosError) => {
      axiosFailed(err);
      clientStore.clientsLoading = false;
    });
};
export default getClients;
