import { useBasicStore } from 'src/stores/basicStore';
import mergeItems from '../common/mergeItems';
import axiosFailed from './AxiosFailed';
import type PoliciesResponse from 'src/types/axios/PoliciesResponse';
import type NeedsResponse from 'src/types/axios/NeedsResponse';

const getCentreDataForClient = (
  clientId: number,
  dataName: 'policies' | 'needs'
): Promise<void> => {
  const basicStore = useBasicStore();
  return basicStore.http
    .get(`clients/${clientId}/${dataName}`)
    .then((response: PoliciesResponse | NeedsResponse) => {
      mergeItems(response.data, dataName);
    })
    .catch(axiosFailed);
};

export default getCentreDataForClient;
