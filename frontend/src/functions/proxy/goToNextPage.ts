import type { Router } from 'vue-router';

const goToNextPage = (router: Router): void => {
  if (
    router.currentRoute.value.query.nextUrl &&
    router.currentRoute.value.query.nextUrl !== '/'
  ) {
    router
      .push(router.currentRoute.value.query.nextUrl as string)
      .catch((err: Error) => err);
  } else {
    router.push({ name: 'Clients' }).catch((err: Error) => err);
  }
};
export default goToNextPage;
