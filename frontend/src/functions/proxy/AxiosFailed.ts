import { Notify } from 'quasar';
import type CustomAxiosError from 'src/types/CustomAxiosError';

const axiosFailed = (err?: CustomAxiosError): void => {
  let message = '';
  if (err?.response?.data) {
    const data = err.response.data;
    if (data.non_field_errors) {
      message = data.non_field_errors[0];
    }
    if (data.error && typeof data.error !== 'string') {
      message = data.error[0];
    }
    if (data.error && typeof data.error === 'string') {
      message = data.error;
    }
    if (data.detail) {
      message = data.detail;
    }
  } else {
    console.warn('axios error:', err);
    message = 'There was a problem connecting to the backend server';
  }
  if (message) {
    Notify.create({
      type: 'negative',
      message
    });
  }
};

export default axiosFailed;
