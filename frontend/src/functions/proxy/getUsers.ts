import { useUserStore } from 'src/stores/userStore';
import { useBasicStore } from 'src/stores/basicStore';
import axiosFailed from './AxiosFailed';
import setUsers from './setUsers';

const getUsers = (): Promise<void> => {
  const userStore = useUserStore();
  const basicStore = useBasicStore();
  const url =
    userStore.user.group === 'admin' ? 'shared-data' : 'display-names';
  return basicStore.http
    .get(`users/${url}/`)
    .then(response => setUsers(response))
    .catch(() => axiosFailed());
};

export default getUsers;
