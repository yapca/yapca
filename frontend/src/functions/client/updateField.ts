import { useBasicStore } from 'src/stores/basicStore';
import axiosFailed from '../proxy/AxiosFailed';
import type Client from 'src/types/Client';
import type { AxiosResponse } from 'axios';

const updateField = (
  value: string | boolean | number,
  field: keyof Client | '',
  clientId: number
): Promise<void | AxiosResponse> | null => {
  const basicStore = useBasicStore();
  if (value !== '') {
    return basicStore.http
      .patch(`clients/${clientId}/update/`, {
        [field]: value
      })
      .catch(axiosFailed);
  }
  return null;
};
export default updateField;
