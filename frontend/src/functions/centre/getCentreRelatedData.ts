import { useBasicStore } from 'src/stores/basicStore';
import mergeItems from '../common/mergeItems';
import axiosFailed from '../proxy/AxiosFailed';
import type NeedsResponse from 'src/types/axios/NeedsResponse';
import type PoliciesResponse from 'src/types/axios/PoliciesResponse';

const getCentreRelatedData = (
  centreId: number,
  itemName: 'policies' | 'needs'
): Promise<void> => {
  const basicStore = useBasicStore();
  return basicStore.http
    .get(`centres/${centreId}/${itemName}`)
    .then((response: NeedsResponse | PoliciesResponse) => {
      mergeItems(response.data, itemName);
    })
    .catch(() => {
      axiosFailed();
    });
};
export default getCentreRelatedData;
