export default function getBackendPath(): string {
  const host =
    process.env.VUE_APP_API_HOST !== 'undefined'
      ? (process.env.VUE_APP_API_HOST as string)
      : window.location.hostname;
  const apiPort =
    process.env.VUE_APP_API_PORT !== 'undefined'
      ? (process.env.VUE_APP_API_PORT as string)
      : '';
  const apiPath =
    process.env.VUE_APP_API_PATH !== 'undefined' &&
    process.env.VUE_APP_API_PATH !== ''
      ? `/${process.env.VUE_APP_API_PATH as string}`
      : '';
  return `${host}:${apiPort}${apiPath}`;
}
