import { format, formatRelative, parseISO } from 'date-fns';
import enGB from 'date-fns/locale/en-GB';

const formatRelativeLocale = {
  lastWeek: "'Last' eeee",
  yesterday: "'Yesterday'",
  today: "'Today'",
  tomorrow: "'Tomorrow'",
  nextWeek: "'Next' eeee",
  other: 'dd/MM/yy'
};

const locale = {
  ...enGB,
  formatRelative: (token: keyof typeof formatRelativeLocale): string =>
    formatRelativeLocale[token]
};

const formatDateTime = (
  date: string,
  time: boolean,
  humanise = false
): string => {
  try {
    const parsedDate = parseISO(date);
    if (time) {
      return format(parsedDate, 'dd/MM/yy HH:mm');
    }
    if (humanise) {
      return formatRelative(parsedDate, new Date(), { locale });
    }
    return format(parsedDate, 'dd/MM/yy');
  } catch {
    return '';
  }
};

export default formatDateTime;
