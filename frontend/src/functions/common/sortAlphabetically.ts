const sortAlphabetically = (
  stringA: string,
  stringB: string,
  isDesc: boolean
): number => {
  if (isDesc) {
    return stringB.toLowerCase().localeCompare(stringA.toLowerCase());
  }
  return stringA.toLowerCase().localeCompare(stringB.toLowerCase());
};

export default sortAlphabetically;
