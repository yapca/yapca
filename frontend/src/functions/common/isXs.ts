import { Screen } from 'quasar';
import { computed } from 'vue';

const isXs = computed(() => {
  return Screen.width < 600;
});

export default isXs;
