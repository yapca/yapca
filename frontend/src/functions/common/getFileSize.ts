import { filesize } from 'filesize';

const getFileSize = (size: number): string =>
  filesize(size, { base: 2, standard: 'jedec' });

export default getFileSize;
