import { useBasicStore } from 'src/stores/basicStore';
import axiosFailed from '../proxy/AxiosFailed';
import type { AxiosResponse } from 'axios';

const updatePolicies = (clientId: number): Promise<void | AxiosResponse> => {
  const basicStore = useBasicStore();
  return basicStore.http
    .get(`clients/${clientId}/policies/agree/`)
    .catch(axiosFailed);
};

export default updatePolicies;
