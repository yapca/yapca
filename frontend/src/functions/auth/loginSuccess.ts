import { useBasicStore } from 'src/stores/basicStore';
import { useSocketStore } from 'src/stores/websocketStore';
import { useUserStore } from 'src/stores/userStore';
import goToNextPage from '../proxy/goToNextPage';
import type LoginResponse from 'src/types/axios/LoginResponse';
import type { Router } from 'vue-router';

const loginSuccess = (response: LoginResponse, router: Router): void => {
  const basicStore = useBasicStore();
  const wsStore = useSocketStore();
  const userStore = useUserStore();
  basicStore.token = response.data.key;
  wsStore.WS_RECONNECT(basicStore.appServerAddress, basicStore.token);
  userStore.user = response.data.user;
  if (userStore.user.groups && typeof userStore.user.groups[0] !== 'number') {
    userStore.user.group = userStore.user.groups[0].name;
  }
  if (userStore.user.id) {
    userStore.users[userStore.user.id] = userStore.user;
  }
  goToNextPage(router);
};
export default loginSuccess;
