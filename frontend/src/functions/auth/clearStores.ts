import { useBasicStore } from 'src/stores/basicStore';
import { useSocketStore } from 'src/stores/websocketStore';
import { useUserStore } from 'src/stores/userStore';
import { useCentreStore } from 'src/stores/centresStore';
import { useClientStore } from 'src/stores/clientsStore';
import http from '../backend/http';

const clearStores = (stores: string[]): void => {
  if (stores.includes('basic')) {
    const basicStore = useBasicStore();
    const appServerAddress = basicStore.appServerAddress.slice();
    const backendPath = basicStore.backendPath.slice();
    basicStore.$reset();
    basicStore.appServerAddress = appServerAddress;
    basicStore.backendPath = backendPath;
    basicStore.http = http(appServerAddress);
  }
  if (stores.includes('socket')) {
    const wsStore = useSocketStore();
    wsStore.$reset();
  }
  if (stores.includes('user')) {
    const userStore = useUserStore();
    userStore.$reset();
  }
  if (stores.includes('client')) {
    const clientStore = useClientStore();
    clientStore.$reset();
  }
  if (stores.includes('centre')) {
    const centreStore = useCentreStore();
    centreStore.$reset();
  }
};
export default clearStores;
