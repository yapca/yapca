// Required to build icon files for linux packages

exports.default = function (context) {
  context.targets.forEach(target => {
    if (target.helper) {
      target.helper.iconPromise.value = target.helper.iconPromise.value.then(
        icons =>
          icons.map(icon => ({
            ...icon,
            size: icon.size === 0 ? 512 : icon.size
          }))
      );
    }
  });
};
