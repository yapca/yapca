# Contributing to YAPCA

To contribute, simply
[open a Merge Request](https://gitlab.com/yapca/yapca/-/merge_requests/new)

# Getting a Merge Request approved

There is an extensive automatic build process in place that checks new code for linting
problems, uncovered code or code smells, lets dive deeper into them:

## Linting

The backend is tested using [pylama](https://github.com/klen/pylama), which is
installed as a dev-package in poetry. Configuration for pylama is found in
[backend/pylama.ini](backend/pylama.ini)

Setting this up with your IDE usually requires setting your linter to pylama and
specifying which config file to use.

This frontend is linted using eslint automatically and shouldn't require any extra work
out of the box

<b>There can be no linting errors or the build will fail and the merge request will be
rejected.</b>

## Code Coverage

Code coverage is important in this project and as such it's important to make sure any
code added is covered too.

Code coverage in the backend is handled with
[coverage.py](https://coverage.readthedocs.io/en/coverage-5.0.3/), an example of its
usage can be seen in the [docker-compose file](build/dockercompose.yaml) (In the
backend-test section)

Code coverage alone isn't perfect and unit tests will still be reviewed to validate
that code is being properly tested.

<b>Code coverage must be 100% or the build will fail.</b>

## Integration tests

Additions to the frontend will require integration tests to be added or upgraded,
there's no way to test this automatically so the reviewer will decide if the integration
tests accurately cover new features.

## Sonar Check

Code is finally checked with
[SonarCloud](https://sonarcloud.io/dashboard?id=Star-Man_yapca)

The code must have no bugs, vulnerabilities, security hotspots, code smells, or
duplications.

### Once all checks are complete, the merge request will be reviewed properly.
